import 'package:flutter_test/flutter_test.dart';
import 'package:app_core/app_core.dart';
import '../../repository/lib/src/services/id_generator.dart';

void main() {
  group('IdGenerator', () {

    test('Should generate id', () async {
      final offset = 1577836800000;
      final before = DateService().now();
      expect(before, greaterThan(offset));
      final id = IdGenerator().next();
      final after = DateService().now();
      expect(id, greaterThan(((before - offset) << 12) + 0));
      expect(id, lessThan(((after - offset) << 12) + 4096));
    });
  });
}
