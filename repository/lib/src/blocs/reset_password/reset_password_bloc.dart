
import 'package:bloc/bloc.dart';

import '../../../repository.dart';

class ResetPasswordBloc extends Bloc<PasswordSubmitted, ResetPasswordState> {
  final PasswordService service;
  final String uidb64;
  final String token;

  ResetPasswordBloc(
      {required this.service, required this.uidb64, required this.token})
      : super(InitialResetPasswordState()) {
    on<PasswordSubmitted>((event, emit) async {
      emit(ResetPasswordInProgress(password: event.password));
      emit(await service.submitPassword(
          password: event.password, uidb64: uidb64, token: token));
    });
  }
}
