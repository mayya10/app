import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class ConnectivityCubit extends Cubit<ConnectivityResult> {
  late StreamSubscription connectionSubscription;

  ConnectivityCubit() : super(ConnectivityResult.none) {
    Connectivity().checkConnectivity().then((result) => emit(result));
    connectionSubscription =
        Connectivity().onConnectivityChanged.listen((result) => emit(result));
  }

  @override
  Future<void> close() async {
    connectionSubscription.cancel();
    return super.close();
  }
}
