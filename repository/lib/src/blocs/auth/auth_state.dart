import 'package:equatable/equatable.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object?> get props => [];
}

class NoAuth extends AuthState {}
class AuthInProgress extends AuthState {}

abstract class AuthMessageState extends AuthState {
  final Map<String, List<String?>> messages;
  final Map<String, String> credentials;

  const AuthMessageState(this.messages, this.credentials);

  @override
  List<Object?> get props => [messages];

  @override
  String toString() {
    return 'AuthMessageState { messages: $messages }';
  }
}

class ForgotPasswordFailure extends AuthMessageState {
  ForgotPasswordFailure(
      Map<String, List<String?>> messages, Map<String, String> credentials)
      : super(messages, credentials);
}

class RegisterFailure extends AuthMessageState {
  RegisterFailure(
      Map<String, List<String?>> messages, Map<String, String> credentials)
      : super(messages, credentials);
}

class LoginFailure extends AuthMessageState {
  LoginFailure(
      Map<String, List<String?>> messages, Map<String, String> credentials)
      : super(messages, credentials);
}

class ForgotPasswordSuccess extends AuthMessageState {
  ForgotPasswordSuccess(
      Map<String, List<String?>> messages, Map<String, String> credentials)
      : super(messages, credentials);
}

class RegisterSuccess extends AuthMessageState {
  RegisterSuccess(
      Map<String, List<String?>> messages, Map<String, String> credentials)
      : super(messages, credentials);
}

class LoginSuccess extends AuthState {
  final String accessToken;
  final String refreshToken;

  LoginSuccess({required this.accessToken, required this.refreshToken});

  @override
  // ignore accessToken to prevent account reloading in AccountsBloc()
  List<Object?> get props => [refreshToken];

  @override
  String toString() {
    return 'LoginSuccess { accessToken: $accessToken, refreshToken: $refreshToken }';
  }
}
