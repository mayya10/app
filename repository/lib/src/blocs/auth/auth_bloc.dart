import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';

import '../../../repository.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  late AuthService service;
  late Settings settings;

  AuthBloc() : super(NoAuth()) {
    on<AuthEvent>(
            (event, emit) => emit.forEach(mapEventToState(event),
            onData: (AuthState result) => result),
        transformer: sequential());  // process events sequentially
    service = AuthService(this);
    settings = di<Settings>();
  }

  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is AuthLoadedFromSettings) {
      yield* mapAuthLoadedFromSettingsToState();
    } else if (event is LoggedIn) {
      yield* mapLoggedInToState(event);
    } else if (event is PasswordForgotten) {
      yield* mapPasswordForgottenToState(event);
    } else if (event is Registered) {
      yield* mapRegisteredToState(event);
    } else if (event is LoggedOut) {
      yield* mapLoggedOutToState();
    } else if (event is AuthTokenRefreshed) {
      yield* mapAuthTokenRefreshedToState(event);
    }
  }

  Stream<AuthState> mapAuthLoadedFromSettingsToState() async* {
    var accessToken = di<Settings>().get(Settings.ACCESS_TOKEN);
    if (accessToken != null) {
      var refreshToken = settings.get(Settings.REFRESH_TOKEN)!;
      yield LoginSuccess(accessToken: accessToken, refreshToken: refreshToken);
    } else {
      yield NoAuth();
    }
  }

  Stream<AuthState> mapPasswordForgottenToState(PasswordForgotten event) async* {
    yield AuthInProgress();
    yield await service.forgotPassword(email: event.email);
  }

  Stream<AuthState> mapLoggedInToState(Authenticated event) async* {
    yield AuthInProgress();
    AuthState result =
        await service.login(email: event.email, password: event.password);
    if (result is LoginSuccess) {
      settings.put(Settings.ACCESS_TOKEN, result.accessToken);
      settings.put(Settings.REFRESH_TOKEN, result.refreshToken);
      settings.put(Settings.USER_EMAIL, event.email);
      if(settings.hasNewDbVersion) settings.updateDbVersion();
    }
    yield result;
  }

  Stream<AuthState> mapRegisteredToState(Authenticated event) async* {
    yield AuthInProgress();
    yield await service.register(email: event.email, password: event.password);
  }

  Stream<AuthState> mapAccountAttachedToState(Authenticated event) async* {
    yield AuthInProgress();
    yield await service.register(email: event.email, password: event.password);
  }

  Stream<AuthState> mapAuthTokenRefreshedToState(
      AuthTokenRefreshed event) async* {
    settings.put(Settings.ACCESS_TOKEN, event.accessToken);
    settings.put(Settings.REFRESH_TOKEN, event.refreshToken);
    yield LoginSuccess(
        accessToken: event.accessToken,
        refreshToken: event.refreshToken);
  }

  Stream<AuthState> mapLoggedOutToState() async* {
    settings.delete(Settings.ACCESS_TOKEN);
    settings.delete(Settings.REFRESH_TOKEN);
    settings.delete(Settings.USER_EMAIL);
    yield NoAuth();
  }
}
