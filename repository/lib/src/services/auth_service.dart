import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:app_core/app_core.dart';

import '../../repository.dart';

typedef HttpCallback = Future<http.Response?> Function(Map<String, String>);

class AuthService {
  static AuthService? _instance;
  final AuthBloc authBloc;

  factory AuthService([AuthBloc? authBloc]) {
    if (_instance == null) {
      _instance = AuthService._internal(authBloc!);
    }
    return _instance!;
  }

  AuthService._internal(this.authBloc);

  Future<http.Response> call(HttpCallback request) async {
    if (authBloc.state is LoginSuccess) {
      http.Response? response;
      try {
        response = await request(
            headers(accessToken: (authBloc.state as LoginSuccess).accessToken));
        if (response!.statusCode == 403) {
          String? accessToken = await _refreshAccessToken();
          if (accessToken != null) {
            response = await request(headers(accessToken: accessToken));
          } else {
            authBloc.add(LoggedOut());  // todo: clear replication data
            throw MessageException({
              'messages': [L10n.current.t8('User.loggedOut.summary')]
            });
          }
        } else if (response.statusCode >= 500) {
          throw MessageException({
            'messages': [L10n.current.t8('Server.error.summary')]
          });
        } else if (response.statusCode >= 300) {
          throw MessageException(AuthService.parseMessages(
              json.decode(utf8.decode(response.body.codeUnits))));
        }
      } on SocketException catch (e) {
        throw MessageException({
          'messages': [L10n.current.t8('Connection.error.title')]
        });
      } on http.ClientException catch (e) {
        throw MessageException({
          'messages': [L10n.current.t8('Connection.error.title')]
        });
      }
      return response!;
    } else {
      throw MessageException({
        'messages': [L10n.current.t8('User.login.required')]
      });
    }
  }

  /// Sends email always as lowercase
  Future<AuthState> register({
    required String email,
    required String password,
  }) async {
    final credentials =
        jsonEncode({'email': email.toLowerCase(), 'password': password});
    final client = di<http.Client>();
    try {
      final response = await client.post(
          await replicationUri('/v1/access/register/'),
          body: credentials,
          headers: headers());
      if ([201].contains(response.statusCode)) {
        return RegisterSuccess(
            parseMessages(json.decode(utf8.decode(response.body.codeUnits))),
            {'email': email, 'password': password});
      } else {
        var messages = {
          'messages': [L10n.current.t8('Server.error.summary')]
        };
        if (response.statusCode < 500)
          messages =
              parseMessages(json.decode(utf8.decode(response.body.codeUnits)));
        return RegisterFailure(
            messages, {'email': email, 'password': password});
      }
    } on Exception {
      var messages = {
        'messages': [L10n.current.t8('Connection.error.title')]
      };
      return RegisterFailure(messages, {'email': email, 'password': password});
    } finally {
      client.close();
    }
  }

  /// Sends email always as lowercase
  Future<AuthState> login({
    required String email,
    required String password,
  }) async {
    final credentials =
        jsonEncode({'email': email.toLowerCase(), 'password': password});
    final client = di<http.Client>();
    try {
      final response = await client.post(
          await replicationUri('/v1/access/token/'),
          body: credentials,
          headers: headers());
      if ([200].contains(response.statusCode)) {
        final accessToken = jsonDecode(response.body)['access'];
        final refreshToken = jsonDecode(response.body)['refresh'];
        return LoginSuccess(
            accessToken: accessToken, refreshToken: refreshToken);
      } else {
        var messages = {
          'messages': [L10n.current.t8('Server.error.summary')]
        };
        if (response.statusCode < 500)
          messages =
              parseMessages(json.decode(utf8.decode(response.body.codeUnits)));
        return LoginFailure(messages, {'email': email, 'password': password});
      }
    } on Exception catch (e) {
      var messages = {
        'messages': [
          L10n.current.t8('Connection.error.title') + ' (${e.runtimeType}: $e)'
        ]
      };
      return LoginFailure(messages, {'email': email, 'password': password});
    } finally {
      client.close();
    }
  }

  /// Sends email always as lowercase
  Future<AuthState> forgotPassword({
    required String email,
  }) async {
    final credentials = jsonEncode({'email': email.toLowerCase()});
    final client = di<http.Client>();
    try {
      final response = await client.post(
          await replicationUri('/v1/access/forgot-password/'),
          body: credentials,
          headers: headers());
      if ([200].contains(response.statusCode)) {
        return ForgotPasswordSuccess(
            parseMessages(json.decode(utf8.decode(response.body.codeUnits))),
            {'email': email});
      } else {
        var messages = {
          'messages': [L10n.current.t8('Server.error.summary')]
        };
        if (response.statusCode < 500)
          messages =
              parseMessages(json.decode(utf8.decode(response.body.codeUnits)));
        return ForgotPasswordFailure(messages, {'email': email});
      }
    } on Exception {
      var messages = {
        'messages': [L10n.current.t8('Connection.error.title')]
      };
      return ForgotPasswordFailure(messages, {'email': email});
    } finally {
      client.close();
    }
  }

  /// Sends email always as lowercase
  Future<Map<String, dynamic>> changeEmail({
    required String email,
    required String password,
  }) async {
    final credentials =
        jsonEncode({'email': email.toLowerCase(), 'password': password});
    final uri = await replicationUri('/v1/access/change-email/');
    final response = await call(
        (headers) => http.put(uri, body: credentials, headers: headers));
    return json.decode(response.body) as Map<String, dynamic>;
  }

  /// Deletes user access
  Future<void> deleteUser({ required String password }) async {
    final credentials = jsonEncode({'password': password});
    final uri = await replicationUri('/v1/access/delete-user/');
    await call((headers) => http.put(uri, body: credentials, headers: headers));
  }

  Future<String?> _refreshAccessToken(/*http.Client client*/) async {
    if(authBloc.state is! LoginSuccess) return null;  // RETURN
    var refreshToken = (authBloc.state as LoginSuccess).refreshToken;
    var body = jsonEncode({'refresh': refreshToken});
    var response = await http.post(await replicationUri('/v1/access/refresh/'),
        body: body, headers: headers());
    if (response.statusCode == 200) {
      var accessToken = jsonDecode(response.body)['access'];
      refreshToken = jsonDecode(response.body)['refresh'];
      authBloc.add(AuthTokenRefreshed(
          accessToken: accessToken, refreshToken: refreshToken));
      return accessToken;
    } else {
      return null;
    }
  }

  static Map<String, String> headers(
      {String? accessToken} /*http.Client client*/) {
    final headers = <String, String>{
      // see https://www.django-rest-framework.org/api-guide/versioning/
      // HttpHeaders.acceptHeader: 'application/json; version=v1',
      HttpHeaders.acceptLanguageHeader: L10n.current.locale.toLanguageTag(),
      HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
    };
    if (accessToken != null) {
      headers[HttpHeaders.authorizationHeader] = 'Bearer $accessToken';
    }
    return headers;
  }

  static Map<String, List<String>> parseMessages(Map<String, dynamic> json) {
    final Map<String, List<String>> result = <String, List<String>>{};
    for (var key in json.keys) {
      if (key == 'detail') {
        // simplejwt error message format
        result['messages'] = [json[key]];
        break;
      } else if (json[key] is Iterable) {
        Iterable list = json[key];
        result[key] = list.map((msg) => msg.toString()).toList();
      }
    }
    return result;
  }
}
