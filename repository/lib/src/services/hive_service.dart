import 'dart:async';

import 'package:hive_flutter/hive_flutter.dart';
import 'package:repository/src/model_adapters/locale_adapter.dart';

import '../../repository.dart';

/// Registers Hive adapters
/// Note: Since int keys are limited to 32 bit, convert them to String
///
class HiveService {
  static final HiveService _instance = HiveService._internal();

  factory HiveService() {
    return _instance;
  }

  HiveService._internal() : _init = _registerAdapters();

  final Future<void> _init;

  static Future<void> _registerAdapters() async {
    await Hive.initFlutter();
    Hive.registerAdapter(LocalDateAdapter());     // #0
    Hive.registerAdapter(VerseEntityAdapter());   // #1
    Hive.registerAdapter(TagEntityAdapter());     // #2
    Hive.registerAdapter(AccountEntityAdapter()); // #3
    Hive.registerAdapter(ScoreEntityAdapter());   // #4
    Hive.registerAdapter(LocaleAdapter());        // #5
    Hive.registerAdapter(CanonAdapter());         // #6
    Hive.registerAdapter(FontTypeAdapter());      // #7
    Hive.registerAdapter(VerseOrderAdapter());    // #8
  }

  Future<void> init() => _init;
}
