import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:app_core/app_core.dart';

import '../../repository.dart';
import '../utils.dart';

class PasswordService {
  static PasswordService? _instance;
  final http.Client httpClient;

  factory PasswordService(http.Client httpClient) {
    if (_instance == null) {
      _instance = PasswordService._internal(httpClient);
    }
    return _instance!;
  }

  PasswordService._internal(this.httpClient);

  /// Submits the password and returns a ResetPasswordState
  Future<ResetPasswordState> submitPassword({
    required String password,
    required String uidb64,
    required String token,
  }) async {
    final body =
        jsonEncode({'password': password, 'uidb64': uidb64, 'token': token});
    try {
      final response = await http.post(
          await replicationUri('/v1/access/reset-password/'),
          body: body,
          headers: AuthService.headers());
      if ([200].contains(response.statusCode)) {
        return ResetPasswordSuccess(
            messages: AuthService.parseMessages(json.decode(utf8.decode(response.body.codeUnits))));
      } else {
        var messages = {
          'messages': [L10n.current.t8('Server.error.summary')]
        };
        if (response.statusCode < 500)
          messages = AuthService.parseMessages(json.decode(utf8.decode(response.body.codeUnits)));
        return ResetPasswordFailure(messages: messages, password: password);
      }
    } on Exception {
      var messages = {
        'messages': [L10n.current.t8('Connection.error.title')]
      };
      return ResetPasswordFailure(messages: messages, password: password);
    }
  }
}
