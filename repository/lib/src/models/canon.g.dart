// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'canon.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CanonAdapter extends TypeAdapter<Canon> {
  @override
  final int typeId = 6;

  @override
  Canon read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return Canon.lut;
      default:
        return Canon.lut;
    }
  }

  @override
  void write(BinaryWriter writer, Canon obj) {
    switch (obj) {
      case Canon.lut:
        writer.writeByte(0);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CanonAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
