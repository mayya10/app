import 'package:hive/hive.dart';
import 'package:time_machine/time_machine.dart';
import 'package:time_machine/time_machine_text_patterns.dart';

import '../../repository.dart';

part 'score.g.dart';

@HiveType(typeId: 4)
class ScoreEntity extends Accountable {
  @HiveField(3)
  final String vkey;
  @HiveField(4)
  final LocalDate date;
  @HiveField(5)
  final int change;

  ScoreEntity(
      {required this.vkey,
      required this.date,
      required this.change,
      int? accountId,
      int? id,
      int? modified,
      int? modifiedBy,
      bool? deleted})
      : super(
            id: id, modified: modified, deleted: deleted, accountId: accountId);

  @override
  ScoreEntity copyWith({
    String? vkey,
    LocalDate? date,
    int? change,
    int? accountId,
    int? id,
    int? modified,
    int? modifiedBy,
    bool? deleted,
    List<String> nullValues = const [],
  }) {
    return ScoreEntity(
        vkey: vkey ?? this.vkey,
        date: date ?? this.date,
        change: change ?? this.change,
        accountId: accountId ?? this.accountId,
        id: id ?? this.id,
        modified: modified ?? this.modified,
        modifiedBy: modifiedBy ?? this.modifiedBy,
        deleted: deleted ?? this.deleted);
  }

  @override
  List<Object?> get props => super.props + [vkey, date, change];

  @override
  String toString() {
    return 'Score {  vkey: $vkey,  date: $date, change: $change, '
        'account: $accountId, id: $id, modified: $modified, '
        'modifiedBy: $modifiedBy, deleted: $deleted }';
  }

  @override
  Map<String, Object?> toJson() {
    final map = super.toJson();
    map.addAll({
      'vkey': vkey,
      'date': LocalDatePattern.iso.format(date),
      'change': change,
    });
    return map;
  }

  static ScoreEntity fromJson(Map<String, dynamic> json) {
    return ScoreEntity(
      vkey: json['vkey'] as String,
      date: LocalDatePattern.iso.parse(json['date'] as String).value,
      change: json['change'] as int,
      accountId: json['account'] as int,
      id: json['id'] as int,
      modified: json['modified'] as int,
      modifiedBy: Entity.modifiedByFromJson(json['modified_by']),
      deleted: json['deleted'] as bool,
    );
  }
}
