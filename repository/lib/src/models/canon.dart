import 'package:hive/hive.dart';

part 'canon.g.dart';

@HiveType(typeId: 6)
enum Canon {
  @HiveField(0)
  lut
}