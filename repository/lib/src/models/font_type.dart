import 'package:hive/hive.dart';

part 'font_type.g.dart';

@HiveType(typeId: 7)
enum FontType {
  @HiveField(0)
  Sans,
  @HiveField(1)
  Serif,
  @HiveField(2)
  Mono
}
