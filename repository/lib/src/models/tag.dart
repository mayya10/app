import 'package:hive/hive.dart';

import '../../repository.dart';

part 'tag.g.dart';

@HiveType(typeId: 2)
class TagEntity extends Accountable {
  @HiveField(3)
  final String text;
  @HiveField(4)
  final bool? included; // null: neither explicitly included nor excluded
  @HiveField(6, defaultValue: false)
  final bool published; // read only; calculated on server.

  TagEntity(
    this.text, {
    this.included,
    this.published = false,
    int? accountId,
    int? id,
    int? modified,
    int? modifiedBy,
    bool? deleted,
  }) : super(
            accountId: accountId,
            id: id,
            modified: modified,
            modifiedBy: modifiedBy,
            deleted: deleted);

  @override
  TagEntity copyWith({
    String? text,
    bool? included,
    bool? published,
    int? accountId,
    int? id,
    int? modified,
    int? modifiedBy,
    bool? deleted,
    List<String> nullValues = const [],
  }) {
    return TagEntity(text ?? this.text,
        included: included ??
            (nullValues.contains('included') ? null : this.included),
        published: published ?? this.published,
        accountId: accountId ?? this.accountId,
        id: id ?? this.id,
        modified: modified ?? this.modified,
        modifiedBy: modifiedBy ?? this.modifiedBy,
        deleted: deleted ?? this.deleted);
  }

  @override
  List<Object?> get props => super.props + [text, included, published];

  @override
  String toString() {
    return 'Tag {  text: $text,  included: $included, published: $published, '
        'account: $accountId, id: $id, modified: $modified, '
        'modifiedBy: $modifiedBy, deleted: $deleted }';
  }

  @override
  Map<String, Object?> toJson() {
    final map = super.toJson();
    map.addAll({
      'text': text,
      'included': included,
    });
    return map;
  }

  static TagEntity fromJson(Map<String, dynamic> json) {
    return TagEntity(
      json['text'] as String,
      included: json['included'] as bool?,
      published: json['published'] as bool,
      accountId: json['account'] as int,
      id: json['id'] as int,
      modified: json['modified'] as int,
      modifiedBy: Entity.modifiedByFromJson(json['modified_by']),
      deleted: json['deleted'] as bool,
    );
  }

  @override
  String get label {
    return text;
  }
}
