// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'score.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ScoreEntityAdapter extends TypeAdapter<ScoreEntity> {
  @override
  final int typeId = 4;

  @override
  ScoreEntity read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ScoreEntity(
      vkey: fields[3] as String,
      date: fields[4] as LocalDate,
      change: fields[5] as int,
      accountId: fields[12] as int?,
      id: fields[0] as int?,
      modified: fields[1] as int?,
      deleted: fields[2] as bool?,
    );
  }

  @override
  void write(BinaryWriter writer, ScoreEntity obj) {
    writer
      ..writeByte(7)
      ..writeByte(3)
      ..write(obj.vkey)
      ..writeByte(4)
      ..write(obj.date)
      ..writeByte(5)
      ..write(obj.change)
      ..writeByte(12)
      ..write(obj.accountId)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.modified)
      ..writeByte(2)
      ..write(obj.deleted);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ScoreEntityAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
