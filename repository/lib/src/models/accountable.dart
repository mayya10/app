import 'package:hive/hive.dart';

import '../../repository.dart';

class Accountable extends Entity {
  @HiveField(12)
  final int? accountId;

  Accountable({
    this.accountId,
    int? id,
    int? modified,
    int? modifiedBy,
    bool? deleted,
  }) : super(id: id, modified: modified, deleted: deleted);

  Accountable copyWith({
    int? accountId,
    int? id,
    int? modified,
    int? modifiedBy,
    bool? deleted,
  }) {
    return Accountable(
      accountId: accountId ?? this.accountId,
      id: id ?? this.id,
      modified: modified ?? this.modified,
      modifiedBy: modifiedBy ?? this.modifiedBy,
      deleted: deleted ?? this.deleted,
    );
  }

  @override
  List<Object?> get props => [id, modified, modifiedBy, deleted, accountId];

  @override
  String toString() {
    return 'Entity { accountId: $accountId, id: $id, modified: $modified, '
        'modifiedBy: $modifiedBy, deleted: $deleted }';
  }

  Map<String, Object?> toJson() {
    final map = super.toJson();
    map.addAll({
      'account': accountId,
    });
    return map;
  }

  static Accountable fromJson(Map<String, dynamic> json) {
    return Accountable(
      accountId: json['account'] as int,
      id: json['id'] as int?,
      modified: json['modified'] as int?,
      modifiedBy: Entity.modifiedByFromJson(json['modified_by']),
      deleted: json['deleted'] as bool?,
    );
  }

  String get label {
    return id.toString();
  }
}
