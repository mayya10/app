class NotFoundException implements Exception {
  final String name;

  NotFoundException(this.name);

  @override
  String toString() {
    return '$name not found.';
  }
}
