import 'package:hive/hive.dart';
import 'package:time_machine/time_machine.dart';
import 'package:time_machine/time_machine_text_patterns.dart';

import '../../repository.dart';
import 'accountable.dart';

part 'verse.g.dart';

@HiveType(typeId: 1)
class VerseEntity extends Accountable {
  @HiveField(3)
  final String reference;
  @HiveField(4)
  final String passage;
  @HiveField(5)
  final String? source;
  @HiveField(6)
  final String? topic;
  @HiveField(7)
  final String? image;
  @HiveField(8)
  final LocalDate? commit;
  @HiveField(9)
  final LocalDate? review;
  @HiveField(10, defaultValue: -1)
  final int level;
  @HiveField(11, defaultValue: const {})
  final Map<int, String?> tags;

  VerseEntity(this.reference, this.passage,
      {this.source,
      this.topic,
      this.image,
      this.commit,
      this.review,
      this.level = -1,
      this.tags = const {},
      int? accountId,
      int? id,
      int? modified,
      int? modifiedBy,
      bool? deleted})
      : super(
            accountId: accountId,
            id: id,
            modified: modified,
            modifiedBy: modifiedBy,
            deleted: deleted);

  @override
  VerseEntity copyWith({
    String? reference,
    String? passage,
    String? source,
    String? topic,
    String? image,
    LocalDate? commit,
    LocalDate? review,
    int? level,
    Map<int, String?>? tags,
    int? accountId,
    int? id,
    int? modified,
    int? modifiedBy,
    bool? deleted,
    List<String> nullValues = const [],
  }) {
    return VerseEntity(
      reference ?? this.reference,
      passage ?? this.passage,
      source: source ?? (nullValues.contains('source') ? null : this.source),
      topic: topic ?? (nullValues.contains('topic') ? null : this.topic),
      image: image ?? (nullValues.contains('image') ? null : this.image),
      commit: commit ?? (nullValues.contains('commit') ? null : this.commit),
      review: review ?? (nullValues.contains('review') ? null : this.review),
      level: level ?? this.level,
      tags: tags ?? this.tags,
      accountId: accountId ?? this.accountId,
      id: id ?? this.id,
      modified: modified ?? this.modified,
      modifiedBy: modifiedBy ?? this.modifiedBy,
      deleted: deleted ?? this.deleted,
    );
  }

  @override
  List<Object?> get props =>
      super.props +
      [
        passage,
        reference,
        source,
        topic,
        image,
        commit,
        review,
        level,
        tags,
      ];

  @override
  String toString() {
    return 'Verse {  reference: $reference, source: $source, topic: $topic, image: $image, '
        'passage: ${passage.length > 10 ? passage.replaceRange(10, passage.length, '…') : passage}, '
        'commit: $commit, review: $review, level: $level, tags: $tags, '
        'account: $accountId, id: $id, modified: $modified, '
        'modifiedBy: $modifiedBy, deleted: $deleted }';
  }

  @override
  Map<String, Object?> toJson() {
    final map = super.toJson();
    map.addAll({
      'reference': reference,
      'passage': passage,
      'source': source,
      'topic': topic,
      'image': image,
      'commit': commit != null ? LocalDatePattern.iso.format(commit!) : null,
      'review': review != null ? LocalDatePattern.iso.format(review!) : null,
      'level': level,
      'tags': tags.keys.toList(),
    });
    return map;
  }

  static VerseEntity fromJson(Map<String, dynamic> json) {
    return VerseEntity(
      json['reference'] as String,
      json['passage'] as String,
      source: json['source'] as String?,
      topic: json['topic'] as String?,
      image: (json['image'] ?? json['safe_image']) as String?,
      commit: json['commit'] != null
          ? LocalDatePattern.iso.parse(json['commit'] as String).value
          : null,
      review: json['review'] != null
          ? LocalDatePattern.iso.parse(json['review'] as String).value
          : null,
      level: json['level'] != null ? json['level'] as int : -1,
      tags: json['tags'] != null
          ? Map.fromIterable(json['tags'] as Iterable<dynamic>,
              key: (e) => e, value: (e) => null)
          : {},
      accountId: json['account'] as int?,
      id: json['id'] as int?,
      modified: json['modified'] as int?,
      modifiedBy: Entity.modifiedByFromJson(json['modified_by']),
      deleted: json['deleted'] as bool?,
    );
  }

  @override
  String get label {
    return '$reference / $passage';
  }
}
