import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:repository/src/models/font_type.dart';

import '../../repository.dart';
import 'entity.dart';
part 'account.g.dart';

@HiveType(typeId: 3)
class AccountEntity extends Entity {
  @HiveField(3)
  final String name;
  @HiveField(4, defaultValue: Locale('en'))
  final Locale language;
  @HiveField(5, defaultValue: Locale('en'))
  final Locale langRef;
  @HiveField(6, defaultValue: 2.0)
  final double reviewFrequency;
  @HiveField(7, defaultValue: 128)
  final int reviewLimit;
  @HiveField(8, defaultValue: 70)
  final int dailyGoal;
  @HiveField(19, defaultValue: 7)
  final int inverseLimit;
  @HiveField(15, defaultValue: false)
  final bool referenceIncluded;
  @HiveField(16, defaultValue: false)
  final bool topicPreferred;
  @HiveField(17)
  final String? defaultSource;
  @HiveField(9, defaultValue: Canon.lut)
  final Canon canon;
  @HiveField(18)
  final FontType fontType;
  @HiveField(10, defaultValue: VerseOrder.DATE)
  final VerseOrder orderNew;
  @HiveField(11, defaultValue: VerseOrder.LEVEL)
  final VerseOrder orderDue;
  @HiveField(12, defaultValue: VerseOrder.RANDOM)
  final VerseOrder orderKnown;
  @HiveField(13, defaultValue: VerseOrder.CANON)
  final VerseOrder orderAll;
  @HiveField(14, defaultValue: const [])
  final List<int> importedDecks;

  AccountEntity(this.name,
      {this.language = const Locale('en'),
      this.langRef = const Locale('en'),
      this.reviewFrequency = 2.0,
      this.reviewLimit = 128,
      this.dailyGoal = 70,
      this.inverseLimit = 7,
      this.referenceIncluded = false,
      this.topicPreferred = false,
      this.defaultSource,
      this.canon = Canon.lut,
      this.fontType = FontType.Sans,
      this.orderNew = VerseOrder.CANON,
      this.orderDue = VerseOrder.LEVEL,
      this.orderKnown = VerseOrder.DATE,
      this.orderAll = VerseOrder.CANON,
      this.importedDecks = const [],
      int? id,
      int? modified,
      int? modifiedBy,
      bool? deleted})
      : super(
            id: id,
            modified: modified,
            modifiedBy: modifiedBy,
            deleted: deleted);

  @override
  AccountEntity copyWith({
    String? name,
    Locale? language,
    Locale? langRef,
    double? reviewFrequency,
    int? reviewLimit,
    int? dailyGoal,
    int? inverseLimit,
    bool? referenceIncluded,
    bool? topicPreferred,
    String? defaultSource,
    Canon? canon,
    FontType? fontType,
    VerseOrder? orderNew,
    VerseOrder? orderDue,
    VerseOrder? orderKnown,
    VerseOrder? orderAll,
    List<int>? importedDecks,
    int? id,
    int? modified,
    int? modifiedBy,
    bool? deleted,
    List<String> nullValues = const [],
  }) {
    return AccountEntity(name ?? this.name,
        language: language ?? this.language,
        langRef: langRef ?? this.langRef,
        reviewFrequency: reviewFrequency ?? this.reviewFrequency,
        reviewLimit: reviewLimit ?? this.reviewLimit,
        dailyGoal: dailyGoal ?? this.dailyGoal,
        inverseLimit: inverseLimit ?? this.inverseLimit,
        referenceIncluded: referenceIncluded ?? this.referenceIncluded,
        topicPreferred: topicPreferred ?? this.topicPreferred,
        defaultSource: defaultSource ??
            (nullValues.contains('defaultSource') ? null : this.defaultSource),
        canon: canon ?? this.canon,
        fontType: fontType ?? this.fontType,
        orderNew: orderNew ?? this.orderNew,
        orderDue: orderDue ?? this.orderDue,
        orderKnown: orderKnown ?? this.orderKnown,
        orderAll: orderAll ?? this.orderAll,
        importedDecks: importedDecks ?? this.importedDecks,
        id: id ?? this.id,
        modified: modified ?? this.modified,
        modifiedBy: modifiedBy ?? this.modifiedBy,
        deleted: deleted ?? this.deleted);
  }

  @override
  List<Object?> get props =>
      super.props +
      [
        name,
        language,
        langRef,
        reviewFrequency,
        reviewLimit,
        dailyGoal,
        inverseLimit,
        referenceIncluded,
        topicPreferred,
        defaultSource,
        canon,
        fontType,
        orderNew,
        orderDue,
        orderKnown,
        orderAll,
        importedDecks,
      ];

  @override
  String toString() {
    return 'Account {  name: $name,  language: $language,  langRef: $langRef, '
        'reviewFrequency: $reviewFrequency, reviewLimit: $reviewLimit, '
        'referenceIncluded: $referenceIncluded, topicPreferred: $topicPreferred, '
        'dailyGoal: $dailyGoal, defaultSource: $defaultSource, '
        'inverseLimit: $inverseLimit, canon: $canon, fontType: $fontType, '
        'orderNew: $orderNew, orderDue: $orderDue, '
        'orderKnown: $orderKnown, orderAll: $orderAll, importedDecks: $importedDecks, '
        'id: $id, modified: $modified, modifiedBy: $modifiedBy, deleted: $deleted }';
  }

  @override
  Map<String, Object?> toJson() {
    final map = super.toJson();
    map.addAll({
      'name': name,
      'language': language.toString(),
      'lang_ref': langRef.toString(),
      'review_frequency': reviewFrequency,
      'review_limit': reviewLimit,
      'daily_goal': dailyGoal,
      'inverse_limit': inverseLimit,
      'reference_included': referenceIncluded,
      'topic_preferred': topicPreferred,
      'default_source': defaultSource,
      'canon': canon.index,
      'font_type': fontType.index,
      'order_new': orderNew.index,
      'order_due': orderDue.index,
      'order_known': orderKnown.index,
      'order_all': orderAll.index,
      'imported_decks': importedDecks,
    });
    return map;
  }

  static AccountEntity fromJson(Map<String, dynamic> json) {
    // requires corresponding APIs in
    // - LegacyService.attachAccount,
    // - AccountsRepositoryHttp.parseItem,
    // - CurrentAccount._entityFromSettings
    return AccountEntity(
      json['name'] as String,
      language: TextUtil.toLocale(json['language'] as String),
      langRef: TextUtil.toLocale(json['lang_ref'] ?? json['language'] as String),
      reviewFrequency: json['review_frequency'] as double,
      reviewLimit: json['review_limit'] as int,
      dailyGoal: json['daily_goal'] as int,
      inverseLimit: (json['inverse_limit'] ?? 7) as int,
      referenceIncluded: json['reference_included'] as bool,
      topicPreferred: json['topic_preferred'] as bool,
      defaultSource: json['default_source'] as String?,
      canon: enumFrom<Canon>(Canon.values, json['canon']),
      fontType: enumFrom<FontType>(FontType.values, json['font_type']),
      orderNew: enumFrom<VerseOrder>(VerseOrder.values, json['order_new']),
      orderDue: enumFrom<VerseOrder>(VerseOrder.values, json['order_due']),
      orderKnown: enumFrom<VerseOrder>(VerseOrder.values, json['order_known']),
      orderAll: enumFrom<VerseOrder>(VerseOrder.values, json['order_all']),
      importedDecks: (json['imported_decks'] as List<dynamic>)
          .map((id) => id as int)
          .toList(),
      id: json['id'] as int,
      modified: json['modified'] as int,
      modifiedBy: Entity.modifiedByFromJson(json['modified_by']),
      deleted: json['deleted'] as bool,
    );
  }

  @override
  String get label {
    return name;
  }
}
