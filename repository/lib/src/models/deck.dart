import '../../repository.dart';

/// Decks contain meta information for publishing a tag
/// and are not replicated or kept as 'deleted'
class DeckEntity extends Entity {
  final String name;
  final String? description;
  final String? website;
  final String? image;

  DeckEntity(
      {required this.name,
      this.description,
      this.website,
      this.image,
      int? id})
      : super(id: id, modified: 0, modifiedBy: 0, deleted: false);

  @override
  DeckEntity copyWith({
    String? name,
    String? description,
    String? website,
    String? image,
    int? id,
    int? modified,
    int? modifiedBy,
    bool? deleted,
    List<String> nullValues = const [],
  }) {
    return DeckEntity(
      name: name ?? this.name,
      description: description ?? this.description,
      website:
          website ?? (nullValues.contains('website') ? null : this.website),
      image: image ?? (nullValues.contains('image') ? null : this.image),
      id: id ?? this.id,
    );
  }

  @override
  List<Object?> get props => super.props + [name, description, website, image];

  @override
  String toString() {
    return 'Deck { name: $name, description: $description, '
        'website: $website, image: $image, '
        'id: $id, modified: $modified, modifiedBy: $modifiedBy, deleted: $deleted }';
  }

  @override
  Map<String, Object?> toJson() {
    return {
      'tag': id,
      'name': name,
      'description': description,
      'website': website,
      'image': image,
    };
  }

  static DeckEntity fromJson(Map<String, dynamic> json) {
    return DeckEntity(
      name: json['name'] as String,
      description: json['description'] as String,
      website: json['website'] as String?,
      image: json['image'] as String?,
      id: json['tag'] as int,
    );
  }
}
