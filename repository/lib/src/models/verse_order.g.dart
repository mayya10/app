// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'verse_order.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class VerseOrderAdapter extends TypeAdapter<VerseOrder> {
  @override
  final int typeId = 8;

  @override
  VerseOrder read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return VerseOrder.ALPHABET;
      case 1:
        return VerseOrder.CANON;
      case 2:
        return VerseOrder.TOPIC;
      case 3:
        return VerseOrder.DATE;
      case 4:
        return VerseOrder.LEVEL;
      case 5:
        return VerseOrder.RANDOM;
      case 6:
        return VerseOrder.ID;
      default:
        return VerseOrder.ALPHABET;
    }
  }

  @override
  void write(BinaryWriter writer, VerseOrder obj) {
    switch (obj) {
      case VerseOrder.ALPHABET:
        writer.writeByte(0);
        break;
      case VerseOrder.CANON:
        writer.writeByte(1);
        break;
      case VerseOrder.TOPIC:
        writer.writeByte(2);
        break;
      case VerseOrder.DATE:
        writer.writeByte(3);
        break;
      case VerseOrder.LEVEL:
        writer.writeByte(4);
        break;
      case VerseOrder.RANDOM:
        writer.writeByte(5);
        break;
      case VerseOrder.ID:
        writer.writeByte(6);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is VerseOrderAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
