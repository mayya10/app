import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:repository/repository.dart';

class EntitiesRepositoryRpl<T extends Entity> extends EntitiesRepository<T> {
  final EntitiesRepositoryHive<T> hiveRepo;
  final EntitiesRepositoryHttp<T> httpRepo;
  final sendQueues = di.get<Queues>();
  final void Function()? onUpdate;

  EntitiesRepositoryRpl(this.hiveRepo, this.httpRepo, {this.onUpdate});

  @override
  Future<T> create(T entity) {
    return update(entity);
  }

  @override
  Future<List<T>> createList(List<T> entities) async {
    return updateList(entities);
  }

  @override
  Future<void> delete(T entity) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<void> deleteList(List<T> entities) {
    // TODO: implement deleteList
    throw UnimplementedError();
  }

  @override
  Future<T?> read(int id, {int? accountId}) {
    return hiveRepo.read(id, accountId: accountId);
  }

  @override
  Future<List<T>> readList(
      {int? accountId, bool? deleted, int? since, int? client}) async {
    try {
      return await hiveRepo.readList(accountId: accountId, deleted: false);
    } on NotFoundException catch(e) {
      debugPrint(e.toString());
      final entities = await httpRepo.readList(accountId: accountId, deleted: false);
      return await hiveRepo.updateList(entities);
    }
  }

  @override
  Future<T> update(T entity) async {
    return (await updateList([entity])).first;
  }

  @override
  Future<List<T>> updateList(List<T> entities) async {
    entities = stampAll(entities);
    await hiveRepo.updateList(entities);
    final ids = entities.map((entity) => entity.id).toSet();
    final first = entities.first;
    await sendQueues.add(hiveRepo.entityType, ids,
        accountId: first is Accountable ? first.accountId : null);
    if (this.onUpdate != null) this.onUpdate!();
    return entities;
  }
}
