import 'package:app_core/app_core.dart';
import 'package:repository/repository.dart';

class ScoresRepositoryRpl extends EntitiesRepositoryRpl<ScoreEntity>
    implements ScoresRepository {
  ScoresRepositoryRpl({void Function()? onUpdate})
      : super(di.get<EntitiesRepositoryHive<ScoreEntity>>(),
            di.get<EntitiesRepositoryHttp<ScoreEntity>>(),
            onUpdate: onUpdate);
}
