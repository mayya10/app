import 'package:app_core/app_core.dart';
import 'package:repository/repository.dart';

class VersesRepositoryRpl extends EntitiesRepositoryRpl<VerseEntity>
    implements VersesRepository {
  VersesRepositoryRpl({required void Function() onUpdate})
      : super(di.get<EntitiesRepositoryHive<VerseEntity>>(),
            di.get<EntitiesRepositoryHttp<VerseEntity>>(),
            onUpdate: onUpdate);
}
