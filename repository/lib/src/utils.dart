import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:repository/repository.dart';

import '../repository.dart';

const TYPE_KEYS = {
  AccountEntity: 'accounts',
  DeckEntity: 'decks',
  ScoreEntity: 'scores',
  TagEntity: 'tags',
  VerseEntity: 'verses',
};

Future<Uri> replicationUri(String unencodedPath,
    [Map<String, dynamic>? queryParameters]) async {
  return await uriFromAddress(
      Settings.RPL_SERVER, unencodedPath, queryParameters);
}

Future<Uri> legacyUri(String unencodedPath,
    [Map<String, dynamic>? queryParameters]) async {
  return await uriFromAddress(
      Settings.LEGACY_SERVER, unencodedPath, queryParameters);
}

Future<Uri> uriFromAddress(String key, String unencodedPath,
    Map<String, dynamic>? queryParameters) async {
  final address = Settings().get(key) ?? Settings.DEFAULTS[key]!;
  final parts = address.split('://');
  if (parts[0] == 'https') {
    return Uri.https(parts[1], unencodedPath, queryParameters);
  } else {
    return Uri.http(parts[1], unencodedPath, queryParameters);
  }
}

String repositoryKey(Type entityType, {int? id}) {
  var result = TYPE_KEYS[entityType]!;
  if (id != null) {
    result += '_$id';
  }
  return result;
}

T enumFrom<T extends Enum>(List<T> values, i) {
  try {
    if (i is int) {
      return values[i];
    } else if (i is String) {
      return values.byName(i);
    }
  } catch (e) {
    debugPrint('enumFrom $i failed: $e');
  }
  return values[0];
}
