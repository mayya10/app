import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:repository/repository.dart';

import '../../repository.dart';

abstract class EntitiesRepositoryHive<T extends Entity> // todo: use LazyBox
    extends EntitiesRepository<T> {
  final Type entityType = Entity;

  EntitiesRepositoryHive();

  Future<Box<T>> getBox(String boxName) async {
    await HiveService().init();
    return Hive.openBox(boxName);
  }

  Future<void> clear([int? accountId]) async {
    final boxName = repositoryKey(entityType, id: accountId);
    if (await Hive.boxExists(boxName)) {
      Hive.deleteBoxFromDisk(repositoryKey(entityType, id: accountId));
    }
  }

  @override
  Future<List<T>> readList(
      {int? accountId, bool? deleted, int? since, int? client}) async {
    final boxName = repositoryKey(entityType, id: accountId);
    if (!await Hive.boxExists(boxName)) {
      return [];  // fail silently   RETURN
    }
    final box = await getBox(boxName);
    debugPrint('Entities from box ${box.name}: ${box.values.length}');
    var entities = box.values;
    if (deleted != null) entities = entities.where((e) => e.deleted == deleted);
    if (since != null) entities = entities.where((e) => e.modified >= since);
    return entities.toList();
  }

  @override
  Future<List<T>> createList(List<T> entities) {
    // TODO: implement createList
    throw UnimplementedError();
  }

  @override
  Future<List<T>> updateList(List<T> entities) async {
    for (var entity in entities) {
      final box = await getBox(repositoryKey(entityType,
          id: entity is Accountable ? entity.accountId : null));
      await box.put(entity.id.toString(), entity);
    }
    return entities;
  }

  @override
  Future<void> deleteList(List<T> entities) {
    // TODO: implement createList
    throw UnimplementedError();
  }

  @override
  Future<T?> read(int id, {int? accountId}) async {
    final box = await getBox(repositoryKey(entityType, id: accountId));
    return box.get(id.toString());
  }

  @override
  Future<T> create(T entity) => update(entity);

  @override
  Future<T> update(T entity) async {
    final box = await getBox(repositoryKey(entityType,
        id: entity is Accountable ? entity.accountId : null));
    await box.put(entity.id.toString(), entity);
    return entity;
  }

  @override
  Future<void> delete(T entity) async {
    final box = await getBox(repositoryKey(entityType,
        id: entity is Accountable ? entity.accountId : null));
    await box.delete(entity.id.toString());
  }

  List<T> sample({int? accountId}) {
    return [];
  }
}
