
import 'package:repository/repository.dart';

import '../../repository.dart';

class ScoresRepositoryHive extends EntitiesRepositoryHive<ScoreEntity> implements ScoresRepository {
  static final ScoresRepositoryHive _instance = ScoresRepositoryHive._internal();
  final Type entityType = ScoreEntity;


  factory ScoresRepositoryHive() {
    return _instance;
  }

  ScoresRepositoryHive._internal(): super();

  @override
  List<ScoreEntity> sample({int? accountId}) {
    return [];
  }

}