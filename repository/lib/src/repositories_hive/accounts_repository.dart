
import 'package:repository/repository.dart';

import '../../repository.dart';

class AccountsRepositoryHive extends EntitiesRepositoryHive<AccountEntity> implements AccountsRepository {
  static final AccountsRepositoryHive _instance = AccountsRepositoryHive._internal();
  final Type entityType = AccountEntity;


  factory AccountsRepositoryHive() {
    return _instance;
  }

  AccountsRepositoryHive._internal(): super();

  @override
  List<AccountEntity> sample({int? accountId}) {
    return [
      AccountEntity(
        'Bible Study',
        id: 1,
      ),
      AccountEntity(
        'God\'s Unfailing Love',
        id: 2,
      ),
      AccountEntity(
        'Human Failure',
        id: 3,
      ),
    ];

  }

}