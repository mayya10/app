import 'package:repository/repository.dart';

import '../../repository.dart';

class DecksRepositoryHttp extends EntitiesRepositoryHttp<DeckEntity>
    implements DecksRepository {
  static DecksRepositoryHttp? _instance;

  factory DecksRepositoryHttp(
      AuthService authService, HttpService httpService) {
    if (_instance == null) {
      _instance = DecksRepositoryHttp._internal('decks/');
    }
    return _instance!;
  }

  DecksRepositoryHttp._internal(String path)
      : super(path);

  @override
  DeckEntity parseItem(Map<String, dynamic>? json) {
    return DeckEntity.fromJson(json!);
  }

  @override
  Future<void> delete(DeckEntity entity) async {
    final response = await authService
        .call((headers) => httpService.deleteItem(path, entity, headers));
  }
}
