import 'dart:async';
import 'dart:convert';

import 'package:app_core/app_core.dart';
import 'package:repository/repository.dart';

import '../../repository.dart';

abstract class EntitiesRepositoryHttp<T extends Entity>
    extends EntitiesRepository<T> {
  final String path;
  late final AuthService authService;
  late final HttpService httpService;

  EntitiesRepositoryHttp(this.path) {
    authService = di.get<AuthBloc>().service;
    httpService = HttpService();
  }

  @override
  Future<T> read(int id, {int? accountId}) async {
    final response = await (authService
        .call((headers) => httpService.getItem(path, id, headers)));
    return parseItem(jsonDecode(utf8.decode(response.body.codeUnits)));
  }

  @override
  Future<T> create(T entity) async {
    entity = stamp(entity);
    await authService
        .call((headers) => httpService.postItem(path, entity, headers));
    return entity;
  }

  @override
  Future<T> update(T entity) async {
    entity = stamp(entity);
    await authService
        .call((headers) => httpService.patchItem(path, entity, headers));
    return entity;
  }

  @override
  Future<void> delete(T entity) async {
    await authService.call(
        (headers) => httpService.deleteItem(path, entity, headers));
  }

  @override
  Future<List<T>> readList(
      {int? accountId, bool? deleted = false, int? since, int? client }) async {
    final response = await (authService.call((headers) => httpService.getList(
        path,
        accountId: accountId,
        deleted: deleted,
        since: since,
        client: client,
        headers: headers)));
    return parseList(jsonDecode(utf8.decode(response.body.codeUnits)));
  }

  @override
  Future<List<T>> createList(List<T> entities) async {
    entities = stampAll(entities);
    await authService.call(
        (headers) => httpService.postList(path, entities, headers));
    return entities;
  }

  @override
  Future<List<T>> updateList(List<T> entities, {bool isAlreadyStamped = false}) async {
    if (!isAlreadyStamped) entities = stampAll(entities);
    await authService
        .call((headers) => httpService.patchList(path, entities, headers));
    return entities;
  }

  @override
  Future<void> deleteList(List<T> entities) async {
    await authService.call(
        (headers) => httpService.deleteList(path, entities, headers));
  }

  List<T> sample() {
    return [];
  }

  List<T> parseList(List<dynamic> items) {
    return items
        .map((item) => parseItem(item as Map<String, dynamic>))
        .toList();
  }

  T parseItem(Map<String, dynamic>? json);
}
