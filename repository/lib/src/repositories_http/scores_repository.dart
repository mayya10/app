import 'package:repository/repository.dart';

import '../../repository.dart';
import 'entities_repository.dart';

class ScoresRepositoryHttp extends EntitiesRepositoryHttp<ScoreEntity>
    implements ScoresRepository {
  static ScoresRepositoryHttp? _instance;

  factory ScoresRepositoryHttp() {
    if (_instance == null) {
      _instance = ScoresRepositoryHttp._internal('scores/');
    }
    return _instance!;
  }

  ScoresRepositoryHttp._internal(String path)
      : super(path);

  @override
  ScoreEntity parseItem(Map<String, dynamic>? json) {
    return ScoreEntity.fromJson(json!);
  }
}
