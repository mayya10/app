import 'package:repository/repository.dart';

import '../../repository.dart';
import 'entities_repository.dart';

class AccountsRepositoryHttp extends EntitiesRepositoryHttp<AccountEntity>
    implements AccountsRepository {
  static AccountsRepositoryHttp? _instance;

  factory AccountsRepositoryHttp() {
    if (_instance == null) {
      _instance = AccountsRepositoryHttp._internal('accounts/');
    }
    return _instance!;
  }

  AccountsRepositoryHttp._internal(String path): super(path);

  @override
  AccountEntity parseItem(Map<String, dynamic>? json) {
    return AccountEntity.fromJson(json!);
  }

  @override
  List<AccountEntity> sample() {
    return [
      AccountEntity(
        'Test',
        id: 1,
      ),
      AccountEntity(
        'Demo',
        id: 2,
      ),
      AccountEntity(
        'My Verses',
        id: 3,
      ),
    ];
  }
}
