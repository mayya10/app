import 'package:app_core/app_core.dart';
import 'package:repository/repository.dart';

import '../../repository.dart';

class VersesRepositoryHttp extends EntitiesRepositoryHttp<VerseEntity>
    implements VersesRepository {
  static VersesRepositoryHttp? _instance;

  factory VersesRepositoryHttp() {
    if (_instance == null) {
      _instance = VersesRepositoryHttp._internal('verses/');
    }
    return _instance!;
  }

  VersesRepositoryHttp._internal(String path)
      : super(path);

  @override
  VerseEntity parseItem(Map<String, dynamic>? json) {
    return VerseEntity.fromJson(json!);
  }

  List<VerseEntity> sample() {
    return [
      VerseEntity(
        'Joshua 1:8',
        'This Book of the Law shall not depart from your mouth, but you shall meditate on it day and night, so that you may be careful to do according to all that is written in it.'
            'For then you will make your way prosperous, and then you will have good success.',
        source: 'ESV',
        topic: 'Word of God',
        level: -1,
        id: 1,
        commit: DateService().today,
        tags: {1: null, 2: null},
        accountId: 0,
      ),
      VerseEntity(
        'Isaiah 12:1',
        'In that day you will sing:'
            '“I will praise you, O Lord!'
            'You were angry with me, but not any more.'
            'Now you comfort me.',
        source: 'NIV',
        topic: 'Comfort',
        level: 3,
        id: 2,
        tags: {2: null},
        accountId: 0,
      ),
      VerseEntity(
        '1 Kings 18:21',
        'How long will you go limping between two different opinions? '
            'If the Lord is God, follow him;'
            'but if Baal, then follow him.',
        level: 0,
        id: 3,
        commit: DateService().today.subtractDays(10),
        review: DateService().today,
        tags: {},
        accountId: 0,
      ),
      VerseEntity(
        'Psalm 16:11',
        'Lord, You will show me the way of life.',
        source: 'NIVUK',
        level: 23,
        id: 4,
        tags: {},
        accountId: 0,
      ),
      VerseEntity(
        'Luke 23:42',
        'And he said, “Jesus, remember me when you come into your kingdom.”',
        topic: 'Remember Me',
        level: 12,
        id: 5,
        commit: DateService().today.subtractDays(10),
        review: DateService().today.subtractDays(4),
        tags: {2: null, 3: null},
        accountId: 0,
      ),
    ];
  }
}
