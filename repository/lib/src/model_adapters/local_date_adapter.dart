import 'package:hive/hive.dart';
import 'package:time_machine/time_machine.dart';

/// Adapter for LocalDate
class LocalDateAdapter<T extends LocalDate> extends TypeAdapter<T> {
  @override
  final int typeId = 0;

  LocalDateAdapter();

  @override
  T read(BinaryReader reader) {
    return LocalDate.fromEpochDay(reader.readInt()) as T;
  }

  @override
  void write(BinaryWriter writer, LocalDate obj) {
    writer.writeInt(obj.epochDay);
  }
}