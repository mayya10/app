import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

/// Adapter for Locale
class LocaleAdapter<T extends Locale> extends TypeAdapter<T> {
  @override
  final int typeId = 5;

  LocaleAdapter();

  @override
  T read(BinaryReader reader) {
    return TextUtil.toLocale(reader.readString()) as T;
  }

  @override
  void write(BinaryWriter writer, Locale obj) {
    writer.writeString(obj.toString());
  }
}