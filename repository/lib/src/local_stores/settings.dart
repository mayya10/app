import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';

import '../../repository.dart';

class Settings {
  static const CURRENT_RPL_DB_VERSION = 2;
  static const ACCESS_TOKEN = 'access_token';
  static const BIBLE_SERVER = 'bible_server';
  static const REFRESH_TOKEN = 'refresh_token';
  static const CURRENT_ACCOUNT = 'current_account';
  static const DOC_SERVER = 'doc_server';
  static const USER_EMAIL = 'user_email';
  static const IMAGE_SERVER = 'image_server';
  static const INCLUDE_NUMBERS = 'include_numbers';
  static const LEGACY_SERVER = 'legacy_server';
  static const REMINDER_ON = 'reminder_on';
  static const RATING_COUNT = 'rating_count';
  static const REMINDER_TIME_H = 'reminder_time_h';
  static const REMINDER_TIME_M = 'reminder_time_m';
  static const RPL_DB_VERSION = 'rpl_db_version';
  static const RPL_DEVICE_ID = 'rpl_device_id';
  static const RPL_LAST_RECEIVED = 'rpl_last_received';
  static const RPL_SERVER = 'rpl_server';
  static const SPEECH_SPEED = 'speech_speed';
  static const SPEECH_PAUSE = 'speech_pause';
  static const SPEECH_PLAYLIST = 'speech_playlist';
  static const SPEECH_REPEAT = 'speech_repeat';
  static const STUDY_SPEECH_ON = 'study_speech_on';
  static const STUDY_PUZZLE_SIZE = 'study_puzzle_size';
  static const DEFAULTS = {
    BIBLE_SERVER: 'https://bible.remem.me',
    IMAGE_SERVER: 'https://bible.remem.me',
    LEGACY_SERVER: 'https://my.remem.me',
    RPL_SERVER: 'https://rpl.remem.me',
    DOC_SERVER: 'https://www.remem.me',
  };

  static final Settings _instance = Settings._internal();

  final String _boxName = 'settings';

  factory Settings() {
    return _instance;
  }

  Settings._internal() {
    _init = _loadBox();
  }

  late Future<void> _init;
  Box<String>? _box;

  Future<void> _loadBox() async {
    try {
      await HiveService().init();
      _box = await Hive.openBox(_boxName);
    } catch (e) {
      debugPrint('Settings not loaded: $e');
    }
  }

  Future<Settings> init() async {
    await _init;
    return this;
  }

  /// Returns the value associated with the given [key]. If the key does not
  /// exist, `null` is returned.
  String? get(String key) {
    return _box?.get(key);
  }

  /// Returns the value associated with the given [key]. If the key does not
  /// exist, the default value is returned.
  /// Throws if the [key] is not registered in DEFAULTS.
  String getOrDefault(String key) {
    return get(key) ?? DEFAULTS[key]!;
  }

  /// Saves the [key] - [value] pair.
  /// Can be used synchronously.
  /// (https://docs.hivedb.dev/#/basics/read_write?id=write)
  Future<void> put(String key, String value) {
    return _box != null ? _box!.put(key, value) : Future.value();
  }

  int? getInt(String key) {
    final value = get(key);
    return value != null ? int.parse(value) : null;
  }

  Future putInt(String key, int value) {
    return put(key, value.toString());
  }

  double? getDouble(String key) {
    final value = get(key);
    return value != null ? double.parse(value) : null;
  }

  Future putDouble(String key, double value) {
    return put(key, value.toString());
  }

  bool? getBool(String key) {
    final value = get(key);
    return value != null ? value == 'true' : null;
  }

  Future putBool(String key, bool value) {
    return put(key, value.toString());
  }

  /// Deletes the [key] - [value] pair.
  Future delete(String key) {
    return _box != null ? _box!.delete(key) : Future.value();
  }

  /// Retrieves device id or generates it
  int get deviceId {
    var id = getInt(RPL_DEVICE_ID);
    if (id == null) {
      id = IdGenerator().next();
      putInt(RPL_DEVICE_ID, id);
    }
    return id;
  }

  bool get hasNewDbVersion {
    return !kIsWeb &&
        CURRENT_RPL_DB_VERSION > (getInt(Settings.RPL_DB_VERSION) ?? 0);
  }

  Future updateDbVersion() async {
    if (!kIsWeb)
      await putInt(Settings.RPL_DB_VERSION, Settings.CURRENT_RPL_DB_VERSION);
  }
}
