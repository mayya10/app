import 'dart:core';

import '../../repository.dart';




/// A class that Loads and Persists scores. The data layer of the app.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as scores_repository_simple or scores_repository_web.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment, such as
/// web or Flutter.
abstract class ScoresRepository extends EntitiesRepository<ScoreEntity> {}
