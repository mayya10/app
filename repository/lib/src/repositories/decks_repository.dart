import 'dart:core';

import '../../repository.dart';


/// A class that Loads and Persists decks. The data layer of the app.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as decks_repository_simple or decks_repository_web.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment, such as
/// web or Flutter.
abstract class DecksRepository extends EntitiesRepository<DeckEntity> {

  Future<void> delete(DeckEntity entity);

}
