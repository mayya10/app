import 'dart:async';
import 'dart:core';

import 'package:app_core/app_core.dart';

import '../../repository.dart';

/// A class that Loads and Persists entities. The data layer of the app.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as entities_repository_simple or entities_repository_web.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment, such as
/// web or Flutter.
abstract class EntitiesRepository<T extends Entity> {
  /// Loads entities first from File storage. If they don't exist or encounter an
  /// error, it attempts to load the Entities from a Web Client.
  Future<List<T>> readList(
      {int? accountId, bool? deleted, int? since, int? client});

  // Persists entities to local disk and the web
  Future<List<T>> createList(List<T> entities);

  // Persists entities to local disk and the web
  Future<List<T>> updateList(List<T> entities);

  Future<void> deleteList(List<T> entities);

  Future<T?> read(int id, {int? accountId});

  Future<T> create(T entity);

  Future<T> update(T entity);

  Future<void> delete(T entity);

  List<T> stampAll(List<T> entities) {
    return entities.map((entity) => stamp(entity)).toList();
  }

  T stamp(T entity) {
    return entity.copyWith(
        modified: IdGenerator().now(),
        modifiedBy: di<Settings>().deviceId) as T;
  }
}
