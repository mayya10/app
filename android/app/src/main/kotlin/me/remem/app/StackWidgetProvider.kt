package me.remem.app

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Build
import android.view.View
import android.widget.RemoteViews
import androidx.core.widget.RemoteViewsCompat
import es.antonborri.home_widget.HomeWidgetLaunchIntent
import es.antonborri.home_widget.HomeWidgetProvider
import org.json.JSONArray
import org.json.JSONTokener

class StackWidgetProvider : HomeWidgetProvider() {
    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray,
        widgetData: SharedPreferences
    ) {
        appWidgetIds.forEach { widgetId ->
            val views = createCollectionViews(context, widgetId, widgetData)
            appWidgetManager.updateAppWidget(widgetId, views)
        }
    }

    private fun createCollectionViews(context: Context, widgetId: Int, widgetData: SharedPreferences): RemoteViews {
        return RemoteViews(context.packageName, R.layout.stackwidget).apply {
            setPendingIntentTemplate(R.id.stack_view, mutablePendingIntent(context))
            val builder = RemoteViewsCompat.RemoteCollectionItems.Builder()
            val box = widgetData.getString("box", "DUE")!!
            val data = widgetData.getString("verses", "[]")!!
            val emptyMessage = widgetData.getString("empty", "...")!!
            val topicPreferred = widgetData.getBoolean("topicPreferred", false)
            val jsonArray = JSONTokener(data).nextValue() as JSONArray
            for (i in 0 until jsonArray.length()) {
                val id = jsonArray.getJSONObject(i).getLong("id")
                val item = createItemViews(context, jsonArray, i, box, topicPreferred)
                builder.addItem(id, item)
            }
            RemoteViewsCompat.setRemoteAdapter(context, this, widgetId, R.id.stack_view, builder.build())
            setEmptyView(R.id.stack_view, R.id.empty_view)
            setTextViewText(R.id.empty_view, emptyMessage)
            setOnClickPendingIntent(R.id.empty_view, mutablePendingIntent(context))
        }
    }

    private fun createItemViews(
        context: Context,
        jsonArray: JSONArray,
        pos: Int,
        box: String,
        topicPreferred: Boolean
    ): RemoteViews {
        return RemoteViews(context.packageName, R.layout.stackwidget_item_2x3).apply {
            val jsonObject = jsonArray.getJSONObject(pos)
            val id = jsonObject.getLong("id")
            if (box == "KNOWN") {
                setViewVisibility(R.id.widget_text_top, View.GONE)
                setViewVisibility(R.id.widget_text_middle, View.GONE)
                setViewVisibility(R.id.widget_text_bottom, View.GONE)
                val passage = jsonObject.optString("passage")
                setTextViewText(R.id.widget_text, passage)
            } else {
                setViewVisibility(R.id.widget_text, View.GONE)
                val source = if (jsonObject.isNull("source")) null else jsonObject.optString("source")
                val reference = jsonObject.optString("reference")
                val topic = if (jsonObject.isNull("topic")) null else jsonObject.optString("topic")
                if (source != null) {
                    setTextViewText(R.id.widget_text_top, source)
                } else {
                    setViewVisibility(R.id.widget_text_top, View.GONE)
                }
                val referenceResId = if(topicPreferred) R.id.widget_text_bottom else R.id.widget_text_middle;
                val topicResId = if(topicPreferred) R.id.widget_text_middle else R.id.widget_text_bottom;
                setTextViewText(referenceResId, reference)
                if (topic.isNullOrEmpty()) {
                    setViewVisibility(topicResId, View.GONE)
                } else {
                    setTextViewText(topicResId, topic)
                }
            }
            setOnClickFillInIntent(R.id.stackwidget_item_layout, fillInIntent(id, box))
        }
    }

    private fun mutablePendingIntent(context: Context, uri: Uri? = null): PendingIntent {
        val intent = Intent(context, MainActivity::class.java)
        intent.data = uri
        intent.action = HomeWidgetLaunchIntent.HOME_WIDGET_LAUNCH_ACTION

        var flags = PendingIntent.FLAG_UPDATE_CURRENT
        if (Build.VERSION.SDK_INT >= 31) {
            flags = flags or PendingIntent.FLAG_MUTABLE
        }
        return PendingIntent.getActivity(context, 0, intent, flags)
    }

    private fun fillInIntent(id: Long, box: String): Intent {
        return Intent.parseUri("verses://$box:$id", 0)
    }
}