# Contributor Code of Conduct

## Our Pledge 
In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to make participation in our project and
our community a wholesome experience for everyone.

## Our Standards
We follow the general code of conduct for people who put their faith in 
Jesus Christ, laid out by Paul in his letter to the Colossians 3:12-15.

Therefore, as God’s chosen people, holy and dearly loved:
* Clothe yourselves with compassion, kindness, humility, gentleness and patience. 
* Bear with each other and forgive one another if any of you has a grievance against someone. 
* Forgive as the Lord forgave you. 
* And over all these virtues put on love, which binds them all together in perfect unity. 
* Let the peace of Christ rule in your hearts, since as members of one body you were called to peace. 
* And be thankful. 