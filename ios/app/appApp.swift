//
//  appApp.swift
//  app
//
//  Created by pes on 18.12.21.
//

import SwiftUI

@main
struct appApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
