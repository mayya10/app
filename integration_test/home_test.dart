import 'dart:convert';

import 'package:app_core/app_core.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:integration_test/integration_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/main.dart';
import 'package:remem_me/models/account.dart';
import 'package:remem_me/services/home_widget_service.dart';
import 'package:remem_me/widgets/box/box.dart';
import 'package:repository/repository.dart';

class MockClient extends Mock implements http.Client {}

class MockSettings extends Mock implements Settings {}

class MockRepository<T extends Entity> extends Mock
    implements EntitiesRepositoryHive<T> {}

class MockQueues extends Mock implements Queues {}

class MockHomeWidgetService extends Mock implements HomeWidgetService {}

void main() async {
  MockClient client = MockClient();
  MockSettings settings = MockSettings();
  MockRepository<AccountEntity> accountsRepo = MockRepository<AccountEntity>();
  MockRepository<VerseEntity> versesRepo = MockRepository<VerseEntity>();
  MockRepository<TagEntity> tagsRepo = MockRepository<TagEntity>();
  MockRepository<ScoreEntity> scoresRepo = MockRepository<ScoreEntity>();
  MockQueues queues = MockQueues();
  MockHomeWidgetService homeWidgetService = MockHomeWidgetService();

  setUpAll(() {
    final binding = IntegrationTestWidgetsFlutterBinding.ensureInitialized();
    binding.framePolicy = LiveTestWidgetsFlutterBindingFramePolicy.fullyLive;
    registerFallbackValue(Uri());
    registerFallbackValue({});
    registerFallbackValue(Object());
    registerFallbackValue(Entity);
  });

  group('Sanity test', () {
    testWidgets('Test framework should be sane', (WidgetTester tester) async {
      expect(2 + 2, lessThan(5));
    });
  });

  group('Home test', () {
    setUp(() async {
      di.registerSingleton<Settings>(settings);
      di.registerSingleton<HomeWidgetService>(homeWidgetService);
      di.registerSingleton<http.Client>(client);
      di.registerSingleton<EntitiesRepositoryHive<AccountEntity>>(
          accountsRepo);
      di.registerSingleton<EntitiesRepositoryHive<VerseEntity>>(versesRepo);
      di.registerSingleton<EntitiesRepositoryHive<TagEntity>>(tagsRepo);
      di.registerSingleton<EntitiesRepositoryHive<ScoreEntity>>(scoresRepo);
      di.registerSingleton<Queues>(queues);
      when(() => homeWidgetService.updateStackWidget(any()))
          .thenAnswer((_) async => true);
      when(() => settings.put(any(), any())).thenAnswer((_) async {});
      when(() => settings.putInt(any(), any())).thenAnswer((_) async {});
      when(() => settings.deviceId).thenReturn(1);
      when(() => settings.hasNewDbVersion).thenReturn(false);
      when(() => settings.get(Settings.ACCESS_TOKEN)).thenReturn('access');
      when(() => settings.get(Settings.REFRESH_TOKEN)).thenReturn('refresh');
      when(() => settings.get(Settings.CURRENT_ACCOUNT))
          .thenReturn(jsonEncode(Account('test', id: 1).toJson()));
      when(() => client.get(any(), headers: any(named: 'headers')))
          .thenAnswer((_) async => http.Response('[]', 200));
      when(() => accountsRepo.readList(
              accountId: any(named: 'accountId'), deleted: false))
          .thenAnswer((_) async => [AccountEntity('test-account', id: 1)]);
      when(() => versesRepo.readList(accountId: 1, deleted: false)).thenAnswer(
          (_) async => [VerseEntity('reference', 'passage', id: 1, accountId: 1)]);
      when(() => tagsRepo.readList(accountId: 1, deleted: false))
          .thenAnswer((_) async => []);
      when(() => scoresRepo.readList(accountId: 1, deleted: false))
          .thenAnswer((_) async => []);
      when(() => queues.get(any(), accountId: any(named: 'accountId')))
          .thenReturn({});
      when(() => queues.add(any(), any())).thenAnswer((_) async {});
      when(() => accountsRepo.updateList(any())).thenAnswer((_) async => []);
      when(() => versesRepo.updateList(any())).thenAnswer((_) async => []);
      when(() => tagsRepo.updateList(any())).thenAnswer((_) async => []);
      when(() => scoresRepo.updateList(any())).thenAnswer((_) async => []);
      when(() => accountsRepo.entityType).thenReturn(AccountEntity);
      when(() => versesRepo.entityType).thenReturn(VerseEntity);
      when(() => tagsRepo.entityType).thenReturn(TagEntity);
      when(() => scoresRepo.entityType).thenReturn(ScoreEntity);

      await registerServices();
    });

    tearDown(() {
      reset(client);
      reset(accountsRepo);
      reset(versesRepo);
      di.reset(dispose: false);
    });

    testWidgets('Authenticated user should see restored verses',
        (WidgetTester tester) async {
      await initApp();
      final accountsBloc = di.get<AccountsBloc>();
      final versesBloc = di.get<VersesBloc>();

      expectLater(
          accountsBloc.stream,
          emitsInOrder([
            isA<EntitiesLoadInProgress<AccountEntity>>(),
            isA<EntitiesLoadSuccess<AccountEntity>>(),
          ])).then((_) {
        verify(() => accountsRepo.readList(
            accountId: any(named: 'accountId'), deleted: false)).called(1);
      });

      expectLater(
          versesBloc.stream,
          emitsInOrder([
            isA<EntitiesLoadInProgress<VerseEntity>>(),
            isA<EntitiesLoadSuccess<VerseEntity>>(),
          ])).then((_) {
        verify(() => versesRepo.readList(accountId: 1, deleted: false)).called(1);
        expectSync((versesBloc.state as EntitiesLoadSuccess<VerseEntity>).entities.length, equals(1));
      });

      await tester.pumpAndSettle();
      expect(find.byType(SelectableVerseTile), findsOneWidget);
    });

    testWidgets('Authenticated startup should trigger successful replication',
        (WidgetTester tester) async {
      when(() => client.get(any(), headers: any(named: 'headers')))
          .thenAnswer((_) async => http.Response('[]', 200));
      when(() => client.get(
              Uri.parse('https://rpl.remem.me/v1/accounts/?deleted=false'),
              headers: any(named: 'headers')))
          .thenAnswer((_) async => http.Response(
              '[{"id":1,"modified":1,"modified_by":"1","deleted":false,'
              '"name":"test","language":"en","lang_ref":null,'
              '"review_limit":0,"daily_goal":0,"review_frequency":2.0,"inverse_limit":0,'
              '"order_new":2,"order_due":1,"order_known":3,"order_all":1,'
              '"reference_included":false,"topic_preferred":false,'
              '"imported_decks":[],"default_source":"GNTD"}]',
              200));
      await initApp();

      final bloc = di.get<ReplicationBloc>();

      expectLater(
          bloc.stream,
          emitsInOrder([
            isA<ReplicationInProgress>(),
            isA<ReplicationSuccess>(),
          ])).then((_) {
        verify(() => client.get(any(), headers: any(named: 'headers')))
            .called(4);
      });

      await tester.pumpAndSettle();
    });

  });
}