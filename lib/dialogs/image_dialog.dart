import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/models/photos/photos.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';
import 'package:url_launcher/url_launcher_string.dart';

import '../theme.dart';
import '../widgets/widgets.dart';

class ImageDialog extends StatefulWidget {
  const ImageDialog({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ImageDialogState();
  }
}

class _ImageDialogState extends State<ImageDialog> {
  final _searchController = TextEditingController();
  final _scrollController = ScrollController();
  late PhotosBloc _photosBloc;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    _photosBloc = context.read<PhotosBloc>();
    _photosBloc.stream.listen((state) {
      if (_scrollController.hasClients && state.fetchStatus == Status.none) {
        _scrollController.jumpTo(0.0);
      }
    });
  }

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<PhotosBloc, PhotosState>(builder: (context, state) {
      return Dialog(
          child: Column(children: [
        _searchBox(context),
        Expanded(child: _photosList(ctx))
      ]));
    });
  }

  Widget _searchBox(BuildContext ctx) {
    return TextField(
      controller: _searchController,
      decoration: InputDecoration(
          labelText: L10n.of(ctx).t8('Photo.unsplash'),
          contentPadding: const EdgeInsets.all(12),
          suffixIcon: IconButton(
              icon: const Icon(Icons.search_rounded),
              onPressed: () => _photosBloc.add(PhotosInitiated(_searchController.value.text)))),
      textInputAction: TextInputAction.search,
      onSubmitted: (_) => _photosBloc.add(PhotosInitiated(_searchController.value.text)),
    );
  }

  Widget _photosList(BuildContext ctx) {
    return BlocBuilder<PhotosBloc, PhotosState>(builder: (context, state) {
      switch (state.fetchStatus) {
        case Status.broken:
          return Center(child: Text(L10n.of(context).t8('Photo.fetch.error')));
        case Status.done:
          if (state.photos.isEmpty) {
            return Center(child: Text(L10n.of(context).t8('Photo.no.results')));
          }
          return GridView.builder(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 512,
            ),
            itemBuilder: (BuildContext context, int index) {
              return index >= state.photos.length
                  ? BottomLoader()
                  : _card(state.photos[index]);
            },
            itemCount: state.hasReachedMax
                ? state.photos.length
                : state.photos.length + 1,
            controller: _scrollController,
          );
        default:
          return const Center(child: CircularProgressIndicator());
      }
    });
  }

  Widget _card(Photo photo) {
    return Padding(
        padding: const EdgeInsets.all(4),
        child: Stack(fit: StackFit.expand, children: [
          GestureDetector(
            child: FittedBox(
                fit: BoxFit.cover,
                clipBehavior: Clip.antiAlias,
                alignment: Alignment.center,
                child: CachedNetworkImage(
                  errorWidget: (ctx, url, err) => const SizedBox.shrink(),
                  imageUrl: photo.small,
                )),
            onTap: () async {
              Navigator.of(context).pop(photo.regular);
              // call the selected photo's download endpoint to bump usage count
              // https://help.unsplash.com/en/articles/2511258-guideline-triggering-a-download
              final sourceUri = Uri.parse(photo.download);
              final proxyUri = await uriFromAddress(Settings.IMAGE_SERVER,
                  '/unsplash${sourceUri.path}', sourceUri.queryParameters);
              http.get(proxyUri);
            },
          ),
          Positioned(
              bottom: 0,
              right: 0,
              left: 0,
              child: InkWell(
                child: Container(
                  padding: const EdgeInsets.all(8),
                  alignment: Alignment.centerRight,
                  color: Colors.black26,
                  height: 32,
                  child: Text(L10n.of(context).t8('Photo.by', [photo.author]),
                      style: AppTheme.dark.textTheme.labelSmall),
                ),
                onTap: () async {
                  final url = photo.profile;
                  if (await canLaunchUrlString(url)) {
                    await launchUrlString(url);
                  }
                },
              ))
        ]));
  }

  void _onScroll() {
    if (_isBottom) _photosBloc.add(PhotosFetched());
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) return false;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _searchController.dispose();
    super.dispose();
  }
}
