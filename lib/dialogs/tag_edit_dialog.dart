import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/widgets/form/form.dart';
import 'package:remem_me/widgets/loading_indicator.dart';
import 'package:repository/repository.dart';

import '../blocs/current_account/current_account.dart';

class TagEditDialog extends EntityForm<TagEntity> {
  const TagEditDialog(
      {required OnSaveCallback<TagEntity> onSave, TagEntity? tag})
      : super(onSave: onSave, entity: tag);

  @override
  State<StatefulWidget> createState() {
    return _TagEditDialogState();
  }
}

/// Dialog for editing the name of a tag
class _TagEditDialogState extends EntityFormState<TagEntity, TagEditDialog> {
  String? _text;

  @override
  Widget build(BuildContext ctx) {
    return AlertDialog(
        content: Container(
            width: double.minPositive,
            child: Stack(children: [
              Form(
                key: formKey,
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  ListView(shrinkWrap: true, children: fields(ctx))
                  // don't use ListView for fields scrolling off screen
                ]),
              ),
              if (isUpdating) Positioned.fill(child: LoadingIndicator()),
            ])),
        actions: <Widget>[
          IconButton(
            tooltip: L10n.of(ctx).t8('Button.save'),
            icon: Icon(
              Icons.save_rounded,
              color: Theme.of(ctx).colorScheme.primary,
            ),
            onPressed: saveForm,
          )
        ]);
  }

  @override
  List<Widget> fields(BuildContext ctx) {
    return super.fields(ctx)
      ..addAll([
        StringInput(
          l10nKey: 'Tag.text',
          iconData: Icons.label_rounded,
          initialValue: widget.entity?.text,
          autofocus: widget.entity == null,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          validator: Validators.compose([
            Validators.error<String?>(errors['text'], _text),
            Validators.required<String?>(ctx)
          ]),
          onSaved: (value) => _text = value,
          onFieldSubmitted: (_) => saveForm(),
        ),
      ]);
  }

  @override
  TagEntity createdEntity() {
    return TagEntity(_text!, accountId: di.get<CurrentAccount>().state!.id);
  }

  @override
  TagEntity updatedEntity() {
    return widget.entity!.copyWith(text: _text);
  }
}
