import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/models/box.dart';
import 'package:repository/repository.dart';

class BoxOrderDialog extends StatelessWidget {
  const BoxOrderDialog({
    Key? key,
    required this.tab, this.prevOrder,
  }) : super(key: key);

  final Box tab;
  final VerseOrder? prevOrder;

  @override
  Widget build(BuildContext ctx) {
    return SimpleDialog(
      title: Text(L10n.of(ctx).t8('VerseOrder.title')),
      children: <Widget>[
        _option(ctx, VerseOrder.ALPHABET, 'VerseOrder.alphabet'),
        _option(ctx, VerseOrder.CANON, 'VerseOrder.canon'),
        _option(ctx, VerseOrder.TOPIC, 'VerseOrder.topic'),
        _option(ctx, VerseOrder.DATE, 'VerseOrder.date'),
        _option(ctx, VerseOrder.LEVEL, 'VerseOrder.level'),
        _option(ctx, VerseOrder.RANDOM, 'VerseOrder.random'),
      ],
    );
  }

  Widget _option(BuildContext ctx, VerseOrder value, String l10nKey) {
    return RadioListTile<VerseOrder>(
          title: Text(L10n.of(ctx).t8(l10nKey)),
          value: value,
          groupValue: this.prevOrder,
          onChanged: (VerseOrder? value) {
            Navigator.pop(ctx, value);
          },
        );
  }
}
