import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/widgets/loading_indicator.dart';

import 'flashcard.dart';

class FlashcardController extends StatefulWidget {
  final FlashcardBloc bloc;

  const FlashcardController({Key? key, required this.bloc}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _FlashcardControllerState();
  }
}

class _FlashcardControllerState extends State<FlashcardController> {
  final _animatorKey = GlobalKey<FlashcardAnimatorState>();
  var hasInAnimation = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => hasInAnimation = true);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<FlashcardBloc, FlashcardState>(
      bloc: widget.bloc,
      listener: (ctx, state) {
        if (state is FlashcardEnd) {
          Navigator.pop(ctx);
        }
      },
      builder: (ctx, state) {
        if (state is FlashcardRun) {
          if (!state.visible) {
            _animatorKey.currentState?.reset();
            widget.bloc.add(FlipReset());
            return const SizedBox.shrink();
          }
          return BlocProvider<VoiceBloc>(
              create: (_) => VoiceBloc(widget.bloc),
              child: ClipRect(
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Center(
                    child: Column(
                      verticalDirection: VerticalDirection.up,
                      children: <Widget>[
                        Container(
                            margin: const EdgeInsets.fromLTRB(12.0, 0.0, 12.0, 0.0),
                            height: 56.0,
                            child: _buildToolbar(state)),
                        Expanded(
                            child: Container(
                                margin: const EdgeInsets.fromLTRB(0.0, 4.0, 0.0, 0.0),
                                child: FlashcardAnimator(
                                  key: _animatorKey,
                                  bloc: widget.bloc,
                                  hasInAnimation: hasInAnimation
                                ))),
                      ],
                    ),
                  ),
                ),
              ));
        } else {
          return LoadingIndicator();
        }
      },
    );
  }

  Widget _buildToolbar(FlashcardRun state) {
    if (state.flipping) return const SizedBox.shrink();
    if (state is MultiFlashcardRun && !state.reverse) {
      return FlipToolbar(
        animatorKey: _animatorKey,
        bloc: widget.bloc,
      );
    }
    switch (state.box) {
      case Box.NEW:
        return CommitToolbar(animatorKey: _animatorKey, bloc: widget.bloc);
      case Box.DUE:
        return ReviewToolbar(
            animatorKey: _animatorKey, bloc: widget.bloc, run: state);
      case Box.KNOWN:
      default:
        return state is MultiFlashcardRun
            ? ReviewToolbar(
                animatorKey: _animatorKey, bloc: widget.bloc, run: state)
            : FlipToolbar(animatorKey: _animatorKey, bloc: widget.bloc);
    }
  }
}
