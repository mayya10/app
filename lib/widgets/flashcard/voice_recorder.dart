import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:just_audio/just_audio.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/status.dart';

import '../../theme.dart';

class AnimatedIconButton extends AnimatedWidget {
  final void Function() onPressed;

  const AnimatedIconButton({
    super.key,
    required AnimationController controller,
    required this.onPressed,
  }) : super(listenable: controller);

  static final _opacityTween = Tween<double>(begin: 0.6, end: 1);

  Animation<double> get _progress => listenable as Animation<double>;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Ink(
        decoration: ShapeDecoration(
          color: AppTheme.dark.colorScheme.background.withOpacity(0.1),
          shape: const CircleBorder()
        ),
        child: IconButton(
          iconSize: 32,
          color: Colors.grey,
            icon: Icon(
              Icons.mic,
              color: AppTheme.dark.colorScheme.onPrimary
                  .withOpacity(_opacityTween.evaluate(_progress)),
            ),
            onPressed: onPressed),
      ),
    );
  }
}

class VoiceRecorder extends StatefulWidget {
  const VoiceRecorder({super.key});

  @override
  State<StatefulWidget> createState() {
    return VoiceRecorderState();
  }
}

class VoiceRecorderState extends State<VoiceRecorder>
    with TickerProviderStateMixin {
  late final AnimationController _controller = AnimationController(
    duration: const Duration(milliseconds: 500),
    vsync: this,
  );
  final AudioPlayer _player = AudioPlayer();
  late final _animation =
      CurvedAnimation(parent: _controller, curve: Curves.easeIn);

  @override
  void initState() {
    super.initState();
    _player.setVolume(0.5);
    _animation.addStatusListener((status) {
      if (BlocProvider.of<VoiceBloc>(context).state.record == Status.active) {
        if (status == AnimationStatus.completed) {
          _controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          _controller.forward();
        }
      }
    });
  }

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<VoiceBloc, VoiceState>(
        builder: (BuildContext ctx, VoiceState state) {
      return [Status.none, Status.ready, Status.active].contains(state.record)
          ? AnimatedIconButton(
              controller: _controller,
              onPressed: () => _onPressed(state.record),
            )
          : SizedBox.shrink();
    });
  }

  void _onPressed(Status status) async {
    if ([Status.none, Status.ready].contains(status)) {
      _controller.forward();
      await _player.setAsset('assets/audio/start_chime.mp3');
      _player.playerStateStream
          .firstWhere(
              (state) => state.processingState == ProcessingState.completed)
          .then(
              (_) => BlocProvider.of<VoiceBloc>(context).add(RecordStarted()));
      await _player.play();
    } else {
      BlocProvider.of<VoiceBloc>(context).add(RecordStopped());
      await _player.setAsset('assets/audio/stop_chime.mp3');
      await _player.play();
      _controller.reset();
    }
  }

  @override
  void dispose() {
    _player.dispose();
    _controller.dispose();
    super.dispose();
  }
}
