import 'package:blur/blur.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/blocs/blocs.dart';

abstract class FlashcardLayout extends StatelessWidget {
  const FlashcardLayout({
    Key? key,
    required this.run,
    this.onTap,
    this.onLongPress,
  }) : super(key: key);

  final FlashcardRun run;
  final Function? onTap;
  final void Function()? onLongPress;

  Alignment get gradientBegin;

  Alignment get gradientEnd;

  Widget content(BuildContext ctx);

  @override
  Widget build(BuildContext ctx) {
    final layers = backgroundLayers();
    layers.add(Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: Colors.white.withOpacity(0.1),
        highlightColor: Colors.transparent,
        borderRadius: BorderRadius.circular(8),
        onTap: onTap as void Function()?,
        onLongPress: onLongPress,
        child: content(ctx),
      ),
    ));
    return Container(
      color: Colors.transparent,
      width: double.infinity,
      constraints: const BoxConstraints(maxWidth: 700),
      child: Card(
        elevation: 4,
        clipBehavior: Clip.hardEdge,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
        child: Stack(children: layers),
      ),
    );
  }

  List<Widget> backgroundLayers();

  Widget levelBackground() {
    // todo: get default color from account setting
    Color defaultColor = const Color(0xffcc0033);
    return Stack(children: [
      Container(
          color: run.verse.color ?? defaultColor, margin: const EdgeInsets.all(1.0)),
      lightGradientLayer()
    ]);
  }

  Widget blurredImageBackground() {
    return Blur(
        blur: 2.5,
        blurColor: Colors.black,
        child: imageBackground(),
      );
  }

  Widget imageBackground() {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            fit: BoxFit.cover,
            image: CachedNetworkImageProvider(
              run.verse.image!,
            )),
      ),
    );
  }

  Widget defaultBackground() {
    return levelBackground();
  }

  Widget darkGradientLayer() {
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
      colors: const [Color(0x66331100), Color(0x55001122), Color(0x77000022)],
      begin: gradientBegin,
      end: gradientEnd,
    )));
  }

  Widget lightGradientLayer() {
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
      colors: const [Color(0x55330000), Color(0x33000011), Color(0x55000011)],
      begin: gradientBegin,
      end: gradientEnd,
    )));
  }

  List<Shadow> textShadows() {
    return run.verse.image != null
        ? [
            const Shadow(
              blurRadius: 4.0,
              color: Colors.black,
              offset: Offset(1.0, 2.0),
            ),
          ]
        : [];
  }
}
