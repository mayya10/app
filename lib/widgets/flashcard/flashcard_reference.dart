import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/blocs/current_account/current_account.dart';
import 'package:remem_me/models/flashcard_hint.dart';
import 'package:remem_me/services/text_service.dart';

import '../../services/verse_service.dart';
import '../../theme.dart';
import 'flashcard.dart';

class FlashcardReference extends FlashcardLayout {
  final GlobalKey<VoiceRecorderState>? voiceRecorderKey;

  const FlashcardReference({
    Key? key,
    this.voiceRecorderKey,
    required run,
    onTap,
    onLongPress,
  }) : super(key: key, run: run, onTap: onTap, onLongPress: onLongPress);

  @override
  final gradientBegin = Alignment.topLeft;
  @override
  final gradientEnd = Alignment.bottomRight;

  @override
  Widget content(BuildContext ctx) {
    final account = di.get<CurrentAccount>();
    final textDirection = account.langRefDirection;
    final withSubtitle =
        run.hinting == null || run.hinting!.contains(FlashcardHint.subtitle);
    final withTags =
        run.hinting == null || run.hinting!.contains(FlashcardHint.tags);
    final withBeginning =
        run.hinting != null && run.hinting!.contains(FlashcardHint.beginning);
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Stack(alignment: AlignmentDirectional.topCenter, children: [
          Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                if (run.verse.source != null) ...[
                  Text(run.verse.source!,
                      style: AppTheme.dark.textTheme.titleMedium!.copyWith(
                          color: AppTheme.dark.textTheme.titleMedium!.color!
                              .withOpacity(0.76),
                          fontSize: 20.0,
                          fontWeight: FontWeight.w500,
                          shadows: textShadows())),
                  SizedBox.fromSize(size: const Size.fromHeight(8.0))
                ],
                Text(VerseService().title(run.verse),
                    textDirection: textDirection,
                    style: AppTheme.dark.textTheme.titleLarge!.copyWith(
                        fontSize: 24.0,
                        fontWeight: FontWeight.w600,
                        shadows: textShadows())),
                SizedBox.fromSize(size: const Size.fromHeight(8.0)),
                Text(VerseService().subtitle(run.verse),
                    textDirection: textDirection,
                    style: AppTheme.dark.textTheme.titleMedium!.copyWith(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w600,
                        color: withSubtitle ? null : Colors.transparent,
                        shadows: withSubtitle ? textShadows() : null)),
                SizedBox.fromSize(size: const Size.fromHeight(16.0)),
                Wrap(
                    runSpacing: 8.0,
                    alignment: WrapAlignment.center,
                    children: run.verse.tags.values
                        .map((tag) => Container(
                            decoration: BoxDecoration(
                              color: withTags
                                  ? AppTheme.dark.textTheme.titleMedium!.color!
                                      .withOpacity(0.64)
                                  : null,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(4.0)),
                            ),
                            padding: const EdgeInsets.symmetric(
                                vertical: 2.0, horizontal: 6.0),
                            margin: const EdgeInsets.only(left: 6.0),
                            child: Text(tag ?? '',
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                                style: AppTheme.light.textTheme.bodyMedium!
                                    .copyWith(
                                        fontSize: 16,
                                        color: withTags
                                            ? null
                                            : Colors.transparent))))
                        .toList()),
                Text(TextService().beginning(run.verse.passage),
                    textDirection: account.languageDirection,
                    style: AppTheme.dark.textTheme.titleSmall!.copyWith(
                        fontFamily: account.font,
                        color: withBeginning ? null : Colors.transparent,
                        shadows: withBeginning ? textShadows() : null)),
              ]),
          if (voiceRecorderKey != null)
            Positioned(bottom: 0, child: VoiceRecorder(key: voiceRecorderKey))
        ]));
  }

  @override
  List<Widget> backgroundLayers() {
    final layers = <Widget>[];
    layers.add(levelBackground());
    if (run.verse.image != null) {
      layers.add(imageBackground());
    }
    return layers;
  }
}
