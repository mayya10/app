import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:remem_me/widgets/settings/flashcard_setting.dart';

import '../button/button.dart';
import '../widgets.dart';

class ReviewToolbar extends StatelessWidget {
  final GlobalKey<FlashcardAnimatorState> animatorKey;
  final FlashcardRun run;
  final FlashcardBloc bloc;
  final bool isAllRevealed;

  ReviewToolbar(
      {super.key,
      required this.animatorKey,
      required this.run,
      required this.bloc}) : isAllRevealed = _isAllRevealed(run);

  static bool _isAllRevealed(FlashcardRun run) {
    return run.revealLines == 0 ||
        run.revealLines == VerseService().lines(run.verse).length;
  }

  @override
  Widget build(BuildContext context) {
    return Focus(
      autofocus: true,
      onKeyEvent: (node, event) {
        if (event is KeyDownEvent) {
          if (event.logicalKey == LogicalKeyboardKey.arrowRight) {
            _slipRight();
            return KeyEventResult.handled;
          } else if (event.logicalKey == LogicalKeyboardKey.arrowLeft) {
            _slipLeft();
            return KeyEventResult.handled;
          } else if (event.logicalKey == LogicalKeyboardKey.period &&
              !isAllRevealed) {
            bloc.add(const LineRevealed());
            return KeyEventResult.handled;
          } else if (run is MultiFlashcardRun &&
              [LogicalKeyboardKey.enter, LogicalKeyboardKey.numpadEnter]
                  .contains(event.logicalKey)) {
            animatorKey.currentState!.flip();
            return KeyEventResult.handled;
          }
        }
        return KeyEventResult.ignored;
      },
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[_forgottenButton(context)],
        ),
        const HelpButton('study/'),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[_rememberedButton(context)],
        ),
      ]),
    );
  }

  Widget _rememberedButton(BuildContext ctx) {
    return isAllRevealed
        ? OutlinedIconButton(
            tooltip:
                '${L10n.of(ctx).t8('Flashcard.remembered')} ${kIsWeb ? '➡️' : '⇨'}',
            color: Theme.of(ctx).colorScheme.primaryContainer,
            icon: Icons.check_rounded,
            onPressed: () => _slipRight(),
          )
        : OutlinedIconButton(
            tooltip:
                '${L10n.of(ctx).t8('Flashcard.nextLine')} ${kIsWeb ? '⏺️' : '•'}',
            color: Theme.of(ctx).colorScheme.primaryContainer,
            icon: Icons.spellcheck_rounded,
            onPressed: () => bloc.add(const LineRevealed()),
          );
  }

  Widget _forgottenButton(BuildContext ctx) {
    return OutlinedIconButton(
      tooltip:
          '${L10n.of(ctx).t8('Flashcard.forgotten')} ${kIsWeb ? '⬅️' : '⇦'}',
      color: Theme.of(ctx).colorScheme.secondaryContainer,
      icon: Icons.clear_rounded,
      onPressed: () => _slipLeft(),
    );
  }

  void _slipRight() {
    return animatorKey.currentState!
        .slipRight((_) => bloc.add(FlashcardAction(VerseAction.REMEMBERED)));
  }

  void _slipLeft() {
    return animatorKey.currentState!
        .slipLeft((_) => bloc.add(FlashcardAction(VerseAction.FORGOTTEN)));
  }
}

class CommitToolbar extends StatelessWidget {
  final GlobalKey<FlashcardAnimatorState> animatorKey;
  final FlashcardBloc bloc;

  const CommitToolbar({Key? key, required this.animatorKey, required this.bloc})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      OutlinedIconButton(
          tooltip: L10n.of(ctx).t8('Study.title'),
          icon: Icons.school_rounded,
          onPressed: () => bloc.add(FlashcardStudied())),
      OutlinedIconButton(
          tooltip: L10n.of(ctx).t8('Flashcard.commit'),
          color: Theme.of(ctx).colorScheme.primary,
          icon: Icons.launch_rounded,
          onPressed: () {
            animatorKey.currentState!.slipRight(
                (_) => bloc.add(FlashcardAction(VerseAction.COMMITTED)));
          }),
    ]);
  }
}

class FlipToolbar extends StatelessWidget {
  final GlobalKey<FlashcardAnimatorState> animatorKey;
  final FlashcardBloc bloc;

  const FlipToolbar({required this.animatorKey, required this.bloc});

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<FlashcardBloc, FlashcardState>(
        bloc: bloc,
        builder: (BuildContext ctx, FlashcardState state) {
          final hintsEnabled =
              state is FlashcardRun && !state.inverse && !state.reverse;
          final linesEnabled = state is MultiFlashcardRun &&
              [Box.DUE, Box.KNOWN].contains(state.box) &&
              !state.inverse;
          return Focus(
            autofocus: true,
            onKeyEvent: (node, event) {
              return _onKeyEvent(event, linesEnabled, hintsEnabled);
            },
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        if (hintsEnabled)
                          OutlinedIconButton(
                              tooltip:
                                  '${L10n.of(ctx).t8('Flashcard.hint')} ${kIsWeb ? '⏏️' : '␣'}',
                              onPressed: () => bloc.add(FlashcardHinted()),
                              icon: Icons.tips_and_updates_rounded)
                        else
                          SizedBox.fromSize(size: const Size.fromWidth(48.0))
                      ]),
                  CardCounter(state),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        linesEnabled
                            ? OutlinedIconButton(
                                tooltip:
                                    '${L10n.of(ctx).t8('Flashcard.lineUp')} ${kIsWeb ? '⏺️' : '•'}',
                                icon: Icons.rule_rounded,
                                color: Theme.of(ctx).colorScheme.primary,
                                onPressed: () => _revealLines(),
                              )
                            : OutlinedIconButton(
                                tooltip:
                                    '${L10n.of(ctx).t8('Flashcard.flip')} ${kIsWeb ? '↩️' : '↩'}',
                                icon: Icons.flip_rounded,
                                onPressed: () => _flip(),
                              )
                      ]),
                ]),
          );
        });
  }

  KeyEventResult _onKeyEvent(
      KeyEvent event, bool linesEnabled, bool hintsEnabled) {
    if (event is KeyDownEvent) {
      if (event.logicalKey == LogicalKeyboardKey.period && linesEnabled) {
        _revealLines();
        return KeyEventResult.handled;
      } else if (event.logicalKey == LogicalKeyboardKey.arrowUp) {
        _slipUp();
        return KeyEventResult.handled;
      } else if (event.logicalKey == LogicalKeyboardKey.arrowDown) {
        _slipDown();
        return KeyEventResult.handled;
      } else if (event.logicalKey == LogicalKeyboardKey.space && hintsEnabled) {
        _showHint();
        return KeyEventResult.handled;
      } else if ([LogicalKeyboardKey.enter, LogicalKeyboardKey.numpadEnter]
          .contains(event.logicalKey)) {
        _flip();
        return KeyEventResult.handled;
      }
    }
    return KeyEventResult.ignored;
  }

  void _revealLines() {
    bloc.add(const LineRevealed(lines: 1));
    _flip();
  }

  void _flip() {
    animatorKey.currentState!.flip();
  }

  void _showHint() {
    bloc.add(FlashcardHinted());
  }

  void _slipUp() {
    return animatorKey.currentState!
        .slipUp((_) => bloc.add(FlashcardAction(VerseAction.RETRACTED)));
  }

  void _slipDown() {
    return animatorKey.currentState!
        .slipDown((_) => bloc.add(FlashcardAction(VerseAction.SKIPPED)));
  }
}

class CardCounter extends StatelessWidget {
  final FlashcardState state;

  const CardCounter(
    this.state, {
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final state = this.state;
    return Text(state is MultiFlashcardRun
        ? '${state.index + 1}/${state.verses.length}'
        : '');
  }
}
