import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/voice/voice_bloc.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/widgets/animation/animation.dart';

import '../widgets.dart';

enum Slip { downIn, upOut, downOut, leftOut, rightOut }

class FlashcardAnimator extends StatefulWidget {
  final FlashcardBloc bloc;
  final bool hasInAnimation;

  const FlashcardAnimator({Key? key, required this.bloc, this.hasInAnimation = true}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return FlashcardAnimatorState();
  }
}

class FlashcardAnimatorState extends State<FlashcardAnimator>
    with TickerProviderStateMixin {
  final _voiceRecorderKey = GlobalKey<VoiceRecorderState>();
  late AnimationController _flipController;
  late AnimationController _slipController;
  Slip _slip = Slip.downIn;

  @override
  void initState() {
    super.initState();
    _flipController =
        AnimationController(vsync: this, duration: const Duration(milliseconds: 400));
    _slipController =
        AnimationController(vsync: this, duration: const Duration(milliseconds: 400));
    if(widget.hasInAnimation) _slipController..value = 1.0..reverse();
  }

  @override
  void dispose() {
    _flipController.dispose();
    _slipController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _slipController,
        child: _buildCard(),
        builder: (BuildContext context, Widget? child) {
          return _slipper(child!);
        });
  }

  Widget _buildCard() {
    return BlocBuilder<FlashcardBloc, FlashcardState>(
        bloc: widget.bloc,
        builder: (ctx, state) {
          final run = state as FlashcardRun;
          if (run is MultiFlashcardRun) {
            return Flipper(
                animationController: _flipController,
                front: SkippedDismisser(
                    child: _buildFront(ctx, run, onTap: () => flip())),
                back: run.box == Box.NEW
                    ? CommittedDismisser(
                        child: _buildBack(ctx, run, onTap: () => flip()))
                    : ReviewedDismisser(
                        child: _buildBack(ctx, run, onTap: () => flip()),
                      ),
                onTap: () => flip());
          } else {
            switch (run.box) {
              case Box.NEW:
                return CommittedDismisser(child: _buildBack(ctx, run));
              case Box.DUE:
                return ReviewedDismisser(child: _buildBack(ctx, run));
              default:
                return Flipper(
                    animationController: _flipController,
                    front: _buildFront(ctx, run, onTap: () => flip()),
                    back: _buildBack(ctx, run, onTap: () => flip()));
            }
          }
        });
  }

  Widget _buildFront(BuildContext ctx, FlashcardRun state,
      {void Function()? onTap}) {
    return state.inverse
        ? FlashcardPassage(run: state, onTap: onTap)
        : FlashcardReference(
            run: state,
            onTap: onTap,
            onLongPress: hint,
            voiceRecorderKey: _voiceRecorderKey,
          );
  }

  Widget _buildBack(BuildContext ctx, FlashcardRun state,
      {void Function()? onTap}) {
    return state.inverse
        ? FlashcardReference(run: state, onTap: onTap)
        : FlashcardPassage(run: state, onTap: onTap);
  }

  void flip() {
    if (_flipController.isAnimating) return;
    // ignore: close_sinks
    if ((widget.bloc.state as FlashcardRun).reverse) {
      _flipController.reverse().then((_) {
        widget.bloc.add(const LineRevealed(lines: 0));
        widget.bloc.add(FlipEnded());
      });
    } else {
      _flipController.forward().then((_) {
        widget.bloc.add(FlipEnded());
      });
    }
    widget.bloc.add(FlipStarted());
    final voiceBloc = BlocProvider.of<VoiceBloc>(context);
    if (voiceBloc.state.record == Status.active) voiceBloc.add(RecordStopped());
    if (voiceBloc.state.play == Status.active) voiceBloc.add(PlayStopped());
  }

  void hint() {
    widget.bloc.add(FlashcardHinted());
  }

  void reset() {
    _flipController.reset();
  }

  void slipLeft(FutureOr Function(void) onValue) {
    _slip = Slip.leftOut;
    _slipController.forward().then(onValue);
  }

  void slipRight(FutureOr Function(void) onValue) {
    _slip = Slip.rightOut;
    _slipController.forward().then(onValue);
  }

  void slipUp(FutureOr Function(void) onValue) {
    _slip = Slip.upOut;
    _slipController.forward().then(onValue);
  }

  void slipDown(FutureOr Function(void) onValue) {
    _slip = Slip.downOut;
    _slipController.forward().then(onValue);
  }

  Animation<double> get _slipProgress {
    switch (_slip) {
      case Slip.downIn:
        return Tween(
          begin: 0.0,
          end: 1.0,
        ).animate(
            CurvedAnimation(parent: _slipController, curve: Curves.easeInCirc));
      case Slip.upOut:
      case Slip.downOut:
      case Slip.leftOut:
      case Slip.rightOut:
        return Tween(
          begin: 0.0,
          end: 1.0,
        ).animate(
            CurvedAnimation(parent: _slipController, curve: Curves.easeOut));
    }
  }

  Widget _slipper(Widget child) {
    switch (_slip) {
      case Slip.downIn:
        return Opacity(
            opacity: 1.0 - _slipProgress.value * 0.8,
            child: Transform.translate(
              offset: Offset(0.0, -_slipProgress.value * 16.0),
              child: child,
            ));
      case Slip.upOut:
        return Opacity(
            opacity: 1.0 - _slipProgress.value,
            child: Transform.translate(
              offset: Offset(0.0, -_slipProgress.value * 400.0),
              child: child,
            ));
      case Slip.downOut:
        return Opacity(
            opacity: 1.0 - _slipProgress.value,
            child: Transform.translate(
              offset: Offset(0.0, _slipProgress.value * 400.0),
              child: child,
            ));
      case Slip.leftOut:
        return Opacity(
            opacity: 1.0 - _slipProgress.value,
            child: Transform.translate(
              offset: Offset(-_slipProgress.value * 400.0, 0.0),
              child: child,
            ));
      case Slip.rightOut:
        return Opacity(
            opacity: 1.0 - _slipProgress.value,
            child: Transform.translate(
              offset: Offset(_slipProgress.value * 400.0, 0.0),
              child: child,
            ));
    }
  }
}
