import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';

class CommittedDismisser extends StatelessWidget {
  final Widget child;

  const CommittedDismisser({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.startToEnd,
      background: Container(
          alignment: Alignment.centerLeft,
          child: Icon(
            Icons.launch_rounded,
            size: 64.0,
            color: Theme.of(ctx).colorScheme.primary,
          )),
      onDismissed: (direction) {
        if (direction == DismissDirection.startToEnd)
          BlocProvider.of<FlashcardBloc>(ctx)
              .add(FlashcardAction(VerseAction.COMMITTED));
      },
      child: child,
    );
  }
}

class ReviewedDismisser extends StatelessWidget {
  final Widget child;

  const ReviewedDismisser({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.horizontal,
      background: _backgroundRemembered(ctx),
      secondaryBackground: _backgroundForgotten(ctx),
      onDismissed: (direction) => BlocProvider.of<FlashcardBloc>(ctx).add(
          FlashcardAction(direction == DismissDirection.startToEnd
              ? VerseAction.REMEMBERED
              : VerseAction.FORGOTTEN)),
      child: child,
    );
  }

  Container _backgroundRemembered(BuildContext ctx) {
    return Container(
        alignment: Alignment.centerLeft,
        child: Icon(
          Icons.done,
          size: 64.0,
          color: Theme.of(ctx).colorScheme.primaryContainer,
        ));
  }

  Container _backgroundForgotten(BuildContext ctx) {
    return Container(
        alignment: Alignment.centerRight,
        child: Icon(
          Icons.clear_rounded,
          size: 64.0,
          color: Theme.of(ctx).colorScheme.secondaryContainer,
        ));
  }
}

class SkippedDismisser extends StatelessWidget {
  final Widget child;

  const SkippedDismisser({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.vertical,
      background: Container(
          alignment: Alignment.topCenter,
          child: const Icon(Icons.keyboard_double_arrow_down_rounded, size: 64.0)),
      secondaryBackground: Container(
          alignment: Alignment.bottomCenter,
          child: const Icon(Icons.keyboard_double_arrow_up_rounded, size: 64.0)),
      onDismissed: (dismissDirection) => BlocProvider.of<FlashcardBloc>(ctx)
          .add(FlashcardAction(dismissDirection == DismissDirection.down
              ? VerseAction.SKIPPED
              : VerseAction.RETRACTED)),
      child: child,
    );
  }
}
