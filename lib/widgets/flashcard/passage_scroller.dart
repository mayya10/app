import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/services/verse_service.dart';

import '../../theme.dart';

class PassageScroller extends StatefulWidget {
  final FlashcardRun run;

  const PassageScroller({Key? key, required this.run}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PassageScrollerState();
  }
}

class _PassageScrollerState extends State<PassageScroller> {
  final ScrollController _scrollController =
      ScrollController(initialScrollOffset: 0);
  var lines;

  @override
  void initState() {
    super.initState();
    final lines =
        VerseService().lines(widget.run.verse, referenceIncluded: false);
    if ((widget.run.revealLines > 0 &&
        widget.run.revealLines <= lines.length)) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 500),
          curve: Curves.easeOut,
        );
      });
    }
  }

  @override
  Widget build(BuildContext ctx) {
    final account = di.get<CurrentAccount>();
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewport) =>
            RawScrollbar(
                thumbColor: AppTheme.dark.focusColor,
                thickness: 8,
                radius: Radius.circular(8),
                mainAxisMargin: 8,
                controller: _scrollController,
                isAlwaysShown: true,
                child: ScrollConfiguration(
                    behavior: ScrollConfiguration.of(context)
                        .copyWith(scrollbars: false),
                    child: SingleChildScrollView(
                        key: const PageStorageKey<String>('passage_scroller'),
                        controller: _scrollController,
                        child: Container(
                            padding: EdgeInsets.all(16.0),
                            constraints: BoxConstraints(
                              minHeight: viewport.maxHeight,
                              minWidth: double.infinity,
                            ),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    _text(widget.run),
                                    textDirection: account.languageDirection,
                                    style: AppTheme.dark.textTheme.titleSmall!
                                        .copyWith(
                                            height: 1.5,
                                            fontFamily: account.font),
                                  )
                                ]))))));
  }

  String _text(FlashcardRun run) {
    final lines =
        VerseService().lines(run.verse, referenceIncluded: !run.inverse);
    if (run.revealLines == 0 || run.revealLines == lines.length) {
      return lines.join().replaceAll('\t', ' ');
    } else {
      return '${lines.getRange(0, run.revealLines).join()}...';
    }
  }
}
