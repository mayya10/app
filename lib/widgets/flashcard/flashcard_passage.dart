import 'package:flutter/material.dart';

import 'flashcard.dart';

class FlashcardPassage extends FlashcardLayout {
  @override
  final gradientBegin = Alignment.topRight;
  @override
  final gradientEnd = Alignment.bottomLeft;

  const FlashcardPassage({
    Key? key,
    required run,
    onTap,
  }) : super(key: key, run: run, onTap: onTap);


  @override
  Widget content(BuildContext ctx) => PassageScroller(run: run);


  @override
  List<Widget> backgroundLayers() {
    final layers = <Widget>[];
      layers.add(levelBackground());
    if (run.verse.image != null) {
      layers.add(blurredImageBackground());
    }
    return layers;
  }
}



