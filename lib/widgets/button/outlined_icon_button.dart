import 'package:flutter/material.dart';

class OutlinedIconButton extends StatelessWidget {
  final Color? color;
  final String tooltip;
  final IconData icon;
  final VoidCallback? onPressed;

  const OutlinedIconButton({
    Key? key,
    required this.tooltip,
    required this.icon,
    required this.onPressed,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Tooltip(
        message: tooltip,
        child: Material(
            type: MaterialType.transparency,
            child: Ink(
                decoration: BoxDecoration(
                    border: Border.all(
                        color: color ?? Theme.of(context).iconTheme.color!,
                        width: 1.5),
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(50.0)), //<-- SEE HERE
                child: InkWell(
                    borderRadius: BorderRadius.circular(100.0),
                    splashColor: color?.withOpacity(0.2),
                    highlightColor: color?.withOpacity(0.2),
                    onTap: onPressed,
                    child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Icon(icon,
                            color: color ??
                                Theme.of(context).iconTheme.color!))))));
  }
}