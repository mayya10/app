import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/routers/app_route_path.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';

import '../../blocs/text_to_speech/tts_handler.dart';
import '../../utils/utils.dart';

class BoxFabRow extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<TabBloc, TabState>(builder: (context, state) {
      var box = state.box;
      if (!state.isBoxed) {
        box = Box.ALL;
      }
      var buttons = <Widget>[];
      final secondary = Theme.of(ctx).colorScheme.secondary;
      switch (box) {
        case Box.DUE:
          buttons = [
            const SizedBox(width: 48),
            BoxFabTts(),
            const SizedBox(width: 8),
            BoxFabReview(backgroundColor: secondary),
            const SizedBox(width: 8),
            const HelpButton('study/'),
          ];
          break;
        case Box.KNOWN:
          buttons = [
            const SizedBox(width: 48),
            BoxFabTts(),
            const SizedBox(width: 8),
            BoxFabInverse(backgroundColor: secondary),
            const SizedBox(width: 8),
            const HelpButton('study/'),
          ];
          break;
        case Box.NEW:
          buttons = [
            const SizedBox(width: 48),
            BoxFabTts(),
            const SizedBox(width: 8),
            BoxFabAdd(backgroundColor: secondary),
            const SizedBox(width: 8),
            const HelpButton('verses/'),
          ];
          break;
        case Box.ALL:
          buttons = [
            BoxFabTts(),
            const SizedBox(width: 8),
            BoxFabAdd(),
            const SizedBox(width: 8),
            BoxFabInverse()
          ];
          break;
      }
      return Row(mainAxisSize: MainAxisSize.min, children: buttons);
    });
  }
}

/// FloatingActionButton that is aware of which box it is used on
abstract class BoxFab extends StatelessWidget {
  final Color? backgroundColor;

  const BoxFab({super.key, this.backgroundColor});

  @override
  Widget build(BuildContext ctx) => BlocBuilder<TabBloc, TabState>(
          builder: (BuildContext ctx, TabState state) {
        return buildWithBloc(ctx, OrderedVersesBloc.from(state));
      });

  Widget buildWithBloc(BuildContext ctx, OrderedVersesBloc bloc);
}

class BoxFabAdd extends BoxFab {
  BoxFabAdd({super.backgroundColor});

  @override
  Widget buildWithBloc(BuildContext ctx, OrderedVersesBloc bloc) {
    return FloatingActionButton(
      heroTag: 'addBtn',
      onPressed: () {
        BlocProvider.of<NavigationCubit>(ctx)
            .go(HomePath(activity: HomeActivity.add));
      },
      backgroundColor: backgroundColor,
      tooltip: L10n.of(ctx).t8('Verse.add'),
      child: const Icon(
        Icons.add_rounded,
        color: Colors.white,
      ),
    );
  }
}

class BoxFabReview extends BoxFab {
  BoxFabReview({super.backgroundColor});

  @override
  Widget buildWithBloc(BuildContext ctx, OrderedVersesBloc bloc) {
    return FloatingActionButton(
      heroTag: 'reviewBtn',
      onPressed: () {
        final verses = [...(bloc.state as OrderSuccess).verses];
        di
            .get<NavigationCubit>()
            .go(HomePath(id: verses.first.id, reviewMode: ReviewMode.review));
      },
      backgroundColor: backgroundColor,
      child: const SvgAsset('cards_outline', height: 20, color: Colors.white),
    );
  }
}

class BoxFabInverse extends BoxFab {
  BoxFabInverse({super.backgroundColor});

  @override
  Widget buildWithBloc(BuildContext ctx, OrderedVersesBloc bloc) {
    return FloatingActionButton(
      heroTag: 'inverseBtn',
      onPressed: () {
        final state = bloc.state as OrderSuccess;
        final verses = [...state.verses];
        if(state.order == VerseOrder.RANDOM) verses.shuffle();
        BlocProvider.of<NavigationCubit>(ctx).go(HomePath(
            id: verses.first.id,
            reviewMode: state.order == VerseOrder.RANDOM
                ? ReviewMode.random
                : ReviewMode.inverse));
      },
      backgroundColor: backgroundColor,
      child: const SvgAsset('cards_filled', height: 20, color: Colors.white),
    );
  }
}

class BoxFabTts extends BoxFab {
  BoxFabTts({super.backgroundColor});

  @override
  Widget buildWithBloc(BuildContext ctx, OrderedVersesBloc bloc) {
    return FloatingActionButton(
      heroTag: 'ttsBtn',
      onPressed: () {
        TtsHandler()
          ..setUp([...(bloc.state as OrderSuccess).verses], box: bloc.box)
          ..play();
      },
      backgroundColor: backgroundColor,
      child: const Icon(
        Icons.play_arrow_rounded,
        color: Colors.white,
      ),
    );
  }
}
