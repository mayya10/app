import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';

import '../../utils/build_context.dart';

class HelpButton extends StatelessWidget {
  final String path;

  const HelpButton([this.path = '', Key? key]) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return IconButton(
      tooltip: L10n.of(ctx).t8('Documentation.title'),
      onPressed: launchDocumentation(ctx, path: path),
      icon: const Icon(Icons.help_outline_rounded),
    );
  }
}
