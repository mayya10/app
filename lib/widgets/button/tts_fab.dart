import 'package:audio_service/audio_service.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/blocs/text_to_speech/tts_handler.dart';
import 'package:remem_me/widgets/settings/speech_setting.dart';

import '../widgets.dart';

class TtsFabRow extends StatelessWidget {
  const TtsFabRow({super.key});

  Widget build(BuildContext ctx) {
    return StreamBuilder<PlaybackState>(
        stream: TtsHandler().playbackState,
        builder: (ctx, snapshot) {
          var buttons = snapshot.data?.playing ?? false
              ? <Widget>[
                  const SpeechSetting(),
                  const TtsFabSkipPrevious(),
                  const SizedBox(width: 8),
                  const TtsFabStop(),
                  const SizedBox(width: 8),
                  const TtsFabPause(),
                  const SizedBox(width: 8),
                  const TtsFabSkipNext(),
                  const HelpButton('audio/'),
                ]
              : <Widget>[
                  const SpeechSetting(),
                  const TtsFabSkipPrevious(),
                  const SizedBox(width: 8),
                  const TtsFabStop(),
                  const SizedBox(width: 8),
                  const TtsFabResume(),
                  const SizedBox(width: 8),
                  const TtsFabSkipNext(),
                  const HelpButton('audio/')
                ];
          return Row(mainAxisSize: MainAxisSize.min, children: buttons);
        });
  }
}

class TtsFabPause extends StatelessWidget {
  const TtsFabPause({super.key});

  @override
  Widget build(BuildContext ctx) {
    return FloatingActionButton(
      heroTag: 'pauseBtn',
      onPressed: () {
        TtsHandler().pause();
      },
      child: const Icon(
        Icons.pause_rounded,
        color: Colors.white,
      ),
    );
  }
}

class TtsFabResume extends StatelessWidget {
  const TtsFabResume({super.key});

  @override
  Widget build(BuildContext ctx) {
    return FloatingActionButton(
      heroTag: 'resumeBtn',
      onPressed: () {
        TtsHandler().play();
      },
      child: const Icon(
        Icons.play_arrow_rounded,
        color: Colors.white,
      ),
    );
  }
}

class TtsFabStop extends StatelessWidget {
  const TtsFabStop({super.key});

  @override
  Widget build(BuildContext ctx) {
    return FloatingActionButton(
      heroTag: 'stopBtn',
      onPressed: () {
        TtsHandler().stop();
      },
      child: const Icon(
        Icons.stop_rounded,
        color: Colors.white,
      ),
    );
  }
}

class TtsFabSkipNext extends StatelessWidget {
  const TtsFabSkipNext({super.key});

  @override
  Widget build(BuildContext ctx) {
    return FloatingActionButton(
      heroTag: 'skipNextBtn',
      onPressed: () {
        TtsHandler().skipToNext();
      },
      child: const Icon(
        Icons.skip_next_rounded,
        color: Colors.white,
      ),
    );
  }
}

class TtsFabSkipPrevious extends StatelessWidget {
  const TtsFabSkipPrevious({super.key});

  @override
  Widget build(BuildContext ctx) {
    return FloatingActionButton(
      heroTag: 'skipPreviousBtn',
      onPressed: () {
        TtsHandler().skipToPrevious();
      },
      child: const Icon(
        Icons.skip_previous_rounded,
        color: Colors.white,
      ),
    );
  }
}
