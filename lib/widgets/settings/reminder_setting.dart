import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/services/notification_service.dart';
import 'package:repository/repository.dart';

class ReminderSetting extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ReminderSettingState();
  }
}

class _ReminderSettingState extends State<ReminderSetting> {
  final _notification = FlutterLocalNotificationsPlugin();
  bool _isOn = false;

  @override
  void initState() {
    _notification.pendingNotificationRequests().then((value) => setState(() {
          _isOn = value.isNotEmpty;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext ctx) {
    debugPrint('ReminderSetting.build: isOn = $_isOn');
    return _isOn
        ? ListTile(
            leading: Icon(Icons.alarm_on_rounded),
            title: Text(L10n.of(ctx).t8('Reminder.title') +
                ': ' +
                L10n.of(ctx).t8('Settings.on')),
            onTap: () => disable(),
          )
        : ListTile(
            leading: Icon(Icons.alarm_off_rounded),
            title: Text(L10n.of(ctx).t8('Reminder.title') +
                ': ' +
                L10n.of(ctx).t8('Settings.off')),
            onTap: () => enable(ctx),
          );
  }

  Future<String?> enable(BuildContext ctx) async {
    int? h = Settings().getInt(Settings.REMINDER_TIME_H);
    int? m = Settings().getInt(Settings.REMINDER_TIME_M);
    var initialTime = h != null && m != null
        ? TimeOfDay(hour: h, minute: m)
        : TimeOfDay.now();
    var time = await showTimePicker(context: ctx, initialTime: initialTime);
    if (time != null) {
      //NotificationService().scheduleInstantReminder();  // for debugging
      NotificationService().scheduleDailyReminder(time.hour, time.minute);
      Settings().putInt(Settings.REMINDER_TIME_H, time.hour);
      Settings().putInt(Settings.REMINDER_TIME_M, time.minute);
      setState(() {
        _isOn = true;
      });
    }
  }

  void disable() {
    NotificationService().cancelDailyReminder();
    setState(() {
      _isOn = false;
    });
  }
}
