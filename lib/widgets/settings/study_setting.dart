import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/widgets/settings/setting_button.dart';
import 'package:repository/repository.dart';

import '../widgets.dart';

class StudySetting extends StatelessWidget {
  const StudySetting({super.key});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      tooltip: L10n.of(context).t8('Study.title'),
      icon: const Icon(Icons.settings_rounded),
      onPressed: () => edit(context),
    );
  }

  Future<String?> edit(BuildContext ctx) {
    return showDialog<String>(
        context: ctx,
        builder: (BuildContext context) {
          return Dialog(child: _StudySettingForm());
        });
  }
}

class _StudySettingForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StudySettingFormState();
  }
}

class _StudySettingFormState extends State<_StudySettingForm> {
  bool _speechOn = Settings().getBool(Settings.STUDY_SPEECH_ON) ?? true;
  double _speed = Settings().getDouble(Settings.SPEECH_SPEED) ?? 0.5;
  int _puzzleSize = Settings().getInt(Settings.STUDY_PUZZLE_SIZE) ?? 5;

  @override
  Widget build(BuildContext ctx) {
    return Theme(
        data: Theme.of(ctx),
        child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              Row(children: [
                Tooltip(
                    message: L10n.of(ctx).t8('Setting.speech.control'),
                    child: const Icon(Icons.play_arrow_rounded)),
                Expanded(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                      SettingButton(
                          isSelected: _speechOn,
                          iconData: Icons.volume_up_rounded,
                          l10nKey: 'Study.speech',
                          onPressed: () {
                            Settings()
                                .putBool(Settings.STUDY_SPEECH_ON, !_speechOn);
                            setState(() {
                              _speechOn = !_speechOn;
                            });
                          }),
                    ]))
              ]),
              SettingSlider(
                  value: _speed,
                  onChanged: (result) => setState(() => _speed = result),
                  icon: Icons.speed_rounded,
                  l10nKey: 'Setting.speech.rate',
                  settingKey: Settings.SPEECH_SPEED,
                  min: 0.0,
                  max: 1.0),
              Row(children: [
                Tooltip(
                    message: L10n.of(ctx).t8('Setting.puzzle.size'),
                    child: const Icon(Icons.extension_rounded)),
                Expanded(
                    child: Slider(
                  value: _puzzleSize.toDouble(),
                  min: 3,
                  max: 12,
                  divisions: 9,
                  label: _puzzleSize.toString(),
                  onChanged: (double value) {
                    Settings()
                        .putInt(Settings.STUDY_PUZZLE_SIZE, value.round());
                    setState(() {
                      _puzzleSize = value.round();
                    });
                  },
                )),
              ]),
            ])));
  }
}
