import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';

class SettingButton extends StatelessWidget {
  final IconData iconData;
  final bool isSelected;
  final void Function() onPressed;
  final String? l10nKey;

  const SettingButton(
      {Key? key,
      required this.iconData,
      this.isSelected = false,
      required this.onPressed,
      this.l10nKey})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Padding(
        padding: EdgeInsets.only(left: 24),
        child: isSelected
            ? Ink(
                decoration: ShapeDecoration(
                  color: AppColorScheme.appBar.primary,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(2)),
                ),
                child: _tooltip(
                    ctx,
                    _iconButton(ctx, 2,
                        color: AppColorScheme.appBar.onPrimary)))
            : Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 2,
                    color: IconTheme.of(ctx).color!,
                  ),
                  borderRadius: BorderRadius.circular(2),
                ),
                child: _tooltip(ctx, _iconButton(ctx, 0))));
  }

  Widget _tooltip(BuildContext ctx, Widget child) {
    if (l10nKey == null) return child; // RETURN
    final l10n = L10n.of(ctx);
    return Tooltip(
        message:
            '${l10n.t8(l10nKey!)}: ${l10n.t8(isSelected ? 'Settings.on' : 'Settings.off')}',
        child: child);
  }

  IconButton _iconButton(BuildContext ctx, double padding, {Color? color}) {
    return IconButton(
      splashColor: Theme.of(ctx).colorScheme.primary.withOpacity(0.24),
      highlightColor: Theme.of(ctx).colorScheme.primary.withOpacity(0.24),
      hoverColor: Theme.of(ctx).colorScheme.primary.withOpacity(0.12),
      padding: EdgeInsets.all(padding),
      constraints: BoxConstraints(),
      icon: Icon(iconData, color: color),
      onPressed: onPressed,
    );
  }
}
