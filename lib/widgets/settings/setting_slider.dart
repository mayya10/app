import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:repository/repository.dart';

class SettingSlider extends StatelessWidget {
  final IconData icon;
  final String l10nKey;
  final String settingKey;
  final double value;
  final void Function(double) onChanged;
  final double min;
  final double max;

  const SettingSlider(
      {Key? key,
      required this.value,
      required this.onChanged,
      required this.icon,
      required this.l10nKey,
      required this.settingKey,
      required this.min,
      required this.max})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Row(children: [
      Tooltip(message: L10n.of(ctx).t8(l10nKey), child: Icon(icon)),
      Expanded(
          child: Slider(
        value: value,
        min: min,
        max: max,
        divisions: 10,
        label: value.toString(),
        onChanged: (double result) async {
          final rounded = (result * 10).roundToDouble() / 10;
          Settings().putDouble(settingKey, rounded);
          onChanged(rounded);
        },
      )),
    ]);
  }
}
