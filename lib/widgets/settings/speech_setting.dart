import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/widgets/settings/setting_slider.dart';
import 'package:repository/repository.dart';

import '../widgets.dart';

class SpeechSetting extends StatelessWidget {
  const SpeechSetting({super.key});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      tooltip: L10n.of(context).t8('Speech.title'),
      icon: const Icon(Icons.settings_rounded),
      onPressed: () => edit(context),
    );
  }

  Future<String?> edit(BuildContext ctx) {
    return showDialog<String>(
        context: ctx,
        builder: (BuildContext context) {
          return Dialog(child: _SpeechSettingForm());
        });
  }
}

class _SpeechSettingForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SpeechSettingFormState();
  }
}

class _SpeechSettingFormState extends State<_SpeechSettingForm> {
  double _speed = Settings().getDouble(Settings.SPEECH_SPEED) ?? 0.5;
  double _pause = Settings().getDouble(Settings.SPEECH_PAUSE) ?? 1.0;
  bool _playlist = Settings().getBool(Settings.SPEECH_PLAYLIST) ?? true;
  bool _repeat = Settings().getBool(Settings.SPEECH_REPEAT) ?? false;

  @override
  Widget build(BuildContext ctx) {
    return Theme(
      data: Theme.of(ctx),
      child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            SettingSlider(
              value: _speed,
              onChanged: (result) => setState(() => _speed = result),
              icon: Icons.speed_rounded,
              l10nKey: 'Setting.speech.rate',
              settingKey: Settings.SPEECH_SPEED,
              min: 0.0,
              max: 1.0),
            SettingSlider(
              value: _pause,
              onChanged: (result) => setState(() => _pause = result),
              icon: Icons.pause_presentation_rounded,
              l10nKey: 'Setting.speech.pause',
              settingKey: Settings.SPEECH_PAUSE,
              min: 0.0,
              max: 10.0),
            Row(children: [
              Tooltip(
                  message: L10n.of(ctx).t8('Setting.speech.control'),
                  child: const Icon(Icons.play_arrow_rounded)),
              Expanded(
                  child:
                      Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                SettingButton(
                    isSelected: _playlist,
                    iconData: Icons.playlist_play_rounded,
                    l10nKey: 'Speech.playlist',
                    onPressed: () {
                      Settings().putBool(Settings.SPEECH_PLAYLIST, !_playlist);
                      setState(() {
                        _playlist = !_playlist;
                      });
                    }),
                SettingButton(
                    isSelected: _repeat,
                    iconData: _playlist
                        ? Icons.repeat_rounded
                        : Icons.repeat_one_rounded,
                    l10nKey: 'Speech.repeat',
                    onPressed: () {
                      Settings().putBool(Settings.SPEECH_REPEAT, !_repeat);
                      setState(() {
                        _repeat = !_repeat;
                      });
                    }),
              ]))
            ])
          ])),
    );
  }
}
