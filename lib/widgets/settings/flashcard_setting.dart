import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:repository/repository.dart';

import '../widgets.dart';

class FlashcardSetting extends StatelessWidget {
  const FlashcardSetting({super.key});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      tooltip: L10n.of(context).t8('Study.title'),
      icon: const Icon(Icons.settings_rounded),
      onPressed: () => edit(context),
    );
  }

  Future<String?> edit(BuildContext ctx) {
    return showDialog<String>(
        context: ctx,
        builder: (BuildContext context) {
          return Dialog(child: _FlashcardSettingForm());
        });
  }
}

class _FlashcardSettingForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FlashcardSettingFormState();
  }
}

class _FlashcardSettingFormState extends State<_FlashcardSettingForm> {
  double _speed = Settings().getDouble(Settings.SPEECH_SPEED) ?? 0.5;

  @override
  Widget build(BuildContext ctx) {
    return Theme(
        data: Theme.of(ctx),
        child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              SettingSlider(  // todo: replace with flashcard settings
                  value: _speed,
                  onChanged: (result) => setState(() => _speed = result),
                  icon: Icons.speed_rounded,
                  l10nKey: 'Setting.speech.rate',
                  settingKey: Settings.SPEECH_SPEED,
                  min: 0.0,
                  max: 1.0),
            ])));
  }
}
