import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/selection/selection.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/routers/app_route_path.dart';
import 'package:remem_me/widgets/box/verse_tile.dart';

import '../widgets.dart';

/// Layout for displaying a verse in a list
class SelectableVerseTile extends StatelessWidget {
  final Verse verse;

  SelectableVerseTile(this.verse);

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<SelectionBloc, SelectionState>(
        builder: (ctx, state) => VerseTile(
              verse: verse,
              onTap: () => _onTap(ctx, state.selection),
              onLongPress: () {
                _toggleSelected(ctx, state.selection);
              },
              tileColor: _tileColor(ctx, state.selection),
              verseBadge: _verseBadge(ctx, state.selection),
            ));
  }

  void _onTap(BuildContext ctx, Map<int, Verse> selection) {
    if (selection.isNotEmpty) {
      _toggleSelected(ctx, selection);
    } else {
      final bloc = BlocProvider.of<OrderedVersesBloc>(ctx);
      final verses = (bloc.state as OrderSuccess).verses;
      final index = verses.indexWhere((v) => v.id == verse.id);
      BlocProvider.of<FlashcardBloc>(ctx).add(FlashcardLoaded(
          verses: verses,
          index: index,
          box: bloc.box,
          single: true,
          inverse: [Box.KNOWN, Box.ALL].contains(bloc.box)));
      BlocProvider.of<NavigationCubit>(ctx).go(HomePath(id: verse.id));
    }
  }

  Color _tileColor(BuildContext ctx, Map<int, Verse> selection) {
    final theme = Theme.of(ctx);
    return selection.keys.contains(verse.id)
        ? theme.selectedRowColor
        : theme.cardColor;
  }

  Widget _verseBadge(BuildContext ctx, Map<int, Verse> selection) {
    return VerseBadge(
      key: UniqueKey(),
      verse: verse,
      isSelected: selection.keys.contains(verse.id),
      onSelect: () => _toggleSelected(ctx, selection),
    );
  }

  _toggleSelected(BuildContext ctx, Map<int, Verse> selection) {
    // todo: just use bool isSelected, move rest to bloc,
    final updatedSelection = Map<int, Verse>.of(selection);
    if (selection.keys.contains(verse.id)) {
      updatedSelection.remove(verse.id);
      BlocProvider.of<SelectionBloc>(ctx)
          .add(SelectionUpdated(updatedSelection));
    } else {
      updatedSelection[verse.id] = verse;
      BlocProvider.of<SelectionBloc>(ctx)
          .add(SelectionUpdated(updatedSelection));
    }
  }
}
