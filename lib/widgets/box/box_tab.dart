import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/models/models.dart';

import '../../theme.dart';
import '../widgets.dart';

class BoxTab extends StatelessWidget {
  final Box tab;
  final String l10nKey;
  final IconData iconData;
  final bool hasOrderMenu;

  const BoxTab(
      {Key? key,
      required this.tab,
      required this.l10nKey,
      required this.iconData,
      required this.hasOrderMenu})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    final List<Widget> stackItems = [
      SizedBox(
          width: double.infinity,
          height: 48.0,
          child: Tab(
            icon: Icon(iconData, size: 28.0),
            iconMargin: const EdgeInsets.all(0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(L10n.of(ctx).t8(l10nKey).toUpperCase(),
                    style: AppTheme.dark.textTheme.caption),
                SizedBox(width: 4),
                BoxCount(),
              ],
            ),
          )),
    ];
    if (hasOrderMenu) {
      stackItems.add(Positioned(
        bottom: 2,
        right: 0,
        child: SvgAsset('triangle',
            height: 10, color: AppTheme.dark.textTheme.caption!.color),
      ));
    }

    return Stack(children: stackItems);
  }
}
