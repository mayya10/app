import 'package:flutter/material.dart';
import 'package:remem_me/models/models.dart';

class VersesList extends StatefulWidget {
  final List<Verse> verses;
  final Widget Function(Verse verse) createTile;

  const VersesList({Key? key, required this.verses, required this.createTile})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _VersesListState();
  }
}

class _VersesListState extends State<VersesList> {
  final ScrollController _controller = ScrollController();

  @override
  Widget build(BuildContext ctx) {
    return Scrollbar(
        controller: _controller,
        interactive: true,
        child: ListView.builder(
          shrinkWrap: true,
          padding: EdgeInsets.only(bottom: 80),
          controller: _controller,
          itemCount: widget.verses.length,
          itemBuilder: (BuildContext context, int index) {
            final verse = widget.verses[index];
            return widget.createTile(verse);
          },
        ));
  }
}
