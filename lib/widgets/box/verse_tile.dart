import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/models/models.dart';

import '../../services/verse_service.dart';

/// Layout for displaying a verse in a list
class VerseTile extends StatelessWidget {
  final Verse verse;
  final void Function()? onTap;
  final void Function()? onLongPress;
  final Color? tileColor;
  final Widget? verseBadge;

  const VerseTile(
      {Key? key,
      required this.verse,
      this.onTap,
      this.onLongPress,
      this.tileColor,
      this.verseBadge})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return GestureDetector(
        onTap: () => onTap!(),
        onLongPress: () => onLongPress!(),
        child: ListTile(
            tileColor: tileColor,
            leading:
                SizedBox.fromSize(size: const Size.square(48.0), child: verseBadge),
            title: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // _indicator(ctx, verse.level),
                  Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                      _description(ctx, verse),
                      _tags(ctx, verse),
                      Divider(
                          thickness: 1.0,
                          height: 1.0,
                          color: Theme.of(ctx).colorScheme.onBackground.withOpacity(0.12))
                    ],
                  ))
                ])));
  }

  Widget _tags(BuildContext ctx, Verse verse) {
    return Wrap(
      alignment: WrapAlignment.end,
      verticalDirection: VerticalDirection.up,
      children: verse.tags.values
          .map((tag) => Container(
              decoration: BoxDecoration(
                color: Theme.of(ctx).colorScheme.onBackground.withOpacity(0.12),
                borderRadius: const BorderRadius.only(
                    topLeft: const Radius.circular(4.0),
                    topRight: const Radius.circular(4.0)),
              ),
              padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 6.0),
              margin: const EdgeInsets.only(left: 6.0),
              child: Text(tag ?? '',
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: Theme.of(ctx).textTheme.overline)))
          .toList(),
    );
  }

  Widget _description(BuildContext ctx, Verse verse) {
    FromToday fromToday = new FromToday(L10n.of(ctx));
    return Container(
        padding: EdgeInsets.fromLTRB(
            0.0, 16.0, 0.0, verse.tags.isEmpty ? 16.0 : 4.0),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                Text(VerseService().title(verse), style: Theme.of(ctx).textTheme.titleMedium),
                const SizedBox(height: 4.0),
                Text(
                  VerseService().subtitle(verse),
                  style: Theme.of(ctx).textTheme.bodyText2,
                  overflow: TextOverflow.fade,
                )
              ])),
              Column(crossAxisAlignment: CrossAxisAlignment.end, children: <Widget>[
                Text(verse.source ?? '',
                    style: Theme.of(ctx).textTheme.bodyText2),
                const SizedBox(height: 8.0),
                Text(fromToday.format(verse.due)!,
                    style: Theme.of(ctx).textTheme.caption)
              ]),
            ]));
  }
}
