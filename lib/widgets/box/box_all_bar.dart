import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:repository/repository.dart';

import '../../blocs/blocs.dart';
import '../../dialogs/dialogs.dart';
import '../../models/models.dart';
import '../../theme.dart';
import '../widgets.dart';

class BoxAllBar extends StatelessWidget implements PreferredSizeWidget {
  const BoxAllBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    final List<Widget> stackItems = [
      SizedBox(
          width: double.infinity,
          height: 32,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const BoxCount(),
              const SizedBox(width: 4),
              Text(L10n.of(ctx).t8('Box.all').toUpperCase(),
                  style: AppTheme.dark.textTheme.caption),
            ],
          )),
      Positioned(
        bottom: 2,
        right: 2,
        child: SvgAsset('triangle',
            height: 10, color: AppTheme.dark.textTheme.caption!.color),
      )
    ];
    return PreferredSize(
        preferredSize: const Size.fromHeight(48),
        child: BlocProvider<OrderedVersesBloc>.value(
            value: di.get<OrderedVersesAllBloc>(),
            child: InkWell(
              onTap: () => _editOrder(ctx),
              child: Stack(children: stackItems),
            )));
  }

  Future<void> _editOrder(BuildContext ctx) async {
    final bloc = OrderedVersesBloc.from(const TabState(false, Box.ALL));
    final prevOrder = (bloc.state as OrderSuccess).order;
    final nextOrder = await showDialog<VerseOrder>(
        context: ctx,
        builder: (BuildContext context) {
          return BoxOrderDialog(
            tab: Box.ALL,
            prevOrder: prevOrder,
          );
        });
    if (nextOrder != null) bloc.add(OrderUpdated(nextOrder));
  }

  @override
  Size get preferredSize => const Size.fromHeight(32);
}
