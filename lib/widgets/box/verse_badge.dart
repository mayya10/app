import 'package:app_core/app_core.dart';
import 'package:audio_service/audio_service.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/blocs/text_to_speech/tts_handler.dart';
import 'package:remem_me/models/verse.dart';
import 'package:remem_me/theme.dart';
import 'package:remem_me/widgets/animation/animation.dart';

import '../svg.dart';

class VerseBadge extends StatefulWidget {
  final Verse verse;
  final void Function()? onSelect;
  final bool isSelected;

  const VerseBadge(
      {Key? key, required this.verse, this.onSelect, this.isSelected = false})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _VerseBadgeState();
}

class _VerseBadgeState extends State<VerseBadge>
    with SingleTickerProviderStateMixin {
  AnimationController? _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 400));
    if (widget.isSelected) {
      _animationController!.value = 1.0; // start from reverse
    }
  }

  @override
  Widget build(BuildContext ctx) {
    return Flipper(
      animationController: _animationController,
      onTap: _flip,
      front: _front(ctx),
      back: _back(),
    );
  }

  Widget _front(BuildContext ctx) {
    return StreamBuilder<MediaProcessingState>(
        stream: TtsHandler().mediaProcessing,
        builder: (ctx, snapshot) {
          final layers = <Widget>[
            if (widget.verse.level < 0) _placeholder() else _levelPane(),
            if (widget.verse.image != null) _image(),
            if (widget.verse.level > 0) _levelNumber(),
            if (snapshot.data != null &&
                snapshot.data!.playbackState.processingState !=
                    AudioProcessingState.idle &&
                int.parse(snapshot.data!.mediaItem!.id) == widget.verse.id)
              _playing(snapshot.data!.playbackState),
          ];
          return ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(4.0)),
              child: Stack(
                fit: StackFit.expand,
                children: layers,
              ));
        });
  }

  Container _levelPane() {
    return Container(
      color: widget.verse.color ?? Flat.grey,
      width: double.infinity,
      height: double.infinity,
    );
  }

  Container _levelNumber() {
    return Container(
      padding: const EdgeInsets.all(4.0),
      width: double.infinity,
      height: double.infinity,
      child: widget.verse.level > 0
          ? Text(widget.verse.level.toString(),
              style: AppTheme.dark.textTheme.overline!.copyWith(
                  shadows: widget.verse.image != null
                      ? [
                          const Shadow(
                            blurRadius: 2.0,
                            color: Colors.black,
                            offset: Offset(0.5, 1.0),
                          ),
                        ]
                      : []))
          : const SizedBox.shrink(),
    );
  }

  Widget _image() {
    return FittedBox(
        fit: BoxFit.cover,
        child: CachedNetworkImage(
          errorWidget: (ctx, url, err) => const SizedBox.shrink(),
          imageUrl: widget.verse.image!,
        ));
  }

  SvgAsset _placeholder() => const SvgAsset('app_logo');

  Widget _playing(PlaybackState playbackState) {
    return Container(
        color: Colors.black38,
        child: Icon(
          playbackState.playing
              ? playbackState.repeatMode == AudioServiceRepeatMode.group
                  ? Icons.not_started_outlined
                  : Icons.play_circle_outline_rounded
              : Icons.pause_circle_outline_rounded,
          color: Colors.white,
          size: 32,
        ));
  }

  Container _back() {
    return Container(
      decoration: BoxDecoration(
          color: Flat.grey, borderRadius: BorderRadius.circular(4)),
      width: double.infinity,
      height: double.infinity,
      child: const Icon(
        Icons.check_rounded,
        color: Colors.white,
        size: 32.0,
      ),
    );
  }

  _flip() {
    if (!widget.isSelected) {
      _animationController!.forward().then((_) => widget.onSelect!());
    } else {
      _animationController!.reverse().then((_) => widget.onSelect!());
    }
  }
}
