import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/ordered_verses/ordered_verses.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/widgets/empty_list.dart';
import 'package:remem_me/widgets/widgets.dart';

class OrderedVerses extends StatelessWidget {
  OrderedVerses({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Builder(
      builder: (context) {
        final OrderedVersesState orderedVersesState = context.watch<OrderedVersesBloc>().state;
        if (orderedVersesState is OrderInProgress) {
          return LoadingIndicator();
        } else if (orderedVersesState is OrderSuccess &&
            orderedVersesState.verses.length > 0) {
          return Stack(children: [
            VersesList(
                verses: orderedVersesState.verses,
                createTile: (verse) => SelectableVerseTile(verse)),
            if (orderedVersesState.isUpdating) LoadingIndicator(),
          ]);
        } else {
          IconData icon;
          switch (BlocProvider.of<OrderedVersesBloc>(ctx).box) {
            case Box.NEW:
              icon = Icons.inbox_rounded;
              break;
            case Box.DUE:
              icon = Icons.check_box_outline_blank_rounded;
              break;
            case Box.KNOWN:
            default:
              icon = Icons.check_box_rounded;
              break;
          }
          return EmptyList(icon: icon);
        }
      },
    );
  }
}
