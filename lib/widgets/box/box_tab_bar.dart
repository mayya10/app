import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/dialogs/dialogs.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

import '../widgets.dart';

class BoxTabBar extends StatelessWidget implements PreferredSizeWidget {
  final TabController _controller;

  const BoxTabBar(this._controller);

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<TabBloc, TabState>(builder: (context, state) {
      return TabBar(
        //indicatorColor: Flat.amber,
        controller: _controller,
        labelStyle: Theme.of(ctx).textTheme.caption,
        labelPadding: const EdgeInsets.symmetric(horizontal: 2.0),
        tabs: [
          BlocProvider<OrderedVersesBloc>.value(
            value: di.get<OrderedVersesNewBloc>(),
            child: BoxTab(
              iconData: Icons.inbox_rounded,
              l10nKey: 'Box.new',
              tab: Box.NEW,
              hasOrderMenu: state.box == Box.NEW,
            ),
          ),
          BlocProvider<OrderedVersesBloc>.value(
            value: di.get<OrderedVersesDueBloc>(),
            child: BoxTab(
                iconData: Icons.check_box_outline_blank_rounded,
                l10nKey: 'Box.due',
                tab: Box.DUE,
                hasOrderMenu: state.box == Box.DUE),
          ),
          BlocProvider<OrderedVersesBloc>.value(
            value: di.get<OrderedVersesKnownBloc>(),
            child: BoxTab(
                iconData: Icons.check_box_rounded,
                l10nKey: 'Box.known',
                tab: Box.KNOWN,
                hasOrderMenu: state.box == Box.KNOWN),
          )
        ],
        onTap: (i) async {
          if (!_controller.indexIsChanging) {
            await _editOrder(ctx, i);
          }
        },
      );
    });
  }

  Future<void> _editOrder(BuildContext ctx, int i) async {
    final bloc = (OrderedVersesBloc.from(TabState(true, Box.values[i])));
    final prevOrder = (bloc.state as OrderSuccess).order;
    final nextOrder = await showDialog<VerseOrder>(
        context: ctx,
        builder: (BuildContext context) {
          return BoxOrderDialog(
            tab: Box.values[i],
            prevOrder: prevOrder,
          );
        });
    if (nextOrder != null) bloc.add(OrderUpdated(nextOrder));
  }

  @override
  Size get preferredSize => const Size.fromHeight(48.0);
}
