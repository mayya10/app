import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/models/models.dart';

import '../widgets.dart';

/// Layout for displaying a verse in a list
class SimpleVerseTile extends StatelessWidget {
  final Verse verse;
  final void Function()? onTap;

  const SimpleVerseTile(this.verse, {Key? key,  this.onTap }) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return VerseTile(
          verse: verse,
          onTap: onTap ?? () {},
          onLongPress: () {},
          tileColor: _tileColor(ctx),
          verseBadge: _verseBadge(ctx),
        );
  }

  Color _tileColor(BuildContext ctx) {
    return Theme.of(ctx).cardColor;
  }

  Widget _verseBadge(BuildContext ctx) {
    final layers = <Widget>[];
    layers.add(_placeholder());
    if (verse.image != null) {
      layers.add(_image());
    }
    return ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(4.0)),
        child: Stack(
          fit: StackFit.expand,
          children: layers,
        ));
  }

  Widget _image() {
    return FittedBox(
        fit: BoxFit.cover,
        child: CachedNetworkImage(
          errorWidget: (ctx, url, err) => const SizedBox.shrink() ,
          imageUrl: verse.image!,
        ));
  }

  SvgAsset _placeholder() => const SvgAsset('app_logo');
}
