// ignore: avoid_web_libraries_in_flutter
import 'dart:html';

import 'package:flutter/material.dart';
import 'dart:ui' as ui;

class WebFrame extends StatelessWidget {
  final String url;

  const WebFrame(this.url) : super();

  @override
  Widget build(BuildContext context) {
    // ignore: undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory(
        url,
            (int viewId) => IFrameElement()
          ..style.width = '100%'
          ..style.height = '100%'
          ..src = url
          ..style.border = 'none');

    return HtmlElementView(viewType: url, key: UniqueKey());
  }

}