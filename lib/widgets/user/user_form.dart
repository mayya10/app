import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/widgets/form/form.dart';

typedef OnSubmitCallback = Function(Map<String, String> credentials);

class UserForm extends StatefulWidget {
  final Map<String, String?>? initialValues;
  final OnSubmitCallback onSubmit;
  final OnSubmitCallback? onForgotPassword;
  final OnSubmitCallback? onDelete;
  final void Function()? onCancel;
  final String? titleL10nKey;
  final String? submitL10nKey;
  final bool hasRepeatPassword;
  final bool hasUsername;
  final bool hasEmailAsUsername;
  final Map<String, List<String?>>? errors;

  const UserForm(
      {Key? key,
      this.initialValues,
      required this.onSubmit,
      this.onForgotPassword,
      this.onDelete,
      this.onCancel,
      this.hasRepeatPassword = false,
      this.hasUsername = true,
      this.hasEmailAsUsername = true,
      this.titleL10nKey,
      this.submitL10nKey,
      this.errors = const {}})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _UserFormState();
  }
}

/// Form for credentials
class _UserFormState extends State<UserForm> {
  final _formKey = GlobalKey<FormState>();
  final _fieldKeyEmail = GlobalKey<FormFieldState>();
  final _fieldKeyPassword = GlobalKey<FormFieldState>();
  final _credentials = <String, String>{};

  static final _spacing = 16.0;

  @override
  Widget build(BuildContext ctx) {
    // final fields = [fieldUser(ctx), fieldPassword(ctx)];

    final fields = <Widget>[
      Row(
        children: [
          Expanded(
              child: Text(L10n.of(ctx).t8(widget.titleL10nKey!),
                  style: Theme.of(ctx).textTheme.titleLarge)),
          if (widget.onDelete != null) _deleteButton(ctx),
        ],
      ),
    ];

    if (widget.errors?['messages'] != null) {
      fields.addAll([SizedBox(height: _spacing), _errorMessages(ctx)]);
    }

    if (widget.hasUsername) {
      fields.addAll([
        SizedBox(height: _spacing),
        widget.hasEmailAsUsername ? _emailField(ctx) : _nameField(ctx),
      ]);
    }

    fields.addAll([
      SizedBox(height: _spacing),
      _passwordField(ctx),
    ]);

    if (widget.hasRepeatPassword) {
      fields.addAll([
        SizedBox(height: _spacing),
        _repeatPasswordField(ctx),
      ]); // todo: validation
    }

    fields.addAll([
      Wrap(
        alignment: WrapAlignment.spaceBetween,
        children: [
          if (widget.onCancel != null) _cancelButton(ctx),
          if (widget.onForgotPassword != null) _forgotPasswordButton(ctx),
          SizedBox(width: _spacing),
          _submitButton(ctx),
        ],
      ),
    ]);

    return _form(ctx, fields);
  }

  _form(BuildContext ctx, List<Widget> fields) {
    return Container(
      constraints: BoxConstraints(maxWidth: 512),
      padding: EdgeInsets.all(16),
      child: SingleChildScrollView(
          child: Form(
              key: _formKey,
              child: AutofillGroup(
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: fields,
              )))),
    );
  }

  Text _errorMessages(BuildContext ctx) {
    debugPrint('Errors:\n' + widget.errors!['messages']!.join('\n'));
    return Text(widget.errors!['messages']!.join('\n'),
        key: ValueKey('errorText'),
        style: Theme.of(ctx)
            .textTheme
            .bodyText1!
            .copyWith(color: Theme.of(ctx).errorColor));
  }

  StringInput _emailField(BuildContext ctx) {
    return StringInput(
      fieldKey: _fieldKeyEmail,
      l10nKey: 'User.email',
      iconData: Icons.email_rounded,
      initialValue: widget.initialValues!['email'],
      autofillHints: [AutofillHints.email],
      validator: Validators.compose([
        Validators.error<String?>(
            widget.errors!['email'], widget.initialValues!['email']),
        Validators.email(ctx),
        Validators.required(ctx),
      ]),
      autovalidateMode: widget.initialValues!['email'] != null
          ? AutovalidateMode.always
          : AutovalidateMode.disabled,
      onSaved: (value) => _credentials['email'] = value!.trim(),
      onFieldSubmitted: (_) => _submit(),
    );
  }

  StringInput _nameField(BuildContext ctx) {
    return StringInput(
      l10nKey: 'User.name',
      iconData: Icons.folder_shared_rounded,
      initialValue: widget.initialValues!['name'],
      autofillHints: [AutofillHints.username],
      validator: Validators.error<String?>(
          widget.errors!['name'], widget.initialValues!['name']),
      autovalidateMode: widget.initialValues!['name'] != null
          ? AutovalidateMode.always
          : AutovalidateMode.disabled,
      onSaved: (value) => _credentials['name'] = value!.trim(),
      onFieldSubmitted: (_) => _submit(),
    );
  }

  StringInput _repeatPasswordField(BuildContext ctx) {
    return StringInput(
      l10nKey: 'User.passwordRepeat',
      iconData: Icons.replay_rounded,
      obscureText: true,
      validator: Validators.equal(
        ctx,
        _fieldKeyPassword,
        'User.password',
      ),
      onSaved: (_) {},
      onFieldSubmitted: (_) => _submit(),
    );
  }

  StringInput _passwordField(BuildContext ctx) {
    return StringInput(
      fieldKey: _fieldKeyPassword,
      l10nKey: 'User.password',
      iconData: Icons.security_rounded,
      initialValue: widget.initialValues!['password'],
      autofillHints: [AutofillHints.password],
      obscureText: true,
      validator: Validators.compose([
        Validators.error<String?>(
            widget.errors!['password'], widget.initialValues!['password']),
        Validators.required(ctx),
      ]),
      autovalidateMode: widget.initialValues!['password'] != null
          ? AutovalidateMode.always
          : AutovalidateMode.disabled,
      onSaved: (value) => _credentials['password'] = value?.trim() ?? '',
      onFieldSubmitted: (_) => _submit(),
    );
  }

  TextButton _submitButton(BuildContext ctx) {
    return TextButton(
        key: ValueKey('submitButton'),
        style: TextButton.styleFrom(padding: EdgeInsets.all(16.0)),
        onPressed: _submit,
        child: Text(L10n.of(ctx).t8(widget.submitL10nKey!).toUpperCase()));
  }

  void _submit() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      widget.onSubmit(_credentials);
    }
  }

  IconButton _deleteButton(BuildContext ctx) {
    return IconButton(
        tooltip: L10n.of(ctx).t8('Button.delete'),
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
            widget.onDelete!(_credentials);
          }
        },
        icon: Icon(Icons.delete_rounded));
  }

  TextButton _forgotPasswordButton(BuildContext ctx) {
    return TextButton(
        style: TextButton.styleFrom(
            primary: Theme.of(ctx).textTheme.caption!.color,
            padding: EdgeInsets.all(16.0)),
        onPressed: () {
          if (_fieldKeyEmail.currentState!.validate()) {
            _formKey.currentState!.save();
            widget.onForgotPassword!(_credentials);
          }
        },
        child: Text(L10n.of(ctx).t8('User.forgotPassword').toUpperCase()));
  }

  TextButton _cancelButton(BuildContext ctx) {
    return TextButton(
        style: TextButton.styleFrom(
            primary: Theme.of(ctx).textTheme.caption!.color,
            padding: EdgeInsets.all(16.0)),
        onPressed: widget.onCancel,
        child: Text(L10n.of(ctx).t8('Button.cancel').toUpperCase()));
  }
}
