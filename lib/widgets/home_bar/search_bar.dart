import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/widgets/home_bar/filter_icon_button.dart';

import '../../theme.dart';

class SearchBar extends StatelessWidget {
  final bool hasEndDrawer;

  const SearchBar({
    Key? key,
    required this.hasEndDrawer,
  }) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return AppBar(
        elevation: 0,
        title: _searchField(ctx),
        leading: _clearSearchBtn(ctx),
        actions: [
          if (hasEndDrawer) FilterIconButton(),
        ]);
  }

  IconButton _clearSearchBtn(BuildContext ctx) {
    return IconButton(
        tooltip: L10n.of(ctx).t8('Button.cancel'),
        icon: Icon(Icons.close_rounded),
        onPressed: () {
          BlocProvider.of<SelectionBloc>(ctx).add(SearchDeactivated());
          BlocProvider.of<FilteredVersesBloc>(ctx).add(SearchFilterUpdated(''));
        });
  }

  Theme _searchField(BuildContext ctx) {
    return Theme(
        data: AppTheme.appBar,
        child: TextFormField(
          autofocus: true,
          initialValue: (BlocProvider.of<FilteredVersesBloc>(ctx).state
                  as FilteredVersesSuccess)
              .activeFilter!
              .query,
          decoration: InputDecoration(
            hintText: L10n.of(ctx).t8('Button.search'),
            border: InputBorder.none,
          ),
          onChanged: (String value) {
            BlocProvider.of<FilteredVersesBloc>(ctx)
                .add(SearchFilterUpdated(value));
          },
        ));
  }

  IconButton _confirmSearchBtn(BuildContext ctx) {
    return IconButton(
        tooltip: L10n.of(ctx).t8('Button.apply'),
        icon: Icon(Icons.check_rounded),
        onPressed: () {
          BlocProvider.of<SelectionBloc>(ctx).add(SearchDeactivated());
        });
  }
}
