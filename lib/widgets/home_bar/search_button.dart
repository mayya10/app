import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/blocs.dart';

class SearchButton extends StatelessWidget {

  @override
  Widget build(BuildContext ctx) {
    return IconButton(
      tooltip: L10n.of(ctx).t8('Button.search'),
      icon: Builder(builder: (context) {
        final filteredState = context.watch<FilteredVersesBloc>().state;
        return _hasQuery(filteredState)
            ? _iconHasQuery(ctx) : _iconDefault(ctx);
      }),
      onPressed: () {
        BlocProvider.of<SelectionBloc>(ctx).add(SearchActivated());
      },
    );
  }

  bool _hasQuery(
      FilteredVersesState filteredState) {
    if (filteredState is FilteredVersesSuccess &&
        (filteredState.activeFilter?.query.length ?? 0) > 0) return true;
    return false;
  }

  Icon _iconDefault(BuildContext ctx) {
    return Icon(Icons.search_rounded);
  }

  Icon _iconHasQuery(BuildContext ctx) {
    return Icon(Icons.search_rounded, color: AppColorScheme.appBar.secondary);
  }
}
