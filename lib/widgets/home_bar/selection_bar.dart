import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/dialogs/dialogs.dart';
import 'package:remem_me/dialogs/postpone_dialog.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';

// This is the type used by the popup menu.
enum SelectionMenuAction {
  selectAll,
  setTags,
  delete,
  commit,
  remembered,
  moveToNew,
  moveToDue,
  postpone,
}

class SelectionBar extends StatelessWidget {
  late UndoBloc undoBloc;
  late VersesBloc versesBloc;

  SelectionBar({
    Key? key,
  }) : super(key: key) {
    undoBloc = di.get<UndoBloc>();
    versesBloc = di.get<VersesBloc>();
  }

  @override
  Widget build(BuildContext ctx) {
    return AppBar(
      elevation: 0,
      title: Text(L10n.of(ctx).t8('Verses.selected', [
        BlocProvider.of<SelectionBloc>(ctx).state.selection.length.toString()
      ])),
      leading: IconButton(
          tooltip: L10n.of(ctx).t8('Button.cancel'),
          icon: const Icon(Icons.arrow_back_rounded),
          onPressed: () => BlocProvider.of<SelectionBloc>(ctx)
              .add(const SelectionUpdated({}))),
      actions: _menu(ctx, BlocProvider.of<SelectionBloc>(ctx).state.selection),
    );
  }

  /// Creates a menu for actions related to the selected verses
  List<Widget> _menu(BuildContext ctx, Map<int, Verse> selection) {
    return <Widget>[
      _btnSelectAll(ctx, selection),
      _btnSetTags(ctx, selection),
      PopupMenuButton<SelectionMenuAction>(
        icon: const Icon(Icons.more_vert_rounded),
        onSelected: (SelectionMenuAction action) => _do(ctx, selection, action),
        itemBuilder: (BuildContext context) {
          final TabState state = di.get<TabBloc>().state;
          return <PopupMenuItem<SelectionMenuAction>>[
            if (state.isBoxed) ...[
              if ([Box.NEW].contains(state.box)) _itmCommit(ctx),
              if ([Box.DUE].contains(state.box)) _itmRemembered(ctx),
              if ([Box.KNOWN].contains(state.box)) _itmMoveToDue(ctx),
              if ([Box.DUE, Box.KNOWN].contains(state.box)) _itmMoveToNew(ctx),
            ],
            if (!state.isBoxed || state.box != Box.NEW)_itmPostpone(ctx),
            _itmDelete(ctx),
          ];
        },
      )
    ];
  }

  IconButton _btnSelectAll(BuildContext ctx, Map<int, Verse> selection) {
    return IconButton(
      tooltip: L10n.of(ctx).t8('Button.selectAll'),
      icon: const Icon(Icons.select_all_rounded),
      onPressed: () => _do(ctx, selection, SelectionMenuAction.selectAll),
    );
  }

  IconButton _btnSetTags(BuildContext ctx, Map<int, Verse> selection) {
    return IconButton(
      tooltip: L10n.of(ctx).t8('Tags.addAndRemove'),
      icon: const Icon(Icons.label_rounded),
      onPressed: () => _do(ctx, selection, SelectionMenuAction.setTags),
    );
  }

  PopupMenuItem<SelectionMenuAction> _itmDelete(BuildContext ctx) {
    return PopupMenuItem<SelectionMenuAction>(
      value: SelectionMenuAction.delete,
      child: Text(L10n.of(ctx).t8('Verses.delete')),
    );
  }

  PopupMenuItem<SelectionMenuAction> _itmCommit(BuildContext ctx) {
    return PopupMenuItem<SelectionMenuAction>(
      value: SelectionMenuAction.commit,
      child: Text(L10n.of(ctx).t8('Verses.commit')),
    );
  }

  PopupMenuItem<SelectionMenuAction> _itmRemembered(BuildContext ctx) {
    return PopupMenuItem<SelectionMenuAction>(
      value: SelectionMenuAction.remembered,
      child: Text(L10n.of(ctx).t8('Verses.remembered')),
    );
  }

  PopupMenuItem<SelectionMenuAction> _itmMoveToDue(BuildContext ctx) {
    return PopupMenuItem<SelectionMenuAction>(
      value: SelectionMenuAction.moveToDue,
      child: Text(L10n.of(ctx).t8('Verses.moveToDue')),
    );
  }

  PopupMenuItem<SelectionMenuAction> _itmMoveToNew(BuildContext ctx) {
    return PopupMenuItem<SelectionMenuAction>(
      value: SelectionMenuAction.moveToNew,
      child: Text(L10n.of(ctx).t8('Verses.moveToNew')),
    );
  }

  PopupMenuItem<SelectionMenuAction> _itmPostpone(BuildContext ctx) {
    return PopupMenuItem<SelectionMenuAction>(
      value: SelectionMenuAction.postpone,
      child: Text(L10n.of(ctx).t8('Verses.postpone')),
    );
  }

  /// Perfoms actions initiated from the menu
  _do(BuildContext ctx, Map<int, Verse> selection,
      SelectionMenuAction action) async {
    switch (action) {
      case SelectionMenuAction.selectAll:
        _selectAll(ctx);
        break;
      case SelectionMenuAction.setTags:
        await _setTags(ctx, selection);
        break;
      case SelectionMenuAction.delete:
        _delete(ctx, selection);
        break;
      case SelectionMenuAction.commit:
        _commit(ctx, selection);
        break;
      case SelectionMenuAction.remembered:
        _remembered(ctx, selection);
        break;
      case SelectionMenuAction.moveToDue:
        _moveToDue(ctx, selection);
        break;
      case SelectionMenuAction.moveToNew:
        _moveToNew(ctx, selection);
        break;
      case SelectionMenuAction.postpone:
        _postpone(ctx, selection);
        break;
    }
  }

  void _selectAll(BuildContext ctx) {
    final tabState = di.get<TabBloc>().state;
    final Box box = !tabState.isBoxed ? Box.ALL : tabState.box;
    late List<Verse> verses;
    switch (box) {
      case Box.NEW:
        verses = (di.get<OrderedVersesNewBloc>().state as OrderSuccess).verses;
        break;
      case Box.DUE:
        verses = (di.get<OrderedVersesDueBloc>().state as OrderSuccess).verses;
        break;
      case Box.KNOWN:
        verses =
            (di.get<OrderedVersesKnownBloc>().state as OrderSuccess).verses;
        break;
      case Box.ALL:
        verses = (di.get<OrderedVersesAllBloc>().state as OrderSuccess).verses;
        break;
    }
    BlocProvider.of<SelectionBloc>(ctx).add(SelectionUpdated(
        Map.fromEntries(verses.map((verse) => MapEntry(verse.id, verse)))));
  }

  Future _setTags(BuildContext ctx, Map<int, Verse> selection) async {
    final updatedVerses = await showDialog<List<Verse>>(
        context: ctx,
        builder: (BuildContext context) {
          return TagSelectionDialog(
            tags: List.from((BlocProvider.of<TagsBloc>(ctx).state
                    as EntitiesLoadSuccess<TagEntity>)
                .entities),
            selection: selection,
          );
        });
    if (updatedVerses != null) {
      versesBloc.add(EntitiesUpdated(updatedVerses, persist: false));
      undoBloc.add(
          UndoAdded([Undo(EntityAction.update, selection.values.toList())]));
    }
  }

  void _delete(BuildContext ctx, Map<int, Verse> selection) {
    versesBloc.add(EntitiesUpdated<Verse>(selection.values
        .map((verse) => verse.copyWith(deleted: true))
        .toList()));
    undoBloc
        .add(UndoAdded([Undo(EntityAction.delete, selection.values.toList())]));
  }

  void _commit(BuildContext ctx, Map<int, Verse> selection) {
    versesBloc.add(EntitiesUpdated<Verse>(selection.values
        .map((verse) => VerseService().committed(verse))
        .toList()));
    undoBloc
        .add(UndoAdded([Undo(EntityAction.update, selection.values.toList())]));
  }

  void _remembered(BuildContext ctx, Map<int, Verse> selection) {
    final verses = selection.values
        .map((verse) => VerseService().remembered(verse))
        .toList();
    versesBloc.add(EntitiesUpdated<Verse>(verses));
    final scores = verses
        .map((verse) => ScoreEntity(
              vkey: verse.id.toString(),
              date: DateService().today,
              change: TextService().countWordsOfText(verse.passage),
              accountId: verse.accountId,
            ))
        .toList();
    BlocProvider.of<ScoresBloc>(ctx).add(EntitiesAdded<ScoreEntity>(scores));
    undoBloc.add(UndoAdded([
      Undo(EntityAction.update, selection.values.toList()),
      Undo(EntityAction.create, scores),
    ]));
  }

  void _moveToDue(BuildContext ctx, Map<int, Verse> selection) {
    versesBloc.add(EntitiesUpdated<Verse>(selection.values
        .map((verse) => VerseService().movedToDue(verse))
        .toList()));
    undoBloc
        .add(UndoAdded([Undo(EntityAction.update, selection.values.toList())]));
  }

  void _moveToNew(BuildContext ctx, Map<int, Verse> selection) {
    versesBloc.add(EntitiesUpdated<Verse>(selection.values
        .map((verse) => VerseService().movedToNew(verse))
        .toList()));
    undoBloc
        .add(UndoAdded([Undo(EntityAction.update, selection.values.toList())]));
    //Navigator.pop(ctx); see https://medium.com/@felangelov/hi-eliseu-codinhoto-zeucxb-glad-you-enjoyed-the-post-4e2ea186e39f
  }

  Future _postpone(BuildContext ctx, Map<int, Verse> selection) async {
    final updatedVerses = await showDialog<List<Verse>>(
        context: ctx,
        builder: (BuildContext context) {
          return PostponeDialog(
            selection: selection,
          );
        });
    if (updatedVerses != null) {
      versesBloc.add(EntitiesUpdated(updatedVerses, persist: false));
      undoBloc.add(
          UndoAdded([Undo(EntityAction.update, selection.values.toList())]));
    }
  }
}
