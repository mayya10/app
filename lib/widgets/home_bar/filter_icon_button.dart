import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/blocs.dart';
import '../../screens/screens.dart';

class FilterIconButton extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return

      IconButton(
        tooltip: L10n.of(ctx).t8('Tags.navigation'),
        icon: Builder(builder: (context) {
          final filteredState = context.watch<FilteredVersesBloc>().state;
          final tagsState = context.watch<TaggedVersesBloc>().state;
          return _anyIncluded(filteredState, tagsState)
              ? _iconIncluded(ctx)
              : _anyExcluded(filteredState, tagsState)
              ? _iconExcluded(ctx)
              : _iconDefault(ctx);
        }),
        onPressed: () => HomeScreen.scaffoldKey.currentState!.openEndDrawer(),
      );
  }

  bool _anyIncluded(
      FilteredVersesState filteredState, TaggedVersesState tagsState) {
    if (filteredState is FilteredVersesSuccess) {
        if(filteredState.activeFilter?.query.isNotEmpty == true) return false;
        if(filteredState.activeFilter?.reviewedToday == true) return true;
    }
    if (tagsState is TaggedVersesSuccess &&
        tagsState.tags.any((tag) => tag.included == true && !tag.deleted)) {
      return true;
    }
    return false;
  }

  bool _anyExcluded(
      FilteredVersesState filteredState, TaggedVersesState tagsState) {
    if (filteredState is FilteredVersesSuccess) {
      if (filteredState.activeFilter?.query.isNotEmpty == true) return false;
      if (filteredState.activeFilter?.reviewedToday == false) return true;
    }
    if (tagsState is TaggedVersesSuccess &&
        tagsState.tags.any((tag) => tag.included == false && !tag.deleted)) {
      return true;
    }
    return false;
  }

  Icon _iconDefault(BuildContext ctx) {
    return Icon(Icons.visibility_outlined);
  }

  Icon _iconExcluded(BuildContext ctx) {
    return Icon(Icons.visibility_off_outlined,
        color: AppColorScheme.appBar.secondary);
  }

  Icon _iconIncluded(BuildContext ctx) {
    return Icon(Icons.visibility, color: AppColorScheme.appBar.secondary);
  }
}
