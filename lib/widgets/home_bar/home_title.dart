import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:repository/repository.dart';

import '../../blocs/current_account/current_account.dart';

class HomeTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) =>
      BlocBuilder<AccountsBloc, EntitiesState<AccountEntity>>(
          builder: (ctx, accountsState) {
        return BlocBuilder<FilteredVersesBloc, FilteredVersesState>(
            builder: (ctx, filteredState) {
          if (filteredState is FilteredVersesSuccess &&
              filteredState.activeFilter!.query.isNotEmpty) {
            return Text('«${filteredState.activeFilter!.query}»');
          } else if (di.get<CurrentAccount>().state != null) {
            return Text(di.get<CurrentAccount>().state!.name);
          } else {
            return Text(L10n.of(ctx).t8('App.name'));
          }
        });
      });
}
