import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/undo/undo_bloc.dart';

import '../../blocs/blocs.dart';
import '../../models/models.dart';

class UndoButton extends StatelessWidget {
  final Function? onUndo;

  const UndoButton({super.key, this.onUndo});

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<UndoBloc, List<Undo>?>(
        builder: (ctx, undos) => undos == null
            ? const SizedBox.shrink()
            : IconButton(
                tooltip: L10n.of(ctx).t8('Button.undo'),
                icon: const Icon(Icons.undo_rounded),
                onPressed: () {
                  BlocProvider.of<UndoBloc>(ctx).add(UndoApplied());
                  if(onUndo != null) onUndo!();
                },
              ));
  }
}
