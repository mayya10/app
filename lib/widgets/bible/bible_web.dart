import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/models/bibles/bibles.dart';
import 'package:remem_me/theme.dart';
import 'package:remem_me/widgets/web_frame/web_frame.dart';

import 'bible.dart';

class BibleWeb extends StatelessWidget {
  final BibleQuery query;

  const BibleWeb(this.query, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Dialog(
        child: Scaffold(
      body: WebFrame(query.uri.toString()),
      bottomNavigationBar: BottomAppBar(
        child: Container(
            height: 48.0,
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            alignment: Alignment.centerLeft,
            child: Text(query.version.site!.name!,
                style: AppTheme.dark.textTheme.titleLarge!.copyWith(
                  fontSize: 20.0,
                  fontWeight: FontWeight.w600,
                  color: Flat.amber,
                ))),
        color: Colors.black87,
      ),
      extendBody: true,
      floatingActionButton: PasteFab(query: query),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    ));
  }
}

