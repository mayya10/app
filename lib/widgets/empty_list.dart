import 'package:flutter/material.dart';

class EmptyList extends StatelessWidget {
  final IconData icon;

  EmptyList({Key? key, required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Center(
      child: Icon(
        icon,
        color: Theme.of(ctx).textTheme.bodyText1!.color!.withOpacity(0.1),
        size: 256.0,
      ),
    );
  }
}
