import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/routers/routers.dart';
import 'package:remem_me/widgets/settings/reminder_setting.dart';
import 'package:repository/repository.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../services/rating_service.dart';
import '../../utils/utils.dart';

class NavigationMenu extends StatelessWidget {
  final bool poppable;

  const NavigationMenu({Key? key, this.poppable = false}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    final nav = BlocProvider.of<NavigationCubit>(ctx);
    return BlocBuilder<NavigationCubit, AppRoutePath>(
        builder: (BuildContext ctx, state) {
      final auth = BlocProvider.of<AuthBloc>(ctx).state;
      return Column(children: [
        ListTile(
          leading: const Icon(Icons.home_rounded),
          title: Text(L10n.of(ctx).t8('Verses.title')),
          selected: state is HomePath,
          onTap: () async {
            nav.go(HomePath());
            if (poppable) Navigator.pop(ctx);
          },
        ),
        if (auth is LoginSuccess)
          ListTile(
            leading: const Icon(Icons.new_label_rounded),
            title: Text(L10n.of(ctx).t8('Tags.edit')),
            selected: state is TagsPath,
            onTap: () async {
              nav.go(TagsPath());
              if (poppable) Navigator.pop(ctx);
            },
          ),
        ListTile(
          leading: const Icon(Icons.collections_bookmark_rounded),
          title: Text(L10n.of(ctx).t8('Collections.title')),
          selected: state is CollectionsPath,
          onTap: () async {
            nav.go(CollectionsPath());
            if (poppable) Navigator.pop(ctx);
          },
        ),
        if (auth is LoginSuccess)
          ListTile(
            leading: const Icon(Icons.trending_up_rounded),
            title: Text(L10n.of(ctx).t8('Scores.title')),
            selected: state is ScoresPath,
            onTap: () async {
              nav.go(ScoresPath());
              if (poppable) Navigator.pop(ctx);
            },
          ),
        if (auth is LoginSuccess)
          ListTile(
            leading: const Icon(Icons.folder_shared_rounded),
            title: Text(L10n.of(ctx).t8('Account.title')),
            selected: state is AccountEditPath,
            onTap: () {
              nav.go(AccountEditPath(di.get<CurrentAccount>().state!.id));
              if (poppable) Navigator.pop(ctx);
            },
          ),
        if (auth is LoginSuccess && kIsWeb)
          ListTile(
            leading: const Icon(Icons.restore_from_trash_rounded),
            title: Text(L10n.of(ctx).t8('Verses.restore')),
            selected: state is VerseBinPath,
            onTap: () async {
              nav.go(VerseBinPath());
              if (poppable) Navigator.pop(ctx);
            },
          ),
        if (!kIsWeb) ReminderSetting(),
        ListTile(
          leading: const Icon(Icons.help_outline_rounded),
          title: Text(L10n.of(ctx).t8('Documentation.title')),
          onTap: () async {
            await launchDocumentation(ctx)();
            if (poppable) Navigator.pop(ctx);
          },
        ),
        if (RatingService().isReadyForRating())
          ListTile(
            leading: const Icon(Icons.star_rounded),
            title: Text(L10n.of(ctx).t8('Rating.title')),
            onTap: () async {
              RatingService().markRatingDone();
              final uri = await RatingService().uriStore();
              if (await canLaunchUrl(uri)) await launchUrl(uri);
              if (poppable) Navigator.pop(ctx);
            },
          ),
        if (RatingService().isReadyForRating() &&
            !RatingService().isRatingDone())
          ListTile(
              subtitle: Text(
            L10n.of(ctx).t8('Rating.message'),
          )),
        if (kDebugMode)
          ListTile(
            leading: const Icon(Icons.style_rounded),
            title: const Text('Styles'),
            onTap: () async {
              nav.go(StylesPath());
              if (poppable) Navigator.pop(ctx);
            },
          ),
      ]);
    });
  }
}
