import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/routers/routers.dart';
import 'package:remem_me/services/legacy_service.dart';
import 'package:remem_me/widgets/navigation/navigation.dart';
import 'package:remem_me/widgets/user/user.dart';
import 'package:repository/repository.dart';


class NavigationAccounts extends StatelessWidget implements Navigation {
  final bool poppable;

  const NavigationAccounts({Key? key, this.poppable = true}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    final nav = BlocProvider.of<NavigationCubit>(ctx);
    final accountsState = ctx.watch<AccountsBloc>().state;
    final auth = ctx.watch<AuthBloc>().state;
    final currentAccount = ctx.watch<CurrentAccount>().state;

    final tiles = [
      if (accountsState is EntitiesLoadSuccess<AccountEntity>) ...[
        ...accountsState.entities.map((AccountEntity entity) => ListTile(
              leading: Icon(Icons.folder_shared_rounded),
              title: Text(entity.name),
            selected: currentAccount?.id == entity.id,
              onTap: () async {
                await (BlocProvider.of<CurrentAccount>(ctx)).setEntity(entity);
                nav.go(HomePath());
                if (poppable) Navigator.pop(ctx);
              },
            )),
        ListTile(
          leading: Icon(Icons.drive_folder_upload),
          title: Text(L10n.of(ctx).t8('Account.attach')),
          onTap: () {
            _attachAccount(ctx);
          },
        ),
        ListTile(
          leading: Icon(Icons.add),
          title: Text(L10n.of(ctx).t8('Account.create')),
          selected: nav.state is AccountCreatePath,
          onTap: () async {
            nav.go(AccountCreatePath());
            if (poppable) Navigator.pop(ctx);
          },
        ),
        if (kIsWeb)
          ListTile(
            leading: Icon(Icons.restore_from_trash_rounded),
            title: Text(L10n.of(ctx).t8('Account.restore')),
            selected: nav.state is AccountBinPath,
            onTap: () async {
              nav.go(AccountBinPath());
              if (poppable) Navigator.pop(ctx);
            },
          )
      ],
      if (auth is LoginSuccess) ...[
        ListTile(
            leading: Icon(Icons.account_circle_rounded),
            title: Text(L10n.of(ctx).t8('User.title')),
            onTap: () {
              _editUser(ctx);
            }),
        ListTile(
            leading: Icon(Icons.logout),
            title: Text(L10n.of(ctx).t8('Auth.logout')),
            onTap: () {
              _logOut(ctx);
              if (poppable) Navigator.pop(ctx);
            }),
      ] else
        ListTile(
            leading: Icon(Icons.login_rounded),
            title: Text(L10n.of(ctx).t8('Auth.login')),
            onTap: () {
              nav.go(HomePath());
              if (poppable) Navigator.pop(ctx);
            }),
    ];
    return Column(children: tiles);
  }

  void _logOut(BuildContext ctx) {
    if (kIsWeb) {
      BlocProvider.of<AuthBloc>(ctx).add(LoggedOut());
    } else {
      BlocProvider.of<ReplicationBloc>(ctx).add(RplLoggedOut());
    }
    BlocProvider.of<NavigationCubit>(ctx).go(HomePath());
  }

  Future<void> _attachAccount(BuildContext ctx) async {
    Map<String, String> initialCredentials = const {};
    Map<String, List<String>>? errors = const {};
    while (errors != null) {
      errors = await (showDialog<Map<String, List<String>>>(
          barrierDismissible: false,
          context: ctx,
          builder: (BuildContext context) => Dialog(
                  child: UserForm(
                initialValues: initialCredentials,
                errors: errors,
                hasRepeatPassword: false,
                hasEmailAsUsername: false,
                titleL10nKey: 'User.login.title',
                submitL10nKey: 'User.login.submit',
                onCancel: () => Navigator.of(ctx, rootNavigator: true).pop(),
                onSubmit: (Map<String, String> credentials) async {
                  initialCredentials = credentials;
                  try {
                    final account = await LegacyService().attachAccount(
                        name: credentials['name'],
                        password: credentials['password']);
                    BlocProvider.of<AccountsBloc>(ctx).add(
                        // todo: Looking up a deactivated widget's ancestor is unsafe.
                        EntitiesAdded<AccountEntity>([account],
                            persist: false));
                    Navigator.of(ctx, rootNavigator: true).pop();
                    if (poppable) Navigator.pop(ctx);
                  } on MessageException catch (e) {
                    Navigator.of(ctx, rootNavigator: true).pop(e.messages);
                  }
                },
              ))));
    }
  }

  Future<void> _editUser(BuildContext ctx) async {
    Map<String, String> initialCredentials = {
      'email': Settings().get(Settings.USER_EMAIL)!
    };
    Map<String, List<String>>? errors = const {};
    while (errors != null) {
      errors = await (showDialog<Map<String, List<String>>>(
          barrierDismissible: false,
          context: ctx,
          builder: (BuildContext context) => Dialog(
                  child: UserForm(
                initialValues: initialCredentials,
                errors: errors,
                titleL10nKey: 'User.title',
                submitL10nKey: 'Button.save',
                onCancel: () => Navigator.of(ctx, rootNavigator: true).pop(),
                onSubmit: (Map<String, String> credentials) async {
                  initialCredentials = credentials;
                  try {
                    final stored = await AuthService().changeEmail(
                        email: credentials['email']!,
                        password: credentials['password']!);
                    Settings().put(Settings.USER_EMAIL, stored['email']!);
                    Navigator.of(ctx, rootNavigator: true).pop();
                  } on MessageException catch (e) {
                    Navigator.of(ctx, rootNavigator: true).pop(e.messages);
                  }
                },
                onDelete: (Map<String, String> credentials) async {
                  initialCredentials = credentials;
                  try {
                    await AuthService()
                        .deleteUser(password: credentials['password']!);
                    Navigator.of(ctx, rootNavigator: true).pop();
                    _logOut(ctx);
                    MessageService().info(L10n.of(ctx).t8('User.deleted'));
                  } on MessageException catch (e) {
                    Navigator.of(ctx, rootNavigator: true).pop(e.messages);
                  }
                },
              ))));
    }
  }
}
