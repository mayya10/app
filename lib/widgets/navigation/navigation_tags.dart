import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/widgets/file/file_menu.dart';
import 'package:remem_me/widgets/loading_indicator.dart';
import 'package:repository/repository.dart';

import '../../utils/utils.dart';
import '../box/box.dart';
import '../search/search.dart';

class NavigationTags extends StatelessWidget {
  final bool popping;

  const NavigationTags({Key? key, this.popping = true}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Drawer(
        elevation: 0,
        child: BlocBuilder<TaggedVersesBloc, TaggedVersesState>(
            builder: (context, state) {
          if (state is TaggedVersesSuccess) {
            final items = [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  IconButton(
                    tooltip: L10n.of(ctx).t8('Documentation.title'),
                    onPressed:
                        launchDocumentation(ctx, path: 'labels/'),
                    icon: const Icon(Icons.help_outline_rounded),
                  ),
                  if (!kIsWeb) FileMenuButton(onDo: () => Navigator.pop(ctx)),
                ],
              ),
              const BoxedToggle(),
              _displayAll(ctx, context, state),
              _displayUntagged(ctx, context, state),
              const ReviewedToday(),
            ];
            items.addAll(state.tags.map((tag) => ListTile(
                  leading: _leadingIcon(tag, state),
                  title: Text(tag.text),
                  trailing: Text(tag.size.toString()),
                  onTap: () {
                    (BlocProvider.of<TagsBloc>(context)).add(
                        EntitiesUpdated<TagEntity>([_toggleTap(tag)],
                            wait: false));
                  },
                  onLongPress: () {
                    (BlocProvider.of<TagsBloc>(context)).add(
                        EntitiesUpdated<TagEntity>([_toggleLongPress(tag)],
                            wait: false));
                  },
                )));
            return Stack(
              children: [
                ListView(children: items),
                if (state.isUpdating) LoadingIndicator(),
              ],
            );
          } else {
            return const SizedBox.shrink();
          }
        }));
  }

  ListTile _displayUntagged(
      BuildContext ctx, BuildContext context, TaggedVersesSuccess state) {
    return ListTile(
      leading: const Icon(Icons.label_off_outlined),
      title: Text(L10n.of(ctx).t8('Verses.untagged')),
      onTap: () {
        (BlocProvider.of<FilteredVersesBloc>(context))
            .add(const ReviewedTodayFilterUpdated(null));
        (BlocProvider.of<TagsBloc>(context)).add(EntitiesUpdated<TagEntity>(
            _tagsToExclude(state.tags),
            wait: false));
      },
    );
  }

  ListTile _displayAll(
      BuildContext ctx, BuildContext context, TaggedVersesSuccess state) {
    return ListTile(
      leading: const Icon(Icons.apps_outlined),
      title: Text(L10n.of(ctx).t8('Verses.all')),
      onTap: () {
        (BlocProvider.of<FilteredVersesBloc>(context))
            .add(const ReviewedTodayFilterUpdated(null));
        (BlocProvider.of<TagsBloc>(context)).add(
            EntitiesUpdated<TagEntity>(_tagsToClear(state.tags), wait: false));
      },
    );
  }

  Widget _leadingIcon(Tag tag, TaggedVersesSuccess state) {
    return Icon(tag.included != null
        ? tag.included!
            ? Icons.visibility_rounded
            : Icons.visibility_off_outlined
        : Icons.visibility_outlined);
  }

  Tag _toggleLongPress(Tag tag) {
    return tag.copyWith(included: false);
  }

  Tag _toggleTap(Tag tag) {
    if (tag.included == null) {
      return tag.copyWith(included: true);
    } else {
      return tag.copyWith(nullValues: ['included']);
    }
  }

  List<Tag> _tagsToClear(List<Tag> tags) {
    return tags
        .where((tag) => tag.included != null)
        .map((tag) => tag.copyWith(nullValues: ['included']))
        .toList();
  }

  List<Tag> _tagsToExclude(List<Tag> tags) {
    return tags
        .where((tag) => tag.included != false)
        .map((tag) => tag.copyWith(included: false))
        .toList();
  }
}
