import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:repository/repository.dart';

import '../../blocs/current_account/current_account.dart';
import '../../models/account.dart';
import '../widgets.dart';

class NavDrawer extends StatefulWidget implements Navigation {
  final bool poppable;
  final NavigationCubit nav;

  const NavDrawer({Key? key, this.poppable = true, required this.nav})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _NavDrawerState();
}

class _NavDrawerState extends State<NavDrawer> {
  bool isInAccountMode = false;

  @override
  Widget build(BuildContext ctx) {
    final items = <Widget>[];
    items.add(_header(ctx));
    if (isInAccountMode) {
      items.add(NavigationAccounts(
        poppable: widget.poppable,
      ));
    } else {
      items.add(NavigationMenu(poppable: widget.poppable));
    }
    return Drawer(
      elevation: 0,
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: Column(children: [
        Expanded(
            child: ListView(
                // Important: Remove any padding from the ListView.
                padding: EdgeInsets.zero,
                children: items)),
        _appVersion(ctx),
      ]),
    );
  }

  Widget _header(BuildContext ctx) {
    return BlocBuilder<AccountsBloc, EntitiesState<AccountEntity>>(
        builder: (context, state) => UserAccountsDrawerHeader(
              accountName: Row(children: [
                Expanded(
                    child: BlocBuilder<CurrentAccount, Account?>(
                        builder: (context, account) => Text(
                            account != null
                                ? account.name
                                : L10n.of(ctx).t8('Account.none')))),
                if (!kIsWeb)
                  ReplicationButton(onDone: () {
                    if (widget.poppable) Navigator.pop(ctx);
                  }),
              ]),
              accountEmail: null,
              onDetailsPressed: () =>
                  setState(() => isInAccountMode = !isInAccountMode),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/img/bg_title.png'),
                    fit: BoxFit.cover,
                    alignment: Alignment.topCenter),
              ),
            ));
  }

  Widget _appVersion(BuildContext ctx) {
    return FutureBuilder(
      future: PackageInfo.fromPlatform(),
      builder: (BuildContext context, AsyncSnapshot<PackageInfo> snapshot) {
        final info = snapshot.data;
        return info != null
            ? Text('${L10n.of(ctx).t8('App.name')} ${info.version} (${info.buildNumber})')
            : SizedBox.shrink();
      },
    );
  }
}
