import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/services/file_service.dart';
import 'package:repository/repository.dart';

import '../../models/models.dart';

enum FileMenuAction { export, import }

class FileMenuButton extends StatelessWidget {
  final void Function()? onDo;

  const FileMenuButton({Key? key, this.onDo}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return PopupMenuButton<FileMenuAction>(
      icon: const Icon(Icons.more_vert_rounded),
      onSelected: (FileMenuAction action) => _do(ctx, action),
      itemBuilder: (BuildContext context) => <PopupMenuEntry<FileMenuAction>>[
        PopupMenuItem<FileMenuAction>(
          value: FileMenuAction.export,
          child: Text(L10n.of(ctx).t8('File.export')),
        ),
        PopupMenuItem<FileMenuAction>(
          value: FileMenuAction.import,
          child: Text(L10n.of(ctx).t8('File.import')),
        ),
      ],
    );
  }

  void _do(BuildContext ctx, FileMenuAction action) async {
    if (onDo != null) onDo!();
    switch (action) {
      case FileMenuAction.export:
        _export(ctx);
        break;
      case FileMenuAction.import:
        _import(ctx);
        break;
    }
  }

  void _export(BuildContext ctx) async {
    final tabState = di.get<TabBloc>().state;
    final bloc = OrderedVersesBloc.from(tabState);
    final verses = (bloc.state as OrderSuccess).verses;
    await FileService().writeCsv(verses);
    MessageService().info(
        L10n.current.t8('File.export.success', [verses.length.toString()]));
  }

  Future<void> _import(BuildContext ctx) async {
    final tagsRepository = RepositoryProvider.of<TagsRepository>(ctx);
    final tagsBloc = BlocProvider.of<TagsBloc>(ctx);
    final versesBloc = BlocProvider.of<VersesBloc>(ctx);
    final undoBloc = BlocProvider.of<UndoBloc>(ctx);
    final newTags = <TagEntity>[];

    final verses = await FileService().readCsv(newTags);
    if (verses.isNotEmpty) {
      if (newTags.isNotEmpty) {
        await tagsRepository.createList(newTags);
        tagsBloc.add(EntitiesAdded<TagEntity>(newTags, persist: false));
      }
      versesBloc.add(EntitiesAdded<VerseEntity>(verses));
      undoBloc.add(UndoAdded([
        Undo(EntityAction.create, verses),
        if (newTags.isNotEmpty) Undo(EntityAction.create, newTags),
      ]));
      // undoBloc.add(UndoCleared());
      MessageService().info(
          L10n.current.t8('File.import.success', [verses.length.toString()]));
    } else {
      MessageService().info(
          L10n.current.t8('File.import.empty', [verses.length.toString()]));
    }
  }
}
