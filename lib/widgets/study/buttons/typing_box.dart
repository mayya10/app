import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';

import '../../../theme.dart';

class TypingBox extends StatefulWidget {
  
  @override
  State<TypingBox> createState() => _TypingBoxState();
}

class _TypingBoxState extends State<TypingBox> with WidgetsBindingObserver {
  late FocusNode _focusNode;
  late TextEditingController _typingController;

  @override
  void initState() {
    super.initState();
    _typingController = TextEditingController();
    _focusNode = FocusNode();
    _focusNode.requestFocus();
  }

  @override
  void dispose() {
    _focusNode.ancestors.first.requestFocus();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    print('state = $state');
  }


  @override
  Widget build(BuildContext ctx) {
    return Stack(
      children: [
        _display(ctx),
        Positioned.fill(child: _listener(ctx)),
      ],
    );
  }

  Widget _display(BuildContext ctx) {
    return BlocBuilder<StudyBloc, StudyState>(
        builder: (BuildContext ctx, StudyState state) {
      final _state = state as Typing;
      return Container(
          padding: EdgeInsets.only(left: 8, right: 8),
          constraints: BoxConstraints(minWidth: 32),
          decoration: BoxDecoration(
            color: _state.isCorrect == null
                ? AppTheme.dark.floatingActionButtonTheme.backgroundColor
                : _state.isCorrect!
                    ? Flat.amber
                    : Flat.indigo,
            borderRadius: BorderRadius.all(Radius.circular(8)),
          ),
          height: 32,
          child: Center(
              child: Text(
            _state.letters ?? '',
            style: AppTheme.dark.textTheme.button!.copyWith(
                fontSize: 18,
                fontWeight: FontWeight.w400,
                fontFamily: di.get<CurrentAccount>().font),
          )));
    });
  }

  Widget _listener(BuildContext ctx) {
    return Opacity(
      opacity: 0.0,
      child: TextField(
        controller: _typingController,
        focusNode: _focusNode,
        showCursor: false,
        enableSuggestions: false,
        enableInteractiveSelection: false,
        autocorrect: false,
        keyboardType: TextInputType.text,
        onChanged: (value) {
          _typingController.clear();
          if(value.trim().isNotEmpty) {
            BlocProvider.of<StudyBloc>(ctx).add(Typed(letters: value));
          }
        },
      ),
    );
  }
}
