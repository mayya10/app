import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/study/study.dart';

import '../study.dart';

class HintButton extends StudyButton {
  final Function() onPressed;

  const HintButton(
      {Key? key,
      bool small = false,
      required this.onPressed})
      : super(key: key, small: small);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StudyBloc, StudyState>(
        builder: (BuildContext ctx, StudyState state) {
      final lineUp = state as LineUp;
      return IconButton(
          tooltip: '${L10n.of(ctx).t8('Study.hint')} ${kIsWeb ? '🈁' : '␣'}',
          padding: padding(),
          onPressed: lineUp.isComplete ? null : this.onPressed, // () => nextHint(ctx, lineUp),
          icon: Icon(hintIcon(ctx, lineUp)));
    });
  }

  IconData hintIcon(BuildContext ctx, LineUp state) {
    final p = state.placeholders != null, i = state.initials != null;
    if (p && i) return Icons.format_underline_rounded;
    if (p && !i) return Icons.minimize_rounded;
    if (!p && i) return Icons.title_outlined;
    return Icons.tips_and_updates_rounded; // !p && !i
  }

/*  void nextHint(BuildContext ctx, LineUp lineUp) {
    if (lineUp.isComplete) return; // RETURN
    BlocProvider.of<StudyBloc>(ctx).add(StudyHinted(
        hasPlaceholders: ifPlaceholders(lineUp),
        hasInitials: ifInitials(lineUp)));
  }*/
}
