import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';

import 'study.dart';

typedef AnimationCallback = void Function(FutureOr<dynamic> Function(void));

class StudyAnimator extends StatefulWidget {
  StudyAnimator({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return StudyAnimatorState();
  }
}

class StudyAnimatorState extends State<StudyAnimator>
    with SingleTickerProviderStateMixin {
  late AnimationController _slipController;

  @override
  void initState() {
    super.initState();
    _slipController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 100))
          ..value = 1.0
          ..reverse();
  }

  @override
  void dispose() {
    _slipController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _slipController,
        child: _control(),
        builder: (BuildContext context, Widget? child) {
          return _slipper(child!);
        });
  }

  Widget _control() {
    return BlocBuilder<StudyBloc, StudyState>(builder: (ctx, state) {
      switch (state.runtimeType) {
        case Obfuscation:
          return ObfuscationControl(onIn: slipIn, onOut: slipOut);
        case Puzzle:
          return PuzzleControl(onIn: slipIn, onOut: slipOut);
        case LineUp:
          return LineUpControl(onIn: slipIn, onOut: slipOut);
        case Typing:
          return TypingControl(onIn: slipIn, onOut: slipOut);
        default:
          return StudyControl(onIn: slipIn, onOut: slipOut);
      }
    });
  }

  void slipIn(FutureOr Function(void) onValue) {
    _slipController.reverse().then(onValue);
  }

  void slipOut(FutureOr Function(void) onValue) {
    _slipController.forward().then(onValue);
  }

  Animation<double> get _slipProgress {
    return Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(
        new CurvedAnimation(parent: _slipController, curve: Curves.easeIn));
  }

  Widget _slipper(Widget child) {
    return Opacity(
        opacity: 1.0 - _slipProgress.value,
        child: Transform.translate(
          offset: Offset(0.0, _slipProgress.value * 10.0),
          child: child,
        ));
  }
}
