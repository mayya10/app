import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/study/study.dart';

import '../study.dart';

class ObfuscationControl extends StudyControl<Obfuscation> {
  ObfuscationControl(
      {required AnimationCallback onIn, required AnimationCallback onOut})
      : super(onIn: onIn, onOut: onOut);

  @override
  KeyEventResult keyPressed(
      KeyDownEvent event, BuildContext ctx, Obfuscation state) {
    if (event.logicalKey == LogicalKeyboardKey.space) {
      state.isComplete ? menu(ctx) : obfuscate(ctx);
      return KeyEventResult.handled;
    }
    return KeyEventResult.ignored;
  }

  @override
  List<Widget> buttons(BuildContext ctx, Obfuscation state) {
    if (state.isComplete) {
      return [
        IconButton(
            tooltip: '${L10n.of(ctx).t8('Button.menu')} ${kIsWeb ? '🈁' : '␣'}',
            onPressed: () => menu(ctx),
            icon: Icon(Icons.more_horiz_rounded)),
      ];
    } else {
      return [
        IconButton(
            tooltip: '${L10n.of(ctx).t8('Study.obfuscate')} ${kIsWeb ? '🈁' : '␣'}',
            onPressed: () => obfuscate(ctx),
            icon: Icon(Icons.wb_cloudy_rounded)),
      ];
    }
  }

  void obfuscate(BuildContext ctx) =>
      BlocProvider.of<StudyBloc>(ctx).add(Obfuscated());
}
