import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/widgets/study/study.dart';

class PuzzleControl extends StudyControl<Puzzle> {
  PuzzleControl(
      {required AnimationCallback onIn, required AnimationCallback onOut})
      : super(onIn: onIn, onOut: onOut);

  @override
  Widget bar(BuildContext ctx, Puzzle state) {
    return ListView(
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      children: buttons(ctx, state),
    );
  }

  @override
  List<Widget> buttons(BuildContext ctx, Puzzle state) {
    final indices =
        Iterable<int>.generate(state.choice!.options.length).toList();

    return indices
        .map((i) => Padding(
            padding: EdgeInsets.symmetric(vertical: 1, horizontal: 0.5),
            child: Container(
                clipBehavior: Clip.hardEdge,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color:
                      Theme.of(ctx).floatingActionButtonTheme.backgroundColor,
                ),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                      highlightColor: i == state.choice!.solution
                          ? FlatDark.amber.withOpacity(0.5)
                          : FlatDark.indigo.withOpacity(0.5),
                      splashColor: i == state.choice!.solution
                          ? Flat.amber.withOpacity(0.5)
                          : Flat.indigo.withOpacity(0.5),
                      onTap: () {
                        final isCorrect = i == state.choice!.solution;
                        if (isCorrect) {
                          onOut((_) => BlocProvider.of<StudyBloc>(ctx)
                              .add(Puzzled(true)));
                        } else {
                          BlocProvider.of<StudyBloc>(ctx).add(Puzzled(false));
                        }
                      },
                      child: Container(
                          constraints: BoxConstraints(minWidth: 56),
                          height: double.infinity,
                          padding: EdgeInsets.all(8),
                          alignment: Alignment.center,
                          child: Text(state.choice!.options[i],
                              style: Theme.of(ctx)
                                  .textTheme
                                  .titleMedium!
                                  .copyWith(
                                      fontFamily:
                                          di.get<CurrentAccount>().font)))),
                ))))
        .toList();
  }
}
