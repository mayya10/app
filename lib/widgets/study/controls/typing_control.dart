import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/study/study.dart';
import 'package:remem_me/widgets/study/study.dart';

class TypingControl extends StudyControl<Typing> {
  TypingControl(
      {required AnimationCallback onIn, required AnimationCallback onOut})
      : super(
            onIn: onIn,
            onOut: onOut,
            height: 32,
            mainAxisAlignment: MainAxisAlignment.spaceBetween);

  @override
  KeyEventResult keyPressed(
      KeyDownEvent event, BuildContext ctx, Typing state) {
    if (event.logicalKey == LogicalKeyboardKey.backspace) {
      BlocProvider.of<StudyBloc>(ctx).add(Retracted());
      return KeyEventResult.handled;
    } else if (event.logicalKey == LogicalKeyboardKey.space &&
        !state.isComplete) {
      nextHint(ctx, state);
      return KeyEventResult.handled;
    } else if (event.logicalKey == LogicalKeyboardKey.space &&
        state.isComplete) {
      menu(ctx);
      return KeyEventResult.handled;
    }
    return KeyEventResult.ignored;
  }

  @override
  List<Widget> buttons(BuildContext ctx, LineUp state) {
    return [
      HintButton(onPressed: () => nextHint(ctx, state)),
      state.isComplete ? MenuButton(onPressed: () => menu(ctx)) : TypingBox(),
      RetractButton(small: true),
    ];
  }

  void nextHint(BuildContext ctx, LineUp state) {
    final p = state.placeholders != null, i = state.initials != null;
    BlocProvider.of<StudyBloc>(ctx)
        .add(StudyHinted(hasPlaceholders: !i, hasInitials: !i && p));
  }
}
