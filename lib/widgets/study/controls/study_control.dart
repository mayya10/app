import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/study/study.dart';

import '../study.dart';

class StudyControl<T extends Study> extends StatelessWidget {
  final AnimationCallback onIn;
  final AnimationCallback onOut;
  final double height;
  final MainAxisAlignment mainAxisAlignment;

  const StudyControl(
      {Key? key,
      required this.onOut,
      required this.onIn,
      this.height = 56,
      this.mainAxisAlignment = MainAxisAlignment.spaceAround})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return BlocConsumer<StudyBloc, StudyState>(
        listener: (BuildContext ctx, StudyState state) => onIn((_) {}),
        builder: (BuildContext ctx, StudyState state) {
          return Container(
              height: height,
              alignment: Alignment.center,
              child: state is Study ? bar(ctx, state as T) : SizedBox.shrink());
        });
  }

  Widget bar(BuildContext ctx, T state) {
    return Focus(
      autofocus: true,
      onKeyEvent: (node, event) {
        if (event is KeyDownEvent) {
          return keyPressed(event, ctx, state);
        }
        return KeyEventResult.ignored;
      },
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: mainAxisAlignment,
        mainAxisSize: MainAxisSize.max,
        children: buttons(ctx, state),
      ),
    );
  }

  KeyEventResult keyPressed(KeyDownEvent event, BuildContext ctx, T state) {
    return KeyEventResult.ignored;
  }

  List<Widget> buttons(BuildContext ctx, T state) {
    return [
      IconButton(
          tooltip: L10n.of(ctx).t8('Study.obfuscate'),
          onPressed: () =>
              onOut((_) => BlocProvider.of<StudyBloc>(ctx).add(Obfuscated())),
          icon: Icon(Icons.wb_cloudy_rounded)),
      IconButton(
          tooltip: L10n.of(ctx).t8('Study.puzzle'),
          onPressed: () =>
              onOut((_) => BlocProvider.of<StudyBloc>(ctx).add(Puzzled())),
          icon: Icon(Icons.extension_rounded)),
      IconButton(
          tooltip: L10n.of(ctx).t8('Study.lineUp'),
          onPressed: () =>
              onOut((_) => BlocProvider.of<StudyBloc>(ctx).add(LinedUp())),
          icon: Icon(Icons.subject_rounded)),
      IconButton(
          tooltip: L10n.of(ctx).t8('Study.typing'),
          onPressed: () =>
              onOut((_) => BlocProvider.of<StudyBloc>(ctx).add(Typed())),
          icon: Icon(Icons.keyboard_rounded)),
    ];
  }

  void menu(BuildContext ctx) {
    onOut((_) => BlocProvider.of<StudyBloc>(ctx).add(StudyStarted()));
  }
}
