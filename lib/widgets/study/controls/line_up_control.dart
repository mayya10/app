import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/study/study.dart';

import '../../../blocs/blocs.dart';
import '../study.dart';

class LineUpControl extends StudyControl<LineUp> {
  LineUpControl(
      {required AnimationCallback onIn, required AnimationCallback onOut})
      : super(onIn: onIn, onOut: onOut);


  @override
  KeyEventResult keyPressed(KeyDownEvent event, BuildContext ctx, LineUp state) {
    if (event.logicalKey == LogicalKeyboardKey.backspace) {
      BlocProvider.of<StudyBloc>(ctx).add(Retracted());
      return KeyEventResult.handled;
    } else if (event.logicalKey == LogicalKeyboardKey.space && !state.isComplete) {
      nextHint(ctx, state);
      return KeyEventResult.handled;
    } else if (event.logicalKey == LogicalKeyboardKey.space && state.isComplete) {
      menu(ctx);
      return KeyEventResult.handled;
    } else if (event.logicalKey == LogicalKeyboardKey.arrowRight) {
      BlocProvider.of<StudyBloc>(ctx).add(LinedUp(byLine: false));
      return KeyEventResult.handled;
    } else if (event.logicalKey == LogicalKeyboardKey.arrowDown) {
      BlocProvider.of<StudyBloc>(ctx).add(LinedUp(byLine: true));
      return KeyEventResult.handled;
    }
    return KeyEventResult.ignored;
  }


  @override
  List<Widget> buttons(BuildContext ctx, LineUp state) {
    return [
      HintButton(onPressed: () => nextHint(ctx, state)),
      Expanded(
          child: state.isComplete
              ? MenuButton(onPressed: () => menu(ctx))
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _btnNextWord(ctx),
                    _btnNextLine(ctx),
                  ],
                )),
      RetractButton(),
    ];
  }

  IconButton _btnNextWord(BuildContext ctx) {
    return IconButton(
        tooltip: '${L10n.of(ctx).t8('Study.nextWord')} ${kIsWeb ? '➡️' : '⇨'}',
        onPressed: () {
          BlocProvider.of<StudyBloc>(ctx).add(LinedUp(byLine: false));
        },
        icon: Icon(Icons.plus_one_rounded));
  }

  IconButton _btnNextLine(BuildContext ctx) {
    return IconButton(
        tooltip: '${L10n.of(ctx).t8('Study.nextLine')} ${kIsWeb ? '⬇️️' : '⇩'}',
        onPressed: () {
          BlocProvider.of<StudyBloc>(ctx).add(LinedUp(byLine: true));
        },
        icon: Icon(Icons.playlist_add_rounded));
  }

  void nextHint(BuildContext ctx, LineUp state) {
        final p = state.placeholders != null, i = state.initials != null;
        BlocProvider.of<StudyBloc>(ctx).add(StudyHinted(
            hasPlaceholders: state.placeholders == null,
            hasInitials: p == i &&
                di.get<CurrentAccount>().language.languageCode != 'zh'));
      }

}
