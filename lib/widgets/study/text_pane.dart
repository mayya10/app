import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/blocs/current_account/current_account.dart';
import 'package:remem_me/blocs/study/study.dart';
import 'package:remem_me/models/models.dart';

import 'framer.dart';

class TextPane extends StatefulWidget {
  final StudyState state;

  const TextPane(this.state, {super.key});

  @override
  State<StatefulWidget> createState() {
    return _TextPaneState();
  }
}

class _TextPaneState extends State<TextPane> {
  final ScrollController _scrollController = ScrollController();

  final GlobalKey scrollTo = GlobalKey();

  @override
  void didUpdateWidget(Widget oldWidget) {
    super.didUpdateWidget(oldWidget as TextPane);
    if ([LineUp, Puzzle, Typing].contains(widget.state.runtimeType)) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        final scrollToContext = scrollTo.currentContext;
        if (scrollToContext != null) {
          Scrollable.ensureVisible(scrollToContext,
              duration: const Duration(milliseconds: 400));
        }
      });
    }
  }

  @override
  Widget build(BuildContext ctx) {
    if (widget.state is Study) {
      final framer = Framer(ctx, widget.state as Study);
      List<Widget> wraps = _wraps(framer);
      return Scrollbar(
          thumbVisibility: true,
          controller: _scrollController,
          child: SingleChildScrollView(
              key: const PageStorageKey<String>('study_scroller'),
              controller: _scrollController,
              child: Padding(
                  //key: scrollView,
                  padding: EdgeInsets.all(16.0),
                  child: DefaultTextStyle(
                      style: framer.stylePlain(),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: wraps)))));
    } else {
      return SizedBox.shrink();
    }
  }

  List<Widget> _wraps(Framer framer) {
    final study = widget.state as Study;
    var wraps = <Widget>[];
    for (var i = 0; i < study.lines.length && i <= study.pos.line; i++) {
      var items = <Widget>[];
      for (var j = 0;
          i < study.pos.line && j < study.lines[i].length ||
              i == study.pos.line && j <= study.pos.word;
          j++) {
        items.add(framer.format(Pos(i, j)));
        if (study is LineUp && study.revealed == Pos(i, j) ||
            study is Puzzle && study.pos == Pos(i, j)) {
          items.add(SizedBox.shrink(key: scrollTo));
        }
      }
      wraps.add(Wrap(
        textDirection: di.get<CurrentAccount>().languageDirection,
        children: items,
      ));
      final lineBreaks = '\n'.allMatches(study.lines[i].last).length;
      for (int i = 1; i < lineBreaks; i++) wraps.add(SizedBox(height: 16));
    }
    return wraps;
  }
}
