import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/current_account/current_account.dart';
import 'package:remem_me/blocs/study/study.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/text_service.dart';

typedef FormatCallback = Widget Function(String);

class Framer {
  final BuildContext ctx;
  final Study state;

  Framer(this.ctx, this.state);

  Widget format(Pos pos) {
    final study = state;
    if (study is Obfuscation && study.revealed![pos.line][pos.word]) {
      return _blurShow(study.lines[pos.line][pos.word]);
    } else if (study is Obfuscation &&
        study.obfuscated![pos.line][pos.word]) {
      return _blur(pos);
    } else if ((study is LineUp) &&
        (pos.line > study.revealed.line ||
            pos.line == study.revealed.line &&
                pos.word > study.revealed.word)) {
      return _formatHint(pos);
    } else {
      return _strip(study.lines[pos.line][pos.word], _plain);
    }
  }

  Widget _plain(String word) {
    return _text(word);
  }

  Widget _blur(Pos pos) {
    return InkWell(
        child: ImageFiltered(
            imageFilter: ImageFilter.blur(sigmaX: 6.0, sigmaY: 6.0, tileMode: TileMode.decal),
            child: _text(TextService().stripLinebreaks(state.lines[pos.line][pos.word]))),
        onTap: () => BlocProvider.of<StudyBloc>(ctx).add(Revealed(pos)));
  }

  Widget _blurShow(String word) {
    return _text(TextService().stripLinebreaks(word), style: TextStyle(shadows: _shadows()));
  }

  Widget _underline(String word) {
    return Stack(children: [
      _lineFill(),
      _text(word, style: _styleTransparent()),
    ]);
  }

  Widget _lineShow(String word) {
    return Stack(children: [
      _lineFill(),
      _text(word),
    ]);
  }

  Widget _initial(String word) {
    if (word.isEmpty) return SizedBox.shrink();
    return _lineShow(word.substring(0, 1));
  }

  Widget _initialUnderline(String word) {
    if (word.isEmpty) return SizedBox.shrink();
    return Stack(
        textDirection: di.get<CurrentAccount>().languageDirection,
        children: [
      _lineFill(),
      _text(word, style: _styleTransparent()),
      _text(word.substring(0, 1)),
    ]);
  }

  Widget _formatHint(Pos pos) {  // todo: initial depending on position
    final lineUp = state as LineUp;
    String word = lineUp.lines[pos.line][pos.word];
    final p = pos <= lineUp.placeholders, i = pos <= lineUp.initials;
    if (p && i) return _strip(word, _initialUnderline);
    if (p && !i) return _strip(word, _underline);
    if (!p && i) return _strip(word, _initial);
    return SizedBox.shrink();
  }

  _strip(String word, FormatCallback format) {
    final before = TextService().punctuationBefore(word);
    final after = TextService().punctuationAfter(word);
    return Row(mainAxisSize: MainAxisSize.min, children: [
      if (before > 0) _text(word.substring(0, before)),
      if (before < word.length && after < word.length)
        format(word.substring(before, word.length - after)),
      if (after > 0) _text(TextService()
            .stripLinebreaks(word.substring(word.length - after, word.length))),
    ]);
  }

  Widget _lineFill() {
    return Positioned.fill(
        child: Container(
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
                  color: Theme.of(ctx).colorScheme.primary.withOpacity(0.4),
                  width: 7.0))),
    ));
  }

  TextStyle stylePlain() {
    return Theme.of(ctx).textTheme.titleSmall!;
  }

  TextStyle _styleTransparent() {
    return Theme.of(ctx)
        .textTheme
        .titleSmall!
        .copyWith(color: Colors.transparent);
  }

  TextStyle _styleUnderline() {
    return TextStyle(
      decoration: TextDecoration.underline,
      decorationThickness: 8.0,
      decorationColor: Flat.amber,
    );
  }

  TextStyle _styleUnderlineTransparent() {
    return _styleUnderline().copyWith(color: Colors.transparent);
  }

  List<Shadow> _shadows() {
    return [
      Shadow(
        blurRadius: 10.0,
        color: Theme.of(ctx).textTheme.titleSmall!.color!,
      )
    ];
  }

  Text _text(String value, {TextStyle style = const TextStyle()}) {
    final font = di.get<CurrentAccount>().font;
    return Text(value, style: style.copyWith(fontFamily: font));
  }

  @Deprecated('use blur() instead')
  Widget _blurDeprecated(Study state, Pos pos) {
    return InkWell(
      child: Text(state.lines[pos.line][pos.word],
          style: TextStyle(color: Colors.transparent, shadows: _shadows())),
      onTap: () => BlocProvider.of<StudyBloc>(ctx).add(Revealed(pos)),
    );
  }

  @Deprecated('use underline() instead')
  Widget lineDeprecated(String word) {
    return Text(word, style: _styleUnderlineTransparent());
  }

  @Deprecated('use lineShow() instead')
  Widget lineShowDeprecated(String word) {
    return Stack(children: [
      Text(word, style: _styleUnderlineTransparent()),
      Text(word)
    ]);
  }

  @Deprecated('use initialUnderline() instead')
  Widget initialLineDeprecated(String word) {
    return Stack(children: [
      Text(word, style: _styleUnderlineTransparent()),
      Text(word.substring(0, 1))
    ]);
  }
}
