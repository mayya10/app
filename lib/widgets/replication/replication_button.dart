import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';

import '../../theme.dart';

class ReplicationButton extends StatelessWidget {
  const ReplicationButton({
    Key? key,
    required this.onDone,
  }) : super(key: key);

  final void Function() onDone;

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<ReplicationBloc, ReplicationState>(
        builder: (context, state) {
      if (state is ReplicationInProgress) {
        return SizedBox(
            width: 20,
            height: 20,
            child: CircularProgressIndicator(
              strokeWidth: 2,
              color: AppTheme.appBar.colorScheme.onPrimary,
            ));
      } else {
        return Material(
            color: Colors.transparent,
            child: InkWell(
                splashColor: AppTheme.appBar.splashColor,
                highlightColor: Colors.transparent,
                onTap: () {
                  ctx.read<ReplicationBloc>().add(RplStarted());
                  onDone();
                },
                onLongPress: () {
                  ctx
                      .read<ReplicationBloc>()
                      .add(RplStarted(all: true));
                  onDone();
                },
                child: Icon(
                    state is ReplicationDirty ? Icons.sync_rounded : state is ReplicationFailure
                        ? Icons.sync_problem_rounded : Icons.refresh_rounded,
                    color: AppTheme.appBar.iconTheme.color)));
      }
    });
  }
}
