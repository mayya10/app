import 'package:charts_flutter/flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/widget_keys.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';

import '../../services/score_service.dart';
import '../../services/verse_service.dart';

class TotalScores extends StatelessWidget {
  TotalScores({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<TaggedVersesBloc, TaggedVersesState>(
      builder: (ctx, state) {
        if (state is TaggedVersesInProgress) {
          return LoadingIndicator(
            key: RememMeKeys.scoresLoadInProgressIndicator,
          );
        } else if (state is TaggedVersesSuccess) {
          final knownVerses = VerseService()
              .mapVersesToBoxKnown(state.verses, VerseOrder.ALPHABET);
          final levelScores = ScoreService().scoreLevels(knownVerses);
          final totalScore = ScoreService().totalScore(levelScores);
          return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            SizedBox(
              height: 8,
            ),
            Text(
              totalScore.toString(),
              style: Theme.of(ctx).textTheme.headlineMedium!.copyWith(
                  fontWeight: FontWeight.w700,
                  color: Theme.of(ctx).colorScheme.primaryContainer),
            ),
            Text(
              L10n.of(ctx).t8('Scores.total.summary'),
              style: Theme.of(ctx).textTheme.bodyMedium,
            ),
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(4, 4, 4, 32),
                    child: ScoreChart(
                      _createSeries(ctx, levelScores),
                    ))),
          ]);
        } else {
          return SizedBox.shrink();
        }
      },
    );
  }

  List<Series<int, String>> _createSeries(BuildContext ctx, List<int> data) {
    return [
      new Series<int, String>(
        id: 'Scores',
        colorFn: (_, i) =>
            ColorUtil.fromDartColor(VerseService().color(i! + 1)),
        domainFn: (score, i) => (i! + 1).toString(),
        measureFn: (score, _) => score,
        labelAccessorFn: (score, _) => score > 0 ? score.toString() : '',
        data: data,
      )
    ];
  }
}
