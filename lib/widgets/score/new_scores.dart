import 'package:charts_flutter/flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/blocs/current_account/current_account.dart';
import 'package:remem_me/services/score_service.dart';
import 'package:repository/repository.dart';

import '../../blocs/blocs.dart';
import '../widgets.dart';

class NewScores extends StatelessWidget {
  NewScores({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<ScoresBloc, EntitiesState<ScoreEntity>>(
      builder: (ctx, state) {
        if (state is EntitiesLoadInProgress<ScoreEntity>) {
          return LoadingIndicator();
        } else if (state is EntitiesLoadSuccess<ScoreEntity>) {
          final dayScores = ScoreService()
              .rangeDays(ScoreService().scoreDays(state.entities), 7)
              .values
              .toList();
          final weekScore = ScoreService()
              .totalScore(dayScores.map((score) => score.change).toList());
          return Column(
            children: [
              const SizedBox(height: 8),
              Text(
                weekScore.toString(),
                style: Theme.of(ctx).textTheme.headlineMedium!.copyWith(
                    fontWeight: FontWeight.w700,
                    color: Theme.of(ctx).colorScheme.primaryContainer),
              ),
              Text(
                L10n.of(ctx).t8('Scores.newest.summary'),
                style: Theme.of(ctx).textTheme.bodyMedium,
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(4, 4, 4, 32),
                  child: ScoreChart(
                    _createSeries(ctx, dayScores),
                    labelRotation: 30,
                    goal: di.get<CurrentAccount>().dailyGoal,
                  ),
                ),
              ),
            ],
          );
        } else {
          return SizedBox.shrink();
        }
      },
    );
  }

  List<Series<DayScore, String>> _createSeries(
      BuildContext ctx, List<DayScore> data) {
    return [
      Series<DayScore, String>(
        id: 'Scores',
        colorFn: (score, __) => ColorUtil.fromDartColor(
            score.change >= di.get<CurrentAccount>().dailyGoal
                ? Theme.of(ctx).colorScheme.primaryContainer
                : Theme.of(ctx).colorScheme.secondaryContainer),
        domainFn: (DayScore score, _) => score.date == DateService().today
            ? L10n.of(ctx).t8('Days.today')
            : score.date.toString('dddd', L10n.of(ctx).culture),
        measureFn: (DayScore score, _) => score.change,
        labelAccessorFn: (DayScore score, _) =>
            score.change > 0 ? score.change.toString() : '',
        data: data,
      )
    ];
  }
}
