import 'package:charts_flutter/flutter.dart';
import 'package:flutter/material.dart';

class ScoreChart extends StatelessWidget {
  final List<Series<dynamic, String>> seriesList;
  final int labelRotation;
  final int goal;

  ScoreChart(this.seriesList,
      {Key? key, this.labelRotation = 0, this.goal = 0})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return BarChart(
      seriesList,
      behaviors: [
        if (goal > 0)
          RangeAnnotation([
            LineAnnotationSegment(goal, RangeAnnotationAxisType.measure,
                color: _goalColor(ctx))
          ]),
      ],
      domainAxis: OrdinalAxisSpec(
        renderSpec: SmallTickRendererSpec(
          labelRotation: labelRotation,
          labelStyle: TextStyleSpec(color: _legendColor(ctx)),
          lineStyle: LineStyleSpec(color: _lineColor(ctx)),
        ),
      ),
      primaryMeasureAxis: NumericAxisSpec(
          renderSpec: GridlineRendererSpec(
              labelStyle: TextStyleSpec(color: _legendColor(ctx)),
              lineStyle: LineStyleSpec(color: _lineColor(ctx)))),
      animate: false,
      barRendererDecorator: BarLabelDecorator<String>(
          insideLabelStyleSpec: TextStyleSpec(
              color: ColorUtil.fromDartColor(
                  Theme.of(ctx).colorScheme.background)),
          outsideLabelStyleSpec: TextStyleSpec(
              color: ColorUtil.fromDartColor(
                  Theme.of(ctx).colorScheme.onBackground))),
    );
  }

  Color _legendColor(BuildContext ctx) => ColorUtil.fromDartColor(
      Theme.of(ctx).colorScheme.onBackground.withOpacity(0.64));

  Color _lineColor(BuildContext ctx) => ColorUtil.fromDartColor(
      Theme.of(ctx).colorScheme.onBackground.withOpacity(0.12));

  Color _goalColor(BuildContext ctx) => ColorUtil.fromDartColor(
      Theme.of(ctx).colorScheme.secondaryContainer);
}
