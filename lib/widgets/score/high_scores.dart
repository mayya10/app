import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/services/score_service.dart';
import 'package:repository/repository.dart';

import '../../blocs/blocs.dart';
import '../widgets.dart';

class HighScores extends StatelessWidget {
  HighScores({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ScoresBloc, EntitiesState<ScoreEntity>>(
      builder: (context, state) {
        if (state is EntitiesLoadInProgress<ScoreEntity>) {
          return LoadingIndicator();
        } else if (state is EntitiesLoadSuccess<ScoreEntity>) {
          var dayScores = ScoreService().sortByHighest(
              ScoreService().scoreDays(state.entities).values.toList());
          if (dayScores.length > 7) dayScores = dayScores.sublist(0, 7);
          return Padding(
            padding: EdgeInsets.all(16),
            child: ListView.separated(
              itemCount: dayScores.length,
              itemBuilder: (ctx, i) => Row(
                children: [
                  Container(
                    alignment: Alignment.center,
                    width: 48,
                    child: Text(
                      (i + 1).toString(),
                      style: Theme.of(ctx).textTheme.headlineMedium!.copyWith(
                          color: Theme.of(ctx).textTheme.headlineLarge!.color),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerRight,
                    width: 128,
                    child: Text(
                      dayScores[i].change.toString(),
                      style: Theme.of(ctx).textTheme.headlineMedium!.copyWith(
                          fontWeight: FontWeight.w700,
                          color: Theme.of(ctx).colorScheme.primaryContainer),
                    ),
                  ),
                  SizedBox(width: 16),
                  Text(
                    FromToday(L10n.of(ctx)).format2Lines(dayScores[i].date),
                    style: Theme.of(ctx).textTheme.bodyMedium,
                  ),
                ],
              ),
              separatorBuilder: (ctx, i) => Divider(thickness: 1),
            ),
          );
        } else {
          return SizedBox.shrink();
        }
      },
    );
  }
}
