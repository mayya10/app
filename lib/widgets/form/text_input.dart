import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:repository/repository.dart';

typedef OptionBuilder = List<PopupMenuEntry<String>> Function(BuildContext);

class StringInput extends StatelessWidget {
  final GlobalKey<FormFieldState>? fieldKey;
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final int maxLines;
  final bool obscureText;
  final String l10nKey;
  final String? l10nKeySummary;
  final TextDirection textDirection;
  final IconData? iconData;
  final List<String>? autofillHints;
  final String? initialValue;
  final bool? autofocus;
  final FormFieldValidator<String>? validator;
  final AutovalidateMode? autovalidateMode;
  final OptionBuilder? optionBuilder;
  final Widget? actionButton;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final ValueChanged<String>? onChanged;
  final FormFieldSetter<String>? onSaved;
  final void Function(String)? onFieldSubmitted;

  const StringInput(
      {Key? key,
      this.fieldKey,
      this.keyboardType,
      this.inputFormatters,
      this.maxLines = 1,
      this.obscureText = false,
      required this.l10nKey,
      this.l10nKeySummary,
      this.textDirection = TextDirection.ltr,
      this.iconData,
      this.autofillHints,
      this.initialValue,
      this.autofocus,
      this.validator,
      this.autovalidateMode,
      this.optionBuilder,
      this.actionButton,
      this.controller,
      this.focusNode,
      this.onChanged,
      this.onSaved,
      this.onFieldSubmitted})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return TextFormField(
      key: fieldKey,
      decoration: DefaultInputDecoration(
        iconData: iconData,
        labelText: l10nKey != null ? L10n.of(ctx).t8(l10nKey) : '',
        helperText:
            l10nKeySummary != null ? L10n.of(ctx).t8(l10nKeySummary!) : '',
        optionButton:
            optionBuilder != null ? _optionButton(optionBuilder!) : null,
        actionButton: actionButton,
      ),
      textDirection: textDirection,
      autofillHints: autofillHints,
      initialValue: controller == null ? initialValue ?? '' : null,
      autofocus: autofocus ?? false,
      keyboardType: keyboardType,
      inputFormatters: inputFormatters,
      maxLines: maxLines,
      obscureText: obscureText,
      validator: validator,
      autovalidateMode: autovalidateMode,
      controller: controller,
      focusNode: focusNode,
      onChanged: onChanged,
      onSaved: ((value) => onSaved!(value!.isNotEmpty ? value : null)),
      onFieldSubmitted: onFieldSubmitted,
    );
  }

  _optionButton(OptionBuilder builder) {
    return PopupMenuButton<String>(
      icon: Icon(Icons.arrow_drop_down),
      onSelected: (String result) {
        fieldKey!.currentState!.didChange(result);
      },
      itemBuilder: builder,
    );
  }
}

class IntegerInput extends StringInput {
  IntegerInput({
    Key? key,
    required String l10nKey,
    String? l10nKeySummary,
    IconData? iconData,
    int? initialValue,
    bool? autofocus,
    FormFieldValidator<int>? validator,
    FormFieldSetter<int>? onSaved,
    Function(int)? onFieldSubmitted,
  }) : super(
            key: key,
            keyboardType: TextInputType.number,
            inputFormatters: [FilteringTextInputFormatter.digitsOnly],
            l10nKey: l10nKey,
            l10nKeySummary: l10nKeySummary,
            iconData: iconData,
            initialValue: initialValue == null || initialValue == 0
                ? ''
                : initialValue.toString(),
            autofocus: autofocus,
            validator: validator != null
                ? (value) => validator(value!.isNotEmpty ? int.parse(value) : 0)
                : null,
            onSaved: (value) => onSaved!(
                value != null && value.isNotEmpty ? int.parse(value) : 0),
            onFieldSubmitted: (value) =>
                onFieldSubmitted!(value.isNotEmpty ? int.parse(value) : 0));
}

class DecimalInput extends StringInput {
  DecimalInput(
      {Key? key,
      required String l10nKey,
      String? l10nKeySummary,
      IconData? iconData,
      double? initialValue,
      bool? autofocus,
      FormFieldValidator<double>? validator,
      FormFieldSetter<double>? onSaved})
      : super(
            key: key,
            keyboardType: TextInputType.number,
            l10nKey: l10nKey,
            l10nKeySummary: l10nKeySummary,
            iconData: iconData,
            initialValue: initialValue == null || initialValue == 0.0
                ? ''
                : initialValue.toString(),
            autofocus: autofocus,
            validator: validator != null
                ? (value) =>
                    validator(value!.isNotEmpty ? double.parse(value) : 0)
                : null,
            onSaved: (value) =>
                onSaved!(value!.isNotEmpty ? double.parse(value) : 0.0));
}

class DropdownInput<T> extends StatelessWidget {
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final String l10nKey;
  final String? l10nKeySummary;
  final IconData? iconData;
  final T? initialValue;
  final bool? autofocus;
  final Map<T, String?> items;
  final FormFieldValidator<T>? validator;
  final FormFieldSetter<T>? onSaved;

  const DropdownInput(
      {Key? key,
      this.keyboardType,
      this.inputFormatters,
      required this.l10nKey,
      this.l10nKeySummary,
      this.iconData,
      this.initialValue,
      this.autofocus,
      required this.items,
      this.validator,
      this.onSaved})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return DropdownButtonFormField<T>(
      decoration: DefaultInputDecoration(
        iconData: iconData,
        labelText: L10n.of(ctx).t8(l10nKey),
        helperText:
            l10nKeySummary != null ? L10n.of(ctx).t8(l10nKeySummary!) : '',
      ),
      value: initialValue,
      items: items.entries.map<DropdownMenuItem<T>>((entry) {
        return DropdownMenuItem<T>(
          value: entry.key,
          child: Text(entry.value!),
        );
      }).toList(),
      validator: validator,
      onSaved: onSaved,
      onChanged: (value) {},
    );
  }
}

// todo: inherit from FormField<FontType> (example: TextFormField)
class FontToggle extends StatefulWidget {
  final GlobalKey<FormFieldState>? fieldKey;
  final String l10nKey;
  final String? l10nKeySummary;
  final IconData? iconData;
  final FontType? initialValue;
  final bool? autofocus;
  final Widget? actionButton;
  final ValueChanged<FontType>? onChanged;
  final FormFieldSetter<FontType>? onSaved;
  final void Function(FontType)? onFieldSubmitted;

  const FontToggle(
      {Key? key,
      this.fieldKey,
      required this.l10nKey,
      this.l10nKeySummary,
      this.iconData,
      this.initialValue,
      this.autofocus,
      this.actionButton,
      this.onChanged,
      this.onSaved,
      this.onFieldSubmitted})
      : super(key: key);

  @override
  State<FontToggle> createState() => _FontToggleState();
}

class _FontToggleState extends State<FontToggle> {
  static const double RADIO_HEIGHT = 24;
  FontType? _fontType;

  @override
  void initState() {
    _fontType = widget.initialValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InputDecorator(
      decoration: DefaultInputDecoration(
        iconData: widget.iconData,
        labelText: L10n.of(context).t8(widget.l10nKey),
        helperText: widget.l10nKeySummary != null
            ? L10n.of(context).t8(widget.l10nKeySummary!)
            : '',
      ),
      child: Row(children: <Widget>[
        Expanded(
            child: Row(children: [
          ConstrainedBox(
              constraints: const BoxConstraints(maxHeight: RADIO_HEIGHT),
              child: Radio<FontType>(
                value: FontType.Sans,
                groupValue: _fontType,
                onChanged: _onChanged,
              )),
          Text(
            'Sans',
            style: TextStyle(fontFamily: FontType.Sans.name),
          ),
        ])),
        Expanded(
            child: Row(children: [
          ConstrainedBox(
            constraints: const BoxConstraints(maxHeight: RADIO_HEIGHT),
            child: Radio<FontType?>(
              value: FontType.Serif,
              groupValue: _fontType,
              onChanged: _onChanged,
            ),
          ),
          Text(
            'Serif',
            style: TextStyle(fontFamily: FontType.Serif.name),
          )
        ])),
        Expanded(
            child: Row(children: [
          ConstrainedBox(
            constraints: const BoxConstraints(maxHeight: RADIO_HEIGHT),
            child: Radio<FontType?>(
              value: FontType.Mono,
              groupValue: _fontType,
              onChanged: _onChanged,
            ),
          ),
          Text(
            'Mono',
            style: TextStyle(fontFamily: FontType.Mono.name),
          )
        ])),
      ]),
    );
  }

  void _onChanged(FontType? value) {
    if (widget.onChanged != null && value != null) widget.onChanged!(value);
    setState(() {
      _fontType = value;
      if (widget.onSaved != null) widget.onSaved!(value);
    });
  }
}

class BoolToggle extends StatefulWidget {
  final GlobalKey<FormFieldState>? fieldKey;
  final String l10nKey;
  final String? l10nKeySummary;
  final IconData? iconData;
  final bool? initialValue;
  final bool? autofocus;
  final ValueChanged<bool>? onChanged;
  final FormFieldSetter<bool>? onSaved;

  const BoolToggle(
      {Key? key,
      this.fieldKey,
      required this.l10nKey,
      this.l10nKeySummary,
      this.iconData,
      this.initialValue,
      this.autofocus,
      this.onChanged,
      this.onSaved})
      : super(key: key);

  @override
  State<BoolToggle> createState() => _BoolToggleState();
}

class _BoolToggleState extends State<BoolToggle> {
  static const double SWITCH_HEIGHT = 24;
  late bool? _boolValue;

  @override
  void initState() {
    _boolValue = widget.initialValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InputDecorator(
      decoration: DefaultInputDecoration(
        iconData: widget.iconData,
        labelText: L10n.of(context).t8(widget.l10nKey),
        helperText: widget.l10nKeySummary != null
            ? L10n.of(context).t8(widget.l10nKeySummary!)
            : '',
      ),
      child: Row(children: <Widget>[
        Expanded(
            child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          ConstrainedBox(
              constraints: const BoxConstraints(maxHeight: SWITCH_HEIGHT),
              child: Switch(
                value: _boolValue ?? false,
                onChanged: _onChanged,
              ))
        ])),
      ]),
    );
  }

  void _onChanged(bool value) {
    if (widget.onChanged != null) widget.onChanged!(value);
    setState(() {
      _boolValue = value;
      if (widget.onSaved != null) widget.onSaved!(value);
    });
  }
}

class DefaultInputDecoration extends InputDecoration {
  DefaultInputDecoration(
      {String? labelText,
      String? helperText,
      IconData? iconData,
      Widget? optionButton,
      Widget? actionButton})
      : super(
            prefixIcon: iconData != null ? Icon(iconData) : null,
            suffixIcon: optionButton ?? actionButton,
            isDense: true,
            filled: true,
            labelText: labelText,
            helperText: helperText,
            border: InputBorder.none);
}

class StringAutocomplete extends StatelessWidget {
  final FutureOr<Iterable<String>> Function(TextEditingValue) optionsBuilder;
  final String? initialValue;
  final GlobalKey<FormFieldState>? fieldKey;
  final TextDirection textDirection;
  final bool? autofocus;
  final AutovalidateMode? autovalidateMode;
  final String? Function(String?)? validator;
  final FormFieldSetter<String>? onSaved;

  const StringAutocomplete(
      {Key? key,
      required this.optionsBuilder,
      this.initialValue,
      this.fieldKey,
      this.textDirection = TextDirection.ltr,
      this.autofocus,
      this.autovalidateMode,
      this.validator,
      this.onSaved})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomAutocomplete<String>(
      optionsBuilder: optionsBuilder,
      initialValue: TextEditingValue(text: initialValue ?? ''),
      fieldViewBuilder: (BuildContext context,
          TextEditingController textEditingController,
          FocusNode focusNode,
          VoidCallback onFieldSubmitted) {
        return StringInput(
            fieldKey: fieldKey,
            l10nKey: 'Verse.reference',
            textDirection: textDirection,
            autofocus: autofocus,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: validator,
            controller: textEditingController,
            focusNode: focusNode,
            onFieldSubmitted: (_) => onFieldSubmitted(),
            onSaved: onSaved);
      },
    );
  }
}

class CustomAutocomplete<T extends Object> extends Autocomplete<T> {
  const CustomAutocomplete({
    Key? key,
    required FutureOr<Iterable<T>> Function(TextEditingValue) optionsBuilder,
    required AutocompleteFieldViewBuilder fieldViewBuilder,
    TextEditingValue? initialValue,
  }) : super(
          key: key,
          optionsBuilder: optionsBuilder,
          fieldViewBuilder: fieldViewBuilder,
          initialValue: initialValue,
        );

  @override
  Widget build(BuildContext context) {
    return RawAutocomplete<T>(
      displayStringForOption: displayStringForOption,
      fieldViewBuilder: fieldViewBuilder,
      initialValue: initialValue,
      optionsBuilder: optionsBuilder,
      optionsViewBuilder: optionsViewBuilder ??
          (BuildContext context, AutocompleteOnSelected<T> onSelected,
              Iterable<T> options) {
            return _AutocompleteOptions<T>(
              displayStringForOption: displayStringForOption,
              onSelected: onSelected,
              options: options,
              maxOptionsHeight: optionsMaxHeight,
            );
          },
      onSelected: onSelected,
    );
  }
}

class _AutocompleteOptions<T extends Object> extends StatelessWidget {
  const _AutocompleteOptions({
    super.key,
    required this.displayStringForOption,
    required this.onSelected,
    required this.options,
    required this.maxOptionsHeight,
  });

  final AutocompleteOptionToString<T> displayStringForOption;

  final AutocompleteOnSelected<T> onSelected;

  final Iterable<T> options;
  final double maxOptionsHeight;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Material(
        elevation: 4.0,
        child: ConstrainedBox(
          constraints: BoxConstraints(maxHeight: maxOptionsHeight),
          child: ListView.builder(
            padding: EdgeInsets.zero,
            shrinkWrap: true,
            itemCount: options.length,
            itemBuilder: (BuildContext context, int index) {
              final T option = options.elementAt(index);
              return InkWell(
                onTap: () {
                  onSelected(option);
                },
                child: Builder(builder: (BuildContext context) {
                  final bool highlight =
                      AutocompleteHighlightedOption.of(context) == index;
                  if (highlight) {
                    SchedulerBinding.instance
                        .addPostFrameCallback((Duration timeStamp) {
                      Scrollable.ensureVisible(context, alignment: 0.5);
                    });
                  }
                  return Container(
                    color: highlight ? Theme.of(context).focusColor : null,
                    padding: const EdgeInsets.all(16.0),
                    child: Text(
                      displayStringForOption(option),
                      style: TextStyle(fontFamily: FontType.Sans.name),
                    ),
                  );
                }),
              );
            },
          ),
        ),
      ),
    );
  }
}
