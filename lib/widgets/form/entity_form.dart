import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:repository/repository.dart';

import '../widgets.dart';

typedef OnSaveCallback<T extends Entity> = Function(
    BuildContext context, T entity);

// Parent class for all edit screens
// abstract class EditScreen<T extends Entity> extends StatefulWidget {}

abstract class EntityForm<T extends Entity> extends BaseForm {
  final OnSaveCallback<T> onSave;
  final OnSaveCallback<T>? onDelete;
  final T? entity;

  const EntityForm({
    Key? key,
    required this.onSave,
    this.onDelete,
    this.entity,
    bool poppable = false,
  }) : super(key: key, poppable: poppable);
}

abstract class EntityFormState<T extends Entity, W extends EntityForm<T>>
    extends BaseFormState<W> {
  void saveForm() async {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      final result = widget.entity != null ? updatedEntity() : createdEntity();
      try {
        setState(() {
          isUpdating = true;
        });
        await widget.onSave(context, result);
        if (widget.poppable) Navigator.of(context).pop();
      } on MessageException catch (e) {
        setState(() {
          errors = e.messages;
        });
      } finally {
        setState(() {
          isUpdating = false;
        });
      }
    }
  }

  void deleteEntity() async {
    if (widget.onDelete != null) {
      try {
        await widget.onDelete!(context, widget.entity!);
        if (widget.poppable) Navigator.of(context).pop();
      } on MessageException catch (e) {
        setState(() {
          errors = e.messages;
        });
      }
    }
  }

  T updatedEntity();

  T createdEntity();
}
