import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/filtered_verses/filtered_verses.dart';

class ReviewedToday extends StatelessWidget {
  const ReviewedToday({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<FilteredVersesBloc, FilteredVersesState>(
        builder: (context, state) {
      final reviewedToday =
          (state as FilteredVersesSuccess).activeFilter?.reviewedToday;
      return ListTile(
        leading: _leadingIcon(reviewedToday),
        trailing: _trailingText(ctx),
        title: Text(L10n.of(context).t8('Filter.reviewedToday')),
        onTap: () {
          (BlocProvider.of<FilteredVersesBloc>(context))
              .add(ReviewedTodayFilterUpdated(_toggleTap(reviewedToday)));
        },
        onLongPress: () {
          (BlocProvider.of<FilteredVersesBloc>(context))
              .add(ReviewedTodayFilterUpdated(_toggleLongPress(reviewedToday)));
        },
      );
    });
  }

  Widget _leadingIcon(bool? reviewedToday) {
    return Icon(reviewedToday != null
        ? reviewedToday
            ? Icons.visibility_rounded
            : Icons.visibility_off_outlined
        : Icons.visibility_outlined);
  }

  Widget _trailingText(BuildContext ctx) {
    final _state = BlocProvider.of<TaggedVersesBloc>(ctx).state;
    if (_state is TaggedVersesSuccess) {
      final today = DateService().today;
      return Text(_state.verses
          .where((verse) => verse.review == today)
          .length
          .toString());
    }
    return Text('');
  }

  bool? _toggleLongPress(bool? reviewedToday) {
    return false;
  }

  bool? _toggleTap(bool? reviewedToday) {
    return reviewedToday == null ? true : null;
  }
}
