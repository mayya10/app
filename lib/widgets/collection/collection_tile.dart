import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/collections/collections.dart';
import 'package:remem_me/routers/app_route_path.dart';
import 'package:remem_me/widgets/collection/collection.dart';

/// Layout for displaying a collection in a list or grid
class CollectionTile extends StatelessWidget {
  const CollectionTile(this.collection, {Key? key}) : super(key: key);

  final Collection collection;

  @override
  Widget build(BuildContext ctx) {
    return GestureDetector(
      onTap: () {
        BlocProvider.of<NavigationCubit>(ctx).go(CollectionsPath(id: collection.id));
      },
      onLongPress: () {},
      child: _card(ctx),
    );
  }

  Widget _card(BuildContext ctx) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: CollectionContent(collection, hasFixedHeight: true),
    );
  }


}
