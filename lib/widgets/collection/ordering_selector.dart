import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/collections/collections.dart';

class OrderingSelector extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return Expanded(
        child: Row(children: [
      Icon(
        Icons.sort_rounded,
        color: Theme.of(ctx).textTheme.caption!.color,
      ),
      SizedBox(width: 8.0),
      Expanded(child: BlocBuilder<CollectionsBloc, CollectionsState>(
          builder: (BuildContext ctx, CollectionsState state) {
        return DropdownButton<CollectionOrder>(
          dropdownColor: Theme.of(ctx).dialogBackgroundColor,
          isExpanded: true,
          value: state.query.order,
          underline: SizedBox.shrink(),
          onChanged: (CollectionOrder? value) {
            BlocProvider.of<CollectionsBloc>(ctx)
                .add(CollectionsInitiated(state.query.copyWith(order: value)));
          },
          items: CollectionOrder.values
              .map<DropdownMenuItem<CollectionOrder>>((CollectionOrder value) {
            return DropdownMenuItem<CollectionOrder>(
              value: value,
              child: Text(L10n.of(ctx).t8(value.toString()),
                overflow: TextOverflow.ellipsis,
              ),
            );
          }).toList(),
        );
      }))
    ]));
  }
}
