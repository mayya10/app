import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';

import '../widgets.dart';
import 'collection.dart';


class CollectionsList extends StatefulWidget {
  @override
  _CollectionsListState createState() => _CollectionsListState();
}

class _CollectionsListState extends State<CollectionsList> {
  final _scrollController = ScrollController();
  late CollectionsBloc _collectionsBloc;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    _collectionsBloc = context.read<CollectionsBloc>();
    _collectionsBloc.stream.listen((state) {
      if(_scrollController.hasClients && state.fetchStatus == Status.none) _scrollController.jumpTo(0.0);
    });
  }

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<CollectionsBloc, CollectionsState>(
      builder: (context, state) {
        switch (state.fetchStatus) {
          case Status.broken:
            return Center(child: Text(L10n.of(ctx).t8('Collections.fetch.error')));
          case Status.done:
            if (state.collections.isEmpty) {
              return Center(child: Text(L10n.of(ctx).t8('Collections.empty')));
            }
            return GridView.builder(
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 640,
                  childAspectRatio: 2 / 1,
                  crossAxisSpacing: 0,
                  mainAxisSpacing: 0),
              itemBuilder: (BuildContext context, int index) {
                return index >= state.collections.length
                    ? BottomLoader()
                    : CollectionTile(state.collections[index]);
              },
              itemCount: state.hasReachedMax
                  ? state.collections.length
                  : state.collections.length + 1,
              controller: _scrollController,
            );
          default:
            return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_isBottom) _collectionsBloc.add(CollectionsFetched());
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) return false;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }
}
