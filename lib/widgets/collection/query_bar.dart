import 'package:flutter/material.dart';
import 'package:remem_me/theme.dart';

import 'collection.dart';

class QueryBar extends StatelessWidget implements PreferredSizeWidget {
  final Size preferredSize = Size.fromHeight(48.0);

  @override
  Widget build(BuildContext ctx) {
    // BlocProvider.of<CollectionsBloc>(ctx)
    return Theme(
      data: AppTheme.dark,
        child: Padding(
        padding: EdgeInsets.only(left: 16.0, right: 16.0),
        child: Row(children: [
          LanguageSelector(),
          OrderingSelector(),
        ])));
  }
}



