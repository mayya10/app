import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';

import '../../theme.dart';

class GlobalSearch extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _GlobalSearchState();
  }
}

class _GlobalSearchState extends State<GlobalSearch> {
  var search = '';

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<CollectionsBloc, CollectionsState>(
        builder: (BuildContext ctx, CollectionsState state) {
      return AppBar(
          elevation: 0,
          title: Row(children: [
            _clearSearchBtn(ctx, state),
            SizedBox(width: 8.0),
            Expanded(child: _searchField(ctx, state)),
          ]),
          actions: []);
    });
  }

  IconButton _clearSearchBtn(BuildContext ctx, CollectionsState state) {
    return IconButton(
      tooltip: L10n.of(ctx).t8('Button.cancel'),
      icon: Icon(Icons.close_rounded),
      onPressed: () {
        BlocProvider.of<CollectionsBloc>(ctx).add(CollectionsInitiated(
            state.query.copyWith(search: ''),
            isSearchVisible: false));
      },
    );
  }

  Widget _searchField(BuildContext ctx, CollectionsState state) {
    return Theme(
        data: AppTheme.appBar,
        child: TextFormField(
          autofocus: true,
          textInputAction: TextInputAction.search,
          decoration: InputDecoration(
            hintText: L10n.of(ctx).t8('Button.search'),
            border: InputBorder.none,
          ),
          onChanged: (String value) {
            setState(() {
              search = value;
              BlocProvider.of<CollectionsBloc>(ctx).add(CollectionsInitiated(
                  state.query.copyWith(search: search),
                  isSearchVisible: true));
            });
          },
        ));
  }
}
