import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:remem_me/blocs/blocs.dart';

import '../../utils/utils.dart';

class LanguageSelector extends StatelessWidget {

  @override
  Widget build(BuildContext ctx) {
    return Expanded(
        child: Row(children: [
      Icon(
        Icons.language_rounded,
        color: Theme.of(ctx).textTheme.caption!.color,
      ),
      SizedBox(width: 8.0),
      Expanded(child: BlocBuilder<CollectionsBloc, CollectionsState>(
          builder: (BuildContext ctx, CollectionsState state) {
        return DropdownButton<String>(
            dropdownColor: Theme.of(ctx).dialogBackgroundColor,
            isExpanded: true,
            value: state.query.language,
            underline: SizedBox.shrink(),
            onChanged: (String? value) {
              BlocProvider.of<CollectionsBloc>(ctx).add(
                  CollectionsInitiated(state.query.copyWith(language: value)));
            },
            items: languageMap(ctx).entries.map<DropdownMenuItem<String>>((entry) {
              return DropdownMenuItem<String>(
                value: entry.key,
                child: Text(
                  entry.value,
                  overflow: TextOverflow.ellipsis,
                ),
              );
            }).toList());
      }))
    ]));
  }
}
