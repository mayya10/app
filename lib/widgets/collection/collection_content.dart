import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:intl/intl.dart'; //for date format
import 'package:remem_me/models/collections/collections.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../utils/utils.dart';

/// Layout for displaying a collection in a list
class CollectionContent extends StatelessWidget {
  const CollectionContent(this.collection,
      {Key? key, this.hasFixedHeight = false})
      : super(key: key);

  final Collection? collection;
  final bool hasFixedHeight;

  @override
  Widget build(BuildContext ctx) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        ListTile(
          title: Text(collection!.name),
          subtitle: _topInfoRow(ctx),
        ),
        Flexible(
            flex: hasFixedHeight ? 1 : 0,
            fit: hasFixedHeight ? FlexFit.tight : FlexFit.loose,
            child: collection!.description.isNotEmpty
                ? hasFixedHeight
                    ? _descriptionFixedHeight(ctx)
                    : _descriptionProperHeight(ctx)
                : const SizedBox.shrink()),
        const Divider(
          thickness: 1.0,
          height: 0,
        ),
        _bottomInfoRow(ctx),
      ],
    );
  }

  Widget _descriptionProperHeight(BuildContext ctx) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
        child: Text(collection!.description));
  }

  Widget _descriptionFixedHeight(BuildContext ctx) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Text(
          collection!.description,
          overflow: TextOverflow.fade,
        ));
  }

  Widget _topInfoRow(BuildContext ctx) {
    return Row(
      children: [
        _infoItem(ctx, Icons.menu_book_sharp,
            L10n.of(ctx).t8('Verses.count', [collection!.size.toString()])),
        const SizedBox(width: 16.0),
        _infoItem(ctx, Icons.person_outline, collection!.publisher!.name,
            flex: 2),
      ],
    );
  }

  Widget _bottomInfoRow(BuildContext ctx) {
    return SizedBox(
      height: 52,
      child: Row(
        children: [
          const SizedBox(width: 16.0),
          _infoItem(
              ctx,
              Icons.calendar_today_rounded,
              DateFormat.yMMMd(L10n.of(ctx).locale.toString())
                  .format(collection!.created!)),
          const SizedBox(width: 16.0),
          _infoItem(
              ctx,
              Icons.language_rounded,
              localeNameOf(ctx, collection!.publisher!.language.toString())),
          // todo: flutter_localized_locales
          const SizedBox(width: 16.0),
          _infoItem(
              ctx, Icons.download_rounded, collection!.downloads.toString(),
              trailing: _link()),
          const SizedBox(width: 8.0),
        ],
      ),
    );
  }

  Widget? _link() {
    return collection!.website != null &&
            collection!.website!.startsWith('http')
        ? IconButton(
            visualDensity: VisualDensity.compact,
            splashRadius: 28,
            alignment: Alignment.centerRight,
            icon: const Icon(Icons.link_rounded),
            tooltip: collection!.website,
            onPressed: () async {
              if (await canLaunch(collection!.website!)) {
                await launch(collection!.website!);
              }
            })
        : null;
  }

  Widget _infoItem(BuildContext ctx, IconData icon, String text,
      {int flex = 1, Widget? trailing}) {
    return Expanded(
        flex: flex,
        child: Row(
          children: [
            Icon(icon,
                size: 16.0, color: Theme.of(ctx).textTheme.caption!.color),
            const SizedBox(width: 4.0),
            Expanded(
                child: Text(
              text,
              overflow: TextOverflow.ellipsis,
            )),
            if (trailing != null) trailing,
          ],
        ));
  }
}
