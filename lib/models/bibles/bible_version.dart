import 'bible_site.dart';

class BibleVersion {
  final String? name;
  final String? tag;
  final BibleSite? site;
  final Map<String, String>? books;

  BibleVersion(this.name, this.tag, this.site, this.books);
}