import 'dart:convert';
import 'dart:math';

import 'package:html_unescape/html_unescape_small.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart' show debugPrint, kIsWeb;
import 'package:remem_me/services/bible_service.dart';
import 'package:repository/repository.dart';
import 'bibles.dart';

class BibleQuery {
  final server = Settings().getOrDefault(Settings.BIBLE_SERVER);
  Uri? _uri;
  late Uri _proxyUri;
  final BibleVersion version;
  final String bookKey;
  final int chapter;
  final int first;
  final int last;

  Uri? get uri {
    return _uri;
  }

  BibleQuery(versionKey, this.version, this.bookKey, this.chapter, this.first,
      this.last) {
    _uri = _getUri();
    _proxyUri = _getProxyUri(versionKey);
  }

  Future<String> get() async {
    final response =
        await http.get(!kIsWeb || version.site!.cors! ? _uri! : _proxyUri);
    final body = utf8.decode(response.bodyBytes,
        allowMalformed: true); // ignore characters that are encoded incorrectly
    debugPrint('Response status: ${response.statusCode}');
    debugPrint('Response body: ${body.substring(0, min(body.length, 100))}');
    final pattern = RegExp(
        _interpolate(version.site!.pattern!, lastPlusOne: true),
        dotAll: true);
    var result = pattern.firstMatch(body)?[0];
    if (result != null) {
      result = HtmlUnescape().convert(result);
      final includeNumbers =
          Settings().getBool(Settings.INCLUDE_NUMBERS) ?? false;
      result = _extractText(result, includeNumbers);
      return result;
    }
    return '';
  }

  String _extractText(String result, bool includeNumbers) {
    final numberReplacements = includeNumbers
        ? version.site!.keepNumbers
        : version.site!.removeNumbers;
    final replacements = {
      ...numberReplacements,
      ...version.site!.removeClutter,
      '<br />': '\n',
    };
    replacements.forEach((key, value) {
      result = result.replaceAll(RegExp(key, dotAll: true), value);
    });
    return result.trim();
  }

  Uri _getUri() {
    return Uri.parse(_interpolate(version.site!.url!));
  }

  Uri _getProxyUri(String? versionKey) {
    return Uri.parse(
        '$server/${_interpolate(BIBLE_PROXY_PATH, versionKey: versionKey)}');
  }

  String _interpolate(String text,
      {bool lastPlusOne = false, String? versionKey}) {
    final start = first == 0 ? '1' : first;
    final end = last == 0
        ? ''
        : lastPlusOne
            ? last + 1
            : last;
    return text
        .replaceAll('%(tag)s', versionKey ?? version.tag!)
        .replaceAll('%(book)s', version.books![bookKey]!)
        .replaceAll('%(chap)d', chapter.toString())
        .replaceAll('%(start)d', start.toString())
        .replaceAll('%(end)d', end.toString());
  }
}
