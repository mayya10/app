class BibleSite {
  final String? name;
  final String? url;
  final String? pattern;
  final Map<String, String> removeClutter;
  final Map<String, String> removeNumbers;
  final Map<String, String> keepNumbers;
  final String charset; // todo: Ignored. Can be removed.
  final bool? cors;

  BibleSite(this.name, this.url, this.pattern,
      {this.removeClutter = const {},
      this.removeNumbers = const {},
      this.keepNumbers = const {},
      this.charset = 'utf-8',
      this.cors = true});

  static BibleSite fromJson(Map<String, dynamic> json) {
    return BibleSite(
      json['name'] as String?,
      json['url'] as String?,
      json['pattern'] as String?,
      removeClutter: Map<String, String>.from(json['removeClutter'] as Map<dynamic, dynamic>),
      removeNumbers: Map<String, String>.from(json['removeNumbers'] as Map<dynamic, dynamic>),
      keepNumbers: Map<String, String>.from(json['keepNumbers'] as Map<dynamic, dynamic>),
      charset: json['charset'] ?? 'utf-8',
      cors: json['cors'] as bool?,
    );
  }
}
