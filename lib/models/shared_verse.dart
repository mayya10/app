import 'package:flutter/foundation.dart';

class SharedVerse {
  String? reference;
  String? source;
  String? passage;

  SharedVerse(String data) {
    try {
      if (data.contains('//bible.com/')) {
        _parseYouVersion(data);
      } else if (data.contains('//olivetree.com/')) {
        _parseOliveTree(data);
      } else if (data.contains('//bibleis.app.link/')) {
        _parseBibleIs(data);
      } else {
        passage = data;
      }
    } catch(e) {
      debugPrint(e.toString());
      passage = data;
    }
    debugPrint(toString());
  }

  // Ein Lied von David. Der Herr ist mein Hirte, nichts wird mir fehlen. Er weidet mich auf saftigen Wiesen und führt mich zu frischen Quellen. Er gibt mir neue Kraft. Er leitet mich auf sicheren Wegen und macht seinem Namen damit alle Ehre. Auch wenn es durch dunkle Täler geht, fürchte ich kein Unglück, denn du, Herr, bist bei mir. Dein Hirtenstab gibt mir Schutz und Trost. Du lädst mich ein und deckst mir den Tisch vor den Augen meiner Feinde. Du begrüßt mich wie ein Hausherr seinen Gast und füllst meinen Becher bis zum Rand. Deine Güte und Liebe begleiten mich Tag für Tag; in deinem Haus darf ich bleiben mein Leben lang.
  // Psalm 23:1‭-‬6 HFA
  //
  // https://bible.com/bible/73/psa.23.1-6.HFA
  void _parseYouVersion(String data) {
    var segments = data.replaceAll(RegExp(r'[\u202D,\u202C]'), '').split('\n');
    passage = segments[0].trim();
    final i = segments[1].lastIndexOf(' ');
    reference = segments[1].substring(0, i).trim();
    source = segments[1].substring(i + 1, segments[1].length).trim();
  }

  // Psalmen 23:1-2 (ESV) " The LORD Is My Shepherd
  // A PSALM OF DAVID.
  // ​1 The LORD is my shepherd; I shall not want.
  // 2 He makes me lie down in green pastures.
  // He leads me beside still waters.
  // "
  //
  // Sent from Bible Study (http://olivetree.com/b3/Ps.23.1-2.ESV)
  void _parseOliveTree(String data) {
    var segments = data.split('"');
    passage = segments[1].trim();
    if (segments[0].contains('(')) {
      reference = RegExp(r'.*(?=\()').firstMatch(segments[0])?.group(0)?.trim();
      source =
          RegExp(r'(?<=\().*(?=\))').firstMatch(segments[0])?.group(0)?.trim();
    } else {
      reference = segments[0].trim();
    }
  }

  // Yesu Krisita Kibaru Duman min sɛbɛnna MATIYU fɛ 5:3-4
  //
  // «Minnu y'a dɔn ko u ye faantanw ye
  //   Ala ta fan fɛ, olu ye dubadenw ye,
  //   katuguni sankolo masaya ye u ta ye. «Minnu dusu kasilen don, olu ye dubadenw ye,
  //   katuguni u dusuw na saalo.
  //
  // https://bibleis.app.link/ojCwvBheusb - Freier Zugang zur Bibel in Video, Audio und Text in 1652 Sprachen:
  void _parseBibleIs(String data) {
    var segments = data.split('\n\n');
    reference = segments[0];
    passage = segments[1];
  }

  @override
  String toString() {
    return 'SharedVerse { reference: $reference, source: $source, passage: $passage }';
  }
}
