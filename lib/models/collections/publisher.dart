import 'package:app_core/app_core.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class Publisher extends Equatable {
  final int id;
  final String name;
  final Locale language;
  final Locale langRef;

  const Publisher(this.id, this.name,
      {required this.language, required this.langRef});

  @override
  List<Object?> get props => [id, name, language, langRef];

  Publisher copyWith({
    int? id,
    String? name,
    Locale? language,
    Locale? langRef,
  }) {
    return Publisher(id ?? this.id, name ?? this.name,
        language: language ?? this.language, langRef: langRef ?? this.langRef);
  }

  @override
  String toString() {
    return 'Collection { id: $id, name: $name, '
        'language: $language, langRef: $langRef }';
  }

  static Publisher fromJson(Map<String, dynamic> json) {
    return Publisher(
      json['id'] as int,
      json['name'] as String,
      language: TextUtil.toLocale(json['language'] as String),
      langRef: TextUtil.toLocale(json['lang_ref'] as String),
    );
  }
}
