import 'package:equatable/equatable.dart';
import 'package:repository/repository.dart';

import '../models.dart';
import 'collections.dart';

/// A collection is a deck published by another user
class Collection extends Equatable {
  final int id;
  final String label;
  final String name;
  final String description;
  final String? website;
  final String? image;
  final DateTime? created;
  final int? size;
  final int? downloads;
  final Publisher? publisher;
  final List<Verse> verses;

  Collection(this.id, this.label, this.name,
      {required this.description,
      this.website,
      this.image,
      this.created,
      this.size,
      this.downloads,
      this.publisher,
      this.verses = const []});

  Collection copyWith({
    int? id,
    String? label,
    String? name,
    String? description,
    String? website,
    String? image,
    DateTime? created,
    int? size,
    int? downloads,
    int? publisher,
    List<Verse>? verses,
  }) {
    return Collection(
      id ?? this.id,
      label ?? this.label,
      name ?? this.name,
      description: description ?? this.description,
      website: website ?? this.website,
      image: image ?? this.image,
      created: created ?? this.created,
      size: size ?? this.size,
      downloads: downloads ?? this.downloads,
      publisher: publisher as Publisher? ?? this.publisher,
      verses: verses ?? this.verses,
    );
  }

  @override
  List<Object?> get props => [
        id,
        label,
        name,
        description,
        website,
        image,
        created,
        size,
        downloads,
        publisher,
        verses,
      ];

  @override
  String toString() {
    return 'Collection { id: $id, label: $label, name: $name, description: $description, '
        'website: $website, image: $image, created: $created, size: $size, '
        'downloads: $downloads, publisher: $publisher, verses: ${verses.length} }';
  }

  static Collection fromJson(Map<String, dynamic> json) {
    return Collection(
      json['id'] as int,
      json['label'] as String,
      json['name'] as String,
      description: json['description'] as String,
      website: json['website'] as String?,
      image: json['image'] as String?,
      created: json['created'] != null
          ? DateTime.parse(json['created'] as String)
          : null,
      size: json['size'] as int?,
      downloads: json['downloads'] as int?,
      publisher: Publisher.fromJson(json['publisher'] as Map<String, dynamic>),
      verses: versesFromJson(json['verses']),
    );
  }

  static List<Verse> versesFromJson(List<dynamic>? json) {
    return json != null
        ? json.map((item) => Verse.fromEntity(VerseEntity.fromJson(item))).toList()
        : const [];
  }
}
