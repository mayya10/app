import 'package:equatable/equatable.dart';

import 'collections.dart';

class Query extends Equatable {
  final String? search;
  final String? language;
  final bool importedOnly;
  final CollectionOrder order;

  const Query({
    this.search,
    this.language,
    this.importedOnly = false,
    this.order = CollectionOrder.featured,
  });

  @override
  List<Object?> get props => [search, language, importedOnly, order];

  @override
  String toString() =>
      'Query { search: $search, language: $language, importedOnly: $importedOnly, order: $order }';

  Query copyWith({
    String? search,
    String? language,
    bool? importedOnly,
    CollectionOrder? order,
  }) {
    return Query(
      search: search ?? this.search,
      language: language ?? this.language,
      importedOnly: importedOnly ?? this.importedOnly,
      order: order ?? this.order,
    );
  }
}