import 'package:app_core/app_core.dart';
import 'package:equatable/equatable.dart';

import 'models.dart';

class VisibilityFilter extends Equatable {
  final String query;
  final bool? reviewedToday;

  VisibilityFilter({this.query = '', this.reviewedToday});

  VisibilityFilter copyWith({
    String? query,
    bool? reviewedToday,
    List<String> nullValues = const [],
  }) =>
      VisibilityFilter(
        query: query ?? this.query,
        reviewedToday: reviewedToday ??
            (nullValues.contains('reviewedToday') ? null : this.reviewedToday),
      );

  @override
  String toString() =>
      'VisibilityFilter { query: $query, reviewedToday: $reviewedToday }';

  bool appliesTo(Verse verse) {
    return _queryAppliesTo(verse) && _reviewedTodayAppliesTo(verse);
  }

  bool _queryAppliesTo(Verse verse) {
    return query.isEmpty ||
        verse.reference.toLowerCase().contains(query.toLowerCase()) ||
        verse.passage.toLowerCase().contains(query.toLowerCase()) ||
        verse.topic != null &&
            verse.topic!.toLowerCase().contains(query.toLowerCase());
  }

  bool _reviewedTodayAppliesTo(Verse verse) {
    final today = DateService().today;
    return reviewedToday == null ||
        reviewedToday == true && verse.review == today ||
        reviewedToday == false && verse.review != today;
  }

  @override
  List<Object?> get props => [query, reviewedToday];
}
