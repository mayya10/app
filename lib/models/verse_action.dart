enum VerseAction {
  COMMITTED,
  FORGOTTEN,
  REMEMBERED,
  MOVED_TO_DUE,
  MOVED_TO_NEW,
  SKIPPED,
  RETRACTED,
  UNDONE,
}
