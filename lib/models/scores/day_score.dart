import 'package:equatable/equatable.dart';
import 'package:time_machine/time_machine.dart';

class DayScore extends Equatable {
  final LocalDate date;
  final int change;

  DayScore(this.date, this.change);

  DayScore add(int value) {
    return DayScore(date, change + value);
  }

  @override
  List<Object?> get props => [date, change];

  @override
  String toString() {
    return 'DayScore { date: $date, change: $change }';
  }
}