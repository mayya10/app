class Book {
  final String? key;
  final int? index;
  final String? name;
  final String? spokenName;

  Book(this.key, this.index, this.name, this.spokenName);
}