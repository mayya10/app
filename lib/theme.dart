import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


class AppTheme {

  static const AppBarTheme appBarTheme = AppBarTheme(
    backgroundColor: AppColor.dark,
    systemOverlayStyle: SystemUiOverlayStyle.light,
  );

  static final FloatingActionButtonThemeData fabTheme =
      FloatingActionButtonThemeData(backgroundColor: Colors.grey[700]);

  static const PageTransitionsTheme pageTransitionsTheme =
      PageTransitionsTheme(builders: {
    TargetPlatform.iOS: ZoomPageTransitionsBuilder(),
    TargetPlatform.android: ZoomPageTransitionsBuilder(),
  });

  static const TextTheme appTextTheme = TextTheme(
    headlineMedium: TextStyle(),
    titleSmall: TextStyle(fontSize: 18.0, height: 2.0),
    titleMedium: TextStyle(fontFamily: 'Sans'),
    labelSmall: TextStyle(fontSize: 12.0, letterSpacing: 0.0),
  );

  static ThemeData light = ThemeData.from(
    colorScheme: AppColorScheme.light,
    textTheme: appTextTheme.apply(displayColor: AppColor.dark, fontFamily: 'Sans'),
  ).copyWith(
    pageTransitionsTheme: pageTransitionsTheme,
    appBarTheme: appBarTheme,
    indicatorColor: Flat.amber,
    floatingActionButtonTheme: fabTheme,
    iconTheme: const IconThemeData(color: FlatDark.grey),
  );

  static ThemeData dark = ThemeData.from(
    colorScheme: AppColorScheme.dark,
    textTheme: appTextTheme.apply(displayColor: AppColor.light, fontFamily: 'Sans'),
  ).copyWith(
    pageTransitionsTheme: pageTransitionsTheme,
    appBarTheme: appBarTheme,
    indicatorColor: Flat.amber,
    floatingActionButtonTheme: fabTheme,
    iconTheme: const IconThemeData(color: Flat.grey),
    drawerTheme: DrawerThemeData(backgroundColor: Colors.grey[900]),
    dialogBackgroundColor: Colors.grey[850],
    selectedRowColor: Colors.white10,
    toggleableActiveColor: AppColor.light,
    cardColor: FlatDark.tar,
  );

  static ThemeData appBar = ThemeData.from(
    colorScheme: AppColorScheme.appBar,
    textTheme: appTextTheme.apply(fontFamily: 'Sans'),
  ).copyWith(
    appBarTheme: appBarTheme,
    indicatorColor: Flat.amber,
    textSelectionTheme: const TextSelectionThemeData(cursorColor: Flat.amber),
  );

/*static ThemeData light = ThemeData(
      //brightness: Brightness.light,
      appBarTheme: appBarTheme,
      colorScheme: AppColorScheme.light,
      textTheme: thinTextTheme.apply(displayColor: appColorDark),
      backgroundColor: Colors.blueGrey[100],
      selectedRowColor: Colors.blueGrey[50],
      dialogBackgroundColor: Colors.white,
      scaffoldBackgroundColor: Colors.white,
      toggleableActiveColor: AppColorScheme.dark.secondary,
      iconTheme: IconThemeData(color: Colors.black45), // same as ListTileTheme.iconColor
/*    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(primary: Flat.indigo)
    ),
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(primary: FlatDark.indigo)
    ),*/
      );*/

/*static ThemeData dark = ThemeData(
    brightness: Brightness.dark,
    appBarTheme: appBarTheme,
    colorScheme: AppColorScheme.dark,
    textTheme: thinTextTheme.apply(displayColor: appColorLight),
    toggleableActiveColor: AppColorScheme.dark.secondary,
    backgroundColor: Colors.blueGrey[700],
    selectedRowColor: Colors.blueGrey[800],
    dialogBackgroundColor: Colors.blueGrey[800],
    scaffoldBackgroundColor: Colors.blueGrey[900],
    // elevatedButtonTheme: ElevatedButtonThemeData(
    //    style: ElevatedButton.styleFrom(primary: Flat.indigo)),
    //textButtonTheme:
    //    TextButtonThemeData(style: TextButton.styleFrom(primary: Flat.indigo)),
  );*/
}
