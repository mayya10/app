import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:repository/repository.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:collection/collection.dart';

Future<void> Function() launchDocumentation(BuildContext ctx, {String path = ''}) {
  return () async {
    final server = Settings().getOrDefault(Settings.DOC_SERVER);
    final doc = L10n.of(ctx).t8('Documentation.path');
    final uri = Uri.parse('$server/$doc$path');
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri, mode: LaunchMode.externalApplication);
    }
  };
}

ScreenSize screenSize(BuildContext ctx) {
  final screenWidth = MediaQuery.of(ctx).size.width;
  if (screenWidth <= 960) return ScreenSize.small;
  if (screenWidth <= 1280) return ScreenSize.medium;
  return ScreenSize.large;
}

String localeNameOf(BuildContext ctx, String locale) {
  return LocaleNames.of(ctx)!.nameOf(locale) ??
      L10n.of(ctx).t8('Locale.$locale');
}


Map<String, String> languageMap(BuildContext ctx) {
  final current = Localizations.localeOf(ctx);
  final locales = BIBLE_LOCALES.toSet();
  if (locales.contains(current.languageCode)) {
    locales.add(current.toString());  // specific locale of available language
  }
  locales.add(di.get<CurrentAccount>().language.toString());
  final result = _mapLocales(ctx, locales);
  result.addEntries([MapEntry('ot', L10n.of(ctx).t8('Locale.ot'))]);
  return result;
}

Map<String, String> langRefMap(BuildContext ctx) {
  final current = Localizations.localeOf(ctx);
  final locales = REFERENCE_LOCALES.toSet();
  if (locales.contains(current.languageCode)) {
    locales.add(current.toString());
  }
  locales.add(di.get<CurrentAccount>().langRef.toString());
  return _mapLocales(ctx, locales);
}

Map<String, String> _mapLocales(BuildContext ctx, Set<String> locales) {
  return Map.fromEntries(locales
      .map((locale) =>
      MapEntry(locale, localeNameOf(ctx, locale)))
      .sortedBy((a) => a.value));
}
