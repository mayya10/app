import 'package:flutter/widgets.dart';

class RememMeKeys {
  static final extraActionsPopupMenuButton =
      const Key('__extraActionsPopupMenuButton__');
  static final extraActionsEmptyContainer =
      const Key('__extraActionsEmptyContainer__');
  static final filteredVersesEmptyContainer =
      const Key('__filteredVersesEmptyContainer__');
  static final scoresLoadInProgressIndicator =
      const Key('__scoresLoadingIndicator__');
  static final emptyScoresContainer = const Key('__emptyScoresContainer__');
  static final emptyDetailsContainer = const Key('__emptyDetailsContainer__');
  static final detailsScreenCheckBox = const Key('__detailsScreenCheckBox__');
}
