part of 'photos_bloc.dart';

class PhotosState extends Equatable {
  final List<Photo> photos;
  final Status fetchStatus;
  final bool hasReachedMax;
  final String? search;

  const PhotosState(
      {this.photos = const <Photo>[],
      this.fetchStatus = Status.none,
      this.hasReachedMax = false,
      this.search});

  @override
  List<Object?> get props => [photos, fetchStatus, hasReachedMax, search];

  PhotosState copyWith({
    List<Photo>? photos,
    Status? fetchStatus,
    bool? hasReachedMax,
    String? search,
  }) {
    return PhotosState(
      photos: photos ?? this.photos,
      fetchStatus: fetchStatus ?? this.fetchStatus,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      search: search ?? this.search,
    );
  }

  @override
  String toString() => 'PhotosState { photos: [${photos.length}], '
      'fetchStatus: $fetchStatus, hasReachedMax: $hasReachedMax, search: $search }';
}
