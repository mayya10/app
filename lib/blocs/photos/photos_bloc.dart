import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'package:rxdart/rxdart.dart';

import 'package:remem_me/models/photos/photos.dart';
import 'package:remem_me/models/status.dart';
import 'package:remem_me/services/photos_service.dart';

part 'photos_event.dart';

part 'photos_state.dart';

class PhotosBloc extends Bloc<PhotosEvent, PhotosState> {
  final PhotosService service;

  EventTransformer<PhotosEvent> debounce<PhotosEvent>() {
    return (events, mapper) =>
        events.debounceTime(const Duration(milliseconds: 500)).flatMap(mapper);
  }

  PhotosBloc(this.service) : super(const PhotosState()) {
    on<PhotosEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (PhotosState result) => result),
        transformer: debounce());
  }

  Stream<PhotosState> mapEventToState(PhotosEvent event) async* {
    debugPrint('mapEventToState: $event');
    if (event is PhotosInitiated) {
      yield* _mapPhotosInitiatedToState(event);
    } else if (event is PhotosFetched) {
      yield* _mapPhotosFetchedToState(event);
    }
  }

  Stream<PhotosState> _mapPhotosInitiatedToState(PhotosInitiated event) async* {
    try {
      yield const PhotosState();
      final batch = event.search == null
          ? await service.fetchRandomPhotos()
          : await service.fetchSearchPhotos(event.search!);
      yield PhotosState(
        fetchStatus: Status.done,
        search: event.search,
        photos: batch.photos,
        hasReachedMax: service.hasReachedMax(batch.photos.length, batch.total),
      );
    } on Exception {
      yield state.copyWith(fetchStatus: Status.broken);
    }
  }

  Stream<PhotosState> _mapPhotosFetchedToState(PhotosFetched event) async* {
    debugPrint('_mapPhotosFetchedToState: $state');
    if (state.hasReachedMax) {
      yield state;
    } else {
      try {
        final batch =
            await service.fetchPhotos(state.search, offset: state.photos.length);
        final allPhotos =  List.of(state.photos)..addAll(batch.photos);
        yield batch.photos.isEmpty
            ? state.copyWith(hasReachedMax: true)
            : state.copyWith(
                fetchStatus: Status.done,
                photos: allPhotos,
                hasReachedMax: service.hasReachedMax(allPhotos.length, batch.total));
      } on Exception {
        yield state.copyWith(fetchStatus: Status.broken);
      }
    }
  }
}
