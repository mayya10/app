import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:just_audio/just_audio.dart';
import 'package:record/record.dart';
import 'package:remem_me/models/status.dart';

import '../blocs.dart';

part 'voice_event.dart';

part 'voice_state.dart';

class VoiceBloc extends Bloc<VoiceEvent, VoiceState> {
  final FlashcardBloc flashcardBloc;
  final _recorder = Record();
  final _player = AudioPlayer();
  late StreamSubscription _flashcardSubscription;
  late StreamSubscription _completedSubscription;
  AudioSource? _source;

  VoiceBloc(this.flashcardBloc) : super(VoiceState(Status.none, Status.none)) {
    on<RecordPrepared>(_onRecordPrepared);
    on<RecordStarted>(_onRecordStarted);
    on<RecordStopped>(_onRecordStopped);
    on<PlayPrepared>(_onPlayPrepared);
    on<PlayStarted>(_onPlayStarted);
    on<PlayCompleted>(_onPlayCompleted);
    on<PlayStopped>(_onPlayStopped);

    _flashcardSubscription = flashcardBloc.stream
        .distinct((prev, next) =>
            prev is FlashcardRun &&
            next is FlashcardRun &&
            prev.flipping == next.flipping)
        .where((state) =>
            state is FlashcardRun &&
            !state.flipping &&
            !state.inverse &&
            state.reverse)
        .listen((_) => add(PlayPrepared()));

    _completedSubscription = _player.playerStateStream
        .where((state) => state.processingState == ProcessingState.completed)
        .listen((_) => add(PlayCompleted()));

    // add(RecordPrepared());
  }

  void _onRecordPrepared(_, Emitter<VoiceState> emit) async {
    final isAllowed = await _recorder.hasPermission();
    emit(state.copyWith(record: isAllowed ? Status.ready : Status.forbidden));
  }

  void _onRecordStarted(_, Emitter<VoiceState> emit) async {
    var status = state.record;
    if (status == Status.none) {
      if (await _recorder.hasPermission()) {
        status = Status.ready;
      } else {
        emit(state.copyWith(record: Status.forbidden));
        return; // RETURN
      }
    }
    if (status == Status.ready) {
      try {
        await _recorder.start();
        bool isActive = await _recorder.isRecording();
        if (!isActive) throw Exception('No valid recording session');
        emit(state.copyWith(record: Status.active));
      } catch (e) {
        emit(state.copyWith(record: Status.broken));
        debugPrint(e.toString());
      }
    }
  }

  void _onRecordStopped(_, Emitter<VoiceState> emit) async {
    final path = await _recorder.stop();
    if (path == null) {
      emit(state.copyWith(record: Status.broken));
      debugPrint('Recording not saved.');
    } else {
      _source = AudioSource.uri(Uri.parse(path));
      emit(state.copyWith(record: Status.ready));
      debugPrint('Recording saved as: $path');
    }
  }

  void _onPlayPrepared(_, Emitter<VoiceState> emit) async {
    if (_source != null) {
      await _player.setAudioSource(_source!);
      emit(state.copyWith(play: Status.ready));
      add(PlayStarted());
    }
  }

  void _onPlayStarted(_, Emitter<VoiceState> emit) async {
    if (state.play == Status.ready) {
      emit(state.copyWith(play: Status.active));
      _player.play();
    }
  }

  void _onPlayCompleted(_, Emitter<VoiceState> emit) async {
    if (state.play == Status.active) {
      emit(state.copyWith(play: Status.done));
    }
  }

  void _onPlayStopped(_, Emitter<VoiceState> emit) async {
    _player.stop();
  }

  @override
  Future<void> close() async {
    _flashcardSubscription.cancel();
    _completedSubscription.cancel();
    _player.dispose();
    super.close();
  }
}
