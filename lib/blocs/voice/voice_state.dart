part of 'voice_bloc.dart';

class VoiceState extends Equatable {
  final Status record;
  final Status play;

  const VoiceState(this.record, this.play);

  @override
  List<Object?> get props => [this.record, this.play];

  VoiceState copyWith({
    Status? record,
    Status? play,
  }) {
    return VoiceState(record ?? this.record, play ?? this.play);
  }

  @override
  String toString() {
    return 'VoiceState { record: $record, play: $play }';
  }
}
