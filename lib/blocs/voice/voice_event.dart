part of 'voice_bloc.dart';

abstract class VoiceEvent extends Equatable {
  const VoiceEvent();

  @override
  List<Object?> get props => [];
}

class RecordPrepared extends VoiceEvent {}
class RecordStarted extends VoiceEvent {}
class RecordStopped extends VoiceEvent {}
class RecordFailed extends VoiceEvent {}
class PlayPrepared extends VoiceEvent {}
class PlayStarted extends VoiceEvent {}
class PlayStopped extends VoiceEvent {}
class PlayCompleted extends VoiceEvent {}
class PlayFailed extends VoiceEvent {}





