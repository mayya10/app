import 'package:bloc/bloc.dart';
import 'package:remem_me/blocs/tab/tab.dart';

import '../../models/box.dart';

class TabBloc extends Bloc<TabEvent, TabState> {
  TabBloc() : super(const TabState(true, Box.NEW)) {
    on<TabUpdated>((event, emit) => emit(state.copyWith(box: event.tab)));
    on<BoxedToggled>(
        (event, emit) => emit(state.copyWith(isBoxed: !state.isBoxed)));
    on<Restarted>(
        (event, emit) => emit(state.copyWith(restarts: state.restarts + 1)));
  }
}
