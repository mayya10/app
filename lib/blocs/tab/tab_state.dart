import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';

class TabState extends Equatable {
  final bool isBoxed;
  final Box box;
  final int restarts;

  const TabState(this.isBoxed, this.box, [this.restarts = 0]);

  @override
  List<Object?> get props => [isBoxed, box, restarts];

  TabState copyWith(
      {bool? isBoxed, Box? box, int? restarts}) {
    return TabState(
      isBoxed ?? this.isBoxed,
      box ?? this.box,
      restarts ?? this.restarts,
    );
  }

  @override
  String toString() {
    return 'TabState { isBoxed: $isBoxed, box: $box, restarts: $restarts }';
  }
}
