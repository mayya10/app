import 'package:equatable/equatable.dart';
import '../../models/box.dart';

abstract class TabEvent extends Equatable {
  const TabEvent();

  @override
  List<Object?> get props => [];
}

class TabUpdated extends TabEvent {
  final Box tab;

  const TabUpdated(this.tab);

  @override
  List<Object?> get props => [tab];

  @override
  String toString() => 'TabUpdated { tab: $tab }';
}

class BoxedToggled extends TabEvent {}

class Restarted extends TabEvent {}

