import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/verses/verses_event.dart';
import 'package:repository/repository.dart';

import '../blocs.dart';

class VersesBloc extends EntitiesBloc<VerseEntity> {

  VersesBloc()
      : super(
            repository: di.get<EntitiesRepository<VerseEntity>>(),
            initialState: InitialEntitiesState<VerseEntity>()) {
    subscribeToAccountChange();
  }

  @override
  Stream<EntitiesState<VerseEntity>> mapEventToState(
      EntitiesEvent<VerseEntity> event) async* {
    yield* super.mapEventToState(event);
    if (event is TagDeleted) {
      yield* _mapTagDeletedToState(event);
    }
  }

  Stream<EntitiesState<VerseEntity>> _mapTagDeletedToState(
    TagDeleted event,
  ) async* {
    if (state is EntitiesLoadSuccess<VerseEntity>) {
      final updatedVerses = (state as EntitiesLoadSuccess<VerseEntity>)
          .entities
          .where((verse) => verse.tags.containsKey(event.tagId))
          .map((verse) {
        final tags = new Map.of(verse.tags);
        tags.remove(event.tagId);
        return verse.copyWith(tags: tags); // todo: batch update
      }).toList();
      yield* mapEntitiesUpdatedToState(
          EntitiesUpdated<VerseEntity>(updatedVerses));
    }
  }

  @override
  List<VerseEntity> sorted(Iterable<VerseEntity> entities) {
    return entities.toSet().toList();
  }

}
