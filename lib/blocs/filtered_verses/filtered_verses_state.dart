import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';
import 'package:time_machine/time_machine.dart';

abstract class FilteredVersesState extends Equatable {
  @override
  List<Object?> get props => [];
}

class InitialFilterVersesState extends FilteredVersesState {

}
class FilterInProgress extends FilteredVersesState {}
class FilterFailure extends FilteredVersesState {}

class FilteredVersesSuccess extends FilteredVersesState {
  final List<Verse> filteredVerses;
  final LocalDate date;
  final VisibilityFilter? activeFilter;
  final bool isUpdating;

  FilteredVersesSuccess(this.filteredVerses, {
    required this.date,
    this.activeFilter,
    this.isUpdating = false,
  });

  @override
  List<Object?> get props => [filteredVerses, date, activeFilter, isUpdating];
  // list comparation: https://github.com/felangel/equatable/issues/85

  FilteredVersesSuccess copyWith({
    List<Verse>? filteredVerses,
    LocalDate? date,
    VisibilityFilter? activeFilter,
    bool? isUpdating,
  }) =>
      FilteredVersesSuccess(
        this.filteredVerses,
        date: date ?? this.date,
        activeFilter: activeFilter ?? this.activeFilter,
        isUpdating: isUpdating ?? this.isUpdating,
      );

  @override
  String toString() {
    return 'FilteredVersesLoadSuccess { filteredVerses: '
        '${filteredVerses.length}, date: $date, activeFilter: $activeFilter, '
        'isUpdating: $isUpdating }';
  }
}
