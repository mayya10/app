import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/models/models.dart';
import 'package:rxdart/rxdart.dart';
import 'package:time_machine/time_machine.dart';

import '../../services/home_widget_service.dart';
import '../blocs.dart';

class FilteredVersesBloc
    extends Bloc<FilteredVersesEvent, FilteredVersesState> {
  late TaggedVersesBloc versesBloc;
  late StreamSubscription versesSubscription;
  late StreamSubscription homeWidgetSubscription;

  FilteredVersesBloc() : super(_initialState()) {
    versesBloc = di.get<TaggedVersesBloc>();
    on<FilteredVersesEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (FilteredVersesState result) => result),
        transformer: sequential()); // process events sequentially
    versesSubscription = versesBloc.stream
        .startWith(versesBloc.state)
        .listen(_versesBlocUpdated);
    homeWidgetSubscription = stream.listen((state) {
      if (!kIsWeb && state is FilteredVersesSuccess) {
        di.get<HomeWidgetService>().updateStackWidget(state.filteredVerses);
      }
    });
  }

  static FilteredVersesState _initialState() {
    final versesBloc = di.get<TaggedVersesBloc>();
    switch (versesBloc.state.runtimeType) {
      case TaggedVersesSuccess:
        return FilteredVersesSuccess(
          (versesBloc.state as TaggedVersesSuccess).verses,
          date: DateService().today,
          activeFilter: VisibilityFilter(),
        );
      case TaggedVersesInProgress:
        return FilterInProgress();
      case InitialTaggedVersesState:
      default:
        return InitialFilterVersesState();
    }
  }

  void _versesBlocUpdated(TaggedVersesState? state) {
    if (state is TaggedVersesSuccess) {
      if (state.isUpdating) {
        add(TaggedVersesUpdated());
      } else {
        add(TaggedVersesLoaded(state.verses));
      }
    } else if (state is TaggedVersesInProgress) {
      add(FilteredVersesReloaded());
    } else if (state is TaggedVersesFailure) {
      add(TaggingFailed());
    }
  }

  Stream<FilteredVersesState> mapEventToState(
      FilteredVersesEvent event) async* {
    if (event is SearchFilterUpdated) {
      yield* _mapSearchFilterUpdatedToState(event);
    } else if (event is ReviewedTodayFilterUpdated) {
      yield* _mapReviewedTodayFilterUpdatedToState(event);
    } else if (event is TaggedVersesLoaded) {
      yield* _mapVersesLoadedToState(event);
    } else if (event is TaggedVersesUpdated) {
      yield* _mapVersesUpdatedToState();
    } else if (event is FilteredVersesReloaded) {
      yield* _mapFilteredVersesReloadedToState();
    } else if (event is AppResumed) {
      yield* _mapAppResumedToState();
    } else if (event is TaggingFailed) {
      yield FilterFailure();
    }
  }

  Stream<FilteredVersesState> _mapFilteredVersesReloadedToState() async* {
    yield FilterInProgress();
  }

  Stream<FilteredVersesState> _mapSearchFilterUpdatedToState(
    SearchFilterUpdated event,
  ) async* {
    if (state is FilteredVersesSuccess) {
      final visibilityFilter = (state as FilteredVersesSuccess)
          .activeFilter!
          .copyWith(query: event.query);
      yield _filteredVersesLoadSuccess(visibilityFilter);
    }
  }

  Stream<FilteredVersesState> _mapReviewedTodayFilterUpdatedToState(
    ReviewedTodayFilterUpdated event,
  ) async* {
    if (state is FilteredVersesSuccess) {
      final prevFilter = (state as FilteredVersesSuccess).activeFilter!;
      final visibilityFilter = event.value == null
          ? prevFilter.copyWith(nullValues: ['reviewedToday'])
          : prevFilter.copyWith(reviewedToday: event.value);
      yield _filteredVersesLoadSuccess(visibilityFilter);
    }
  }

  Stream<FilteredVersesState> _mapVersesLoadedToState(
    TaggedVersesLoaded event,
  ) async* {
    final visibilityFilter = state is FilteredVersesSuccess
        ? (state as FilteredVersesSuccess).activeFilter
        : VisibilityFilter();
    if (versesBloc.state is TaggedVersesSuccess) {
      final stopwatch = Stopwatch()..start();
      final resultState = _filteredVersesLoadSuccess(visibilityFilter);
      debugPrint(
          '$runtimeType._mapVersesLoadedToState executed in ${stopwatch.elapsed}');
      yield resultState;
    }
  }

  Stream<FilteredVersesState> _mapVersesUpdatedToState() async* {
    final state = this.state;
    if (state is FilteredVersesSuccess) {
      yield state.copyWith(isUpdating: true);
    }
  }

  Stream<FilteredVersesState> _mapAppResumedToState() async* {
    final state = this.state;
    final LocalDate today = DateService().today;
    if (state is FilteredVersesSuccess && state.date != today) {
      yield state.copyWith(date: today);
    }
  }

  FilteredVersesSuccess _filteredVersesLoadSuccess(
      VisibilityFilter? visibilityFilter) {
    final tagged = (versesBloc.state as TaggedVersesSuccess);
    var verses = _applyFilter(tagged.verses, visibilityFilter);
    if (visibilityFilter?.query == null || visibilityFilter!.query.isEmpty) {
      verses = _applyTags(verses, tagged.tags);
    }
    return FilteredVersesSuccess(
      verses,
      date: DateService().today,
      activeFilter: visibilityFilter,
    );
  }

  List<Verse> _applyFilter(List<Verse> verses, VisibilityFilter? filter) {
    return verses.where((verse) {
      return filter == null || filter.appliesTo(verse);
    }).toList();
  }

  List<Verse> _applyTags(List<Verse> verses, List<Tag> tags) {
    final Map<int, Tag> tagsMap = {for (var tag in tags) tag.id: tag};
    final isAnySelected =
        tagsMap.values.any((tag) => tag.included == true); // ||
    return verses
        .where((verse) =>
            (!isAnySelected || _isIncluded(verse, tagsMap)) &&
            !_isExcluded(verse, tagsMap))
        .toList();
  }

  bool _isIncluded(Verse verse, Map<int, Tag> tagsMap) {
    return verse.tags.keys.any((id) => tagsMap[id]?.included == true);
  }

  bool _isExcluded(Verse verse, Map<int, Tag> tagsMap) {
    return verse.tags.keys.any((id) => tagsMap[id]?.included == false);
  }

  @override
  Future<void> close() {
    versesSubscription.cancel();
    homeWidgetSubscription.cancel();
    return super.close();
  }
}
