part of 'collections_bloc.dart';

class CollectionsState extends Equatable {
  final Status fetchStatus;
  final List<Collection> collections;
  final bool hasReachedMax;
  final Query query;
  final bool isSearchVisible;

  const CollectionsState({
    this.collections = const <Collection>[],
    this.fetchStatus = Status.none,
    this.hasReachedMax = false,
    this.query = const Query(),  // todo: use account language
    this.isSearchVisible = false,
  });

  @override
  List<Object?> get props => [collections, fetchStatus, hasReachedMax, query, isSearchVisible];

  @override
  String toString() => 'CollectionsState { collections: ${collections.length}, '
      'fetchStatus: $fetchStatus, hasReachedMax: $hasReachedMax, query: $query, '
      'isSearchVisible: $isSearchVisible }';

  CollectionsState copyWith({
    List<Collection>? collections,
    Status? fetchStatus,
    bool? hasReachedMax,
    Query? query,
    bool? isSearchVisible,
  }) {
    return CollectionsState(
        collections: collections ?? this.collections,
        fetchStatus: fetchStatus ?? this.fetchStatus,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        query: query ?? this.query,
        isSearchVisible: isSearchVisible ?? this.isSearchVisible,
    );
  }
}
