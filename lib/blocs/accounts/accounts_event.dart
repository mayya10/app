import 'package:repository/repository.dart';

import '../blocs.dart';

class AccountsAddedCurrent extends EntitiesAdded<AccountEntity> {
  const AccountsAddedCurrent(super.entities, {super.persist = true});
}

class AccountsUpdatedCurrent extends EntitiesUpdated<AccountEntity> {
  const AccountsUpdatedCurrent(super.entities, {super.persist = true, super.wait = true});
}
