import 'dart:async';
import 'dart:collection';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/text_to_speech/tts_handler.dart';
import 'package:repository/repository.dart';

import '../blocs.dart';

class AccountsBloc extends EntitiesBloc<AccountEntity> {
  late final ReplicationBloc rplBloc;
  late final AuthBloc auth;
  late final CurrentAccount currentAccount;
  late final StreamSubscription connectionSubscription;
  late final StreamSubscription accountSelectedSubscription;

  AccountsBloc(
      [EntitiesState<AccountEntity>? initialState =
          const InitialEntitiesState<AccountEntity>()])
      : super(
            repository: di.get<EntitiesRepository<AccountEntity>>(),
            initialState: initialState) {
    rplBloc = di.get<ReplicationBloc>();
    auth = di.get<AuthBloc>();
    currentAccount = di.get<CurrentAccount>();
    connectionSubscription =
        di.get<ConnectivityCubit>().stream.distinct((prev, next) {
      debugPrint('prev: $prev | next: $next');
      return ![prev, next].contains(ConnectivityResult.none) || prev == next;
    }).listen((ConnectivityResult result) {
      if (result != ConnectivityResult.none && auth.state is LoginSuccess) {
        if (currentAccount.state != null) {
          if (!kIsWeb) {
            debugPrint('RplStarted from ConnectivityChange');
            rplBloc.add(const RplStarted());
          }
        } else if (state is! EntitiesLoadInProgress<AccountEntity>) {
          add(EntitiesLoaded<AccountEntity>());
        }
      }
    });
    accountSelectedSubscription = currentAccount.stream
        .distinct((prev, next) => prev?.id == next?.id)
        .skip(1) // first event is not caught by distinct
        .listen((state) {
      TtsHandler().stop();
      di.get<UndoBloc>().add(UndoCleared());
      if (!kIsWeb && state != null && rplBloc.state is! ReplicationInProgress) {
        debugPrint('RplStarted from Account selected');
        rplBloc.add(const RplStarted());
      }
    });
  }

  @override
  Stream<EntitiesState<AccountEntity>> mapEntitiesLoadedToState(
      EntitiesLoaded<AccountEntity> event) async* {
    yield EntitiesLoadInProgress();
    try {
      var entities = sorted(await repository.readList());
      if (entities.isNotEmpty) {
        AccountEntity? currentEntity;
        final currentId = currentAccount.id;
        if (currentId > -1) {
          currentEntity = entities.firstWhereOrNull((a) => a.id == currentId);
        }
        currentEntity ??= entities.first;
        await currentAccount.setEntity(currentEntity);
      }
      yield EntitiesLoadSuccess<AccountEntity>(entities);
    } on MessageException catch (e) {
      MessageService().warn(e.messages['messages']!.join('\n'));
      yield EntitiesLoadFailure<AccountEntity>('AccountsLoadedException');
    } on Exception catch (e) {
      MessageService().warn(e.toString());
      yield EntitiesLoadFailure<AccountEntity>('AccountsLoadedException');
    }
    if (!kIsWeb) {
      debugPrint('RplStarted from AccountEntitiesLoaded');
      rplBloc.add(const RplStarted());
    }
  }

  @override
  Stream<EntitiesState<AccountEntity>> mapEntitiesAddedToState(
      EntitiesAdded<AccountEntity> event) {
    Stream<EntitiesState<AccountEntity>> result =
        super.mapEntitiesAddedToState(event);
    if (event is AccountsAddedCurrent) {
      result = result.asyncMap(_determineCurrentAccount(event.entities));
    }
    return result;
  }

  @override
  Stream<EntitiesState<AccountEntity>> mapEntitiesUpdatedToState(
      EntitiesUpdated<AccountEntity> event) {
    Stream<EntitiesState<AccountEntity>> result = super
        .mapEntitiesUpdatedToState(event);
    if (event is AccountsUpdatedCurrent) {
      result = result.asyncMap(_determineCurrentAccount(event.entities));
    }
    return result;
  }

  // todo: test to make sure a current account gets set
  _determineCurrentAccount(List<AccountEntity> options) =>
      (EntitiesState<AccountEntity> nextState) async {
        if (nextState is EntitiesLoadSuccess<AccountEntity>) {
          var active = options.where((entity) => !entity.deleted).toList();
          if (active.isEmpty) {
            active =
                nextState.entities.where((entity) => !entity.deleted).toList();
          }
          if (active.isNotEmpty) {
            await currentAccount.setEntity(active[0]);
          } else {
            debugPrint('No active account. Setting currentAccount to null.');
            currentAccount.setAccount(null);
          }
        }
        return nextState;
      };

  @override
  void subscribeToAuthChange() {
    final auth = di.get<AuthBloc>();
    authSubscription = auth.stream.listen((authState) {
      if (authState is LoginSuccess) {
        if (state is! EntitiesLoadInProgress<AccountEntity>) {
          add(EntitiesLoaded<AccountEntity>());
        }
      } else if (authState is NoAuth) {
        debugPrint('authStat is NoAuth. Setting currentAccount to null.');
        currentAccount.setAccount(null);
      }
    });
  }

  @override
  Future<void> close() async {
    connectionSubscription.cancel();
    accountSelectedSubscription.cancel();
    return super.close();
  }

  @override
  List<AccountEntity> sorted(Iterable<AccountEntity> entities) {
    return entities.toSet().sortedBy((it) => it.name);
  }
}
