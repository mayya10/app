import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:app_core/app_core.dart';
import 'package:repository/repository.dart';

import '../blocs.dart';

class BinBloc<T extends Entity> extends Bloc<BinEvent<T>, BinState<T>> {
  final EntitiesRepository<T> repository;
  final EntitiesBloc<T> entitiesBloc;

  BinBloc(
      {required this.repository,
      required this.entitiesBloc,
      required BinState<T> initialState})
      : super(initialState) {
    on<BinEvent<T>>(
            (event, emit) => emit.forEach(mapEventToState(event),
            onData: (BinState<T> result) => result),
        transformer: sequential());  // process events sequentially
  }

  Stream<BinState<T>> mapEventToState(BinEvent<T> event) async* {
    if (event is BinLoaded<T>) {
      yield* mapBinLoadedToState(event);
    } else if (event is Shredded) {
      yield* mapShreddedToState();
    } else if (event is Restored) {
      yield* mapRestoredToState();
    } else if (event is WasteSelected) {
      yield* mapWasteSelectedToState(event as WasteSelected<T>);
    } else if (event is AllWasteSelected) {
      yield* mapAllWasteSelectedToState();
    } else if (event is NoWasteSelected) {
      yield* mapNoWasteSelectedToState();
    }
  }

  Stream<BinState<T>> mapBinLoadedToState(BinLoaded<T> event) async* {
    yield BinLoadInProgress();
    try {
      var entities = await repository.readList(
          accountId: event.account?.id, deleted: true);
      yield BinLoadSuccess<T>(entities);
    } on MessageException {
      yield BinLoadFailure<T>('RemoteException');
    }
  }

  Stream<BinState<T>> mapShreddedToState() async* {
    final BinState<T> _state = state;
    if (_state is BinLoadSuccess<T>) {
      yield _state.copyWith(isUpdating: true);
      await Future.forEach<T>(_state.selection.values, (entity) async {
        await repository.delete(entity);
      });
      yield BinLoadSuccess<T>(_reducedList(_state.selection.values));
    }
  }

  Stream<BinState<T>> mapRestoredToState() async* {
    final BinState<T> _state = state;
    if (_state is BinLoadSuccess<T>) {
      yield _state.copyWith(isUpdating: true);
      final List<T> restoredEntities = _state.selection.values
          .map((entity) => entity.copyWith(deleted: false) as T)
          .toList();
      await repository.updateList(restoredEntities);
      entitiesBloc.add(EntitiesAdded<T>(restoredEntities, persist: false));
      yield BinLoadSuccess(_reducedList(_state.selection.values));
    }
  }

  Stream<BinState<T>> mapWasteSelectedToState(WasteSelected<T> event) async* {
    final BinState<T> _state = state;
    if (_state is BinLoadSuccess<T>) {
      final updatedSelection = Map.of(_state.selection);
      if (_state.selection.keys.contains(event.entity.id)) {
        updatedSelection.remove(event.entity.id);
      } else {
        updatedSelection[event.entity.id] = event.entity;
      }
      yield _state.copyWith(selection: updatedSelection);
    }
  }

  Stream<BinState<T>> mapAllWasteSelectedToState() async* {
    final BinState<T> _state = state;
    if (_state is BinLoadSuccess<T>) {
      yield _state.copyWith(
          selection: Map.fromEntries(
              _state.entities.map((entity) => MapEntry(entity.id, entity))));
    }
  }

  Stream<BinState<T>> mapNoWasteSelectedToState() async* {
    final BinState<T> _state = state;
    if (_state is BinLoadSuccess<T>) {
      yield _state.copyWith(selection: const {});
    }
  }

  List<T> _reducedList(Iterable<T> entities) {
    return (state as BinLoadSuccess<T>)
        .entities
        .where((entity) => !entities.contains(entity))
        .toList();
  }
}
