import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/foundation.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import '../blocs.dart';

abstract class EntitiesBloc<T extends Entity>
    extends Bloc<EntitiesEvent<T>, EntitiesState<T>> {
  final EntitiesRepository<T> repository;
  StreamSubscription? accountSubscription;
  StreamSubscription? authSubscription;

  EntitiesBloc({required this.repository, required EntitiesState? initialState})
      : super(initialState as EntitiesState<T>) {
    on<EntitiesEvent<T>>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (EntitiesState<T> result) => result),
        transformer: sequential()); // process events sequentially
    subscribeToAuthChange();
  }

  Stream<EntitiesState<T>> mapEventToState(EntitiesEvent<T> event) async* {
    if (event is EntitiesLoaded<T>) {
      yield* mapEntitiesLoadedToState(event);
    } else if (event is EntitiesAdded<T>) {
      yield* mapEntitiesAddedToState(event);
    } else if (event is EntitiesUpdated<T>) {
      yield* mapEntitiesUpdatedToState(event);
    } else if (event is CurrentAccountUnset<T>) {
      yield EntitiesLoadFailure<T>('No current account');
    }
  }

  Stream<EntitiesState<T>> mapEntitiesLoadedToState(
      EntitiesLoaded<T> event) async* {
    yield EntitiesLoadInProgress<T>();
    try {
      var entities = await repository.readList(accountId: event.account?.id);
      yield EntitiesLoadSuccess<T>(sorted(entities));
    } on MessageException catch (e) {
      MessageService().warn(e.messages['messages']!.join('\n'));
      yield EntitiesLoadFailure<T>('EntitiesLoadedException');
    } on Exception catch (e) {
      MessageService().warn(e.toString());
      yield EntitiesLoadFailure<T>('EntitiesLoadedException');
    }
  }

  Stream<EntitiesState<T>> mapEntitiesAddedToState(
      EntitiesAdded<T> event) async* {
    if (event.entities.isEmpty) return; // RETURN
    final state = this.state;
    if (state is EntitiesLoadSuccess<T>) {
      try {
        if (event.persist) {
          yield state.copyWith(isUpdating: true);
          await repository.createList(event.entities);
        }
        final List<T> updatedEntities = state.entities + event.entities;
        yield state.copyWith(
            entities: sorted(updatedEntities), isUpdating: false);
      } on MessageException catch (e) {
        MessageService().warn(e.messages['messages']!.join('\n'));
        yield state.copyWith(isUpdating: false);
      }
    } else if (state is! EntitiesLoadInProgress<T>) {
      yield EntitiesLoadSuccess<T>(sorted(event.entities));
    }
  }

  Stream<EntitiesState<T>> mapEntitiesUpdatedToState(
      EntitiesUpdated<T> event) async* {
    if (event.entities.isEmpty) return; // RETURN
    final state = this.state;
    if (state is EntitiesLoadSuccess<T> && event.entities.isNotEmpty) {
      try {
        final stopwatch = Stopwatch()..start();
        if (event.persist) {
          if (event.wait) {
            yield state.copyWith(isUpdating: true);
            await repository.updateList(event.entities);
          } else {
            repository.updateList(event.entities).then((_) {},
                onError: (e) =>
                    MessageService().warn(e.messages['messages']!.join('\n')));
          }
        }
        final List<T> allEntities = state.entities
            .map((original) =>
                event.entities
                    .firstWhereOrNull((updated) => updated.id == original.id) ??
                original)
            .where((entity) => !entity.deleted)
            .toList();
        final resultState = state.copyWith(entities: sorted(allEntities));
        debugPrint(
            '$runtimeType.mapEntitiesUpdatedToState executed in ${stopwatch.elapsed}');
        yield resultState;
      } on MessageException catch (e) {
        MessageService().warn(e.messages['messages']!.join('\n'));
        yield state.copyWith(isUpdating: false);
      }
    }
  }

  void subscribeToAccountChange() {
    final currentAccount = di.get<CurrentAccount>();
    accountSubscription = currentAccount.stream
        .startWith(currentAccount.state)
        .distinct((prev, next) => prev?.id == next?.id)
        .skip(1) // skips the starting state
        .listen((account) => add(account == null
            ? CurrentAccountUnset<T>()
            : EntitiesLoaded<T>(account: account)));
  }

  void subscribeToAuthChange() {
    final currentAccount = di.get<CurrentAccount>();
    final auth = di.get<AuthBloc>();
    authSubscription = auth.stream.listen((authState) {
      if (authState is LoginSuccess && currentAccount.state != null) {
        if (state is! EntitiesLoadInProgress<T>) {
          add(EntitiesLoaded<T>(account: currentAccount.state));
        }
      }
    });
  }

  List<T> sorted(Iterable<T> entities);

  @override
  Future<void> close() {
    accountSubscription?.cancel();
    authSubscription?.cancel();
    return super.close();
  }
}
