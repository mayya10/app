import 'package:equatable/equatable.dart';
import 'package:remem_me/models/account.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

abstract class EntitiesEvent<T extends Entity> extends Equatable {
  const EntitiesEvent();

  @override
  List<Object?> get props => [];
}


class EntitiesLoaded<T extends Entity> extends EntitiesEvent<T> {
  final Account? account;

  EntitiesLoaded({this.account});
}

class CurrentAccountUnset<T extends Entity> extends EntitiesEvent<T> {}


class EntitiesAdded<T extends Entity> extends EntitiesEvent<T> {
  final List<T> entities;
  final bool persist;

  const EntitiesAdded(this.entities, {this.persist = true});

  @override
  List<Object?> get props => [entities, persist];

  @override
  String toString() => 'EntitiesAdded { entities: ${entities.runtimeType}(${entities.length}), persist: $persist }';
}


class EntitiesUpdated<T extends Entity> extends EntitiesEvent<T> {
  final List<T> entities;
  final bool persist;
  final bool wait;

  const EntitiesUpdated(this.entities, {this.persist = true, this.wait = true});

  @override
  List<Object?> get props => [entities, persist, wait];

  @override
  String toString() => 'EntitiesUpdated { updatedEntities: ${entities.length}, '
      'persist: $persist, wait: $wait }';
}
