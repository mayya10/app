import 'package:equatable/equatable.dart';
import 'package:repository/repository.dart';


abstract class TaggedVersesEvent extends Equatable {
  const TaggedVersesEvent();
  @override
  List<Object?> get props => [];
}

class TagsUpdated extends TaggedVersesEvent {}
class VersesUpdated extends TaggedVersesEvent {}

class VersesLoaded extends TaggedVersesEvent {
  final List<VerseEntity> verses;

  const VersesLoaded(this.verses);

  @override
  List<Object?> get props => [verses];

  @override
  String toString() => 'VersesLoaded { verses: ${verses.length} }';
}

class TaggedVersesReloaded extends TaggedVersesEvent {}
class VersesFailed extends TaggedVersesEvent {}

class TagsLoaded extends TaggedVersesEvent {
  final List<TagEntity> tags;

  const TagsLoaded(this.tags);

  @override
  List<Object?> get props => [tags];

  @override
  String toString() => 'TagsLoaded { tags: ${tags.length} }';
}
