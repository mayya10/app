import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import '../blocs.dart';

class TaggedVersesBloc extends Bloc<TaggedVersesEvent, TaggedVersesState> {
  late VersesBloc versesBloc;
  late TagsBloc tagsBloc;
  late StreamSubscription versesSubscription;
  late StreamSubscription tagsSubscription;

  TaggedVersesBloc() : super(InitialTaggedVersesState()) {
    versesBloc = di.get<VersesBloc>();
    tagsBloc = di.get<TagsBloc>();
    on<TaggedVersesEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (TaggedVersesState result) => result),
        transformer: sequential()); // process events sequentially
    versesSubscription = versesBloc.stream
        .startWith(versesBloc.state)
        .listen(_versesBlocUpdated);
    tagsSubscription =
        tagsBloc.stream.startWith(tagsBloc.state).listen(_tagsBlocUpdated);
  }

  void _tagsBlocUpdated(EntitiesState<TagEntity>? state) {
    if (state is EntitiesLoadSuccess<TagEntity>) {
      if (state.isUpdating) {
        add(TagsUpdated());
      } else {
        add(TagsLoaded(state.entities));
      }
    } else if (state is EntitiesLoadFailure<TagEntity>) {
      add(TagsLoaded([])); // fail silently
    }
  }

  void _versesBlocUpdated(EntitiesState<VerseEntity>? state) {
    if (state is EntitiesLoadSuccess<VerseEntity>) {
      if (state.isUpdating) {
        add(VersesUpdated());
      } else {
        add(VersesLoaded(state.entities));
      }
    } else if (state is EntitiesLoadInProgress<VerseEntity>) {
      add(TaggedVersesReloaded());
    } else if (state is EntitiesLoadFailure<VerseEntity>) {
      add(VersesFailed());
    }
  }

  Stream<TaggedVersesState> mapEventToState(TaggedVersesEvent event) async* {
    if (event is VersesUpdated) {
      yield* _mapUpdatedToState();
    } else if (event is TagsUpdated) {
      yield* _mapUpdatedToState();
    } else if (event is VersesLoaded) {
      yield* _mapVersesLoadedToState(event);
    } else if (event is TagsLoaded) {
      yield* _mapTagsLoadedToState(event);
    } else if (event is TaggedVersesReloaded) {
      yield* _mapTaggedVersesReloadedToState();
    } else if (event is VersesFailed) {
      yield TaggedVersesFailure();
    }
  }

  Stream<TaggedVersesState> _mapTaggedVersesReloadedToState() async* {
    yield TaggedVersesInProgress();
  }

  Stream<TaggedVersesState> _mapUpdatedToState() async* {
    final state = this.state;
    if (state is TaggedVersesSuccess) {
      yield state.copyWith(isUpdating: true);
    }
  }

  Stream<TaggedVersesState> _mapVersesLoadedToState(
    VersesLoaded event,
  ) async* {
    if (tagsBloc.state is EntitiesLoadSuccess<TagEntity>) {
      final stopwatch = Stopwatch()..start();
      final resultState = _taggedVersesLoadSuccess(
          (tagsBloc.state as EntitiesLoadSuccess<TagEntity>).entities,
          event.verses);
      debugPrint('$runtimeType._mapVersesLoadedToState executed in ${stopwatch.elapsed}');
      yield resultState;
    } else if (tagsBloc.state is EntitiesLoadFailure<TagEntity>) {
      yield _taggedVersesLoadSuccess([], event.verses);  // fail silently
    }
  }

  Stream<TaggedVersesState> _mapTagsLoadedToState(
    TagsLoaded event,
  ) async* {
    if (versesBloc.state is EntitiesLoadSuccess<VerseEntity>) {
      yield _taggedVersesLoadSuccess(event.tags,
          (versesBloc.state as EntitiesLoadSuccess<VerseEntity>).entities);
    }
  }

  TaggedVersesSuccess _taggedVersesLoadSuccess(
      List<TagEntity> tags, List<VerseEntity> entities) {
    final verses = VerseService().fromEntities(entities);
    final nonEmptyTags =
        _mapVersesToTags(verses, tags).where((tag) => tag.size! > 0).toList();

    return TaggedVersesSuccess(
        verses: _mapTagsToVerses(nonEmptyTags, verses), tags: nonEmptyTags);
  }

  List<Verse> _mapTagsToVerses(List<TagEntity> tags, List<Verse> verses) {
    if (verses.isNotEmpty && tags.isNotEmpty) {
      return verses
          .map((verse) => verse.copyWith(tags: _tagsOfVerse(verse, tags)))
          .toList();
    } else {
      return verses;
    }
  }

  Map<int, String?> _tagsOfVerse(Verse verse, List<TagEntity> tags) {
    final Map<int, TagEntity?> tagsMap = {for (var tag in tags) tag.id: tag};
    final entries = verse.tags.entries
        .where((entry) => tagsMap[entry.key] != null)
        .map((entry) =>
            MapEntry<int, String?>(entry.key, tagsMap[entry.key]!.text));
    return Map.fromEntries(entries);
  }

  List<Tag> _mapVersesToTags(List<Verse> verses, List<TagEntity> tags) {
    return tags.map((tag) {
      return Tag.fromEntity(tag,
          size:
              verses.where((verse) => verse.tags.keys.contains(tag.id)).length);
    }).toList();
  }

  @override
  Future<void> close() {
    versesSubscription.cancel();
    tagsSubscription.cancel();
    return super.close();
  }
}
