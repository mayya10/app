import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';


abstract class SelectionEvent extends Equatable {
  const SelectionEvent();

  @override
  List<Object?> get props => [];
}

class SearchActivated extends SelectionEvent {}
class SearchDeactivated extends SelectionEvent {}
class SelectionCleared extends SelectionEvent {}


class SelectionUpdated extends SelectionEvent {
  final Map<int, Verse> selection;

  const SelectionUpdated(this.selection);

  @override
  List<Object?> get props => [selection];

  @override
  String toString() => 'SelectionUpdated { selection: ${selection.keys} }';
}

