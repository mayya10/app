import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';


class SelectionState extends Equatable {
  final Map<int, Verse> selection;
  final isSearchActive;

  SelectionState(this.selection, { this.isSearchActive = false });

  @override
  List<Object?> get props => [selection, isSearchActive];


  SelectionState copyWith({
    Map<int, Verse>? selection,
    bool? isSearchActive,
  }) {
    return SelectionState(
        selection ?? this.selection,
        isSearchActive: isSearchActive ?? this.isSearchActive,
    );
  }


  @override
  String toString() => 'Selection { selection: ${selection.length} }';
}

