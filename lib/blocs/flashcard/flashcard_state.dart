import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';

abstract class FlashcardState extends Equatable {
  const FlashcardState();

  @override
  List<Object?> get props => [];
}

class FlashcardLoadInProgress extends FlashcardState {}

class FlashcardEnd extends FlashcardState {}

abstract class FlashcardRun extends FlashcardState {
  final Box? box;
  final List<Verse> verses;
  final int index;
  final bool inverse; // true -> passage on front, reference on back
  final bool flipping; // true -> flip animation is running
  final bool reverse; // true -> back is visible
  final bool visible; // false -> hide card (e.g. during app navigation)
  final Set<FlashcardHint>? hinting;
  final int revealLines; // 2 -> display 2 lines of the passage

  const FlashcardRun({
    this.box,
    required this.verses,
    this.index = 0,
    required this.inverse,
    required this.reverse,
    required this.flipping,
    required this.visible,
    this.hinting,
    required this.revealLines,
  });

  Verse get verse => verses[index];

  @override
  List<Object?> get props => [
        verses,
        box,
        index,
        inverse,
        reverse,
        flipping,
        visible,
        hinting,
        revealLines,
      ];

  FlashcardRun copyWith({
    List<Verse>? verses,
    int? index,
    bool? reverse,
    bool? flipping,
    bool? visible,
    Set<FlashcardHint>? hinting,
    int? revealLines,
  });
}

class SingleFlashcardRun extends FlashcardRun {
  const SingleFlashcardRun(
      {box,
      required verses,
      index = 0,
      required inverse,
      reverse = false,
      flipping = false,
      visible = true,
      hinting,
      revealLines = 0})
      : super(
            box: box,
            verses: verses,
            index: index,
            inverse: inverse,
            reverse: reverse,
            flipping: flipping,
            visible: visible,
            hinting: hinting,
            revealLines: revealLines);

  @override
  SingleFlashcardRun copyWith({
    List<Verse>? verses,
    int? index,
    bool? reverse,
    bool? flipping,
    bool? visible,
    Set<FlashcardHint>? hinting,
    int? revealLines,
  }) {
    return SingleFlashcardRun(
        box: this.box,
        verses: verses ?? this.verses,
        index: index ?? this.index,
        inverse: this.inverse,
        reverse: reverse ?? this.reverse,
        flipping: flipping ?? this.flipping,
        visible: visible ?? this.visible,
        hinting: hinting ?? this.hinting,
        revealLines: revealLines ?? this.revealLines);
  }

  @override
  String toString() {
    return 'SingleFlashcardRun { verse: $verse, box: $box, inverse: $inverse, '
        'reverse: $reverse, flipping: $flipping, visible: $visible, '
        'hinting: $hinting, revealLines: $revealLines }';
  }
}

class MultiFlashcardRun extends FlashcardRun {
  final int countRemembered;
  final int countForgotten;
  final MultiFlashcardRun? before;

  const MultiFlashcardRun({
    required verses,
    box,
    required inverse,
    reverse = false,
    flipping = false,
    visible = true,
    revealLines = 0,
    hinting,
    index = 0,
    this.countRemembered = 0,
    this.countForgotten = 0,
    this.before,
  }) : super(
            box: box,
            verses: verses,
            index: index,
            inverse: inverse,
            reverse: reverse,
            flipping: flipping,
            visible: visible,
            hinting: hinting,
            revealLines: revealLines);

  @override
  List<Object?> get props => [
        verses,
        box,
        index,
        inverse,
        reverse,
        flipping,
        visible,
        hinting,
        revealLines,
        countRemembered,
        countForgotten,
        before,
      ];

  @override
  MultiFlashcardRun copyWith({
    bool? reverse,
    bool? flipping,
    bool? visible,
    Set<FlashcardHint>? hinting,
    int? revealLines,
    int? index,
    List<Verse>? verses,
    int? countRemembered,
    int? countForgotten,
    MultiFlashcardRun? before,
  }) {
    return MultiFlashcardRun(
      verses: verses ?? this.verses,
      box: this.box,
      inverse: this.inverse,
      reverse: reverse ?? this.reverse,
      flipping: flipping ?? this.flipping,
      visible: visible ?? this.visible,
      hinting: hinting ?? this.hinting,
      revealLines: revealLines ?? this.revealLines,
      index: index ?? this.index,
      countRemembered: countRemembered ?? this.countRemembered,
      countForgotten: countForgotten ?? this.countForgotten,
      before: before ?? this.before,
    );
  }

  @override
  String toString() {
    return 'MultiFlashcardRun { verses: ${verses.length}, box: $box, '
        'inverse: $inverse, reverse: $reverse, flipping: $flipping, '
        'visible: $visible, hinting: $hinting, revealLines: $revealLines, '
        'index: $index, countRemembered: $countRemembered, '
        'countForgotten: $countForgotten, before: ${before != null}';
  }
}
