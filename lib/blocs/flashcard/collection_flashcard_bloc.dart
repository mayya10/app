import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:rxdart/rxdart.dart';

import '../../models/models.dart';
import '../../routers/routers.dart';

class CollectionFlashcardBloc extends FlashcardBloc {
  final CollectionDetailBloc collectionBloc;
  @override
  get defaultHints => const <FlashcardHint>{FlashcardHint.subtitle};

  CollectionFlashcardBloc({required this.collectionBloc})
      : super(FlashcardLoadInProgress());

  @override
  Stream<FlashcardState> mapSingleInitiatedToState(
      SingleFlashcardInitiated event) async* {
    final collectionState = await collectionBloc.stream
        .startWith(collectionBloc.state)
        .firstWhere((state) =>
            state is CollectionDetailLoadSuccess ||
            state is CollectionDetailLoadFailure);
    if (collectionState is CollectionDetailLoadSuccess) {
      final index = collectionState.collection.verses
          .indexWhere((verse) => verse.id == event.id);
      yield* mapSingleLoadedToState(FlashcardLoaded(
          verses: collectionState.collection.verses,
          index: index,
          single: true,
          inverse: false));
    }
  }

  @override
  Stream<FlashcardState> mapActionToState(FlashcardAction event) {
    throw UnimplementedError();
  }

  @override
  Stream<FlashcardState> mapStudiedToState() async* {
    var run = state;
    if (run is FlashcardRun) {
      di.get<NavigationCubit>().go(CollectionsPath(
          id: (collectionBloc.state as CollectionDetailLoadSuccess)
              .collection
              .id,
          verseId: run.verse.id,
          activity: CollectionActivity.study));
    }
  }
}
