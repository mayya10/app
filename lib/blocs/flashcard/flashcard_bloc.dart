import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';

import '../blocs.dart';

abstract class FlashcardBloc extends Bloc<FlashcardEvent, FlashcardState> {
  final defaultHints = const <FlashcardHint>{};

  FlashcardBloc(FlashcardState initialState) : super(initialState) {
    on<FlashcardEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (FlashcardState result) => result),
        transformer: sequential()); // process events sequentially
  }

  Stream<FlashcardState> mapEventToState(FlashcardEvent event) async* {
    if (event is SingleFlashcardInitiated) {
      yield* mapSingleInitiatedToState(event);
    } else if (event is FlashcardLoaded && event.single) {
      yield* mapSingleLoadedToState(event);
    } else if (event is FlashcardLoaded && !event.single) {
      yield* mapMultiLoadedToState(event);
    } else if (event is FlashcardAction) {
      yield* mapActionToState(event);
    } else if (event is FlipStarted) {
      yield* mapFlipStartedToState(event);
    } else if (event is FlipEnded) {
      yield* mapFlipEndedToState(event);
    } else if (event is FlipReset) {
      yield* mapFlipResetToState(event);
    } else if (event is FlashcardVerseUpdated) {
      yield* mapFlashcardVerseUpdatedToState(event);
    } else if (event is LineRevealed) {
      yield* mapLineRevealedToState(event);
    } else if (event is FlashcardHinted) {
      yield* mapHintedToState();
    } else if (event is FlashcardStudied) {
      yield* mapStudiedToState();
    }
  }

  Stream<FlashcardState> mapSingleInitiatedToState(
      SingleFlashcardInitiated event);

  Stream<FlashcardState> mapSingleLoadedToState(
      FlashcardLoaded event) async* {
    yield SingleFlashcardRun(
      verses: event.verses,
      index: event.index,
      box: event.box,
      inverse: event.inverse,
      hinting: event.inverse ? null : defaultHints,
      reverse: [Box.NEW, Box.DUE].contains(event.box) ? true : false,
    );
  }

  Stream<FlashcardState> mapMultiLoadedToState(
      FlashcardLoaded event) async* {
    yield MultiFlashcardRun(
        verses: [...event.verses],
        index: event.index,
        box: event.box,
        inverse: event.inverse,
        hinting: event.inverse ? null : defaultHints,
        reverse: false,
        flipping: false);
  }

  Stream<FlashcardState> mapActionToState(FlashcardAction event);

  Stream<FlashcardState> mapFlipStartedToState(FlipStarted event) async* {
    if (state is FlashcardRun)
      yield (state as FlashcardRun).copyWith(flipping: true);
  }

  Stream<FlashcardState> mapFlipEndedToState(FlipEnded event) async* {
    if (state is FlashcardRun) {
      final run = state as FlashcardRun;
      yield run.copyWith(reverse: !run.reverse, flipping: false);
    }
  }

  Stream<FlashcardState> mapFlipResetToState(FlipReset event) async* {
    if (state is FlashcardRun) {
      final run = state as FlashcardRun;
      yield run.copyWith(reverse: false, visible: true);
    }
  }

  Stream<FlashcardState> mapFlashcardVerseUpdatedToState(
      FlashcardVerseUpdated event) async* {
    if (state is FlashcardRun) {
      final run = state as FlashcardRun;
      yield run.copyWith(
          verses: run.verses
              .map((verse) => verse.id == event.verse.id ? event.verse : verse)
              .toList());
    }
  }

  Stream<FlashcardState> mapHintedToState() async* {
    if (state is FlashcardRun) {
      final run = state as FlashcardRun;
      assert(run.hinting != null); // runs that can hint need to start with []
      var hints = {...run.hinting!};
      final hasSubtitle = VerseService().subtitle(run.verse).isNotEmpty;
      final hasTags = run.verse.tags.isNotEmpty;
      if (hasSubtitle && !hints.contains(FlashcardHint.subtitle)) {
        hints.add(FlashcardHint.subtitle);
      } else if (hasTags && !hints.contains(FlashcardHint.tags)) {
        hints.add(FlashcardHint.tags);
      } else if (!hints.contains(FlashcardHint.beginning)) {
        hints.add(FlashcardHint.beginning);
      } else {
        hints = defaultHints;
      }
      yield run.copyWith(hinting: hints);
    }
  }

  Stream<FlashcardState> mapLineRevealedToState(LineRevealed event) async* {
    if (state is FlashcardRun) {
      final run = state as FlashcardRun;
      yield run.copyWith(revealLines: event.lines ?? (run.revealLines + 1));
    }
  }

  Stream<FlashcardState> mapStudiedToState();
}
