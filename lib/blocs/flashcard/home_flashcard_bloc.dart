import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/routers/routers.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';

import '../blocs.dart';

class HomeFlashcardBloc extends FlashcardBloc {
  @override
  get defaultHints => di.get<CurrentAccount>().referenceIncluded
      ? <FlashcardHint>{}
      : {FlashcardHint.subtitle, FlashcardHint.tags};

  late VersesBloc versesBloc;
  late ScoresBloc scoresBloc;
  late EntitiesRepository<VerseEntity> versesRepository;
  late NavigationCubit nav;

  HomeFlashcardBloc() : super(FlashcardLoadInProgress()) {
    versesBloc = di.get<VersesBloc>();
    scoresBloc = di.get<ScoresBloc>();
    versesRepository = di.get<EntitiesRepository<VerseEntity>>();
    nav = di.get<NavigationCubit>();
  }

  @override
  Stream<FlashcardState> mapActionToState(FlashcardAction event) async* {
    if (state is! FlashcardRun) yield state;
    _updateEntitiesFromAction(event);
    yield* _nextStateFromAction(event);
  }

  void _updateEntitiesFromAction(FlashcardAction event) {
    final run = state as FlashcardRun?;
    final undoBloc = di.get<UndoBloc>();
    switch (event.action) {
      case VerseAction.COMMITTED:
        versesBloc.add(
          EntitiesUpdated<Verse>([VerseService().committed(run!.verse)],
              wait: false),
        );
        undoBloc.add(UndoAdded([
          Undo(EntityAction.update, <VerseEntity>[run.verse])
        ]));
        break;
      case VerseAction.FORGOTTEN:
        if (!run!.inverse) {
          versesBloc.add(
            EntitiesUpdated<Verse>([VerseService().forgotten(run.verse)],
                wait: false),
          );
          undoBloc.add(UndoAdded([
            Undo(EntityAction.update, <VerseEntity>[run.verse])
          ]));
        }
        break;
      case VerseAction.REMEMBERED:
        if (!run!.inverse) {
          versesBloc.add(
            EntitiesUpdated<Verse>([VerseService().remembered(run.verse)],
                wait: false),
          );
          final score = ScoreEntity(
            vkey: run.verse.id.toString(),
            date: DateService().today,
            change: TextService().countWordsOfText(run.verse.passage),
            accountId: run.verse.accountId!,
          );
          scoresBloc.add(EntitiesAdded<ScoreEntity>([score]));
          undoBloc.add(UndoAdded([
            Undo(EntityAction.update, <VerseEntity>[run.verse]),
            Undo(EntityAction.create, <ScoreEntity>[score]),
          ]));
        }
        break;
      case VerseAction.MOVED_TO_DUE:
        versesBloc.add(
          EntitiesUpdated<Verse>([VerseService().movedToDue(run!.verse)],
              wait: false),
        );
        undoBloc.add(UndoAdded([
          Undo(EntityAction.update, <VerseEntity>[run.verse])
        ]));
        break;
      case VerseAction.MOVED_TO_NEW:
        versesBloc.add(
          EntitiesUpdated<Verse>([VerseService().movedToNew(run!.verse)],
              wait: false),
        );
        undoBloc.add(UndoAdded([
          Undo(EntityAction.update, <VerseEntity>[run.verse])
        ]));
        break;
      default:
        undoBloc.add(UndoCleared());
        break;
    }
  }

  Stream<FlashcardState> _nextStateFromAction(FlashcardAction event) async* {
    final run = state;
    if (run is! MultiFlashcardRun ||
        [VerseAction.REMEMBERED, VerseAction.COMMITTED]
                .contains(event.action) &&
            run.verses.length == 1 ||
        VerseAction.FORGOTTEN == event.action &&
            run.verses.length == 1 &&
            !run.inverse) {
      yield FlashcardEnd();
    } else {
      yield _nextMultiFlashcardRun(run, event);
    }
  }

  MultiFlashcardRun _nextMultiFlashcardRun(
      MultiFlashcardRun run, FlashcardAction event) {
    if (event.action == VerseAction.UNDONE && run.before != null) {
      return run.before!; // RETURN
    }
    final countRemembered = event.action == VerseAction.REMEMBERED
        ? run.countRemembered + 1
        : run.countRemembered;
    final countForgotten = event.action == VerseAction.FORGOTTEN
        ? run.countForgotten + 1
        : run.countForgotten;
    late MultiFlashcardRun nextState;
    if ([VerseAction.REMEMBERED, VerseAction.COMMITTED]
            .contains(event.action) ||
        VerseAction.FORGOTTEN == event.action && !run.inverse) {
      nextState = run.copyWith(
          index: run.index % (run.verses.length - 1),
          verses: [
            ...run.verses.sublist(0, run.index),
            ...run.verses.sublist(run.index + 1)
          ],
          countRemembered: countRemembered,
          countForgotten: countForgotten,
          reverse: false,
          visible: false,
          revealLines: 0,
          hinting: run.hinting != null ? defaultHints : null,
          before: run.copyWith(reverse: false));
    } else {
      nextState = run.copyWith(
          index: (event.action == VerseAction.RETRACTED
                  ? run.index - 1
                  : run.index + 1) %
              run.verses.length,
          verses: [...run.verses],
          countForgotten: countForgotten,
          reverse: false,
          visible: false,
          revealLines: 0,
          hinting: run.hinting != null ? defaultHints : null,
          before: run.copyWith(reverse: false));
    }
    nav.go(HomePath(id: nextState.verse.id));
    return nextState;
  }

  @override
  Stream<FlashcardState> mapSingleInitiatedToState(
      SingleFlashcardInitiated event) {
    throw UnimplementedError();
  }

  @override
  Stream<FlashcardState> mapStudiedToState() async* {
    var run = state;
    if (run is FlashcardRun) {
      di
          .get<NavigationCubit>()
          .go(HomePath(id: run.verse.id, activity: HomeActivity.study));
    }
  }
}
