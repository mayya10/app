import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/models/study/study.dart';
import 'package:remem_me/services/text_service.dart';

abstract class StudyState extends Equatable {
  @override
  List<Object?> get props => [];
}

class NoStudy extends StudyState {}

class Study extends StudyState {
  final Verse verse;
  final List<List<String>> lines;
  final Pos pos;
  final bool isComplete;

  Study(this.verse, this.lines, { Pos? pos, bool? isComplete})
      : isComplete = isComplete ?? false,
        pos = pos ?? TextService().lastPos(lines);

  @override
  List<Object?> get props => [lines, pos, isComplete];

  Study copyWith() {
    return Study(
      verse, lines,
      pos: pos,
      isComplete: isComplete,
    );
  }

  @override
  String toString() => 'StudyState { verse: $verse, lines: [${lines.length}], '
      'isComplete: $isComplete }';
}

class Obfuscation extends Study {
  final List<List<bool>>? obfuscated;
  final List<List<bool>>? revealed;

  Obfuscation(Verse verse, List<List<String>> lines,
      {this.obfuscated, this.revealed, bool? isComplete})
      : super(verse, lines, isComplete: isComplete);

  @override
  List<Object?> get props => super.props + [obfuscated, revealed];

  @override
  Obfuscation copyWith({
    bool? isComplete,
    List<List<bool>>? obfuscated,
    List<List<bool>>? revealed,
  }) {
    return Obfuscation(
      verse,
      lines,
      isComplete: isComplete ?? this.isComplete,
      obfuscated: obfuscated ?? this.obfuscated,
      revealed: revealed ?? this.revealed,
    );
  }

  @override
  String toString() => 'Obfuscation { lines: [${lines.length}], pos: $pos, '
      'obfuscated: [${obfuscated?.length}], revealed: [${revealed?.length}], isComplete: $isComplete }';
}

class Puzzle extends Study {
  final Choice? choice;

  Puzzle(
    Verse verse, List<List<String>> lines, {
    Pos? pos,
    bool? isComplete,
    this.choice,
  }) : super(verse, lines, pos: pos, isComplete: isComplete);

  @override
  List<Object?> get props => super.props + [choice];

  @override
  Puzzle copyWith({
    Pos? pos,
    bool? isComplete,
    Choice? choice,
  }) {
    return Puzzle(
      verse, lines,
      isComplete: isComplete ?? this.isComplete,
      pos: pos ?? this.pos,
      choice: choice ?? this.choice,
    );
  }

  @override
  String toString() =>
      'Puzzle { lines: [${lines.length}], pos: $pos, choice: $choice, '
      'isComplete: $isComplete }';
}

class LineUp extends Study {
  final Pos revealed;
  final Pos? initials;
  final Pos? placeholders;

  LineUp(
    Verse verse, List<List<String>> lines, {
    Pos? pos,
    bool? isComplete,
    Pos? revealed,
    this.initials,
    this.placeholders,
  })  : revealed = revealed ?? const Pos(0, -1),
        super(verse, lines, pos: pos, isComplete: isComplete);

  @override
  List<Object?> get props => super.props + [revealed, initials, placeholders];

  @override
  LineUp copyWith({
    Pos? pos,
    bool? isComplete,
    Pos? revealed,
    Pos? initials,
    Pos? placeholders,
    List<String> nullValues = const [],
  }) {
    return LineUp(
      verse, lines,
      isComplete: isComplete ?? this.isComplete,
      pos: pos ?? this.pos,
      revealed: revealed ?? this.revealed,
      initials:
          nullValues.contains('initials') ? null : (initials ?? this.initials),
      placeholders: nullValues.contains('placeholders')
          ? null
          : (placeholders ?? this.placeholders),
    );
  }

  @override
  String toString() => 'LineUp { lines: [${lines.length}], pos: $pos, '
      'isComplete: $isComplete, revealed: $revealed, '
      'initials: $initials, placeholders: $placeholders }';
}

class Typing extends LineUp {
  final String? letters;
  final String? nextWord;
  final bool? isCorrect;

  Typing(
    Verse verse, List<List<String>> lines, {
    Pos? pos,
    bool? isComplete,
    Pos? revealed,
    Pos? initials,
    Pos? placeholders,
    this.letters,
    this.nextWord,
    this.isCorrect,
  }) : super(verse, lines,
            pos: pos,
            isComplete: isComplete,
            revealed: revealed,
            initials: initials,
            placeholders: placeholders);

  @override
  List<Object?> get props => super.props + [letters, nextWord, isCorrect];

  @override
  Typing copyWith({
    Pos? pos,
    bool? isComplete,
    Pos? revealed,
    Pos? initials,
    Pos? placeholders,
    String? letters,
    String? nextWord,
    bool? isCorrect,
    List<String> nullValues = const [],
  }) {
    return Typing(
      verse, lines,
      isComplete: isComplete ?? this.isComplete,
      pos: pos ?? this.pos,
      letters:
          nullValues.contains('letters') ? null : (letters ?? this.letters),
      nextWord: nextWord ?? this.nextWord,
      isCorrect: nullValues.contains('isCorrect')
          ? null
          : (isCorrect ?? this.isCorrect),
      revealed: revealed ?? this.revealed,
      initials:
          nullValues.contains('initials') ? null : (initials ?? this.initials),
      placeholders: nullValues.contains('placeholders')
          ? null
          : (placeholders ?? this.placeholders),
    );
  }

  @override
  String toString() => 'Typing { lines: [${lines}], pos: $pos, '
      'isComplete: $isComplete, revealed: $revealed, '
      'initials: $initials, placeholders: $placeholders, '
      'letters: $letters, nextWord: $nextWord, isCorrect: $isCorrect }';
}
