import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/models/verse.dart';

abstract class StudyEvent extends Equatable {
  const StudyEvent();

  @override
  List<Object?> get props => [];
}

/// A verse has been selected and can be displayed on the study screen
class StudyStarted extends StudyEvent {
  final Verse? verse;

  const StudyStarted([this.verse]);

  @override
  List<Object?> get props => [verse];

  @override
  String toString() => 'StudyStarted { verse: $verse }';
}


class Obfuscated extends StudyEvent {}


class Revealed extends StudyEvent {
  final Pos pos;

  const Revealed(this.pos);

  @override
  List<Object?> get props => [pos];

  @override
  String toString() => 'Revealed { pos: $pos }';
}


class Puzzled extends StudyEvent {
  final bool? isCorrect;

  Puzzled([this.isCorrect]);

  @override
  List<Object?> get props => [isCorrect];

  @override
  String toString() => 'Puzzled { isCorrect: $isCorrect }';
}

class LinedUp extends StudyEvent {
  final bool? byLine;

  LinedUp({this.byLine});

  @override
  List<Object?> get props => [byLine];

  @override
  String toString() => 'Advanced { byLine: $byLine }';
}

class Retracted extends StudyEvent {}

class StudyHinted extends StudyEvent {
  final bool hasPlaceholders;
  final bool hasInitials;

  const StudyHinted({this.hasPlaceholders = true, this.hasInitials = true});

  @override
  List<Object?> get props => [hasPlaceholders, hasInitials];

  @override
  String toString() => 'HintsChanged { hasPlaceholders: $hasPlaceholders, '
      'hasInitials: $hasInitials }';
}



class Typed extends StudyEvent {
  final String? letters;
  final bool? isCorrect;

  Typed({this.letters, this.isCorrect});

  @override
  List<Object?> get props => [letters, isCorrect];

  @override
  String toString() => 'Typed { letters: $letters, isCorrect: $isCorrect }';
}
