import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/models/study/out_of_words_exception.dart';
import 'package:remem_me/models/study/study.dart';
import 'package:remem_me/blocs/current_account/current_account.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:remem_me/services/tts_service.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';

import '../../services/study_service.dart';
import 'study.dart';

class Hints {
  bool placeholders;
  bool initials;

  Hints({required this.placeholders, required this.initials});
}

class StudyBloc extends Bloc<StudyEvent, StudyState> {
  final _service = StudyService();
  final _hints = Map<String, Hints>();
  final _ttsKey = 'study';
  String? _ttsPending;
  String? _ttsSpeaking;
  late TtsService _ttsService;

  get _language {
    return di.get<CurrentAccount>().language;
  }

  StudyBloc() : super(NoStudy()) {
    _ttsService = di.get<TtsService>();
    on<StudyEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (StudyState result) => result),
        transformer: sequential()); // process events sequentially

    _ttsService.registerCancelHandler(_ttsKey, () async {
      if (_ttsPending != null) {
        _ttsSpeaking = _ttsPending;
        _ttsPending = null;
        await _ttsService.speakText(_ttsKey, _ttsSpeaking!, _language);
      }
    });
    _ttsService.registerCompleteHandler(_ttsKey, () async {
      _ttsSpeaking = null;
    });
  }

  Stream<StudyState> mapEventToState(StudyEvent event) async* {
    if (event is StudyStarted) {
      yield* mapStudyStartedToState(event);
    } else if (event is Obfuscated) {
      yield* mapObfuscatedToState(event);
    } else if (event is Revealed) {
      yield* mapRevealedToState(event);
    } else if (event is Puzzled) {
      yield* mapPuzzledToState(event);
    } else if (event is LinedUp) {
      yield* mapLinedUpToState(event);
    } else if (event is Typed) {
      yield* mapTypedToState(event);
    } else if (event is Retracted) {
      yield* mapRetractedToState(event);
    } else if (event is StudyHinted) {
      yield* mapHintedToState(event);
    }
  }

  Stream<StudyState> mapStudyStartedToState(StudyStarted event) async* {
    if (event.verse != null) {
      _resetHints();
      final lines = VerseService().words(event.verse!);
      yield Study(event.verse!, lines, pos: TextService().lastPos(lines));
    } else {
      final _state = state as Study;
      if (_state is LineUp) {
        if (_state is Typing) {
          _hints['typing']!.initials = _state.initials != null;
          _hints['typing']!.placeholders = _state.placeholders != null;
        } else {
          _hints['line_up']!.initials = _state.initials != null;
          _hints['line_up']!.placeholders = _state.placeholders != null;
        }
      }
      yield Study(_state.verse, _state.lines);
    }
  }

  void _resetHints() {
    _hints['typing'] = Hints(placeholders: true, initials: true);
    _hints['line_up'] =
        Hints(placeholders: true, initials: _language.languageCode != 'zh');
  }

  Stream<StudyState> mapObfuscatedToState(Obfuscated event) async* {
    final study = state as Study;
    if (study is Obfuscation) {
      final obfuscated = _service.obfuscate(study.lines, study.obfuscated!);
      yield study.copyWith(
          obfuscated: _service.obfuscate(study.lines, study.obfuscated!),
          revealed: TextService().wordsToMarks(study.lines),
          isComplete: TextService().countWordsOfRows(study.lines) ==
              TextService().countMarks(obfuscated));
    } else {
      yield Obfuscation(study.verse, study.lines,
          obfuscated: _service.obfuscate(
              study.lines, TextService().wordsToMarks(study.lines)),
          revealed: TextService().wordsToMarks(study.lines));
    }
  }

  Stream<StudyState> mapRevealedToState(Revealed event) async* {
    final study = state as Study;
    _speak(TextService().wordsToText(study.lines, event.pos, false));
    if (study is Obfuscation) {
      final revealed = _service.reveal(study.revealed!, event.pos);
      yield study.copyWith(revealed: revealed);
    }
  }

  Stream<StudyState> mapPuzzledToState(Puzzled event) async* {
    final study = state as Study;
    final puzzleSize = Settings().getInt(Settings.STUDY_PUZZLE_SIZE) ?? 5;
    if (study is Puzzle) {
      if (event.isCorrect!) {
        try {
          final pos = _service.advanceByWord(study.lines, study.pos);
          _speak(TextService().wordsToText(study.lines, pos, false));
          final nextPos = _service.advanceByWord(study.lines, pos);
          final choice = _service.puzzle(study.lines, nextPos, puzzleSize);
          yield study.copyWith(pos: pos, choice: choice);
        } on OutOfWordsException {
          yield Study(study.verse, study.lines);
        }
      }
    } else {
      yield Puzzle(study.verse,
          study.lines,
          pos: Pos(0, -1),
          choice: _service.puzzle(study.lines, Pos(0, 0), puzzleSize));
    }
  }

  Stream<StudyState> mapLinedUpToState(LinedUp event) async* {
    final study = state as Study;
    if (study is LineUp) {
      try {
        var revealed = _service.advanceByWord(study.lines, study.revealed);
        _speak(
            TextService().wordsToText(study.lines, revealed, event.byLine!));
        if (event.byLine!) {
          revealed = _service.advanceByLine(study.lines, study.revealed);
        }
        var pos = _service.lookAhead(study.lines, revealed);
        yield study.copyWith(
            revealed: revealed,
            pos: pos,
            initials: pos,
            placeholders: pos,
            nullValues: [
              if (study.initials == null) 'initials',
              if (study.placeholders == null) 'placeholders',
            ]);
      } on OutOfWordsException {
        yield study.copyWith(revealed: study.pos, isComplete: true);
      }
    } else {
      const revealed = Pos(0, -1);
      final pos = Pos(0, study.lines[0].length - 1);
      yield LineUp(
        study.verse,
        study.lines,
        revealed: revealed,
        pos: pos,
        placeholders: _hints['line_up']!.placeholders ? pos : null,
        initials: _hints['line_up']!.initials ? pos : null,
      );
    }
  }

  Stream<StudyState> mapRetractedToState(Retracted event) async* {
    final study = state as Study;
    if (study is LineUp) {
      final revealed = _service.retractLine(study.lines, study.revealed);
      if (study is Typing) {
        var rendered = _service.lookAhead(study.lines, revealed);
        final next = _service.advanceByWord(study.lines, revealed);
        yield study.copyWith(
          pos: rendered,
          revealed: revealed,
          nextWord: study.lines[next.line][next.word],
          isComplete: false,
          nullValues: [
            'isCorrect',
            'letters',
            if (_language.languageCode == 'zh') 'initials'
          ],
        );
      } else {
        var pos = _service.lookAhead(study.lines, revealed);
        yield study.copyWith(revealed: revealed, pos: pos, isComplete: false);
      }
    }
  }

  Stream<StudyState> mapHintedToState(StudyHinted event) async* {
    final study = state as Study;
    if (study is LineUp) {
      yield study.copyWith(
          initials: study is Typing
              ? _service.advanceByWord(study.lines, study.revealed)
              : study.pos,
          placeholders: study.pos,
          nullValues: [
            if (!event.hasInitials) 'initials',
            if (!event.hasPlaceholders) 'placeholders'
          ]);
    }
  }

  Stream<StudyState> mapTypedToState(Typed event) async* {
    final study = state as Study;
    if (study is Typing) {
      if (TextService().startsWith(study.nextWord!, event.letters!)) {
        _speak(study.nextWord!);
        try {
          final revealed =
              _service.advanceByWord(study.lines, study.revealed);
          final next = _service.advanceByWord(study.lines, revealed);
          final rendered = _service.lookAhead(study.lines, revealed);
          yield study.copyWith(
            isCorrect: true,
            pos: rendered,
            revealed: revealed,
            letters: event.letters,
            nextWord: study.lines[next.line][next.word],
            placeholders: study.placeholders != null ? rendered : null,
            nullValues: ['initials'],
          );
        } on OutOfWordsException {
          yield study.copyWith(
            revealed: study.pos,
            isComplete: true,
            placeholders: study.placeholders != null ? study.pos : null,
            nullValues: ['initials'],
          );
        }
      } else {
        yield study.copyWith(letters: event.letters, isCorrect: false);
      }
    } else {
      const revealed = Pos(0, -1);
      const next = Pos(0, 0);
      final rendered = _service.lookAhead(study.lines, revealed);
      yield Typing(
        study.verse,
        study.lines,
        revealed: revealed,
        pos: rendered,
        nextWord: study.lines[next.line][next.word],
        initials: null,
        placeholders: _hints['typing']!.placeholders ? rendered : null,
      );
    }
  }

  _speak(String text) async {
    final isSpeechOn = Settings().getBool(Settings.STUDY_SPEECH_ON) ?? true;
    if (isSpeechOn) {
      if (_ttsSpeaking == null) {
        _ttsSpeaking = text;
        await _ttsService.speakText(_ttsKey, text, _language);
      } else {
        _ttsPending = text;
        await _ttsService.stop();
      }
    }
  }
}
