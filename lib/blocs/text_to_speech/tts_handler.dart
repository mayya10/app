import 'dart:async';
import 'dart:io';

import 'package:audio_service/audio_service.dart';
import 'package:audio_session/audio_session.dart';
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:remem_me/blocs/text_to_speech/tts_controller.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';

import '../blocs.dart';

class Pending {
  final Speaking trigger;
  final Future<void> Function() callback;

  Pending(this.trigger, this.callback);
}

class MediaProcessingState {
  final PlaybackState playbackState;
  final MediaItem? mediaItem;

  MediaProcessingState(this.playbackState, [this.mediaItem]);
}

class TtsHandler extends BaseAudioHandler with QueueHandler {
  static final TtsHandler _instance = TtsHandler._internal();
  final TtsController _controller = TtsController();
  late Uri _defaultArtUri;
  Pending? _pending;
  int _index = 0;
  bool _interrupted = false;
  List<Verse> _verses = [];
  Box? _box;
  StreamSubscription<OrderedVersesState>? _boxSubscription;

  factory TtsHandler() {
    return _instance;
  }

  TtsHandler._internal() {
    _init();
  }

  Future<void> _init() async {
    _defaultArtUri = await _createDefaultArtUri();
    _controller.stream.listen((engineState) {
      if (_pending != null && _pending!.trigger == engineState) {
        _pending!.callback();
        _pending = null;
      }
    });
    final session = await AudioSession.instance;
    // Handle audio interruptions.
    session.interruptionEventStream.listen((event) {
      if (event.begin) {
        if (playbackState.value.playing) {
          pause();
          _interrupted = true;
        }
      } else {
        switch (event.type) {
          case AudioInterruptionType.pause:
          case AudioInterruptionType.duck:
            if (!playbackState.value.playing && _interrupted) {
              play();
            }
            break;
          case AudioInterruptionType.unknown:
            break;
        }
        _interrupted = false;
      }
    });
    // Handle unplugged headphones.
    session.becomingNoisyEventStream.listen((_) {
      if (playbackState.value.playing) pause();
    });
  }

  void setUp(List<Verse> verses, {int index = 0, Box? box}) {
    _verses = verses;
    _index = index;
    _box = box;
    if (_box == null) queue.add(_createMediaItems(_verses));
    _updateBoxSubscription();
  }

  void refresh() {
    assert(_box != null);
    var blocState = OrderedVersesBloc.from(TabState(true, _box!)).state;
    if (blocState is OrderSuccess) {
      final currentVerse = _verses[_index];
      _verses = blocState.verses;
      queue.add(_createMediaItems(_verses));
      _index = _verses.indexWhere((verse) => verse.id == currentVerse.id);
      _updateBoxSubscription();
    }
  }

  List<MediaItem> _createMediaItems(List<Verse> verses) {
    return verses
        .map((verse) => MediaItem(
              id: '${verse.id}',
              title: verse.reference,
              album: verse.topic != null && verse.topic!.isNotEmpty
                  ? verse.source
                  : null,
              artist: verse.topic != null && verse.topic!.isNotEmpty
                  ? verse.topic
                  : verse.source,
              artUri: verse.image != null
                  ? Uri.parse(verse.image!)
                  : _defaultArtUri,
            ))
        .toList();
  }

  void _updateBoxSubscription() {
    if (_boxSubscription != null) {
      _boxSubscription!.cancel();
      _boxSubscription = null;
    }
    if (_box != null) {
      OrderedVersesBloc bloc = OrderedVersesBloc.from(TabState(true, _box!));
      _boxSubscription = bloc.stream.listen((state) {
        if (state is OrderInProgress ||
            state is OrderSuccess &&
                state.verses.any((verse) => verse.id == _verses[_index].id)) {
          pause();
        } else {
          _boxSubscription!.cancel();
          _boxSubscription = null;
          stop();
        }
      });
    }
  }

  @override
  Future<void> play({bool willPause = false}) async {
    final session = await AudioSession.instance;
    // flutter_tts doesn't activate the session, so we do it here. This
    // allows the app to stop other apps from playing audio while we are
    // playing audio.
    if (await session.setActive(true)) {
      // If we successfully activated the session, set the state to playing
      // and resume playback.
      if (_box != null) refresh();
      final playlist = Settings().getBool(Settings.SPEECH_PLAYLIST) ?? true;
      final repeat = Settings().getBool(Settings.SPEECH_REPEAT) ?? false;
      if (!playlist && !repeat) willPause = true;

      mediaItem.add(queue.value[_index]);
      playbackState.add(playbackState.value.copyWith(
          controls: [
            MediaControl.skipToPrevious,
            MediaControl.pause,
            MediaControl.skipToNext
          ],
          processingState: AudioProcessingState.ready,
          playing: true,
          repeatMode: willPause
              ? AudioServiceRepeatMode.group
              : repeat
                  ? playlist
                      ? AudioServiceRepeatMode.all
                      : AudioServiceRepeatMode.one
                  : AudioServiceRepeatMode.none));
      var next = _nextIndex(queue.value.length, _index, playlist, repeat);
      _speak(_index, next, willPause);
    }
  }

  @override
  Future<void> stop() async {
    playbackState.add(playbackState.value.copyWith(
      controls: [],
      processingState: AudioProcessingState.idle,
      playing: false,
    ));
    _controller.stop();
    await super.stop();
  }

  @override
  Future<void> pause() async {
    _interrupted = false;
    mediaItem.add(queue.value[_index]);
    playbackState.add(playbackState.value.copyWith(
      controls: [MediaControl.stop, MediaControl.play, MediaControl.skipToNext],
      processingState: AudioProcessingState.ready,
      playing: false,
    ));
    _controller.stop();
  }

  @override
  Future<void> skipToNext() async {
    if (_box != null) refresh();
    skipToQueueItem(_index + 1);
  }

  @override
  Future<void> skipToPrevious() async {
    if (_box != null) refresh();
    skipToQueueItem(_index - 1);
  }

  @override
  Future<void> skipToQueueItem(int next) async {
    if (next >= 0 && next < queue.value.length) {
      if (playbackState.value.playing) {
        _pending = Pending(Speaking.cancelled, () async {
          _index = next;
          play();
        });
        _controller.stop();
      } else {
        _index = next;
        mediaItem.add(queue.value[_index]);
      }
    }
  }

  void _speak(int index, int next, bool willPause) async {
    if (willPause) next = index;
    if (next >= 0) {
      _pending = Pending(Speaking.completed, () async {
        _index = next;
        willPause ? pause() : play();
      });
    } else {
      _pending = Pending(Speaking.completed, stop);
    }
    _controller.speakVerse(_verses[index]);
  }

  int _nextIndex(int length, int index, bool playlist, bool repeat) {
    if (!playlist && repeat) return index;
    if (repeat) return (index + 1) % length;
    if (index + 1 < length) return index + 1;
    return -1;
  }

  /// A stream reporting the combined state of the current playback state
  /// and the current media item.
  Stream<MediaProcessingState> get mediaProcessing =>
      Rx.combineLatest2<PlaybackState, MediaItem?, MediaProcessingState>(
          playbackState,
          mediaItem,
          (playbackState, mediaItem) =>
              MediaProcessingState(playbackState, mediaItem));

  Future<Uri> _createDefaultArtUri() async {
    if (kIsWeb) return Uri.parse('assets/icon/app.png');
    Directory appDir = await getApplicationDocumentsDirectory();
    ByteData bytes = await rootBundle.load('assets/icon/app.png');
    File _file = File(join(appDir.path, 'app.png'));
    File _newFile = await _file.writeAsBytes(bytes.buffer.asUint8List());
    return Uri.file(_newFile.path);
  }
}
