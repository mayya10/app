import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:remem_me/models/models.dart';

abstract class TtsEvent extends Equatable {
  const TtsEvent();

  @override
  List<Object?> get props => [];
}

class TtsStarted extends TtsEvent {
  final List<Verse> verses;
  final int index;
  final bool? repeat;

  const TtsStarted(
      {required this.verses, required this.index, this.repeat});

  @override
  List<Object?> get props => [verses, index, repeat];

  @override
  String toString() => 'TtsStarted { verses: ${verses.length}, index: $index, '
      'repeat: $repeat }';
}

class TtsStopped extends TtsEvent {}

class TtsFailed extends TtsEvent {
  final String message;

  TtsFailed(this.message);

  @override
  List<Object?> get props => [message];

  @override
  String toString() => 'TtsFailed { message: $message }';
}

class TtsPaused extends TtsEvent {}

class TtsSkipped extends TtsEvent {
  final int offset;

  TtsSkipped(this.offset);

  @override
  List<Object?> get props => [offset];

  @override
  String toString() => 'TtsSkipped { offset: $offset }';
}

class TtsResumed extends TtsEvent {
  final int? index;

  TtsResumed([this.index]);

  @override
  List<Object?> get props => [index];

  @override
  String toString() => 'TtsResumed { index: $index }';
}
