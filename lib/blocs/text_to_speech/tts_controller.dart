import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/current_account/current_account.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/tts_service.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';

enum Speaking { started, paused, cancelled, completed }

class TtsController extends Cubit<Speaking> {
  final _ttsKey = 'home';
  var _progress = -1;
  Verse? _verse;
  Timer? _silence;

  TtsController() : super(Speaking.completed) {
    TtsService().registerCompleteHandler(_ttsKey, () async {
      _proceed();
    });
    TtsService().registerCancelHandler(_ttsKey, () async {
      emit(Speaking.cancelled);
    });
  }

  void _proceed() {
    debugPrint('proceed $_progress of ${_stages.length}');
    if (_progress + 1 == _stages.length) {
      emit(Speaking.completed);
    } else {
      _progress++;
      _speakCurrentStage();
    }
  }

  List<String> get _stages {
    return [
      if (di.get<CurrentAccount>().topicPreferred) 'topic' else 'reference',
      'silence',
      'passage',
      if (di.get<CurrentAccount>().referenceIncluded) 'reference',
      'silence',
    ];
  }

  void speakVerse(Verse verse) {
    _verse = verse;
    _progress = 0;
    emit(Speaking.started);
    _speakCurrentStage();
  }

  Future<void> stop() async {
    if (_silence != null) {
      _silence!.cancel();
      _silence = null;
      debugPrint('Silence cancelled');
      emit(Speaking.cancelled);
    } else {
      await TtsService().stop();
    }
  }

  Future<void> _speakCurrentStage() async {
    switch (_stages[_progress]) {
      case 'reference':
        return _speakReference();
      case 'passage':
        return _speakPassage();
      case 'topic':
        return _speakTopic();
      case 'silence':
        return _speakSilence();
      default:
        throw UnimplementedError();
    }
  }

  Future<void> _speakReference() async {
    await TtsService().speakText(
      _ttsKey,
      VerseService().spokenReference(_verse!.reference),
      di.get<CurrentAccount>().langRef,
    );
    debugPrint('Speak reference ${_verse!.reference}');
  }

  Future<void> _speakPassage() async {
    await TtsService().speakText(
      _ttsKey,
      VerseService()
          .spokenPassage(_verse!.passage, di.get<CurrentAccount>().language),
      di.get<CurrentAccount>().language,
    );
    debugPrint('Speak passage ${_verse!.reference}');
  }

  Future<void> _speakTopic() async {
    await TtsService().speakText(
      _ttsKey,
      _verse!.topic ?? '',
      di.get<CurrentAccount>().langRef,
    );
    debugPrint('Speak top ${_verse!.topic}');
  }

  Future<void> _speakSilence() async {
    debugPrint('Silence started');
    final seconds = di.get<Settings>().getDouble(Settings.SPEECH_PAUSE) ?? 1.0;
    _silence = Timer(Duration(seconds: seconds.toInt()), () {
      _silence = null;
      debugPrint('Silence completed');
      _proceed();
    });
  }
}
