import 'package:equatable/equatable.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/models/models.dart';

import 'tts.dart';

abstract class TtsState extends Equatable {
  const TtsState();

  @override
  List<Object?> get props => [];
}

class TtsFailure extends TtsState {
  final String message;

  TtsFailure(this.message);

  @override
  List<Object?> get props => [message];

  @override
  String toString() => 'TtsFailure { message: $message }';
}

class TtsOff extends TtsState {}

class TtsOn extends TtsState {
  final List<Verse> verses;
  final int index;
  final Progress status;
  final TtsEvent? pending;
  final bool? repeat;

  const TtsOn({
    required this.verses,
    required this.index,
    this.status = Progress.STOP,
    this.pending,
    this.repeat,
  });

  @override
  List<Object?> get props => [verses, index, status];

  Verse get current => verses[index];

  TtsOn copyWith({
    int? index,
    Progress? status,
    TtsEvent? pending,
    bool? repeat,
  }) {
    return TtsOn(
      verses: this.verses,
      index: index ?? this.index,
      status: status ?? this.status,
      pending: pending ?? this.pending,
      repeat: repeat ?? this.repeat,
    );
  }

  @override
  String toString() {
    return 'TtsOn { verses: ${verses.length}, index: $index, status: $status, '
        'pending: $pending, repeat: $repeat }';
  }
}
