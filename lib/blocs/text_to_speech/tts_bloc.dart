import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/text_to_speech/tts.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/blocs/text_to_speech/tts_controller.dart';
import 'package:repository/repository.dart';

import '../blocs.dart';

class Pending {
  final Speaking trigger;
  final TtsEvent event;

  Pending(this.trigger, this.event);
}

class TtsBloc extends Bloc<TtsEvent, TtsState> {
  final TtsController _controller = TtsController();
  Pending? _pending;

  TtsBloc() : super(TtsOff()) {
    on<TtsEvent>(
            (event, emit) => emit.forEach(mapEventToState(event),
            onData: (TtsState result) => result),
        transformer: sequential());  // process events sequentially
    _controller.stream.listen((engineState) {
      if (_pending != null && _pending!.trigger == engineState) {
        add(_pending!.event);
        _pending = null;
      }
    });
  }

  Stream<TtsState> mapEventToState(TtsEvent event) async* {
    if (event is TtsStarted) {
      yield* _mapTtsStartedToState(event);
    } else if (event is TtsStopped) {
      yield* _mapTtsStoppedToState();
    } else if (event is TtsPaused) {
      yield* _mapTtsPausedToState();
    } else if (event is TtsResumed) {
      yield* _mapTtsResumedToState(event);
    } else if (event is TtsSkipped) {
      yield* _mapTtsSkippedToState(event);
    } else if (event is TtsFailed) {
      yield* _mapTtsFailedToState(event);
    }
  }

  Stream<TtsState> _mapTtsStartedToState(TtsStarted event) async* {
    yield TtsOn(
      verses: event.verses,
      index: event.index,
      status: Progress.RUN,
    );
    _speak(event.verses, event.index);
  }

  Stream<TtsState> _mapTtsStoppedToState() async* {
    yield TtsOff();
    _controller.stop();
  }

  Stream<TtsState> _mapTtsPausedToState() async* {
    if (state is TtsOn) {
      yield (state as TtsOn).copyWith(status: Progress.PAUSE);
      _controller.stop();
    }
  }

  Stream<TtsState> _mapTtsResumedToState(TtsResumed event) async* {
    final _state = state;
    if (_state is TtsOn) {
      final index = event.index ?? _state.index;
      yield _state.copyWith(status: Progress.RUN, index: index);
      _speak(_state.verses, index);
    }
  }

  void _speak(List<Verse> verses, int index) async {
    final next = _nextIndex(verses.length, index);
    if (next >= 0) {
      _pending = Pending(Speaking.completed, TtsResumed(next));
    } else {
      _pending = Pending(Speaking.completed, TtsStopped());
    }
    _controller.speakVerse(verses[index]);
  }

  int _nextIndex(int length, int index) {
    final playlist = Settings().getBool(Settings.SPEECH_PLAYLIST) ?? true;
    final repeat = Settings().getBool(Settings.SPEECH_REPEAT) ?? false;
    if (!playlist && repeat) return index;
    if (playlist) if (repeat)
      return (index + 1) % length;
    else if (index + 1 < length) return index + 1;
    return -1;
  }

  Stream<TtsState> _mapTtsSkippedToState(TtsSkipped event) async* {
    final _state = state;
    if (_state is TtsOn) {
      final next = _state.index + event.offset;
      if (next >= 0 && next < _state.verses.length) {
        if (_state.status == Progress.RUN) {
          _pending = Pending(Speaking.cancelled, TtsResumed(next));
          _controller.stop();
        }
        yield _state.copyWith(index: next);
      }
    }
  }

  Stream<TtsState> _mapTtsFailedToState(TtsFailed event) async* {
    yield TtsFailure(event.message);
  }

}
