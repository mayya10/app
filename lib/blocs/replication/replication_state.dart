part of 'replication_bloc.dart';

abstract class ReplicationState extends Equatable {

  const ReplicationState();

  @override
  List<Object?> get props => [];
}

class ReplicationInitial extends ReplicationState {}
class ReplicationDirty extends ReplicationState {}
class ReplicationInProgress extends ReplicationState {}
class ReplicationSuccess extends ReplicationState {}

class ReplicationFailure extends ReplicationState {
  final Map<String, List<String>> messages;

  ReplicationFailure(this.messages);

  @override
  List<Object?> get props => [messages];

  @override
  String toString() {
    return 'ReplicationFailure { messages: $messages }';
  }
}
