import 'package:app_core/app_core.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/undo/undo.dart';
import 'package:repository/repository.dart';

import '../../models/models.dart';
import '../blocs.dart';

class UndoBloc extends Bloc<UndoEvent, List<Undo>?> {
  UndoBloc() : super(null) {
    on<UndoAdded>((event, emit) => emit(event.undos));
    on<UndoApplied>(_onUndoApplied);
    on<UndoCleared>((event, emit) => emit(null));
  }

  void _onUndoApplied(UndoEvent event, Emitter<List<Undo>?> emit) async {
    for (Undo undo in (state ?? [])) {
      if (undo.entities.first is VerseEntity) {
        await _undo<VerseEntity>(di.get<VersesBloc>(), undo.action,
            undo.entities as List<VerseEntity>);
      } else if (undo.entities.first is ScoreEntity) {
        await _undo<ScoreEntity>(di.get<ScoresBloc>(), undo.action,
            undo.entities as List<ScoreEntity>);
      } else if (undo.entities.first is TagEntity) {
        await _undo<TagEntity>(di.get<TagsBloc>(), undo.action,
            undo.entities as List<TagEntity>);
      } else if (undo.entities.first is AccountEntity) {
        await _undo<AccountEntity>(di.get<AccountsBloc>(), undo.action,
            undo.entities as List<AccountEntity>);
      } else {
        throw UnimplementedError(
            "Undo for ${undo.entities.first.runtimeType} not implemented");
      }
    }
    emit(null);
  }

  Future<void> _undo<T extends Entity>(
      EntitiesBloc<T> bloc, EntityAction action, List<T> entities) async {
    switch (action) {
      case EntityAction.update:
        bloc.add(EntitiesUpdated<T>(entities));
        break;
      case EntityAction.delete:
        await bloc.repository.updateList(entities);
        bloc.add(EntitiesAdded<T>(entities, persist: false));
        break;
      case EntityAction.create:
        bloc.add(EntitiesUpdated<T>(entities
            .map((entity) => entity.copyWith(deleted: true) as T)
            .toList()));
        break;
    }
  }
}
