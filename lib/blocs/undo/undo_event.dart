import 'package:equatable/equatable.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';


abstract class UndoEvent<T extends Entity> extends Equatable {
  const UndoEvent();

  @override
  List<Object?> get props => [];
}

class UndoAdded<T extends Entity> extends UndoEvent<T> {
  final List<Undo> undos;

  const UndoAdded(this.undos);

  @override
  List<Object?> get props => [undos];
}

class UndoApplied<T extends Entity> extends UndoEvent<T> {}
class UndoCleared<T extends Entity> extends UndoEvent<T> {}


