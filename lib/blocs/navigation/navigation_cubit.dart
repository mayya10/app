import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:remem_me/routers/routers.dart';

class NavigationCubit extends Cubit<AppRoutePath> {
  StreamSubscription? _sharingIntentSubscription;

  NavigationCubit() : super(HomePath()) {
    // For sharing or opening urls/text coming from outside the app while the app is in the memory
    // intent format: ShareMedia://dataUrl=ShareKey#text
    if(!kIsWeb) {
      _sharingIntentSubscription =
          ReceiveSharingIntent.getTextStream().listen((String value) {
            debugPrint('Text received when app in memory: $value');
            go(HomePath(
                activity: HomeActivity.add,
                data: value)); // todo: parse verse, add to Homepath
          }, onError: (err) {
            debugPrint("$err");
          });

      // For sharing or opening urls/text coming from outside the app while the app is closed
      ReceiveSharingIntent.getInitialText().then((String? value) {
        if (value != null) {
          debugPrint('Text received when app closed: $value');
          go(HomePath(activity: HomeActivity.add, data: value));
        }
      });
    }
  }

  void go(AppRoutePath path) {
    emit(path);
  }

  @override
  Future<void> close() {
    _sharingIntentSubscription?.cancel();
    return super.close();
  }
}
