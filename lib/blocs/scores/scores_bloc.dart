import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:collection/collection.dart';
import 'package:remem_me/services/score_service.dart';
import 'package:repository/repository.dart';

import '../blocs.dart';

class ScoresBloc extends EntitiesBloc<ScoreEntity> {

  ScoresBloc() : super(
            repository: di.get<EntitiesRepository<ScoreEntity>>(),
            initialState: InitialEntitiesState<ScoreEntity>()) {
    subscribeToAccountChange();
  }

  @override
  Stream<EntitiesState<ScoreEntity>> mapEntitiesAddedToState(
      EntitiesAdded<ScoreEntity> event) async* {
    /* prevent multiple scoring for the same verse */
    final newScores = event.entities
        .where((newScore) => !(state as EntitiesLoadSuccess<ScoreEntity>)
            .entities
            .any((score) =>
                score.date == newScore.date && score.vkey == newScore.vkey))
        .toList();
    if (newScores.isNotEmpty) {
      final prevScores = (state as EntitiesLoadSuccess<ScoreEntity>).entities;
      final List<ScoreEntity> allScores = prevScores + event.entities;
      ScoreService().showIfGoalReached(prevScores, allScores);
      yield EntitiesLoadSuccess<ScoreEntity>(allScores);
      if (event.persist) {
        repository.createList(
            newScores); // todo: EntitiesUpdateInProgress, EntitiesUpdateFailure
      }
    }
  }

  @override
  List<ScoreEntity> sorted(Iterable<ScoreEntity> entities) {
    return entities.toSet().sortedBy((it) => it.date);
  }
}
