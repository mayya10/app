import 'dart:convert';
import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/models/collections/collections.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import '../../services/book_service.dart';

/// Manages the states and properties of the current account
/// An existing publisher overrules the current account's properties
class CurrentAccount extends Cubit<Account?> {
  Publisher? _publisher;
  Map<String, Book>? _publisherBooks;

  CurrentAccount() : super(null) {
    _loadAccount();
  }

  Future<void> _loadAccount() async {
    final entity = _entityFromSettings();
    if (entity != null) {
      final account = await fromEntity(entity);
      final auth = di<AuthBloc>();
      await auth.stream
          .startWith(auth.state)
          .firstWhere((state) => state is LoginSuccess);
      debugPrint('Current account loaded from Settings: $account');
      emit(account);
    }
  }

  Future<Account> fromEntity(AccountEntity entity) async {
    final books = await BookService().loadBooks(entity.langRef, entity.canon);
    return Account.fromEntity(entity, books: books);
  }

  /// Sets the current account
  /// needs to be set (and unset) by AccountsBloc
  setAccount(Account? account) {
    emit(account);
    if (account != null) {
      Settings().put(Settings.CURRENT_ACCOUNT, jsonEncode(account.toJson()));
    } else {
      Settings().delete(Settings.CURRENT_ACCOUNT);
    }
  }

  /// Sets the current account
  /// needs to be set (and unset) by AccountsBloc
  setEntity(AccountEntity? entity) async {
    setAccount(entity == null ? null : await fromEntity(entity));
  }

  /// Sets the current publisher
  /// needs to be set (and unset) by CollectionDetailBloc
  Future<void> setPublisher(Publisher? publisher) async {
    _publisher = publisher;
    if (publisher != null) {
      _publisherBooks = await BookService()
          .loadBooks(publisher.langRef, Canon.lut); // todo: canon
    } else {
      _publisherBooks = null;
    }
  }

  /// Works without initialization
  int get id {
    if (state != null) {
      return state!.id;
    } else {
      final entity = _entityFromSettings();
      return entity?.id ?? -1;
    }
  }

  AccountEntity? _entityFromSettings() {
    dynamic result = Settings().get(Settings.CURRENT_ACCOUNT);
    if (result != null) result = jsonDecode(result!);
    return result is Map<String, dynamic>
        ? AccountEntity.fromJson(result)
        : null;
  }

  bool get topicPreferred {
    return state != null && state!.topicPreferred;
  }

  bool get referenceIncluded {
    return state != null && state!.referenceIncluded;
  }

  Locale get language {
    return _publisher?.language ?? state?.language ?? const Locale('en');
  }

  Locale get langRef {
    return _publisher?.langRef ?? state?.langRef ?? const Locale('en');
  }

  TextDirection get languageDirection {
    return RTL_LANGUAGES.contains(language.languageCode)
        ? TextDirection.rtl
        : TextDirection.ltr;
  }

  TextDirection get langRefDirection {
    return RTL_LANGUAGES.contains(langRef.languageCode)
        ? TextDirection.rtl
        : TextDirection.ltr;
  }

  double get reviewFrequency {
    return state?.reviewFrequency ?? 2.0;
  }

  int get reviewLimit {
    return state?.reviewLimit ?? 128;
  }

  int get dailyGoal {
    return state?.dailyGoal ?? 70;
  }

  int get inverseLimit {
    return state?.inverseLimit ?? 7;
  }

  Map<String, Book> get books {
    return _publisherBooks ?? state?.books ?? {};
  }

  String get font {
    var family = FontType.Sans.name;
    if (state != null) {
      family = state!.fontType.name;
      if (state!.language.languageCode == 'he') family += 'Hebrew';
    }
    return family;
  }

  String? get defaultSource {
    return state?.defaultSource;
  }

  VerseOrder get orderNew {
    return state?.orderNew ?? VerseOrder.CANON;
  }

  VerseOrder get orderDue {
    return state?.orderDue ?? VerseOrder.LEVEL;
  }

  VerseOrder get orderKnown {
    return state?.orderKnown ?? VerseOrder.DATE;
  }

  VerseOrder get orderAll {
    return state?.orderAll ?? VerseOrder.DATE;
  }
}
