import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:collection/collection.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/routers/app_route_path.dart';
import 'package:remem_me/blocs/current_account/current_account.dart';
import 'package:remem_me/services/collection_service.dart';
import 'package:repository/repository.dart';

import '../../services/verse_service.dart';

class CollectionDetailBloc
    extends Bloc<CollectionDetailEvent, CollectionDetailState> {
  final CollectionService service;
  late TagsBloc tagsBloc;
  late EntitiesRepository<TagEntity> tagsRepository;
  late VersesBloc versesBloc;
  late AccountsBloc accountsBloc;
  late EntitiesRepository<AccountEntity> accountsRepository;
  late NavigationCubit nav;
  late CurrentAccount currentAccount;

  CollectionDetailBloc({
    required this.service,
  }) : super(CollectionDetailInitialState()) {
    nav = di.get<NavigationCubit>();
    accountsRepository = di.get<EntitiesRepository<AccountEntity>>();
    tagsRepository = di.get<EntitiesRepository<TagEntity>>();
    currentAccount = di.get<CurrentAccount>();
    tagsBloc = di.get<TagsBloc>();
    versesBloc = di.get<VersesBloc>();
    accountsBloc = di.get<AccountsBloc>();
    on<CollectionDetailEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (CollectionDetailState result) => result),
        transformer: sequential()); // process events sequentially
  }

  Stream<CollectionDetailState> mapEventToState(
      CollectionDetailEvent event) async* {
    if (event is CollectionDetailLoaded) {
      yield* mapCollectionDetailLoadedToState(event);
    } else if (event is CollectionDetailDismissed) {
      yield* mapCollectionDetailDismissedToState();
    } else if (event is CollectionImported) {
      yield* mapCollectionImportedToState(event);
    }
  }

  Stream<CollectionDetailState> mapCollectionDetailLoadedToState(
      CollectionDetailLoaded event) async* {
    final collection = await service.fetchCollection(event.id);
    await currentAccount.setPublisher(collection.publisher);
    final rankedVerses = collection.verses
        .map((verse) => verse.copyWith(
            rank: VerseService().rank(verse.reference, currentAccount.books)))
        .toList();
    rankedVerses.sort(VerseService.compare(VerseOrder.CANON));
    yield CollectionDetailLoadSuccess(
        collection.copyWith(verses: rankedVerses));
  }

  Stream<CollectionDetailState> mapCollectionDetailDismissedToState() async* {
    await currentAccount.setPublisher(null);
    yield CollectionDetailInitialState();
  }

  Stream<CollectionDetailState> mapCollectionImportedToState(
      CollectionImported event) async* {
    final _state = state;
    if (_state is CollectionDetailLoadSuccess) {
      yield _state.copyWith(isUpdating: true);
      final account = currentAccount.state!;
      final _tagsState = tagsBloc.state;
      TagEntity? tag;
      bool isReimport =
          account.importedDecks.any((id) => id == _state.collection.id);
      if (isReimport && _tagsState is EntitiesLoadSuccess<TagEntity>) {
        // reuse existing tag if collection is re-imported
        tag = _tagsState.entities.firstWhereOrNull(
            (tag) => tag.label == _state.collection.label && !tag.deleted);
      }
      if (tag == null) {
        tag = TagEntity(_state.collection.label, accountId: account.id);
        await tagsRepository.create(tag);
        tagsBloc.add(EntitiesAdded<TagEntity>([tag], persist: false));
      }
      if (!isReimport) {
        final updatedAccount = account.copyWith(importedDecks: <int>[
          ...account.importedDecks,
          _state.collection.id
        ]);
        await accountsRepository.update(updatedAccount);
        accountsBloc.add(
            EntitiesUpdated<AccountEntity>([updatedAccount], persist: false));
      }
      final verses = _state.collection.verses
          .where((remoteVerse) {
            // exclude duplicates if collection is re-imported
            final localState = versesBloc.state;
            return !isReimport ||
                localState is! EntitiesLoadSuccess<VerseEntity> ||
                !localState.entities.any((localVerse) =>
                    localVerse.tags.keys.contains(tag!.id) &&
                    localVerse.reference == remoteVerse.reference &&
                    localVerse.source == remoteVerse.source &&
                    !localVerse.deleted);
          })
          .map((verse) => verse.copyWith(
                id: IdGenerator().next(),
                accountId: account.id,
                tags: {tag!.id: tag.text},
              ))
          .toList();
      if (verses.isNotEmpty) versesBloc.add(EntitiesAdded(verses));
      di.get<UndoBloc>().add(UndoAdded([
            Undo(EntityAction.create, <TagEntity>[tag]),
            Undo(EntityAction.update, <AccountEntity>[account]),
            Undo(EntityAction.create, verses),
          ]));
      yield _state.copyWith(isUpdating: false);
      nav.go(HomePath());
    }
  }
}
