import 'package:equatable/equatable.dart';

abstract class CollectionDetailEvent extends Equatable {
  const CollectionDetailEvent();

  @override
  List<Object?> get props => [];
}

class CollectionDetailDismissed extends CollectionDetailEvent {}

class CollectionDetailLoaded extends CollectionDetailEvent {
  final int id;

  const CollectionDetailLoaded(this.id);

  @override
  List<Object?> get props => [id];
}

class CollectionImported extends CollectionDetailEvent {
  final int id;

  const CollectionImported(this.id);

  @override
  List<Object?> get props => [id];
}
