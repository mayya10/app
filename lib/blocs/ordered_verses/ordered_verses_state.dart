import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

abstract class OrderedVersesState extends Equatable {
  const OrderedVersesState();

  @override
  List<Object?> get props => [];
}

class InitialOrderedVersesState extends OrderedVersesState {}

class OrderInProgress extends OrderedVersesState {}

class OrderFailure extends OrderedVersesState {}

class OrderSuccess extends OrderedVersesState {
  final VerseOrder order;
  final List<Verse> verses;
  final bool isUpdating;

  const OrderSuccess(this.order, this.verses, {this.isUpdating = false});

  @override
  List<Object?> get props => [order, verses, isUpdating];

  OrderSuccess copyWith({
    List<Verse>? verses,
    bool? isUpdating,
  }) =>
      OrderSuccess(this.order, verses ?? this.verses,
          isUpdating: isUpdating ?? this.isUpdating);

  @override
  String toString() {
    return 'OrderSuccess { order: $order, verses: ${verses.length}, isUpdating: $isUpdating }';
  }
}
