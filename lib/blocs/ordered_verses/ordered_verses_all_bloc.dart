import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/ordered_verses/ordered_verses.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';

import '../current_account/current_account.dart';
import '../blocs.dart';

class OrderedVersesAllBloc extends OrderedVersesBloc {
  OrderedVersesAllBloc()
      : super(box: Box.ALL);

  @override
  mapVersesToBox(List<Verse>? verses, VerseOrder order) {
    return VerseService().mapVersesToBoxAll(verses!, order);
  }

  @override
  VerseOrder get currentOrder {
    return di.get<CurrentAccount>().state!.orderAll;
  }

  @override
  Account currentAccountWithOrder(VerseOrder order) {
    return di.get<CurrentAccount>().state!.copyWith(orderAll: order);
  }
}
