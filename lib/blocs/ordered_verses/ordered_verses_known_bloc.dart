import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/ordered_verses/ordered_verses.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';

import '../current_account/current_account.dart';
import '../blocs.dart';

class OrderedVersesKnownBloc extends OrderedVersesBloc {

  OrderedVersesKnownBloc()
      : super(box: Box.KNOWN);


  @override
  mapVersesToBox(List<Verse>? verses, VerseOrder order) {
    return VerseService().mapVersesToBoxKnown(verses!, order);
  }

  @override
  VerseOrder get currentOrder {
    return di.get<CurrentAccount>().state!.orderKnown;
  }

  @override
  Account currentAccountWithOrder(VerseOrder order) {
    return di.get<CurrentAccount>().state!.copyWith(orderKnown: order);
  }

}
