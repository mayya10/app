import 'dart:async';

import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

import '../current_account/current_account.dart';
import '../blocs.dart';

abstract class OrderedVersesBloc
    extends Bloc<OrderedVersesEvent, OrderedVersesState> {
  final Box box;
  late FilteredVersesBloc filterBloc;
  late StreamSubscription versesSubscription;

  OrderedVersesBloc(
      {required Box box})
      : box = box,
        super(initialState(box)) {
    filterBloc = di.get<FilteredVersesBloc>();
    on<OrderedVersesEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (OrderedVersesState result) => result),
        transformer: sequential()); // process events sequentially
    versesSubscription = filterBloc.stream.listen(_filteredBlocUpdated);
    _filteredBlocUpdated(filterBloc.state);
  }

  static OrderedVersesState initialState(Box box) {
    switch (di.get<FilteredVersesBloc>().state.runtimeType) {
      case FilterInProgress:
        return OrderInProgress();
      default:
        return InitialOrderedVersesState();
    }
  }

  void _filteredBlocUpdated(FilteredVersesState? state) {
    if (state is FilteredVersesSuccess) {
      if (state.isUpdating) {
        add(FilteredVersesUpdated());
      } else {
        add(FilteredVersesLoaded(state.filteredVerses));
      }
    } else if (state is FilterInProgress) {
      add(OrderedVersesReloaded());
    } else if (state is FilterFailure) {
      add(FilterFailed());
    }
  }

  static OrderedVersesBloc from(TabState tabState) {
    final box = tabState.isBoxed ? tabState.box : Box.ALL;
    switch (box) {
      case Box.NEW:
        return di.get<OrderedVersesNewBloc>();
      case Box.DUE:
        return di.get<OrderedVersesDueBloc>();
      case Box.KNOWN:
        return di.get<OrderedVersesKnownBloc>();
      case Box.ALL:
        return di.get<OrderedVersesAllBloc>();
    }
  }

  Stream<OrderedVersesState> mapEventToState(OrderedVersesEvent event) async* {
    if (event is OrderUpdated) {
      yield* mapOrderUpdatedToState(event);
    } else if (event is FilteredVersesLoaded) {
      yield* mapVersesLoadedToState(event);
    } else if (event is FilteredVersesUpdated) {
      yield* mapVersesUpdatedToState();
    } else if (event is OrderedVersesReloaded) {
      yield* _mapOrderedVersesReloadedToState();
    } else if (event is FilterFailed) {
      yield OrderFailure();
    }
  }

  Stream<OrderedVersesState> _mapOrderedVersesReloadedToState() async* {
    yield OrderInProgress();
  }

  Stream<OrderedVersesState> mapOrderUpdatedToState(
    OrderUpdated event,
  ) async* {
    if (filterBloc.state is FilteredVersesSuccess) {
      final verses =
          (filterBloc.state as FilteredVersesSuccess).filteredVerses;
      final Account updatedAccount = currentAccountWithOrder(event.order);
      di.get<CurrentAccount>().setAccount(updatedAccount);
      di.get<AccountsBloc>().add(EntitiesUpdated([updatedAccount]));
      yield OrderSuccess(event.order, mapVersesToBox(verses, event.order));
    }
  }

  Stream<OrderedVersesState> mapVersesLoadedToState(
    FilteredVersesLoaded event,
  ) async* {
    final state = this.state;
    final order = (state is OrderSuccess) ? state.order : currentOrder;
    yield OrderSuccess(order, mapVersesToBox(event.verses, order));
  }

  Stream<OrderedVersesState> mapVersesUpdatedToState() async* {
    final state = this.state;
    if (state is OrderSuccess) {
      yield state.copyWith(isUpdating: true);
    }
  }

  Account currentAccountWithOrder(VerseOrder order);

  VerseOrder get currentOrder;

  List<Verse> mapVersesToBox(List<Verse>? verses, VerseOrder order);

  @override
  Future<void> close() {
    versesSubscription.cancel();
    return super.close();
  }
}
