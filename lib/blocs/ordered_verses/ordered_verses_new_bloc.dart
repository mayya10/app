import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/ordered_verses/ordered_verses.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';

import '../current_account/current_account.dart';
import '../blocs.dart';

class OrderedVersesNewBloc extends OrderedVersesBloc {

  OrderedVersesNewBloc()
      : super(box: Box.NEW);


  @override
  mapVersesToBox(List<Verse>? verses, VerseOrder order) {
    return VerseService().mapVersesToBoxNew(verses!, order);
  }

  @override
  VerseOrder get currentOrder {
    return di.get<CurrentAccount>().state!.orderNew;
  }

  @override
  Account currentAccountWithOrder(VerseOrder order) {
    return di.get<CurrentAccount>().state!.copyWith(orderNew: order);
  }

}
