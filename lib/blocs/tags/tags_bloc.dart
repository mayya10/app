import 'package:app_core/app_core.dart';
import 'package:collection/collection.dart';
import 'package:repository/repository.dart';

import '../blocs.dart';

class TagsBloc extends EntitiesBloc<TagEntity> {

  TagsBloc()
      : super(
            repository: di.get<EntitiesRepository<TagEntity>>(),
            initialState: InitialEntitiesState<TagEntity>()) {
    subscribeToAccountChange();
  }

  @override
  List<TagEntity> sorted(Iterable<TagEntity> entities) {
    return entities.toSet().sortedBy((it) => it.text);
  }
}
