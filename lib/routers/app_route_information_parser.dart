import 'package:flutter/material.dart';
import 'package:remem_me/routers/routers.dart';

class AppRouteInformationParser extends RouteInformationParser<AppRoutePath> {
  static final simplePaths = {
    AccountCreatePath(): 'account-add',
    AccountBinPath(): 'account-bin',
    RegisterSuccessPath(): 'register-success',
    RegisterErrorPath(): 'register-error',
    ScoresPath(): 'scores',
    StylesPath(): 'styles',
    VerseBinPath(): 'bin',
  };

  @override
  Future<AppRoutePath> parseRouteInformation(
      RouteInformation routeInformation) async {
    final uri = Uri.parse(routeInformation.location ?? '');
    if (uri.pathSegments.isEmpty || uri.pathSegments.first == 'verses') {
      if (uri.pathSegments.length > 1) {
        if (uri.pathSegments[1] == HomeActivity.add.name) {
          return HomePath(
              activity: HomeActivity.values.byName(uri.pathSegments[1]));
        }
        // todo: load verse before showing detail
        /*final id = int.parse(uri.pathSegments[1]);
        if (uri.pathSegments.length > 2) {
          return HomePath(id: id, activity: uri.pathSegments[2]);
        }*/
      }
      return HomePath();
    }

    for (var entry in simplePaths.entries) {
      if (uri.pathSegments.first == entry.value) return entry.key;
    }

    if (uri.pathSegments.first == 'collections') {
      if (uri.pathSegments.length >= 3) {
        return CollectionsPath(
            id: int.parse(uri.pathSegments[1]),
            activity: uri.pathSegments.length == 4
                ? CollectionActivity.study
                : CollectionActivity.flashcard,
            verseId: int.parse(uri.pathSegments[2]));
      } else if (uri.pathSegments.length == 2) {
        try {
          return CollectionsPath(id: int.parse(uri.pathSegments[1]));
        } on FormatException {
          return CollectionsPath(
              activity: CollectionActivity.values.byName(uri.pathSegments[1]));
        }
      }
      return CollectionsPath();
    }

    if (uri.pathSegments.first == 'account-edit') {
      return AccountEditPath(int.parse(uri.pathSegments[1]));
    }

    if (uri.pathSegments.first == 'labels') {
      if (uri.pathSegments.length > 1) {
        return TagsPath(
            activity: TagActivity.values.byName(uri.pathSegments[1]));
      }
      return TagsPath();
    }

    if (uri.pathSegments.first == 'reset-password') {
      return ResetPasswordPath(
          uidb64: uri.queryParameters['uidb64']!,
          token: uri.queryParameters['token']!);
    }

    return HomePath();
  }

  @override
  RouteInformation restoreRouteInformation(AppRoutePath path) {
    if (simplePaths.keys.contains(path)) {
      return RouteInformation(location: '/${simplePaths[path]}');
    }
    if (path is AccountEditPath) {
      return RouteInformation(location: '/account-edit/${path.id}');
    }
    if (path is CollectionsPath) {
      final parts = [
        '/collections',
        if (path.id != null)
          '/${path.id}'
        else if (path.activity != null)
          '/${path.activity!.name}',
        if (path.verseId != null) '/${path.verseId}',
        if (path.activity == CollectionActivity.study) '/${path.activity!.name}'
      ];
      return RouteInformation(location: parts.join());
    }
    if (path is HomePath) {
      final parts = [
        '/verses',
        if (path.id != null) '/${path.id}',
        if (path.activity != null) '/${path.activity!.name}',
      ];
      return RouteInformation(location: parts.join());
    }
    if (path is TagsPath) {
      final parts = [
        '/labels',
        if (path.activity != null) '/${path.activity!.name}'
      ];
      return RouteInformation(location: parts.join());
    }
    return RouteInformation();
  }
}
