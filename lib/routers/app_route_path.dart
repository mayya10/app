import 'package:equatable/equatable.dart';

abstract class AppRoutePath extends Equatable {
  @override
  List<Object?> get props => [];

  @override
  String toString() => 'AppRoutePath';
}

class AccountCreatePath extends AppRoutePath {}

class AccountEditPath extends AppRoutePath {
  final int id;

  AccountEditPath(this.id);

  @override
  List<Object?> get props => [id];

  @override
  String toString() => 'AccountEditPath { id: $id }';
}

class AccountBinPath extends AppRoutePath {}

enum CollectionActivity {
  imported, publish, flashcard, study
}

class CollectionsPath extends AppRoutePath {
  final int? id;
  final CollectionActivity? activity;
  final int? verseId;

  CollectionsPath({this.id, this.activity, this.verseId});

  @override
  List<Object?> get props => [id, activity, verseId];

  @override
  String toString() =>
      'CollectionsPath { id: $id, activity: $activity, verseId: $verseId }';
}

enum HomeActivity {
  add, edit, study
}

enum ReviewMode {
  review, inverse, random
}

class HomePath extends AppRoutePath {
  final int? id;
  final ReviewMode? reviewMode;
  final HomeActivity? activity;
  final String? data;

  HomePath({this.id, this.reviewMode, this.activity, this.data});

  @override
  List<Object?> get props => [id, activity, data];

  @override
  String toString() =>
      'HomePath { verse: $id, activity: $activity, data: $data, '
          'reviewMode: $reviewMode }';
}

class VerseBinPath extends AppRoutePath {}

class RegisterSuccessPath extends AppRoutePath {}

class RegisterErrorPath extends AppRoutePath {}

class ResetPasswordPath extends AppRoutePath {
  String uidb64;
  String token;

  ResetPasswordPath({required this.uidb64, required this.token});

  @override
  List<Object?> get props => [uidb64, token];

  @override
  String toString() => 'ResetPasswordPath { uidb64: $uidb64, token: $token }';
}

class ScoresPath extends AppRoutePath {}

class StartPath extends AppRoutePath {}

class StylesPath extends AppRoutePath {}

enum TagActivity {
  bin
}

class TagsPath extends AppRoutePath {
  final TagActivity? activity; // e.g. 'bin'

  TagsPath({this.activity});

  @override
  List<Object?> get props => [activity];

  @override
  String toString() => 'TagsPath { activity: $activity }';
}
