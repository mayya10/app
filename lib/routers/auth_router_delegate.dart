import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:remem_me/routers/routers.dart';
import 'package:remem_me/screens/screens.dart';
import 'package:repository/repository.dart';

import '../blocs/blocs.dart';

class AuthRouterDelegate extends RouterDelegate<AppRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<AppRoutePath> {
  @override
  final GlobalKey<NavigatorState> navigatorKey;

  /*AppState appState = AppState();*/
  NavigationCubit nav = di.get<NavigationCubit>();

  AuthRouterDelegate() : navigatorKey = GlobalKey<NavigatorState>() {
    // appState.addListener(notifyListeners);
    nav.stream.listen((event) => notifyListeners());
  }

  @override
  AppRoutePath get currentConfiguration {
    return nav.state;
  }

  @override
  Widget build(BuildContext ctx) {
    return _topLevelProviders(
        child: BlocBuilder<AuthBloc, AuthState>(
            builder: (BuildContext ctx, AuthState auth) => Navigator(
                  key: navigatorKey,
                  pages: [
                    if (nav.state is ResetPasswordPath)
                      _resetPassword(ctx, nav.state as ResetPasswordPath)
                    else if (nav.state is RegisterSuccessPath)
                      _registerSuccess(ctx)
                    else if (nav.state is RegisterErrorPath)
                      _registerError(ctx)
                    else if (auth is LoginSuccess ||
                        nav.state is CollectionsPath)
                      MaterialPage(child: BaseScreen(nav))
                    else
                      MaterialPage(child: StartScreen()),
                  ],
                  onPopPage: (route, result) {
                    if (!route.didPop(result)) {
                      return false;
                    }
                    notifyListeners();
                    return true;
                  },
                )));
  }

  @override
  Future<void> setNewRoutePath(AppRoutePath path) async {
    nav.go(path);
  }

  Page _registerSuccess(BuildContext ctx) {
    return MaterialPage(
        child: MessageScreen(messages: {
      'messages': [L10n.of(ctx).t8('User.register.success.summary')]
    }, title: L10n.of(ctx).t8('User.register.success.title')));
  }

  Page _registerError(BuildContext ctx) {
    return MaterialPage(
        child: MessageScreen(messages: {
      'messages': [L10n.of(ctx).t8('User.register.error.summary')]
    }, title: L10n.of(ctx).t8('User.register.error.title')));
  }

  Page _resetPassword(BuildContext ctx, ResetPasswordPath path) {
    return MaterialPage(
        child: BlocProvider<ResetPasswordBloc>(
            create: (ctx) {
              return ResetPasswordBloc(
                  service: PasswordService(http.Client()),
                  uidb64: path.uidb64,
                  token: path.token);
            },
            child: ResetPasswordScreen()));
  }

  /// Provide top level blocs here, others in routes
  Widget _topLevelProviders({required Widget child}) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<AuthBloc>.value(
              value: di.get<AuthBloc>()..add(AuthLoadedFromSettings())),
          BlocProvider<ConnectivityCubit>.value(
              value: di.get<ConnectivityCubit>()),
          BlocProvider<CurrentAccount>.value(value: di.get<CurrentAccount>()),
          BlocProvider<NavigationCubit>.value(value: nav),
          BlocProvider<ReplicationBloc>.value(value: di.get<ReplicationBloc>())
        ],
        child: MultiRepositoryProvider(
          providers: [
            RepositoryProvider<AccountsRepository>.value(
                value: di.get<EntitiesRepository<AccountEntity>>()
                    as AccountsRepository),
            RepositoryProvider<VersesRepository>.value(
                value: di.get<EntitiesRepository<VerseEntity>>()
                    as VersesRepository),
            RepositoryProvider<TagsRepository>.value(
                value:
                    di.get<EntitiesRepository<TagEntity>>() as TagsRepository),
          ],
          child: MultiBlocProvider(providers: [
            BlocProvider<AccountsBloc>.value(value: di.get<AccountsBloc>()),
            BlocProvider<VersesBloc>.value(value: di.get<VersesBloc>()),
            BlocProvider<TagsBloc>.value(value: di.get<TagsBloc>()),
            BlocProvider<ScoresBloc>.value(value: di.get<ScoresBloc>()),
            BlocProvider<UndoBloc>.value(value: di.get<UndoBloc>()),
            BlocProvider<TaggedVersesBloc>.value(
                value: di.get<TaggedVersesBloc>()),
          ], child: child),
        ));
  }
}
