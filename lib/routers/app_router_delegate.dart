import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/navigators/navigators.dart';
import 'package:remem_me/routers/routers.dart';

class AppRouterDelegate extends RouterDelegate<AppRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<AppRoutePath> {
  final NavigationCubit _nav;

  AppRouterDelegate(
      {required NavigationCubit nav})
      : _nav = nav;

  @override
  GlobalKey<NavigatorState>? get navigatorKey => BaseNavigator.navigatorKey;

  @override
  Widget build(BuildContext ctx) {
    final path = _nav.state;
    if (path is CollectionsPath)
      return CollectionsNavigator(path, onNotify: notifyListeners);
    if (path is HomePath) return HomeNavigator(path, onNotify: notifyListeners);
    if (path is TagsPath) return TagsNavigator(path, onNotify: notifyListeners);
    return BaseNavigator(path, onNotify: notifyListeners);
  }

  @override
  Future<bool> popRoute() {
    // todo: close dialog before navigating (useRootNavigator: false)
    // todo: keep track of open dialogs in AppRoutePath
    if ((_nav.state is AccountEditPath)) {
      _nav.go(HomePath());
      return SynchronousFuture<bool>(true);
    } else {
      return super.popRoute();
    }
  }

  @override
  Future<void> setNewRoutePath(AppRoutePath path) async {
    // This is not required for inner router delegate because it does not
    // parse route
    assert(false);
  }

}
