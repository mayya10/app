import 'dart:convert';
import 'dart:io';

import 'package:app_core/app_core.dart';
import 'package:audio_service/audio_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider_android/path_provider_android.dart';
import 'package:path_provider_ios/path_provider_ios.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/text_to_speech/tts_handler.dart';
import 'package:remem_me/services/home_widget_service.dart';
import 'package:remem_me/services/notification_service.dart';
import 'package:remem_me/services/replication_service.dart';
import 'package:remem_me/services/replicator.dart';
import 'package:remem_me/services/tts_service.dart';
import 'package:repository/repository.dart';
import 'package:workmanager/workmanager.dart';

import 'app.dart';
import 'routers/routers.dart';

/// Used for Background Updates using Workmanager Plugin
void callbackDispatcher() {
  Workmanager().executeTask((taskName, inputData) async {
    debugPrint('Workmanager().executeTask()');
    debugPrint('taskName: $taskName');
    debugPrint('inputData: ${jsonEncode(inputData)}');

    // todo: injections

    // todo: replace plugin registrations with DartPluginRegistrant.ensureInitialized()
    // https://github.com/flutter/flutter/issues/99155#issuecomment-1060956399
    if (Platform.isAndroid) PathProviderAndroid.registerWith();
    if (Platform.isIOS) PathProviderIOS.registerWith();
    return Future.value(
        false); // return true if background execution successful
  });
}

Future<void> main() async {
  usePathUrlStrategy();
  WidgetsFlutterBinding.ensureInitialized();
  // await Workmanager().initialize(callbackDispatcher, isInDebugMode: kDebugMode);
  await AudioService.init(
    builder: () => TtsHandler(),
    config: const AudioServiceConfig(
      androidNotificationChannelId: 'me.remem.app.channel.tts',
      androidNotificationChannelName: 'Remember Me Text-to-Speech',
      androidNotificationIcon: 'drawable/ic_notification',
      androidNotificationOngoing: true,
    ),
  );
  if (!kIsWeb) await NotificationService().init();
  di.registerFactory<http.Client>(() => http.Client());
  di.registerSingleton<Settings>(await Settings().init());
  di.registerSingleton<HomeWidgetService>(HomeWidgetService());
  di.registerSingleton(TtsService());
  Bloc.observer = SimpleBlocObserver();
  await registerServices();
  runApp(App(key: UniqueKey()));
}

Future<void> registerServices() async {
  registerConnectionBlocs();
  registerRepositoriesHttp();
  if (kIsWeb) {
    registerRepositoriesWeb();
  } else {
    registerRepositoriesHive();
    registerRepositoriesMobile();
  }
  registerEntitiesBlocs();
  if (!kIsWeb) registerReplicators();
  registerHomeBlocs();
}

Future<void> initApp() async {
  runApp(App(key: UniqueKey()));
}

void registerConnectionBlocs() {
  di.registerSingleton<CurrentAccount>(CurrentAccount());
  di.registerSingleton<ConnectivityCubit>(ConnectivityCubit());
  di.registerSingleton<AuthBloc>(AuthBloc());
  di.registerSingleton<ReplicationBloc>(ReplicationBloc());
  di.registerSingleton<NavigationCubit>(NavigationCubit());
  di.registerSingleton<TabBloc>(TabBloc());
}

void registerEntitiesBlocs() {
  di.registerSingleton<AccountsBloc>(AccountsBloc());
  di.registerSingleton<VersesBloc>(VersesBloc());
  di.registerSingleton<TagsBloc>(TagsBloc());
  di.registerSingleton<ScoresBloc>(ScoresBloc());
  di.registerSingleton<UndoBloc>(UndoBloc());
}

void registerHomeBlocs() {
  di.registerSingleton<TaggedVersesBloc>(TaggedVersesBloc());
  di.registerSingleton<FilteredVersesBloc>(FilteredVersesBloc());
  di.registerSingleton<OrderedVersesNewBloc>(OrderedVersesNewBloc());
  di.registerSingleton<OrderedVersesDueBloc>(OrderedVersesDueBloc());
  di.registerSingleton<OrderedVersesKnownBloc>(OrderedVersesKnownBloc());
  di.registerSingleton<OrderedVersesAllBloc>(OrderedVersesAllBloc());
  di.registerSingleton<HomeFlashcardBloc>(HomeFlashcardBloc());
}

void registerRepositoriesHttp() {
  di.registerSingleton<EntitiesRepositoryHttp<AccountEntity>>(
      AccountsRepositoryHttp());
  di.registerSingleton<EntitiesRepositoryHttp<VerseEntity>>(
      VersesRepositoryHttp());
  di.registerSingleton<EntitiesRepositoryHttp<TagEntity>>(TagsRepositoryHttp());
  di.registerSingleton<EntitiesRepositoryHttp<ScoreEntity>>(
      ScoresRepositoryHttp());
}

void registerRepositoriesHive() {
  if (!di.isRegistered<Queues>()) di.registerSingleton<Queues>(Queues());
  if (!di.isRegistered<EntitiesRepositoryHive<AccountEntity>>())
    di.registerSingleton<EntitiesRepositoryHive<AccountEntity>>(
        AccountsRepositoryHive());
  if (!di.isRegistered<EntitiesRepositoryHive<VerseEntity>>())
    di.registerSingleton<EntitiesRepositoryHive<VerseEntity>>(
        VersesRepositoryHive());
  if (!di.isRegistered<EntitiesRepositoryHive<TagEntity>>())
    di.registerSingleton<EntitiesRepositoryHive<TagEntity>>(
        TagsRepositoryHive());
  if (!di.isRegistered<EntitiesRepositoryHive<ScoreEntity>>())
    di.registerSingleton<EntitiesRepositoryHive<ScoreEntity>>(
        ScoresRepositoryHive());
}

void registerRepositoriesWeb() {
  di.registerSingleton<EntitiesRepository<AccountEntity>>(
      di.get<EntitiesRepositoryHttp<AccountEntity>>());
  di.registerSingleton<EntitiesRepository<VerseEntity>>(
      di.get<EntitiesRepositoryHttp<VerseEntity>>());
  di.registerSingleton<EntitiesRepository<TagEntity>>(
      di.get<EntitiesRepositoryHttp<TagEntity>>());
  di.registerSingleton<EntitiesRepository<ScoreEntity>>(
      di.get<EntitiesRepositoryHttp<ScoreEntity>>());
}

void registerRepositoriesMobile() {
  final onUpdate = () => di.get<ReplicationBloc>().add(RplQueued());
  di.registerSingleton<EntitiesRepository<AccountEntity>>(
      AccountsRepositoryRpl(onUpdate: onUpdate));
  di.registerSingleton<EntitiesRepository<VerseEntity>>(
      VersesRepositoryRpl(onUpdate: onUpdate));
  di.registerSingleton<EntitiesRepository<TagEntity>>(
      TagsRepositoryRpl(onUpdate: onUpdate));
  di.registerSingleton<EntitiesRepository<ScoreEntity>>(ScoresRepositoryRpl());
}

void registerReplicators() {
  ReplicationService().register(AccountEntity,
      Replicator<AccountEntity>(AccountEntity, di.get<AccountsBloc>()));
  ReplicationService().register(
      VerseEntity, Replicator<VerseEntity>(VerseEntity, di.get<VersesBloc>()));
  ReplicationService().register(
      TagEntity, Replicator<TagEntity>(TagEntity, di.get<TagsBloc>()));
  ReplicationService().register(
      ScoreEntity, Replicator<ScoreEntity>(ScoreEntity, di.get<ScoresBloc>()));
}
