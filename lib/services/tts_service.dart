import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:remem_me/navigators/navigators.dart';
import 'package:repository/repository.dart';

import '../utils/utils.dart';

enum EngineState { started, paused, cancelled, completed }

class TtsService {
  final _flutterTts = FlutterTts();
  bool _isStopRequested = false;
  String? _key;
  final completeHandlers = <MapEntry<String, Function>>[];
  final cancelHandlers = <MapEntry<String, Function>>[];

  static final TtsService _instance = TtsService._internal();

  factory TtsService() {
    return _instance;
  }

  TtsService._internal() {
    _flutterTts.awaitSpeakCompletion(true);
    _flutterTts.setCompletionHandler(() =>
        _notify(_isStopRequested ? cancelHandlers : completeHandlers));
    _flutterTts.setCancelHandler(() => _notify(cancelHandlers));
    _flutterTts.setErrorHandler((_) => _notify(cancelHandlers));
  }

  _notify(List<MapEntry<String, Function>> handlers) {
    handlers.where((item) => item.key == _key).forEach((item) => item.value());
  }

  void registerCompleteHandler(String key, Function fn) {
    completeHandlers.add(MapEntry(key, fn));
  }

  void registerCancelHandler(String key, Function fn) {
    cancelHandlers.add(MapEntry(key, fn));
  }

  Future<void> stop() async {
    _isStopRequested = true;
    var result = await _flutterTts.stop();
    debugPrint('Stop speaking $result');
  }

  Future<void> speakText(String key, String text, Locale locale) async {
    debugPrint('TtsService.speakText $key $text $locale');
    _isStopRequested = false;
    _key = key;
    final rate = Settings().getDouble(Settings.SPEECH_SPEED) ?? 0.5;
    _flutterTts.setSpeechRate(rate);
    await _flutterTts.setLanguage(await _availableLang(locale));
    await _flutterTts.speak(text);
  }

  Future<String> _availableLang(Locale intended) async {
    var available = await _intendedLang(intended);
    if (available == null) {
      _messageLanguageNotAvailable(intended.toString());
      available = await _intendedLang(L10n.current.locale);
    }
    available ??= 'en';
    return available;
  }

  Future<String?> _intendedLang(Locale intended) async {
    var lang = intended.languageCode;
    var ctry = intended.countryCode;
    if (ctry != null) {
      if (await _flutterTts.isLanguageAvailable('$lang-$ctry')) {
        return '$lang-$ctry';
      }
      if (lang == 'zh') lang = '${_chineseLang(ctry)}';
    }
    if (await _flutterTts.isLanguageAvailable(lang)) return lang;
    List<dynamic> locales = await _flutterTts.getLanguages;
    final alternative = locales.firstWhereOrNull(
        (locale) => locale is String && locale.startsWith(lang));
    return alternative;
  }

  void _messageLanguageNotAvailable(String locale) {
    final ctx = BaseNavigator.navigatorKey.currentContext; // basic BuildContext
    if (ctx != null && LocaleNames.of(ctx) != null) {
      MessageService().warn(L10n.current.t8('Speech.languageNotAvailable',
          [localeNameOf(ctx, locale.toString())]));
    }
  }

  _chineseLang(String ctry) {
    switch (ctry) {
      case 'HK':
        return 'yue';
      default: // CN, TW
        return 'cmn';
    }
  }
}
