import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:app_core/app_core.dart';
import 'package:remem_me/models/photos/photos.dart';
import 'package:repository/repository.dart';

import '../blocs/blocs.dart';

const _batchSize = 20;

class PhotosService {
  static PhotosService? _instance;
  late http.Client httpClient;

  factory PhotosService() {
    _instance ??= PhotosService._internal();
    return _instance!;
  }

  PhotosService._internal() {
    httpClient = di.get<http.Client>();
  }

  Future<PhotoBatch> fetchPhotos(String? search, {offset = 0}) async {
    try {
      return search == null
          ? await fetchRandomPhotos()
          : await fetchSearchPhotos(search, offset: offset);
    } on Exception {
      throw Exception('error fetching photos');
    }
  }

  Future<PhotoBatch> fetchRandomPhotos() async {
    final uri =
        await uriFromAddress(Settings.IMAGE_SERVER, '/unsplash/photos/random', {
      'content_filter': 'high',
      'count': _batchSize.toString(),
      'utm_source': 'remember_me',
      'utm_medium': 'referral'
    });
    debugPrint('fetchRandomPhotos: $uri');
    final response = await http.get(uri);
    if (response.statusCode == 200) {
      final items = json.decode(response.body) as List<dynamic>;
      return PhotoBatch(items.map((item) => Photo.fromJson(item)).toList());
    }
    debugPrint('Random photos response status: ${response.statusCode}');
    debugPrint(
        'Response body: ${response.body.substring(0, min(response.body.length, 100))}');
    throw Exception('error fetching random photos');
  }

  Future<PhotoBatch> fetchSearchPhotos(String search, {int offset = 0}) async {
    final uri =
        await uriFromAddress(Settings.IMAGE_SERVER, '/unsplash/search/photos', {
      'query': search,
      'per_page': _batchSize.toString(),
      // todo: allow zh-TW
      // see https://unsplash.com/documentation#supported-languages
      'lang': _currentLanguage(),
      'page': (offset ~/ _batchSize + 1).toString(),
      'utm_source': 'remember_me',
      'utm_medium': 'referral'
    });
    debugPrint('fetchSearchPhotos: $uri');
    final response = await http.get(uri);
    if (response.statusCode == 200) {
      return PhotoBatch.fromJson(json.decode(response.body));
    }
    debugPrint('Search photos response status: ${response.statusCode}');
    debugPrint(
        'Response body: ${response.body.substring(0, min(response.body.length, 100))}');
    throw Exception('error fetching search photos');
  }

  bool hasReachedMax(int currentSize, [int? totalSize]) =>
      currentSize >= (totalSize ?? 20);

  String _currentLanguage() {
    return di.get<CurrentAccount>().state?.language != null
        ? di.get<CurrentAccount>().state!.language.languageCode
        : L10n.current.locale.languageCode;
  }
}
