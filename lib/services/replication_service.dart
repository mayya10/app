import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/services/replicator.dart';
import 'package:repository/repository.dart';

/// Goes through all replication queues,
/// sends all items in the queues,
/// uses repository's updateList() method
class ReplicationService {
  final _replicators = <Type, Replicator>{};
  final _order = [AccountEntity, TagEntity, VerseEntity, ScoreEntity];
  late Settings _settings;

  static final ReplicationService _instance = ReplicationService._internal();

  factory ReplicationService() {
    return _instance;
  }

  ReplicationService._internal() {
    _settings = di.get<Settings>();
  }

  Future<void> register(Type entityType, Replicator replicator) async {
    debugPrint('ReplicationService.register(entityType=$entityType)');
    _replicators.putIfAbsent(entityType, () => replicator);
  }

  Future<void> replicate({bool all = false}) async {
    await send(all: all);
    await receive(all: all);
  }

  Future<void> send({bool all = false}) async {
    final accountState = _replicators[AccountEntity]!.bloc.state;
    if (!(accountState is EntitiesLoadSuccess<AccountEntity>)) {
      throw MessageException({
        'messages': [L10n.current.t8('Account.none')]
      });
    }
    final errors = <String, List<String>>{};
    for (var entityType in _order) {
      if(_replicators[entityType] == null) continue;
      final replicator = _replicators[entityType]!;
      try {
        all
            ? await replicator.sendAll(accountState.entities)
            : await replicator.sendQueued(accountState.entities);
      } on MessageException catch (e) {
        errors.addAll(e.messages);
      }
    }
    if (errors.isNotEmpty) {
      throw MessageException(errors);
    }
  }

  Future<void> receive({bool all = false}) async {
    debugPrint('ReplicationService.receive(all=$all)');
    final errors = <String, List<String>>{};
    for (var entityType in _order) {
      if(_replicators[entityType] == null) continue;
      final since = all ? null : _getLastReceived(entityType);
      final client = all ? null : _settings.deviceId;
      try {
        await _receiveWhenReady(entityType, since, client);
      } on MessageException catch (e) {
        errors.addAll(e.messages);
      }
    }
    if (errors.isNotEmpty) {
      throw MessageException(errors);
    }
  }

  Future<void> clearLocalStorage() async {
    final accountState = _replicators[AccountEntity]!.bloc.state
        as EntitiesLoadSuccess<AccountEntity>;
    for (var entityType in _order) {
      await _replicators[entityType]!.clearLocalStorage(accountState.entities);
      _deleteLastReceived(entityType);
    }
    _settings.delete(Settings.RPL_DEVICE_ID);
  }

  Future<void> _receiveWhenReady(
      Type entityType, int? since, int? client) async {
    await _replicators[entityType]!.receiveWhenReady(since, client);
    _setLastReceived(entityType);
  }

  int _getLastReceived(Type entityType) {
    final key = '${Settings.RPL_LAST_RECEIVED}_${repositoryKey(entityType)}';
    final value = _settings.getInt(
            '${Settings.RPL_LAST_RECEIVED}_${repositoryKey(entityType)}') ??
        0;
    debugPrint('ReplicationService.getLastReceived $key $value');
    return value;
  }

  void _setLastReceived(Type entityType) async {
    final key = '${Settings.RPL_LAST_RECEIVED}_${repositoryKey(entityType)}';
    final value = IdGenerator().now();
    _settings.putInt(key, IdGenerator().now());
    debugPrint('ReplicationService.setLastReceived $key $value');
  }

  void _deleteLastReceived(Type entityType) async {
    _settings
        .delete('${Settings.RPL_LAST_RECEIVED}_${repositoryKey(entityType)}');
  }
}
