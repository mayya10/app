import 'dart:convert';

import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

class BookService {
  static final BookService _instance = BookService._internal();

  factory BookService() {
    return _instance;
  }

  BookService._internal();

  Future<Map<String, Book>> loadBooks(Locale locale, Canon canon) async {
    try {
      String booksJson = await rootBundle
          .loadString('assets/l10n/${_availableCode(locale)}_books.json');
      Map<String, dynamic> booksJsonMap = json.decode(booksJson);
      String canonJson =
          await rootBundle.loadString('assets/canon/${canon.name}.json');
      Map<String, dynamic>? canonJsonMap = json.decode(canonJson);
      final books = booksJsonMap.map((key, value) {
        return MapEntry(
            key,
            Book(value[0], canonJsonMap![value[0]], value[1],
                value.length > 2 ? value[2] : value[1]));
      });
      return books;
    } on FlutterError catch (e) {
      debugPrint('Error while loading books. ${e.message}');
      return {};
    }
  }

  String _availableCode(Locale intended) {
    final available = TextUtil.referenceLocale(intended) ?? L10n.current.locale;
    var code = available.languageCode;
    if(available.scriptCode != null) code += '_${available.scriptCode}';
    return code;
  }
}
