import 'dart:math';

import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';

// RegExp:
// (?<!y)x    negative lookbehind
// x(?!y)     negative lookahead
// \p{P}      any kind of punctuation character
// \p{Letter} unicode letter (without numbers)

// Chinese:
// Block                                   Range       Comment
// --------------------------------------- ----------- ----------------------------------------------------
// CJK Unified Ideographs                  4E00-9FFF   Common
// CJK Unified Ideographs Extension A      3400-4DBF   Rare
// CJK Unified Ideographs Extension B      20000-2A6DF Rare, historic
// CJK Unified Ideographs Extension C      2A700–2B73F Rare, historic
// CJK Unified Ideographs Extension D      2B740–2B81F Uncommon, some in current use
// CJK Unified Ideographs Extension E      2B820–2CEAF Rare, historic
// CJK Compatibility Ideographs            F900-FAFF   Duplicates, unifiable variants, corporate characters
// CJK Compatibility Ideographs Supplement 2F800-2FA1F Unifiable variants
// CJK Symbols and Punctuation             3000-303F

class TextService {
  static final TextService _instance = TextService._internal();

  //static final delimiter = RegExp(r'(?<!^\p{P})\s+(?!\p{P}+(\s+|$))', unicode: true);
  static final _wordDelimiterEn =
      RegExp(r'(?<=[\p{Letter}\d].*\s+)(?=\S*[\p{Letter}\d])', unicode: true);
  static final _wordDelimiterZh = RegExp(
      r'(?<=[\u3400-\u4DBF\u4E00-\u9FFF\d].*\s*)(?=[\u3000-\u303F]*[\u3400-\u4DBF\u4E00-\u9FFF\d])',
      unicode: true);
  static final _lineDelimiter = RegExp(r'(?<=\n+)(?=.*\S)', unicode: true);
  static final _referenceDelimiterEn = RegExp(
      r'((?<=\d[\p{P}\s]+)(?=[\p{Letter}\d])|(?<=[\p{Letter}\d].*\s+)(?=\S*[\p{Letter}\d]))',
      unicode: true); // todo: separate book from chapter in zh
  static final _referenceDelimiterZh = RegExp(
      r'((?<=\d[\u3000-\u303F\s]+)(?=[\u3400-\u4DBF\u4E00-\u9FFF\d])|(?<=[\u3400-\u4DBF\u4E00-\u9FFF\d].*\s*)(?=[\u3000-\u303F]*[\u3000-\u303F]*[\u3400-\u4DBF\u4E00-\u9FFF\d]))',
      unicode: true); // todo: separate book from chapter in zh

  // todo: make language dependent, e.g. inject localized TextService using Provider
  final language = 'en';

  factory TextService() {
    return _instance;
  }

  TextService._internal();

  List<String> lines(String text) {
    return text.trim().split(_lineDelimiter);
  }

  List<List<String>> words(String text) {
    return lines(text).map((line) => line.split(wordDelimiter)).toList();
  }

  List<String> splitReference(String reference) {
    return reference.trim().split(referenceDelimiter);
  }

  String wordsToText(List<List<String>> lines, Pos start, bool tillEndOfLine) {
    if (tillEndOfLine) {
      return lines[start.line].sublist(start.word).join();
    } else {
      return lines[start.line][start.word];
    }
  }

  String stripPunctuation(String word) {
    final result = word
        .toLowerCase()
        .replaceAll(RegExp(r'^[\p{P}\s]+', unicode: true), '')
        .replaceAll(RegExp(r'[\p{P}\s]+$', unicode: true), '');
    return result.isEmpty ? ' --- ' : result;
  }

  String stripLinebreaks(String word) {
    return word.replaceAll(RegExp('[\n\r]+'), '');
  }

  int punctuationBefore(String word) {
    return RegExp(r'^\p{P}+([^\S\n\r]+|(?=[\p{Letter}\d]))', unicode: true)
            .firstMatch(word)
            ?.group(0)
            ?.length ??
        0;
  }

  int punctuationAfter(String word) {
    return RegExp(r'((?<=(\s|^))(\p{P}+\s*)|(?<=[\p{Letter}\d])[\p{P}\s]+)$',
                unicode: true)
            .firstMatch(word)
            ?.group(0)
            ?.length ??
        0;
  }

  Pos lastPos(List<List<String>> lines) {
    return Pos(lines.length - 1, lines[lines.length - 1].length - 1);
  }

  List<List<bool>> wordsToMarks(List<List<String>> lines) {
    final List<List<bool>> marks = [];
    for (var i = 0; i < lines.length; i++) {
      final line = <bool>[];
      for (var j = 0; j < lines[i].length; j++) {
        line.add(false);
      }
      marks.add(line);
    }
    return marks;
  }

  List<List<T>> copyItems<T>(List<List<T>> rows) {
    return rows.map((row) => [...row]).toList();
  }

  int countWordsOfText(String? text) {
      return countWordsOfRows(words(text!));
  }

  int countWordsOfRows(List<List<String>> rows) {
    return rows.fold(0, (count, row) => count + row.length);
  }

  int countMarks(List<List<bool>> rows) {
    return rows.fold(
        0,
        (count, line) =>
            count + line.fold(0, ((count, mark) => mark ? count + 1 : count)));
  }

  bool startsWith(String? word, String letters) {
    return letters.length > 0 &&
        stripPunctuation(word!).toLowerCase().startsWith(letters.toLowerCase());
  }

  String beginning(String text) {
    final firstWords = lines(text)[0].split(wordDelimiter);
    return firstWords.getRange(0, min(3, firstWords.length)).join() + ('…');
  }

  void mark(int index, List<List<bool>> marked) {
    var current = 0;
    for (var i = 0; i < marked.length; i++) {
      for (var j = 0; j < marked[i].length; j++) {
        if (marked[i][j]) continue; // not counting 'true'
        if (current == index) {
          marked[i][j] = true;
          return;
        }
        current += 1;
      }
    }
  }

  RegExp get wordDelimiter {
    final locale = di.get<CurrentAccount>().language;
    return locale.languageCode == 'zh' ? _wordDelimiterZh : _wordDelimiterEn;
  }

  RegExp get referenceDelimiter {
    final locale = di.get<CurrentAccount>().language;
    return locale.languageCode == 'zh'
        ? _referenceDelimiterZh
        : _referenceDelimiterEn;
  }
}
