import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import '../blocs/current_account/current_account.dart';

class Replicator<T extends Entity> {
  final Type entityType;
  final EntitiesBloc<T> bloc;
  final EntitiesRepositoryHive<T> hiveRepo;
  final EntitiesRepositoryHttp<T> httpRepo;

  Replicator(this.entityType, EntitiesBloc<T> bloc,
      {EntitiesRepositoryHive<T>? hiveRepo,
      EntitiesRepositoryHttp<T>? httpRepo})
      : this.bloc = bloc,
        this.hiveRepo =
            hiveRepo ?? (bloc.repository as EntitiesRepositoryRpl<T>).hiveRepo,
        this.httpRepo =
            httpRepo ?? (bloc.repository as EntitiesRepositoryRpl<T>).httpRepo;

  Future<void> sendQueued(List<AccountEntity> accounts) async {
    final entities = <T>[];
    if (entityType == AccountEntity) {
      final queue = di.get<Queues>().get(entityType);
      for (var id in queue) {
        entities.add((await hiveRepo.read(id))!);
      }
    } else {
      for (var account in accounts) {
        final queue =
            di.get<Queues>().get(entityType, accountId: account.id);
        for (var id in queue) {
          entities.add((await hiveRepo.read(id, accountId: account.id))!);
        }
      }
    }
    await _sendAndClear(entities, accounts);
  }

  Future<void> sendAll(List<AccountEntity> accounts) async {
    final entities = <T>[];
    if (entityType == AccountEntity) {
      try {
        entities.addAll(await hiveRepo.readList());
      } on NotFoundException catch(e){
        debugPrint(e.toString());
      }
    } else {
      for (var account in accounts) {
        try {
          entities.addAll(await hiveRepo.readList(accountId: account.id));
        } on NotFoundException catch(e){
          debugPrint(e.toString());
        }
      }
    }
    await _sendAndClear(entities, accounts);
  }

  Future<void> _sendAndClear(
      List<T> entities, List<AccountEntity> accounts) async {
    if (entities.isNotEmpty) {
      await httpRepo.updateList(entities, isAlreadyStamped: true);
      if(entityType == AccountEntity) {
        await di.get<Queues>().delete(entityType);
      } else for (var account in accounts) {
        await di.get<Queues>().delete(entityType, accountId: account.id);
      }
      for (var entity in entities) {
        if (entity.deleted) await hiveRepo.delete(entity);
      }
    }
  }

  Future<void> clearLocalStorage(List<AccountEntity> accounts) async {
    if (entityType == AccountEntity) {
      hiveRepo.clear();
    }
    for (var account in accounts) {
      hiveRepo.clear(account.id);
    }
  }

  /// Receives items since timestamp if entities are loaded locally (EntitiesLoadSuccess)
  /// Receives all items if loading local entities has failed (EntitiesLoadFailure)
  /// Receives items from all accounts, but ui blocs are updated with visible ones only
  /// (depending on current account)
  Future<void> receiveWhenReady(int? since, int? client) async {
    var state = await bloc.stream.startWith(bloc.state).firstWhere((state) => [
          InitialEntitiesState<T>,
          EntitiesLoadSuccess<T>,
          EntitiesLoadFailure<T>
        ].contains(state.runtimeType));
    return _receive(since, client, state);
  }

  Future<void> _receive(int? since, int? client, EntitiesState<T> state) async {
    final received =
        await httpRepo.readList(deleted: null, since: since, client: client);
    final added = <T>[];
    final updated = <T>[];
    for (var remoteItem in received) {
      final localItem = await hiveRepo.read(remoteItem.id,
          accountId: remoteItem is Accountable ? remoteItem.accountId : null);
      if (localItem == null) {
        hiveRepo.create(remoteItem);
        if (_isVisible(remoteItem)) {
          added.add(remoteItem);
        }
      } else if (localItem.modified < remoteItem.modified) {
        if (remoteItem.deleted) {
          hiveRepo.delete(localItem);
        } else {
          hiveRepo.update(remoteItem);
        }
        if (_isVisible(remoteItem)) {
          if (state is EntitiesLoadSuccess<T>) {
            updated.add(remoteItem);
          } else {
            added.add(remoteItem);
          }
        }
      }
    }
    if (updated.isNotEmpty) {
      bloc.add(EntitiesUpdated<T>(updated, persist: false));
    }
    if (added.isNotEmpty) {
      bloc.add(EntitiesAdded<T>(added, persist: false));
    }
  }

  bool _isVisible(T item) {
    final currentAccount = di<CurrentAccount>();
    return !item.deleted &&
        (item is! Accountable || item.accountId == currentAccount.id);
  }
}
