import 'dart:io';

import 'package:app_core/app_core.dart';
import 'package:collection/collection.dart';
import 'package:csv/csv.dart';
import 'package:file_picker/file_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:repository/repository.dart';
import 'package:share_plus/share_plus.dart';
import 'package:time_machine/time_machine_text_patterns.dart';

class FileService {
  static final FileService _instance = FileService._internal();

  factory FileService() {
    return _instance;
  }

  FileService._internal();

  Future<void> writeCsv(List<VerseEntity> verses) async {
    final csv = _encode(verses);
    await _write(csv);
    await _shareFile();
  }

  Future<List<VerseEntity>> readCsv(List<TagEntity> newTags) async {
    final csv = await _read();
    return csv == null ? [] : _decode(csv, newTags);
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/remember_me.csv');
  }

  String _encode(List<VerseEntity> verses) {
    final rows = [_header, ...verses.map((v) => _encodeVerse(v)).toList()];
    return const ListToCsvConverter().convert(rows);
  }

  List<VerseEntity> _decode(String text, List<TagEntity> newTags) {
    final rows = const CsvToListConverter()
        .convert<String>(text, shouldParseNumbers: false);
    final verses =
        rows.skip(1).map((row) => _decodeVerse(row, newTags)).toList();
    return verses;
  }

  List<String> get _header {
    return [
      L10n.current.t8('Verse.reference'),
      L10n.current.t8('Verse.passage'),
      L10n.current.t8('Verse.source'),
      L10n.current.t8('Verse.topic'),
      L10n.current.t8('Verse.commit'),
      L10n.current.t8('Verse.review'),
      L10n.current.t8('Verse.level'),
      L10n.current.t8('Tags.title'),
      L10n.current.t8('Verse.image'),
    ];
  }

  List<String> _encodeVerse(VerseEntity v) {
    return [
      v.reference,
      v.passage,
      v.source ?? '',
      v.topic ?? '',
      v.commit == null ? '' : LocalDatePattern.iso.format(v.commit!),
      v.review == null ? '' : LocalDatePattern.iso.format(v.review!),
      v.level.toString(),
      v.tags.values.join('; '),
      v.image ?? '',
    ];
  }

  VerseEntity _decodeVerse(List<String> row, List<TagEntity> newTags) {
    return VerseEntity(
      row[0],
      row[1],
      source: row[2].isEmpty ? null : row[2],
      topic: row[3].isEmpty ? null : row[3],
      commit: row[4].isEmpty ? null : LocalDatePattern.iso.parse(row[4]).value,
      review: row[5].isEmpty ? null : LocalDatePattern.iso.parse(row[5]).value,
      level: row[6].isEmpty ? -1 : int.parse(row[6]),
      tags: _decodeTags(row[7], newTags),
      image: row[8].isEmpty ? null : row[8],
      accountId: di.get<CurrentAccount>().id,
    );
  }

  Map<int, String?> _decodeTags(String text, List<TagEntity> newTags) {
    final state = di.get<TagsBloc>().state;
    if (state is! EntitiesLoadSuccess<TagEntity>) return {}; // RETURN
    final tags = <int, String?>{};
    text.split(RegExp(r';\s*')).forEach((item) {
      var tag = (state.entities + newTags).firstWhereOrNull(
          (entity) => entity.label == item && !entity.deleted);
      if (tag == null && item.trim().isNotEmpty) {
        tag = TagEntity(item, accountId: di.get<CurrentAccount>().id);
        newTags.add(tag);
      }
      if(tag != null) {
        tags[tag.id] = tag.label;
      }
    });
    return tags;
  }

  Future<File> _write(String text) async {
    final file = await _localFile;

    // Write the file
    return file.writeAsString(text);
  }

  Future<String?> _read() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();
    if (result?.files.single.path == null) return null; // RETURN
    File file = File(result!.files.single.path!);
    return file.readAsString();
  }

  Future<void> _shareFile() async {
    final path = await _localPath;
    await Share.shareFiles(['$path/remember_me.csv']);
  }
}
