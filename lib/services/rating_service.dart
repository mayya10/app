import 'package:app_core/app_core.dart';
import 'package:repository/repository.dart';
import 'package:store_checker/store_checker.dart';

class RatingService {
  static final RatingService _instance = RatingService._internal();
  static const _countdownStart = 40;
  Future<Source> _installationSource;

  factory RatingService() {
    return _instance;
  }

  RatingService._internal() : _installationSource = StoreChecker.getSource;

  bool isReadyForRating() {
    return _count <= 0;
  }

  bool isRatingDone() {
    return _count < 0;
  }

  Future<void> countDownToRating() async {
    if (await isRatable() && _count > 0) {
      await Settings().putInt(Settings.RATING_COUNT, _count - 1);
    }
  }

  Future<void> postponeRating() async {
    await Settings().putInt(Settings.RATING_COUNT, 20);
  }

  Future<void> markRatingDone() async {
    await Settings().putInt(Settings.RATING_COUNT, -1);
  }

  Future<bool> isRatable() async {
    return [
      Source.IS_INSTALLED_FROM_HUAWEI_APP_GALLERY,
      Source.IS_INSTALLED_FROM_AMAZON_APP_STORE,
      Source.IS_INSTALLED_FROM_APP_STORE,
      Source.IS_INSTALLED_FROM_PLAY_STORE,
    ].contains(await _installationSource);
  }

  Future<Uri> uriStore() async {
    Source source = await _installationSource;
    switch (source) {
      case Source.IS_INSTALLED_FROM_HUAWEI_APP_GALLERY:
        return Uri.parse('https://appgallery.huawei.com/app/C107457387');
      case Source.IS_INSTALLED_FROM_AMAZON_APP_STORE:
        return Uri.parse('amzn://apps/android?p=me.remem.app');
      case Source.IS_INSTALLED_FROM_APP_STORE:
        return Uri.parse('https://apps.apple.com/app/id1600128209');
      case Source.IS_INSTALLED_FROM_PLAY_STORE:
        return Uri.parse('market://details?id=me.remem.app');
      default:
        throw MessageException({
          'messages': ['No URI to $source available.']
        });
    }
  }

  int get _count {
    return Settings().getInt(Settings.RATING_COUNT) ?? _countdownStart;
  }
}
