import 'dart:convert';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:home_widget/home_widget.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import '../blocs/blocs.dart';
import '../models/models.dart';
import '../routers/routers.dart';

class HomeWidgetService {
  static final HomeWidgetService _instance = HomeWidgetService._internal();

  factory HomeWidgetService() {
    return _instance;
  }

  HomeWidgetService._internal();

  Future<bool> updateStackWidget(List<Verse> entities) {
    debugPrint(
        'HomeWidgetService.updateStackWidget(${entities.length} entities))');
    final account = di.get<CurrentAccount>();
    final allVerses = VerseService().fromEntities(entities);
    var verses = VerseService().mapVersesToBoxDue(allVerses, account.orderDue);
    var box = Box.DUE;
    if (verses.isEmpty) {
      verses =
          VerseService().mapVersesToBoxKnown(allVerses, account.orderKnown);
      box = Box.KNOWN;
    }
    if (verses.isEmpty) {
      verses = VerseService().mapVersesToBoxNew(allVerses, account.orderNew);
      box = Box.NEW;
    }
    return Future.wait<bool?>([
      HomeWidget.saveWidgetData('topicPreferred', account.topicPreferred),
      HomeWidget.saveWidgetData('box', box.name),
      HomeWidget.saveWidgetData(
          'empty', L10n.current.t8('Verse.passage.default')),
      HomeWidget.saveWidgetData(
        'verses',
        jsonEncode(verses.map((entity) => entity.toJson()).toList()),
      ),
      HomeWidget.updateWidget(
        name: 'StackWidgetProvider',
        iOSName: 'StackWidget',
      ),
    ]).then((value) {
      return !value.contains(false);
    });
  }

  Future<void> launchedFromWidget(Uri? uri) async {
    debugPrint('HomeWidgetService.launchedFromWidget()');
    debugPrint('uri: $uri');
    if (uri != null) {
      int id = uri.port;
      Box box =
          Box.values.firstWhere((box) => box.name == uri.host.toUpperCase());
      final orderBloc = OrderedVersesBloc.from(TabState(true, box));
      final orderState = await orderBloc.stream
          .startWith(orderBloc.state)
          .firstWhere((state) =>
              [OrderSuccess, OrderFailure].contains(state.runtimeType));
      if (orderState is OrderSuccess &&
          orderState.verses.any((verse) => verse.id == id)) {
        di.get<TabBloc>().add(Restarted());
        di.get<NavigationCubit>().go(HomePath(
            id: id,
            reviewMode: box == Box.KNOWN
                ? orderState.order == VerseOrder.RANDOM
                    ? ReviewMode.random
                    : ReviewMode.inverse
                : ReviewMode.review));
      }
    }
  }
}
