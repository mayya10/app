import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:repository/repository.dart';
import 'package:time_machine/time_machine_text_patterns.dart';

import '../blocs/current_account/current_account.dart';

class ScoreService {
  static final ScoreService _instance = ScoreService._internal();

  factory ScoreService() {
    return _instance;
  }

  ScoreService._internal();

  int numActive(List<Verse> verses) {
    return verses.where((verse) => verse.due == null).toList().length;
  }

  int numDue(List<Verse> verses) {
    return verses.where((verse) => verse.due != null).toList().length;
  }

  int totalScore(List<int> scores) {
    return scores.fold(0, (prev, element) => prev + element);
  }

  int todayScore(List<ScoreEntity> scores) {
    return scores
        .where((score) => score.date == DateService().today)
        .fold<int>(0, (acc, score) => acc + score.change);
  }

  List<int> scoreLevels(List<Verse> verses) {
    final result = <int?>[];
    for (var verse in verses) {
      if(verse.level < 1) continue;  // CONTINUE
      final change = TextService().countWordsOfText(verse.passage);
      if (verse.level > result.length) result.length = verse.level;
      if (result[verse.level - 1] == null) {
        result[verse.level - 1] = change;
      } else {
        result[verse.level - 1] = result[verse.level - 1]! + change;
      }
    }
    return result.map((value) => value ?? 0).toList();
  }

  Map<String, DayScore> scoreDays(List<ScoreEntity> scores) {
    final result = <String, DayScore>{};
    scores.forEach((score) {
      final key = LocalDatePattern.iso.format(score.date);
      if (result[key] == null) {
        result[key] = DayScore(score.date, score.change);
      } else {
        result[key] = result[key]!.add(score.change);
      }
    });
    return result;
  }

  Map<String, DayScore> rangeDays(Map<String, DayScore> scores, int length) {
    var date = DateService().today.subtractDays(length);
    final result = <String, DayScore>{};
    for (int i = 0; i <= length; i++) {
      final key = LocalDatePattern.iso.format(date);
      result[key] = DayScore(date, scores[key]?.change ?? 0);
      date = date.addDays(1);
    }
    return result;
  }

  List<DayScore> sortByHighest(List<DayScore> entries) {
    return entries.sorted((a, b) => -a.change.compareTo(b.change)).toList();
  }

  void showIfGoalReached(
      List<ScoreEntity> prevScores, List<ScoreEntity> nextScores) {
    if (ScoreService().todayScore(prevScores) < di.get<CurrentAccount>().dailyGoal &&
        ScoreService().todayScore(nextScores) >= di.get<CurrentAccount>().dailyGoal) {
      MessageService().show(SnackBar(
          behavior: SnackBarBehavior.floating,
          margin: EdgeInsets.all(16),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
              side: BorderSide(color: Flat.amber, width: 1)),
          content: Builder(builder: (ctx) {
            return Text(
              L10n.of(ctx).t8('Scores.goal.message'),
              style: TextStyle(fontSize: 16),
            );
          })));
    }
  }
}
