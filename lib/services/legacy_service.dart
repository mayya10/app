import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:app_core/app_core.dart';
import 'package:repository/repository.dart';
import 'package:repository/repository.dart';


class LegacyService {
  static LegacyService? _instance;

  factory LegacyService() {
    if (_instance == null) {
      _instance = LegacyService._internal();
    }
    return _instance!;
  }

  LegacyService._internal();

  Future<AccountEntity> attachAccount({
    required String? name,
    required String? password,
  }) async {
    final email = Settings().get(Settings.USER_EMAIL);
    final credentials =
        jsonEncode({'email': email, 'name': name, 'password': password});
    try {
      final response = await http.post(await legacyUri('/attach/'),
          body: credentials, headers: _headers());
      if ([200].contains(response.statusCode)) {
        return AccountEntity.fromJson(json.decode(utf8.decode(response.body.codeUnits)));
      } else {
        var messages = {
          'messages': [L10n.current.t8('Server.error.title')]
        };
        if (response.statusCode < 500 && response.statusCode != 404)
          messages = _parseMessages(json.decode(utf8.decode(response.body.codeUnits)));
        throw MessageException(messages);
      }
    } on SocketException catch (e) {
      throw MessageException({
        'messages': [L10n.current.t8('Connection.error.title')]
      });
    } on http.ClientException catch (e) {
      throw MessageException({
        'messages': [L10n.current.t8('Connection.error.title')]
      });
    }
  }

  Map<String, String> _headers() {
    final headers = <String, String>{
      HttpHeaders.acceptLanguageHeader: L10n.current.locale.toLanguageTag(),
      HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
    };
    return headers;
  }

  Map<String, List<String>> _parseMessages(Map<String, dynamic> json) {
    final result = <String, List<String>>{};
    json.keys.forEach((key) {
      Iterable list = json[key];
      result[key] = list.map((msg) => msg.toString()).toList();
    });
    return result;
  }
}
