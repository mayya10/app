import 'dart:math';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:repository/repository.dart';
import 'package:time_machine/time_machine.dart';

import '../blocs/current_account/current_account.dart';

class VerseService {
  static VerseService? _instance;
  late final L10n _refL10n;

  factory VerseService() {
    final referenceLocale =
        TextUtil.appLocale(di
            .get<CurrentAccount>()
            .langRef) ??
            L10n.current.locale;
    if (!TextUtil.localeEqual(_instance?._refL10n.locale, referenceLocale)) {
      _instance = VerseService._internal(referenceLocale);
    }
    return _instance!;
  }

  VerseService._internal(Locale referenceLocale) {
    _refL10n = L10n(referenceLocale);
    _refL10n
        .load()
        .then((value) => null, onError: (err) => debugPrint(err.toString()));
  }

  List<Verse> mapVersesToBoxAll(List<Verse> verses, VerseOrder order) {
    // move to service
    final result = verses.where((verse) {
      return true;
    }).toList();
    result.sort(compare(order));
    return result;
  }

  List<Verse> mapVersesToBoxNew(List<Verse> verses, VerseOrder order) {
    final result = verses.where((verse) {
      return verse.due == null;
    }).toList();
    result.sort(compare(order));
    return result;
  }

  List<Verse> mapVersesToBoxDue(List<Verse> verses, VerseOrder order) {
    final today = DateService().today;
    final result = verses.where((verse) {
      return verse.due != null && today >= verse.due!;
    }).toList();
    result.sort(compare(order));
    return result;
  }

  List<Verse> mapVersesToBoxKnown(List<Verse> verses, VerseOrder order) {
    final today = DateService().today;
    final result = verses.where((verse) {
      return verse.due != null && verse.due! > today;
    }).toList();
    result.sort(compare(order));
    return result;
  }

  static int Function(Verse, Verse) compare(VerseOrder order) {
    // move to service
    switch (order) {
      case VerseOrder.ALPHABET:
        return (a, b) => _or(a, b, order, VerseOrder.TOPIC);
      case VerseOrder.CANON:
        return (a, b) => _or(a, b, order, VerseOrder.ALPHABET);
      case VerseOrder.TOPIC:
        return (a, b) =>
            _or(a, b, order, VerseOrder.CANON, VerseOrder.ALPHABET);
      case VerseOrder.DATE:
        return (a, b) =>
            _or(a, b, order, VerseOrder.ID);
      case VerseOrder.LEVEL:
        return (a, b) =>
            _or(a, b, order, VerseOrder.DATE, VerseOrder.ID);
      default:
        return _compare(order);
    }
  }

  static int Function(Verse, Verse) _compare(VerseOrder order) {
    // move to service
    switch (order) {
      case VerseOrder.ALPHABET:
        return (a, b) => a.reference.compareTo(b.reference);
      case VerseOrder.CANON:
        return (a, b) => (a.rank ?? 0).compareTo(b.rank ?? 0);
      case VerseOrder.TOPIC:
        return (a, b) => (a.topic ?? '').compareTo(b.topic ?? '');
      case VerseOrder.DATE:
        final zero = LocalDate(2001, 1, 1);
        return (a, b) => (a.due ?? zero).compareTo(b.due ?? zero);
      case VerseOrder.LEVEL:
        return (a, b) => a.level.compareTo(b.level);
      case VerseOrder.RANDOM:
        return (a, b) => Random().nextInt(2) * 2 - 1;
      case VerseOrder.ID:
        return (a, b) => -a.id.compareTo(b.id);
    }
  }

  static int _or(Verse a, Verse b, VerseOrder order1, VerseOrder order2,
      [VerseOrder? order3]) {
    final result1 = _compare(order1)(a, b);
    if (result1 != 0) return result1;
    final result2 = _compare(order2)(a, b);
    if (result2 != 0 || order3 == null) return result2;
    return _compare(order3)(a, b);
  }

  List<Verse> fromEntities(List<VerseEntity> entities) {
    return entities
        .map<Verse>((VerseEntity entity) =>
        Verse.fromEntity(entity,
            due: due(
                commit: entity.commit,
                review: entity.review,
                level: entity.level),
            color: color(entity.level),
            rank: rank(entity.reference, di
                .get<CurrentAccount>()
                .books)))
        .toList();
  }

  Color color(int level) {
    // todo: get default color from account settings
    Color defaultColor = const Color(0xffcc0033);
    return level < 0 ? defaultColor : FlatDark.wheel[level % Flat.wheel.length];
  }

  LocalDate? due({LocalDate? commit, LocalDate? review, int? level}) {
    if (review == null || level == null || level < 0) {
      return commit; // RETURN
    }
    final limit = di
        .get<CurrentAccount>()
        .reviewLimit;
    final offset = _dueOffset(level);
    return review.addDays(limit == 0 || limit >= offset ? offset : limit);
  }

  int _dueOffset(int level) {
    final frequency = di
        .get<CurrentAccount>()
        .reviewFrequency;
    return level < 1 ? 0 : min(pow(frequency, level - 1).truncate(), 999);
  }

  Verse committed(Verse v) {
    final today = DateService().today;
    return v.copyWith(
        commit: today, due: due(commit: today), level: 0, color: color(0));
  }

  Verse forgotten(Verse v) {
    final today = DateService().today;
    return v.copyWith(
        review: today,
        due: due(review: today, level: 0),
        level: 0,
        color: color(0));
  }

  Verse remembered(Verse v) {
    final today = DateService().today;
    final limit = di
        .get<CurrentAccount>()
        .reviewLimit;
    final offset = _dueOffset(v.level + 1);
    if (limit == 0 || limit >= offset) {
      return v.copyWith(
          review: today,
          due: today.addDays(offset),
          level: v.level + 1,
          color: color(v.level + 1));
    } else {
      return v.copyWith(review: today, due: today.addDays(limit));
    }
  }

  Verse movedToNew(Verse v) {
    return v
        .copyWith(level: -1, nullValues: ['commit', 'review', 'due', 'color']);
  }

  Verse movedToDue(Verse v) {
    final today = DateService().today;
    if (v.review != null) {
      final level = v.level - 1;
      final review = today.subtractDays(_dueOffset(level));
      return v.copyWith(
          review: review,
          due: due(review: review, level: level),
          level: level,
          color: color(level));
    } else if (v.commit != null) {
      return v.copyWith(
        commit: today,
        due: due(commit: today),
      );
    } else {
      return v;
    }
  }

  Verse postponed(Verse v, int days) {
    if (v.review != null) {
      return v.copyWith(review: v.review!.addDays(days));
    } else if (v.commit != null) {
      return v.copyWith(commit: v.commit!.addDays(days));
    } else {
      return v;
    }
  }

  Verse addTag(Verse v, TagEntity tag) {
    final tags = Map<int, String?>.of(v.tags);
    tags[tag.id] = tag.text;
    return v.copyWith(
      tags: tags,
    );
  }

  Verse removeTag(Verse v, TagEntity tag) {
    final tags = Map<int, String?>.of(v.tags);
    tags.remove(tag.id);
    return v.copyWith(
      tags: tags,
    );
  }

  bool hasTag(Verse v, TagEntity tag) {
    return v.tags.containsKey(tag.id);
  }

  int rank(String reference, Map<String, Book> books) {
    return book(reference, books).index! * 1000000 +
        chapter(reference) * 1000 +
        first(reference);
  }

  Book book(String reference, Map<String, Book> books) {
    final book = books.entries
        .firstWhereOrNull(
            (entry) => reference.toLowerCase().startsWith(_bookPattern(entry)))
        ?.value;
    return book ?? Book(null, 0, null, null);
  }

  RegExp _bookPattern(MapEntry<String, Book> entry) {
    return RegExp(
        '(${entry.key}\\.?|${entry.value.name!.toLowerCase()})\\s*(?=\\d)');
  }

  int chapter(String reference) {
    final pattern = RegExp(r'(?<=\D+)\d+');
    // does not work in Safari (https://stackoverflow.com/questions/51568821/works-in-chrome-but-breaks-in-safari-invalid-regular-expression-invalid-group)
    final match = pattern.firstMatch(reference);
    return match == null ? 0 : int.parse(match.group(0)!);
  }

  int first(String reference) {
    final pattern = RegExp(r'(?<=\d+\s*[:.,。：、．]\s*)\d+');
    final match = pattern.firstMatch(reference);
    return match == null ? 0 : int.parse(match.group(0)!);
  }

  int last(String reference) {
    final pattern = RegExp(r'(?<=\d+\s*[-〜一—])\d+$');
    final match = pattern.firstMatch(reference);
    return match == null ? 0 : int.parse(match.group(0)!);
  }

  String spokenReference(String reference) {
    final _book = book(reference, di
        .get<CurrentAccount>()
        .books).spokenName;
    final _chapter = chapter(reference);
    final _first = first(reference);
    final _last = last(reference);
    return _book != null && _first > 0
        ? _last > 0
        ? '$_book $_chapter, ${_refL10n.t8('Verses.range', [
      _first.toString(),
      _last.toString()
    ])}'
        : '$_book $_chapter, $_first'
        : reference;
  }

  String spokenPassage(String passage, Locale locale) {
    if (locale.languageCode == 'zh') {
      return passage.replaceAll(
          RegExp(r'(?<![\u3000-\u303F]\s*)\s*\n+', unicode: true), '。\n');
    } else {
      return passage.replaceAll(RegExp(r'(?<!\p{P}\s*)\s*\n+', unicode: true), ';\n');
    }
  }

  List<String> lines(Verse verse, {referenceIncluded = true}) {
    var result = TextService().lines(verse.passage);
    if (referenceIncluded && di
        .get<CurrentAccount>()
        .referenceIncluded) {
      result[result.length - 1] += '\n\n';
      result.add(verse.reference);
    }
    return result;
  }

  List<List<String>> words(Verse verse) {
    var result = TextService().words(verse.passage);
    if (di
        .get<CurrentAccount>()
        .referenceIncluded) {
      result[result.length - 1][result[result.length - 1].length - 1] += '\n\n';
      result.add(TextService().splitReference(verse.reference));
    }
    return result;
  }

  String subject(Verse verse) {
    var subject = verse.reference;
    if (verse.topic != null && verse.topic!.isNotEmpty) {
      subject =
      '${VerseService().title(verse)} (${VerseService().subtitle(verse)})';
    }
    return subject;
  }

  String title(Verse verse) {
    return di
        .get<CurrentAccount>()
        .topicPreferred
        ? verse.topic ?? ''
        : verse.reference;
  }

  String subtitle(Verse verse) {
    return di
        .get<CurrentAccount>()
        .topicPreferred
        ? verse.reference
        : verse.topic ?? '';
  }
}
