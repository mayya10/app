import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:home_widget/home_widget.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/services/home_widget_service.dart';
import 'package:remem_me/services/rating_service.dart';

import 'routers/routers.dart';
import 'theme.dart';

class App extends StatefulWidget {
  App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> with WidgetsBindingObserver {
  StreamSubscription? _homeWidgetSubscription;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    if (!kIsWeb) {
      RatingService().countDownToRating();
      HomeWidget.setAppGroupId('me.remem.app');
      final service = di.get<HomeWidgetService>();
      _homeWidgetSubscription =
          HomeWidget.widgetClicked.listen(service.launchedFromWidget);
      HomeWidget.initiallyLaunchedFromHomeWidget()
          .then(service.launchedFromWidget);
    }
  }

  @override
  Widget build(BuildContext ctx) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      theme: AppTheme.light,
      darkTheme: AppTheme.dark,
      supportedLocales: TextUtil.appLocales,
      localizationsDelegates: const [
        L10n.delegate,
        LocaleNamesLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      onGenerateTitle: (BuildContext ctx) => L10n.of(ctx).t8('App.name'),
      scaffoldMessengerKey: scaffoldMessengerKey,
      routerDelegate: AuthRouterDelegate(),
      routeInformationParser: AppRouteInformationParser(),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    debugPrint('App.didChangeAppLifecycleState: $state');
    if (state == AppLifecycleState.resumed) {
      di.get<FilteredVersesBloc>().add(AppResumed());
      RatingService().countDownToRating();
    }
  }

  @override
  void dispose() {
    _homeWidgetSubscription?.cancel();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
}
