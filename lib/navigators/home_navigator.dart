import 'package:app_core/app_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';
import 'package:share_plus/share_plus.dart';

import '../blocs/blocs.dart';
import '../models/models.dart';
import '../routers/routers.dart';
import '../screens/screens.dart';
import 'navigators.dart';

class HomeNavigator extends BaseNavigator<HomePath> {
  late Box _box;
  late List<Verse>? _verses;
  late NavigationCubit _nav;
  late FlashcardBloc _flashcardBloc;

  HomeNavigator(HomePath path, {super.key, required void Function() onNotify})
      : super(path, onNotify: onNotify);

  @override
  Widget build(BuildContext ctx) {
    _init(ctx);
    return Navigator(
      key: BaseNavigator.navigatorKey,
      pages: [
        FadeAnimationPage(
          child: _home(),
          key: const ValueKey('HomePage'),
        ),
        if (path.id != null) ...[
          MaterialPage(
            child: _flashcardScreen(ctx),
            key: const ValueKey('FlashcardPage'),
          ),
          if (path.activity == HomeActivity.study)
            MaterialPage(
              child: _studyScreen(),
              key: const ValueKey('StudyPage'),
            ),
          if (path.activity == HomeActivity.edit)
            CupertinoPage(
              child: _verseEditScreen(),
              key: const ValueKey('VerseEditPage'),
            ),
        ],
        if (path.activity == HomeActivity.add)
          CupertinoPage(
            child: _verseAddScreen(ctx, path.data),
            key: const ValueKey('VerseAddPage'),
          )
      ],
      onPopPage: (route, result) {
        if (path.activity != null) {
          _nav.go(HomePath(id: path.id));
        } else if (path.id != null) {
          _nav.go(HomePath());
        }
        return popPage(route, result);
      },
    );
  }

  Widget _flashcardScreen(BuildContext ctx) {
    if (path.reviewMode != null && _verses != null) {
      var index = 0;
      var verses = [..._verses!];
      if (path.id != null) {
        index = verses.indexWhere((verse) => verse.id == path.id);
        if (index < 0) index = 0;
      }
      if (path.reviewMode != ReviewMode.review) {
        final first = verses.removeAt(index);
        if(path.reviewMode == ReviewMode.random) verses.shuffle();
        final inverseLimit = di.get<CurrentAccount>().inverseLimit;
        if (inverseLimit > 0 && verses.length > inverseLimit - 1) {
          verses = verses.sublist(0, inverseLimit - 1);
        }
        verses = [first, ...verses];
        index = 0;
      }
      _flashcardBloc.add(FlashcardLoaded(
          verses: verses,
          box: _box,
          index: index,
          single: false,
          inverse: path.reviewMode != ReviewMode.review));
    }
    return BlocProvider<FlashcardBloc>.value(
      value: _flashcardBloc,
      child: FlashcardScreen(
        onEdit: (verse) =>
            _nav.go(HomePath(id: verse.id, activity: HomeActivity.edit)),
        onStudy: (verse) =>
            _nav.go(HomePath(id: verse.id, activity: HomeActivity.study)),
        onShare: (verse) => Share.share(
            '${verse.passage}\n\n${VerseService().subject(verse)}\n$WEB_URL',
            subject: VerseService().subject(verse)),
      ),
    );
  }

  Widget _home() => Builder(builder: (ctx) {
        return MultiBlocProvider(
            providers: [
              BlocProvider<TabBloc>.value(value: di.get<TabBloc>()),
              BlocProvider<FilteredVersesBloc>.value(
                  value: di.get<FilteredVersesBloc>()),
              BlocProvider<FlashcardBloc>.value(value: _flashcardBloc),
              BlocProvider<SelectionBloc>(
                  create: (ctx) => SelectionBloc(
                      tabBloc: di.get<TabBloc>(),
                      versesBloc: di.get<VersesBloc>())),
            ],
            child: BlocBuilder<TabBloc, TabState>(
                buildWhen: (prev, next) =>
                    prev.isBoxed != next.isBoxed ||
                    prev.restarts != next.restarts,
                builder: (context, state) {
                  return HomeScreen(state.isBoxed,
                      key: ValueKey('${state.isBoxed}${state.restarts}'));
                }));
      });

  Builder _studyScreen() {
    return Builder(builder: (context) {
      final verse = (_flashcardBloc.state as FlashcardRun).verse;
      return BlocProvider<StudyBloc>(
          create: (ctx) => StudyBloc()..add(StudyStarted(verse)),
          child: const StudyScreen());
    });
  }

  Widget _verseAddScreen(BuildContext ctx, String? data) => EditVerseScreen(
        onSave: (context, verse) async {
          final accountsBloc = BlocProvider.of<AccountsBloc>(ctx);
          var account = di.get<CurrentAccount>().state!;
          if (verse.source != account.defaultSource) {
            if (verse.source == null) {
              account = account.copyWith(nullValues: ['defaultSource']);
            } else {
              account = account.copyWith(defaultSource: verse.source);
            }
            await RepositoryProvider.of<AccountsRepository>(ctx)
                .update(account);
            accountsBloc
                .add(EntitiesUpdated<Account>([account], persist: false));
          }
          await RepositoryProvider.of<VersesRepository>(ctx).create(verse);
          BlocProvider.of<VersesBloc>(ctx)
              .add(EntitiesAdded<Verse>([verse], persist: false));
        },
        sharedData: data,
      );

  Widget _verseEditScreen() {
    return Builder(builder: (ctx) {
      return EditVerseScreen(
          onSave: (context, verse) async {
            await RepositoryProvider.of<VersesRepository>(ctx).update(verse);
            BlocProvider.of<VersesBloc>(ctx)
                .add(EntitiesUpdated<Verse>([verse], persist: false));
            _flashcardBloc.add(FlashcardVerseUpdated(verse));
          },
          verse: (_flashcardBloc.state as FlashcardRun).verse);
    });
  }

  void _init(BuildContext ctx) {
    _nav = di.get<NavigationCubit>();
    _flashcardBloc = di.get<HomeFlashcardBloc>();
    final currentOrderBloc = OrderedVersesBloc.from(di.get<TabBloc>().state);
    if (path.id == null || currentOrderBloc.state is! OrderSuccess) {
      _initNoFlashcards();
    } else {
      OrderedVersesBloc? orderBloc = _findOrderBloc();
      if (orderBloc != null) {
        _initForFlashcards(orderBloc);
      } else {
        _initNoFlashcards();      }
    }
  }

  void _initNoFlashcards() {
    _box = di.get<TabBloc>().state.box;
    _verses = null;
  }

  void _initForFlashcards(OrderedVersesBloc orderBloc) {
    _box = orderBloc.box;
    _verses = (orderBloc.state as OrderSuccess).verses;
  }

  OrderedVersesBloc? _findOrderBloc() {
    OrderedVersesBloc? orderBloc;
    if (di.get<TabBloc>().state.isBoxed) {
      List<OrderedVersesBloc> found = [
        di.get<OrderedVersesNewBloc>(),
        di.get<OrderedVersesDueBloc>(),
        di.get<OrderedVersesKnownBloc>(),
      ].where((bloc) => (bloc.state as OrderSuccess)
          .verses
          .any((verse) => verse.id == path.id)).toList();
      if(found.isNotEmpty) {
        orderBloc = found.first;
      }
    } else {
      orderBloc = di.get<OrderedVersesAllBloc>();
    }
    return orderBloc;
  }
}
