import 'package:app_core/app_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:share_plus/share_plus.dart';

import '../blocs/blocs.dart';
import '../blocs/current_account/current_account.dart';
import '../models/collections/collections.dart';
import '../routers/routers.dart';
import '../screens/screens.dart';
import '../services/collection_service.dart';
import '../services/verse_service.dart';
import 'navigators.dart';

class CollectionsNavigator extends BaseNavigator<CollectionsPath> {
  const CollectionsNavigator(CollectionsPath path,
      {required void Function() onNotify})
      : super(path, onNotify: onNotify);

  @override
  Widget build(BuildContext ctx) {
    final nav = BlocProvider.of<NavigationCubit>(ctx);
    return _collectionsProviders(
      child: Builder(builder: (ctx) {
        BlocProvider.of<CollectionDetailBloc>(ctx).add(path.id == null
            ? CollectionDetailDismissed()
            : CollectionDetailLoaded(path.id!));
        return Navigator(
            key: BaseNavigator.navigatorKey,
            pages: [
              FadeAnimationPage(
                child: _collectionsScreen(ctx),
                key: ValueKey('CollectionsPage'),
              ),
              if (path.activity == CollectionActivity.imported)
                CupertinoPage(
                    child: _importedScreen(ctx), key: ValueKey('ImportedPage'))
              else if (path.activity == CollectionActivity.publish)
                CupertinoPage(child: _decks(ctx), key: ValueKey('DecksPage'))
              else if (path.id != null)
                MaterialPage(
                    child: BlocProvider.value(
                        value: BlocProvider.of<CollectionDetailBloc>(ctx),
                        child: CollectionDetailScreen()),
                    key: ValueKey('CollectionDetailPage-${path.id}')),
              if ([CollectionActivity.flashcard, CollectionActivity.study].contains(path.activity))
                MaterialPage(child: _flashcardScreen(path)),
              if (path.activity == CollectionActivity.study) MaterialPage(child: _studyScreen())
            ],
            onPopPage: (route, result) {
              if (path.verseId != null && path.activity == CollectionActivity.study) {
                nav.go(CollectionsPath(
                    id: path.id, verseId: path.verseId, activity: CollectionActivity.flashcard));
              } else if (path.verseId != null && path.activity == CollectionActivity.flashcard) {
                nav.go(CollectionsPath(id: path.id));
              } else if (path.id != null || path.activity != null) {
                nav.go(CollectionsPath());
              }
              return popPage(route, result);
            });
      }),
    );
  }

  Widget _collectionsProviders({required Widget child}) {
    return BlocProvider<CollectionDetailBloc>(
        create: (ctx) {
          return CollectionDetailBloc(
            service: CollectionService(),
          );
        },
        child: BlocProvider<FlashcardBloc>(
            create: (ctx) => CollectionFlashcardBloc(
                collectionBloc: BlocProvider.of<CollectionDetailBloc>(ctx)),
            child: child));
  }

  Widget _collectionsScreen(BuildContext ctx) => BlocProvider<CollectionsBloc>(
      create: (ctx) {
        return CollectionsBloc(CollectionService())
          ..add(CollectionsInitiated(Query(
              language: di.get<CurrentAccount>().state?.language != null
                  ? di.get<CurrentAccount>().state?.language.languageCode
                  : L10n.current.locale
                      .languageCode))); // todo: improve for Chinese
      },
      child: CollectionsScreen());

  Widget _decks(BuildContext ctx) => RepositoryProvider<DecksRepository>(
      create: (ctx) =>
          DecksRepositoryHttp(ctx.read<AuthBloc>().service, HttpService()),
      child: DecksScreen());

  Widget _importedScreen(BuildContext ctx) => BlocProvider<CollectionsBloc>(
      create: (ctx) {
        return ImportedBloc(CollectionService())
          ..add(CollectionsInitiated(Query(
              language: di
                  .get<CurrentAccount>()
                  .state
                  ?.language
                  .languageCode))); // todo: improve for Chinese
      },
      child: ImportedScreen());

  Widget _flashcardScreen(CollectionsPath path) {
    return Builder(builder: (ctx) {
      if (path.verseId != null)
        BlocProvider.of<FlashcardBloc>(ctx)
            .add(SingleFlashcardInitiated(path.verseId!));
      return FlashcardScreen(
        onStudy: (verse) => BlocProvider.of<NavigationCubit>(ctx).go(
            CollectionsPath(id: path.id, verseId: verse.id, activity: CollectionActivity.study)),
        onShare: (verse) => Share.share(
            '${verse.passage}\n\n${VerseService().subject(verse)}'
            '\n$WEB_URL/collections/${path.id}/${verse.id}',
            subject: VerseService().subject(verse)),
      );
    });
  }

  Widget _studyScreen() {
    return Builder(builder: (context) {
      final flashcardBloc = BlocProvider.of<FlashcardBloc>(context);
      return FutureBuilder<FlashcardState>(
          future: flashcardBloc.stream
              .startWith(flashcardBloc.state)
              .firstWhere((state) => state is SingleFlashcardRun),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              final state = snapshot.data!;
              if (state is SingleFlashcardRun) {
                return BlocProvider<StudyBloc>(
                    create: (ctx) =>
                        StudyBloc()..add(StudyStarted(state.verse)),
                    child: StudyScreen());
              }
            }
            return SizedBox.shrink();
          });
    });
  }
}
