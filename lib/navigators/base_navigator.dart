import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/current_account/current_account.dart';
import 'package:repository/repository.dart';

import '../blocs/blocs.dart';
import '../models/account.dart';
import '../routers/routers.dart';
import '../screens/screens.dart';
import 'navigators.dart';

class BaseNavigator<T extends AppRoutePath> extends StatelessWidget {
  static final GlobalKey<NavigatorState> navigatorKey =
      GlobalKey<NavigatorState>();
  final T path;
  final void Function() onNotify;

  const BaseNavigator(this.path, {Key? key, required this.onNotify})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Navigator(
      key: navigatorKey,
      pages: [
        if (path is AccountCreatePath)
          FadeAnimationPage(
            child: _accountCreate(ctx),
            key: const ValueKey('AccountCreatePage'),
          ),
        if (path is AccountBinPath)
          FadeAnimationPage(
            child: _accountBin(ctx),
            key: const ValueKey('AccountBinPage'),
          ),
        if (path is AccountEditPath)
          FadeAnimationPage(
            child: _accountEdit(ctx, di.get<CurrentAccount>().state!),
            key: const ValueKey('AccountEditPage'),
          ),
        if (path is ScoresPath)
          FadeAnimationPage(
            child: ScoresScreen(),
            key: const ValueKey('ScoresPage'),
          ),
        if (path is StylesPath)
          FadeAnimationPage(
            child: StylesScreen(),
            key: const ValueKey('StylesPage'),
          ),
        if (path is VerseBinPath)
          FadeAnimationPage(
            child: _verseBin(ctx, di.get<CurrentAccount>().state!),
            key: const ValueKey('VerseBinPage'),
          ),
      ],
      onPopPage: popPage,
    );
  }

  bool popPage(Route route, result) {
    /*else if (!(path is HomePath)) {
              _nav.go(HomePath());   // not working with Android back button
            }*/
    onNotify();
    return route.didPop(result);
  }

  Widget _accountCreate(BuildContext ctx) =>
      EditAccountScreen(onSave: (context, entity) async {
        await di
            .get<EntitiesRepository<AccountEntity>>()
            .create(entity);
        await di.get<CurrentAccount>().setEntity(entity);
        BlocProvider.of<AccountsBloc>(ctx)
            .add(AccountsAddedCurrent([entity], persist: false));
        BlocProvider.of<NavigationCubit>(ctx).go(HomePath());
      });

  Widget _accountBin(BuildContext ctx) => BlocProvider<BinBloc<AccountEntity>>(
      create: (ctx) => BinBloc<AccountEntity>(
            initialState: BinLoadInProgress<AccountEntity>(),
            repository: di.get<EntitiesRepository<AccountEntity>>(),
            entitiesBloc: BlocProvider.of<AccountsBloc>(ctx),
          )..add(BinLoaded<AccountEntity>()),
      child: const BinScreen<AccountEntity>(typeIcon: Icons.folder_shared_rounded));

  Widget _accountEdit(BuildContext ctx, Account currentAccount) {
    final nav = BlocProvider.of<NavigationCubit>(ctx);
    final repository = di.get<
        EntitiesRepository<AccountEntity>>();
    final bloc = BlocProvider.of<AccountsBloc>(ctx);
    return EditAccountScreen(
        onSave: (context, entity) async {
          await repository.update(entity);
          await di.get<CurrentAccount>().setEntity(entity);
          BlocProvider.of<AccountsBloc>(ctx)
              .add(AccountsUpdatedCurrent([entity], persist: false));
          nav.go(HomePath());
        },
        onDelete: (context, entity) async {
          final deleted = entity.copyWith(deleted: true);
          await repository.update(entity.copyWith(deleted: true));
          bloc.add(AccountsUpdatedCurrent([deleted], persist: false));
          nav.go(HomePath());
        },
        entity: currentAccount);
  }

  Widget _verseBin(BuildContext ctx, Account account) =>
      BlocProvider<BinBloc<VerseEntity>>(
          create: (ctx) => BinBloc<VerseEntity>(
                initialState: BinLoadInProgress<VerseEntity>(),
                repository: RepositoryProvider.of<VersesRepository>(ctx),
                entitiesBloc: BlocProvider.of<VersesBloc>(ctx),
              )..add(BinLoaded<VerseEntity>(account: account)),
          child: const BinScreen<VerseEntity>(typeIcon: Icons.menu_book_rounded));
}
