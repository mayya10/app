import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repository/repository.dart';

import '../blocs/blocs.dart';
import '../routers/routers.dart';
import '../screens/screens.dart';
import 'navigators.dart';

class TagsNavigator extends BaseNavigator<TagsPath> {

  const TagsNavigator(TagsPath path, {required void Function() onNotify})
      : super(path, onNotify: onNotify);

  @override
  Widget build(BuildContext ctx) {
    final nav = BlocProvider.of<NavigationCubit>(ctx);
    return Navigator(
      key: BaseNavigator.navigatorKey,
      pages: [
        FadeAnimationPage(
          child: TagsScreen(),
          key: ValueKey('TagsPage'),
        ),
        if (path.activity == 'bin')
          FadeAnimationPage(
            child: _tagBin(ctx),
            key: ValueKey('TagsBinPage'),
          ),
      ],
      onPopPage: (route, result) {
        if (path.activity != null) {
          nav.go(TagsPath());
        }
        return popPage(route, result);
      },
    );
  }

  Widget _tagBin(BuildContext ctx) => BlocProvider<BinBloc<TagEntity>>(
      create: (ctx) => BinBloc<TagEntity>(
        initialState: BinLoadInProgress<TagEntity>(),
        repository: RepositoryProvider.of<TagsRepository>(ctx),
        entitiesBloc: BlocProvider.of<TagsBloc>(ctx),
      )..add(BinLoaded<TagEntity>(account: di.get<CurrentAccount>().state!)),
      child: BinScreen<TagEntity>(typeIcon: Icons.label_rounded));

}
