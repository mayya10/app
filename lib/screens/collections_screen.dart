import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/routers/routers.dart';
import 'package:remem_me/widgets/collection/collection.dart';
import 'package:remem_me/widgets/search/search.dart';
import 'package:remem_me/widgets/widgets.dart';

import '../blocs/current_account/current_account.dart';
import '../utils/build_context.dart';

class CollectionsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<CollectionsBloc, CollectionsState>(
        builder: (BuildContext ctx, CollectionsState state) {
      return Scaffold(
        appBar: _appBar(ctx, state),
        body: CollectionsList(),
        drawer: screenSize(ctx) == ScreenSize.small
            ? NavDrawer(nav: BlocProvider.of<NavigationCubit>(ctx))
            : null,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      );
    });
  }

  PreferredSizeWidget _appBar(BuildContext ctx, CollectionsState state) {
    return StackedBar(
      base: _titleBar(ctx, state),
      overlay: Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: state.isSearchVisible ? GlobalSearch() : SizedBox.shrink()),
    );
  }

  PreferredSizeWidget _titleBar(BuildContext ctx, CollectionsState state) {
    final hasCurrentAccount = di.get<CurrentAccount>().state != null;
    return AppBar(
      title: CollectionsTitle(),
      actions: [
        if (hasCurrentAccount) IconButton(
          tooltip: L10n.of(ctx).t8('Imported.title'),
          icon: Icon(Icons.download_done_rounded),
          onPressed: () {
            BlocProvider.of<NavigationCubit>(ctx)
                .go(CollectionsPath(activity: CollectionActivity.imported));
          },
        ),
        if (hasCurrentAccount) IconButton(
          tooltip: L10n.of(ctx).t8('Deck.publish'),
          icon: Icon(Icons.publish_rounded),
          onPressed: () {
            BlocProvider.of<NavigationCubit>(ctx)
                .go(CollectionsPath(activity: CollectionActivity.publish));
          },
        ),
        IconButton(
          tooltip: L10n.of(ctx).t8('Button.search'),
          icon: Icon(Icons.search_rounded),
          onPressed: () {
            BlocProvider.of<CollectionsBloc>(ctx).add(SearchDisplayed(true));
          },
        ),
        HelpButton('collections/')
      ],
      bottom: QueryBar(),
    );
  }
}
