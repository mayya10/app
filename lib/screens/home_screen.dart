import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:audio_service/audio_service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/text_to_speech/tts_handler.dart';
import 'package:remem_me/dialogs/dialogs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/rating_service.dart';
import 'package:remem_me/widgets/home_bar/undo_button.dart';
import 'package:remem_me/widgets/search/search.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import '../utils/build_context.dart';

class HomeScreen extends StatefulWidget {
  static final GlobalKey<ScaffoldState> scaffoldKey =
      GlobalKey<ScaffoldState>();
  final bool isBoxedView;

  const HomeScreen(this.isBoxedView, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  late TabController _tabController;
  late StreamSubscription _tabBlocSubscription;

  @override
  void initState() {
    super.initState();
    debugPrint('_HomeScreenState.initState');
    _tabController = _initTabController();
    _tabBlocSubscription = _subscribeToTabBloc();
    _showDueVerses();
  }

  @override
  void dispose() {
    _tabBlocSubscription.cancel();
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext ctx) {
    final size = screenSize(ctx);
    switch (size) {
      case ScreenSize.small:
        return _scaffoldSmall(ctx);
      case ScreenSize.medium:
        return _scaffoldMedium(ctx);
      case ScreenSize.large:
        return _scaffoldLarge(ctx);
    }
  }

  TabController _initTabController() {
    // TabBloc listens to TabController
    final tabBloc = di.get<TabBloc>();
    return TabController(
        vsync: this, length: 3, initialIndex: tabBloc.state.box.index)
      ..addListener(() {
        if (!_tabController.indexIsChanging) {
          tabBloc.add(TabUpdated(Box.values[_tabController.index]));
        }
      });
  }

  StreamSubscription<int> _subscribeToTabBloc() {
    // TabController listens to TabBloc
    final tabBloc = di.get<TabBloc>();
    return tabBloc.stream
      .startWith(tabBloc.state)
      .where((state) => state.box.index != _tabController.index)
      .map((state) => state.box.index)
      .listen((i) => _tabController.index = i);
  }

  void _showDueVerses() async {
    final tabBloc = di.get<TabBloc>();
    final dueBloc = di.get<OrderedVersesDueBloc>();
    final currentAccount = di.get<CurrentAccount>();
    currentAccount.stream
        .startWith(currentAccount.state)
        .distinct((prev, next) => prev?.id == next?.id)
        .take(1)
        .listen((account) async {
      final state = await dueBloc.stream.firstWhere(
          (state) => [OrderSuccess, OrderFailure].contains(state.runtimeType));
      if (state is OrderSuccess) {
        tabBloc.add(TabUpdated(state.verses.isEmpty ? Box.NEW : Box.DUE));
      }
    });
  }

  Scaffold _scaffoldSmall(BuildContext ctx) {
    return Scaffold(
      key: HomeScreen.scaffoldKey,
      appBar: _appBar(ctx),
      body: _tabView(ctx),
      floatingActionButton: _fabRow(ctx),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      drawer: NavDrawer(nav: BlocProvider.of<NavigationCubit>(ctx)),
      endDrawer: const NavigationTags(),
    );
  }

  Widget _scaffoldMedium(BuildContext ctx) {
    return Scaffold(
      key: HomeScreen.scaffoldKey,
      appBar: _appBar(ctx),
      body: _tabView(ctx),
      floatingActionButton: _fabRow(ctx),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      endDrawer: const NavigationTags(),
    );
  }

  Widget _scaffoldLarge(BuildContext ctx) {
    return Row(children: [
      Expanded(
          flex: 3,
          child: Scaffold(
            key: HomeScreen.scaffoldKey,
            appBar: _appBar(ctx, hasEndDrawer: false),
            body: _tabView(ctx),
            floatingActionButton: _fabRow(ctx),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
          )),
      Expanded(
          flex: 1,
          child: Container(
              decoration: const BoxDecoration(
                border: const Border(
                  left: const BorderSide(width: 1.0, color: AppColor.dark),
                ),
              ),
              child: const NavigationTags(popping: false))),
    ]);
  }

  Widget _tabView(BuildContext ctx) {
    return widget.isBoxedView
        ? TabBarView(controller: _tabController, children: [
            BlocProvider<OrderedVersesBloc>.value(
              value: di.get<OrderedVersesNewBloc>(),
              child: OrderedVerses(),
            ),
            BlocProvider<OrderedVersesBloc>.value(
              value: di.get<OrderedVersesDueBloc>(),
              child: OrderedVerses(),
            ),
            BlocProvider<OrderedVersesBloc>.value(
              value: di.get<OrderedVersesKnownBloc>(),
              child: OrderedVerses(),
            )
          ])
        : BlocProvider<OrderedVersesBloc>.value(
            value: di.get<OrderedVersesAllBloc>(), child: OrderedVerses());
  }

  PreferredSizeWidget _appBar(BuildContext ctx, {bool hasEndDrawer = true}) {
    return StackedBar(
        key: const ValueKey('homeBar'),
        base: _homeBar(ctx, hasEndDrawer),
        overlay: BlocBuilder<SelectionBloc, SelectionState>(
            builder: (context, state) {
          return Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: state.isSearchActive
                  ? SearchBar(hasEndDrawer: hasEndDrawer)
                  : state.selection.isEmpty
                      ? const SizedBox.shrink()
                      : SelectionBar());
        }));
  }

  PreferredSizeWidget _homeBar(BuildContext ctx, bool hasEndDrawer) {
    return AppBar(
      title: HomeTitle(),
      actions: [
        const UndoButton(),
        SearchButton(),
        if (hasEndDrawer) FilterIconButton(),
      ],
      bottom: widget.isBoxedView
          ? BoxTabBar(_tabController)
          : const BoxAllBar() as PreferredSizeWidget,
    );
  }

  Widget _fabRow(BuildContext ctx) {
    return BlocBuilder<AccountsBloc, EntitiesState<AccountEntity>>(builder:
        (BuildContext ctx, EntitiesState<AccountEntity> accountsState) {
      return Visibility(
          visible: accountsState is EntitiesLoadSuccess<AccountEntity>,
          child: StreamBuilder<PlaybackState>(
              stream: TtsHandler().playbackState,
              builder: (ctx, snapshot) =>
                  snapshot.data?.processingState == AudioProcessingState.ready
                      ? TtsFabRow()
                      : BoxFabRow()));
    });
  }
}
