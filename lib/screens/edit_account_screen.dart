import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/widgets/loading_indicator.dart';
import 'package:repository/repository.dart';

import '../utils/utils.dart';
import '../widgets/form/form.dart';

class EditAccountScreen extends EntityForm<AccountEntity> {
  const EditAccountScreen(
      {required OnSaveCallback<AccountEntity> onSave,
      OnSaveCallback<AccountEntity>? onDelete,
      AccountEntity? entity})
      : super(
            onSave: onSave,
            onDelete: onDelete,
            entity: entity,
            poppable: false);

  @override
  _EditAccountScreenState createState() => _EditAccountScreenState();
}

class _EditAccountScreenState
    extends EntityFormState<AccountEntity, EditAccountScreen> {
  final _fontTypeKey = GlobalKey<FormFieldState>();

  String? _name;
  Locale? _language;
  Locale? _langRef;
  double? _reviewFrequency;
  int? _reviewLimit;
  int? _dailyGoal;
  int? _inverseLimit;
  bool? _referenceIncluded;
  bool? _topicPreferred;
  Canon? _canon;
  FontType? _fontType;

  Map<String, String>? _languageMap;
  Map<String, String>? _langRefMap;
  Map<double, String>? _frequencyMap;
  Map<Canon, String>? _canonMap;

  @override
  void didChangeDependencies() {
    _options();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.entity?.name ?? L10n.of(ctx).t8('Account.title')),
        leading: IconButton(
            tooltip: L10n.of(ctx).t8('Button.save'),
            icon: Icon(Icons.save_rounded),
            onPressed: saveForm),
        actions: [
          if (widget.onDelete != null)
            IconButton(
                tooltip: L10n.of(ctx).t8('Button.delete'),
                icon: Icon(Icons.delete),
                onPressed: deleteEntity),
        ],
      ),
      body: Stack(children: [
        Padding(
            padding: EdgeInsets.all(16.0),
            child: Form(
                key: formKey,
                child: SingleChildScrollView(
                    child: Column(
                  children: fields(ctx),
                )))),
        if (isUpdating) LoadingIndicator(),
      ]),
    );
  }

  @override
  AccountEntity updatedEntity() {
    return widget.entity!.copyWith(
        name: _name,
        language: _language,
        langRef: _langRef,
        reviewFrequency: _reviewFrequency,
        reviewLimit: _reviewLimit,
        dailyGoal: _dailyGoal,
        inverseLimit: _inverseLimit,
        referenceIncluded: _referenceIncluded,
        topicPreferred: _topicPreferred,
        canon: _canon,
        fontType: _fontType,
        nullValues: [
          if (_langRef == null) 'langRef',
          if (_reviewFrequency == null) 'reviewFrequency',
          if (_reviewLimit == null) 'reviewLimit',
          if (_dailyGoal == null) 'dailyGoal',
          if (_inverseLimit == null) 'inverseLimit',
          if (_language != widget.entity?.language) 'defaultSource',
        ]);
  }

  @override
  AccountEntity createdEntity() {
    return AccountEntity(
      _name!,
      language: _language!,
      langRef: _langRef!,
      reviewFrequency: _reviewFrequency!,
      reviewLimit: _reviewLimit!,
      dailyGoal: _dailyGoal!,
      inverseLimit: _inverseLimit!,
      referenceIncluded: _referenceIncluded ?? false,
      topicPreferred: _topicPreferred ?? false,
      canon: _canon!,
      fontType: _fontType ?? FontType.Sans,
      orderNew: VerseOrder.CANON,
      orderDue: VerseOrder.LEVEL,
      orderKnown: VerseOrder.DATE,
      orderAll: VerseOrder.CANON,
    );
  }

  List<Widget> fields(BuildContext ctx) {
    return super.fields(ctx)
      ..addAll([
        StringInput(
            l10nKey: 'Account.name',
            iconData: Icons.folder_shared_rounded,
            initialValue: widget.entity?.name,
            autofocus: widget.entity == null,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: Validators.compose([
              Validators.error<String?>(errors['name'], _name),
              Validators.required<String?>(ctx)
            ]),
            onSaved: (value) => _name = value),
        SizedBox(height: spacing),
        DropdownInput<String?>(
            l10nKey: 'Account.language',
            iconData: Icons.language_rounded,
            items: _languageMap!,
            initialValue:
                (widget.entity?.language ?? L10n.of(ctx).locale)
                    .toString(),
            // eg. 'en_US'
            validator: Validators.compose([
              Validators.error<String?>(
                  errors['language'], _language.toString()),
              Validators.required<String?>(ctx)
            ]),
            onSaved: (value) =>
                _language = value != null ? TextUtil.toLocale(value) : null),
        SizedBox(height: spacing),
        DropdownInput<String?>(
            l10nKey: 'Account.langRef',
            iconData: Icons.translate_rounded,
            items: _langRefMap!,
            initialValue:
                (widget.entity?.langRef ?? L10n.of(ctx).locale)
                    .toString(),
            validator: Validators.compose([
              Validators.error<String?>(errors['langRef'], _langRef.toString()),
              Validators.required<String?>(ctx)
            ]),
            onSaved: (value) =>
                _langRef = value != null ? TextUtil.toLocale(value) : null),
        SizedBox(height: spacing),
        IntegerInput(
            l10nKey: 'Account.dailyGoal.title',
            l10nKeySummary: 'Account.dailyGoal.summary',
            iconData: Icons.flag,
            initialValue: widget.entity?.dailyGoal,
            validator: Validators.error<int?>(errors['dailyGoal'], _dailyGoal),
            onSaved: (value) => _dailyGoal = value),
        SizedBox(height: spacing),
        DropdownInput<double>(
            l10nKey: 'Account.reviewBase.title',
            l10nKeySummary: 'Account.reviewBase.summary',
            iconData: Icons.date_range_rounded,
            items: _frequencyMap!,
            initialValue: widget.entity?.reviewFrequency ?? 2.0,
            validator: Validators.compose([
              Validators.error<double?>(
                  errors['reviewFrequency'], _reviewFrequency),
              Validators.required<double?>(ctx)
            ]),
            onSaved: (value) => _reviewFrequency = value),
        SizedBox(height: spacing),
        IntegerInput(
            l10nKey: 'Account.reviewLimit.title',
            l10nKeySummary: 'Account.reviewLimit.summary',
            iconData: Icons.event_rounded,
            initialValue: widget.entity?.reviewLimit,
            validator:
                Validators.error<int?>(errors['reviewLimit'], _reviewLimit),
            onSaved: (value) => _reviewLimit = value),
        SizedBox(height: spacing),
        IntegerInput(
            l10nKey: 'Account.inverseLimit.title',
            l10nKeySummary: 'Account.inverseLimit.summary',
            iconData: Icons.library_add_check_rounded,
            initialValue: widget.entity?.inverseLimit,
            validator:
            Validators.error<int?>(errors['inverseLimit'], _inverseLimit),
            onSaved: (value) => _inverseLimit = value),
        SizedBox(height: spacing),
        BoolToggle(
          l10nKey: 'Account.referenceIncluded.title',
          l10nKeySummary: 'Account.referenceIncluded.summary',
          iconData: Icons.menu_book_rounded,
          initialValue:
          _referenceIncluded ?? widget.entity?.referenceIncluded ?? false,
          onChanged: (bool value) {
            setState(() {
              _referenceIncluded = value;
            });
          },
        ),
        SizedBox(height: spacing),
        BoolToggle(
          l10nKey: 'Account.topicPreferred.title',
          l10nKeySummary: 'Account.topicPreferred.summary',
          iconData: Icons.topic,
          initialValue:
          _topicPreferred ?? widget.entity?.topicPreferred ?? false,
          onChanged: (bool value) {
            setState(() {
              _topicPreferred = value;
            });
          },
        ),
        SizedBox(height: spacing),
        FontToggle(
            fieldKey: _fontTypeKey,
            l10nKey: 'Account.fontType.title',
            iconData: Icons.font_download_rounded,
            initialValue: widget.entity?.fontType ?? _fontType ?? FontType.Sans,
            onSaved: (value) => _fontType = value),
        SizedBox(height: spacing),
        DropdownInput<Canon>(
            l10nKey: 'Account.canon.title',
            iconData: Icons.sort,
            items: _canonMap!,
            initialValue: _canonMap != null &&
                    _canonMap!.containsKey(widget.entity?.canon)
                ? widget.entity?.canon
                : Canon.lut,
            validator: Validators.compose([
              Validators.error<Canon?>(errors['canon'], _canon),
              Validators.required<Canon?>(ctx)
            ]),
            onSaved: (value) => _canon = value),
      ]);
  }

  void _options() {
    _languageMap = languageMap(context);
    _langRefMap = langRefMap(context);

    _frequencyMap = {
      1.25: L10n.of(context).t8('Frequency.veryOften'),
      1.5: L10n.of(context).t8('Frequency.often'),
      2.0: L10n.of(context).t8('Frequency.normally'),
      2.5: L10n.of(context).t8('Frequency.rarely'),
      3.0: L10n.of(context).t8('Frequency.veryRarely')
    };

    _canonMap = {
      Canon.lut: L10n.of(context).t8('Canon.luther'),
    };
  }

}
