import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/routers/routers.dart';
import 'package:remem_me/widgets/navigation/navigation.dart';

import '../utils/build_context.dart';

class BaseScreen extends StatefulWidget {
  final NavigationCubit nav;

  const BaseScreen(this.nav);

  @override
  State<StatefulWidget> createState() {
    return _BaseScreenState();
  }
}

class _BaseScreenState extends State<BaseScreen> {
  late AppRouterDelegate _routerDelegate;
  late ChildBackButtonDispatcher _backButtonDispatcher;

  /*@override
  void didUpdateWidget(covariant BaseScreen oldWidget) {
    super.didUpdateWidget(oldWidget);
    _routerDelegate.nav = widget.nav;
  }*/

  void initState() {
    super.initState();
    _routerDelegate = AppRouterDelegate(nav: widget.nav);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // Defer back button dispatching to the child router
    _backButtonDispatcher = Router.of(context)
        .backButtonDispatcher!
        .createChildBackButtonDispatcher();
  }

  @override
  Widget build(BuildContext ctx) {
    // Claim priority, If there are parallel sub router, you will need
    // to pick which one should take priority;
    _backButtonDispatcher.takePriority();

    final size = screenSize(ctx);
    switch (size) {
      case ScreenSize.small:
        return _layoutSmall(ctx);
      case ScreenSize.medium:
        return _layoutMedium(ctx, 3);
      case ScreenSize.large:
        return _layoutMedium(ctx, 4);
    }
  }

  Widget _layoutSmall(BuildContext ctx) {
    return Router(
      routerDelegate: _routerDelegate,
      routeInformationParser: AppRouteInformationParser(),
      backButtonDispatcher: _backButtonDispatcher,
    );
  }

  Widget _layoutMedium(BuildContext ctx, int flex) {
    return Row(
      children: [
        Expanded(
            flex: 1,
            child: Container(
              decoration: BoxDecoration(
                border: Border(
                  right: BorderSide(width: 1.0, color: AppColor.dark),
                ),
              ),
              child: NavDrawer(
                  nav: widget.nav,
                  poppable: false),
            )),
        Expanded(
            flex: flex,
            child: Router(
              routerDelegate: _routerDelegate,
              routeInformationParser: AppRouteInformationParser(),
              backButtonDispatcher: _backButtonDispatcher,
            ))
      ],
    );
  }
}
