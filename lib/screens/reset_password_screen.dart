import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/screens/message_screen.dart';
import 'package:remem_me/widgets/loading_indicator.dart';
import 'package:remem_me/widgets/user/user.dart';
import 'package:repository/repository.dart';

class ResetPasswordScreen extends MessageScreen {
  const ResetPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
        appBar: appBar(ctx),
        body: BlocBuilder<ResetPasswordBloc, ResetPasswordState>(
            builder: (BuildContext ctx, ResetPasswordState state) {
          if (state is ResetPasswordSuccess) {
            return messageDisplay(ctx, messages: state.messages);
          } else {
            return Stack(children: [
              form(ctx, state),
              if (state is ResetPasswordInProgress)
                Positioned.fill(child: LoadingIndicator()),
            ]);
          }
        }));
  }

  Widget form(BuildContext ctx, ResetPasswordState state) {
    return UserForm(
      errors: state is ResetPasswordFailure ? state.messages : {},
      initialValues: state is ResetPasswordFailure
          ? {'password': state.password}
          : const {},
      hasUsername: false,
      hasRepeatPassword: true,
      titleL10nKey: 'User.resetPassword.title',
      submitL10nKey: 'User.resetPassword.submit',
      onSubmit: (credentials) {
        BlocProvider.of<ResetPasswordBloc>(ctx)
            .add(PasswordSubmitted(credentials['password']!));
      },
    );
  }
}
