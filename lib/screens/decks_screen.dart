import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/dialogs/deck_publish_dialog.dart';
import 'package:remem_me/screens/tags_screen.dart';
import 'package:remem_me/widgets/button/button.dart';
import 'package:repository/repository.dart';
import 'package:share_plus/share_plus.dart';

import '../widgets/loading_indicator.dart';

/// Navigation endpoint for deck management
/// Screen for viewing and managing a list of decks
class DecksScreen extends TagsScreen {
  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: AppBar(
        title: Text(L10n.of(ctx).t8('Decks.edit')),
      ),
      body: Container(margin: const EdgeInsets.all(16.0), child: content(ctx)),
    );
  }

  @override
  Widget tagItem(BuildContext ctx, TagEntity tag) {
    return ListTile(
      leading:
          Icon(tag.published ? Icons.public_rounded : Icons.public_off_rounded),
      title: Text(tag.text),
      onTap: () =>
          tag.published ? _updateDeck(ctx, tag) : _createDeck(ctx, tag),
      trailing: tag.published
          ? IconButton(icon: const Icon(Icons.share_rounded), onPressed: () => _share(ctx, tag))
          : null,
    );
  }

  @override
  empty(BuildContext ctx) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(L10n.of(ctx).t8('Decks.empty')),
        const HelpButton('labels/')
      ],
    );
  }

  @override
  Widget content(BuildContext ctx) {
    return BlocBuilder<TaggedVersesBloc, TaggedVersesState>(
      builder: (BuildContext ctx, TaggedVersesState state) {
        if (state is TaggedVersesSuccess) {
          return Stack(children: [
            listWidget(ctx, state.tags),
            if (state.isUpdating) LoadingIndicator(),
          ]);
        }
        return LoadingIndicator();
      },
    );
  }

  Future<void> _createDeck(BuildContext ctx, TagEntity tag) async {
    final repository = RepositoryProvider.of<DecksRepository>(ctx);
    await showDialog<DeckEntity>(
        useRootNavigator: false,
        context: ctx,
        builder: (BuildContext context) => DeckPublishDialog(
              deck: DeckEntity(id: tag.id, name: tag.text),
              onSave: (context, deck) async {
                await repository.create(deck);
                BlocProvider.of<TagsBloc>(ctx).add(EntitiesUpdated<TagEntity>(
                    [tag.copyWith(published: true)],
                    persist: false));
                Navigator.of(context)
                    .pop(); // get navigator from dialog's context
              },
            ));
  }

  Future<void> _updateDeck(BuildContext ctx, TagEntity tag) async {
    final repository = RepositoryProvider.of<DecksRepository>(ctx);
    final deck = await repository.read(tag.id);
    await showDialog<DeckEntity>(
        useRootNavigator: false,
        context: ctx,
        builder: (BuildContext context) => DeckPublishDialog(
              deck: deck,
              onSave: (context, deck) async {
                await repository.update(deck);
                BlocProvider.of<TagsBloc>(ctx).add(EntitiesUpdated<TagEntity>(
                    [tag.copyWith(published: true)],
                    persist: false));
                Navigator.of(context).pop(); // navigator from dialog's context
              },
              onDelete: (context, deck) async {
                await repository.delete(deck);
                BlocProvider.of<TagsBloc>(ctx).add(EntitiesUpdated<TagEntity>(
                    [tag.copyWith(published: false)],
                    persist: false));
                Navigator.of(context).pop(); // navigator from dialog's context
              },
            ));
  }

  void _share(BuildContext ctx, TagEntity tag) {
    final subject = '${L10n.of(ctx).t8('Collection.title')}: '
        '${tag.text}';
    Share.share(
        '$subject\n$WEB_URL/collections/${tag.id}',
        subject: subject);
  }
}
