import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/routers/app_route_path.dart';
import 'package:remem_me/widgets/start/basic_info.dart';
import 'package:remem_me/widgets/user/user_form.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';

import '../utils/build_context.dart';

class StartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return BlocConsumer<AuthBloc, AuthState>(
      listener: (ctx, state) {
        if (state is LoginFailure || state is ForgotPasswordFailure) {
          login(ctx, state);
        } else if (state is RegisterFailure) {
          register(ctx, state);
        }
      },
      builder: (ctx, state) {
        return Scaffold(
            appBar: AppBar(
              actions: [
                TextButton(
                  key: const ValueKey('loginButton'),
                  style: TextButton.styleFrom(
                      primary: Colors.white, padding: EdgeInsets.all(16.0)),
                  child: Text(L10n.of(ctx).t8('Auth.login').toUpperCase()),
                  onPressed: () => login(ctx, state),
                ),
                IconButton(
                  tooltip: L10n.of(ctx).t8('Documentation.title'),
                  onPressed: launchDocumentation(ctx, path: 'accounts/'),
                  icon: Icon(Icons.help_outline_rounded),
                ),
                if (kDebugMode) ServerSetting(),
              ],
            ),
            body: Stack(children: [
              Column(children: [
                /* if (!kIsWeb && Platform.isAndroid) Padding(
                    padding: EdgeInsets.all(4.0), child: Text('BETA-VERSION')), */
                Expanded(
                    child: BasicInfo(
                        authState: state,
                        onLogin: () => BlocProvider.of<NavigationCubit>(ctx).go(CollectionsPath()),
                        onRegister: () => register(ctx, state))),
              ]),
              if(state is AuthInProgress) LoadingIndicator(),
            ]));
      },
    );
  }

  Future<void> login(BuildContext ctx, AuthState state) {
    return showDialog<void>(
        context: ctx,
        builder: (BuildContext context) {
          return Dialog(
              child: UserForm(
            errors: state is LoginFailure || state is ForgotPasswordFailure
                ? (state as AuthMessageState).messages
                : {},
            initialValues: state is AuthMessageState
                ? {
                    'email': state.credentials['email'],
                    'password': state.credentials['password']
                  }
                : const {},
            hasRepeatPassword: false,
            titleL10nKey: 'User.login.title',
            submitL10nKey: 'User.login.submit',
            onSubmit: (credentials) {
              BlocProvider.of<AuthBloc>(ctx).add(LoggedIn(
                  email: credentials['email']!,
                  password: credentials['password']!));
              Navigator.of(ctx).pop();
            },
            onForgotPassword: (credentials) {
              BlocProvider.of<AuthBloc>(ctx)
                  .add(PasswordForgotten(email: credentials['email']!));
              Navigator.of(ctx).pop();
            },
          ));
        });
  }

  Future<void> register(BuildContext ctx, AuthState state) {
    return showDialog<void>(
      context: ctx,
      builder: (BuildContext context) {
        return Dialog(
            child: UserForm(
          errors: state is RegisterFailure ? state.messages : {},
          initialValues: state is AuthMessageState
              ? {
                  'email': state.credentials['email'],
                  'password': state.credentials['password']
                }
              : const {},
          hasRepeatPassword: true,
          titleL10nKey: 'User.register.title',
          submitL10nKey: 'User.register.submit',
          onSubmit: (credentials) {
            BlocProvider.of<AuthBloc>(ctx).add(Registered(
                email: credentials['email']!,
                password: credentials['password']!));
            Navigator.of(ctx).pop();
          },
        ));
      },
    );
  }

  Future<void> resetPassword(BuildContext ctx, AuthState state) {
    return showDialog<void>(
      context: ctx,
      builder: (BuildContext context) {
        return Dialog(
            child: UserForm(
          errors: state is ForgotPasswordFailure ? state.messages : {},
          initialValues: state is AuthMessageState
              ? {
                  'email': state.credentials['email'],
                  'password': state.credentials['password']
                }
              : const {},
          hasRepeatPassword: true,
          titleL10nKey: 'User.register.title',
          submitL10nKey: 'User.register.submit',
          onSubmit: (credentials) {
            BlocProvider.of<AuthBloc>(ctx).add(Registered(
                email: credentials['email']!,
                password: credentials['password']!));
            Navigator.of(ctx).pop();
          },
        ));
      },
    );
  }
}
