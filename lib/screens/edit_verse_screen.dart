import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/dialogs/dialogs.dart';
import 'package:remem_me/models/bibles/bibles.dart';
import 'package:remem_me/models/photos/photos.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/bible_service.dart';
import 'package:remem_me/services/photos_service.dart';
import 'package:remem_me/theme.dart';
import 'package:remem_me/widgets/bible/bible.dart';
import 'package:repository/repository.dart';

import '../blocs/blocs.dart';
import '../widgets/widgets.dart';

class EditVerseScreen extends EntityForm<Verse> {
  SharedVerse? shared;

  EditVerseScreen(
      {required OnSaveCallback<Verse> onSave, Verse? verse, String? sharedData})
      : super(onSave: onSave, entity: verse, poppable: true) {
    if (sharedData != null) {
      shared = SharedVerse(sharedData);
    }
  }

  @override
  _EditVerseScreenState createState() => _EditVerseScreenState();
}

class _EditVerseScreenState extends EntityFormState<Verse, EditVerseScreen> {
  final _referenceKey = GlobalKey<FormFieldState>();
  final _sourceKey = GlobalKey<FormFieldState>();
  final _passageKey = GlobalKey<FormFieldState>();
  final _imageKey = GlobalKey<FormFieldState>();
  late final CurrentAccount _currentAccount;

  String? _reference;
  String? _passage;
  String? _topic;
  String? _source;
  String? _image;
  int? _level;

  BibleQuery? _query;
  var _includeNumbers = false;

  _EditVerseScreenState() {
    _currentAccount = di.get<CurrentAccount>();
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _includeNumbers = Settings().getBool(Settings.INCLUDE_NUMBERS) ?? false;
    });
    BibleService().init(_currentAccount.language).then((_) => _createQuery());
  }

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
        appBar: AppBar(
            title: _query == null
                ? Text(widget.entity?.reference ?? L10n.of(ctx).t8('Verse.add'))
                : null,
            leading: IconButton(
              tooltip: L10n.of(ctx).t8('Button.save'),
              icon: const Icon(Icons.save_rounded),
              onPressed: saveForm,
            ),
            actions: [
              if (_query != null) ...[
                const Icon(Icons.format_list_numbered_rounded),
                Switch(
                    value: _includeNumbers,
                    activeColor: AppTheme.appBar.colorScheme.secondary,
                    inactiveThumbColor: AppTheme.light.colorScheme.background,
                    onChanged: (value) {
                      Settings().putBool(Settings.INCLUDE_NUMBERS, value);
                      setState(() => _includeNumbers = value);
                    }),
                const SizedBox(width: 96)
              ] else const HelpButton('verses/')
            ]),
        body: Stack(children: [
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              child: Form(
                  key: formKey,
                  child: SingleChildScrollView(
                      child: Column(children: fields(ctx))),
                  onChanged: _createQuery)),
          if (isUpdating) LoadingIndicator(),
        ]),
        floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
        floatingActionButton: _query != null
            ? FloatingActionButton(
                backgroundColor: AppTheme.appBar.colorScheme.secondaryContainer,
                onPressed: () => _retrievePassage(ctx),
                child: const Icon(Icons.menu_book_rounded))
            : null);
  }

  void _retrievePassage(BuildContext ctx) async {
    final result = await showDialog<String>(
        useRootNavigator: false,
        context: ctx,
        builder: (BuildContext context) {
          return BibleWeb(_query!);
        });
    if (result != null) _passageKey.currentState!.didChange(result);
  }

  @override
  Verse updatedEntity() {
    return widget.entity!.copyWith(
        reference: _reference,
        passage: _passage!.replaceAll('\r', ''),
        topic: _topic,
        source: _source,
        image: _image,
        level: _level,
        nullValues: [
          if (_topic == null) 'topic',
          if (_source == null) 'source',
          if (_image == null) 'image',
        ]);
  }

  @override
  Verse createdEntity() {
    final tagged =
        BlocProvider.of<TaggedVersesBloc>(context).state as TaggedVersesSuccess;
    final tags = Map.fromEntries(tagged.tags
        .where((tag) => tag.included == true)
        .map((tag) => MapEntry(tag.id, tag.label)));
    return Verse(_reference!, _passage!,
        topic: _topic,
        source: _source,
        image: _image,
        level: _level ?? -1,
        accountId: _currentAccount.id,
        tags: tags);
  }

  List<Widget> fields(BuildContext ctx) {
    return super.fields(ctx)
      ..addAll([
        StringAutocomplete(
          optionsBuilder: _bookOptionBuilder,
          initialValue: widget.shared?.reference ?? widget.entity?.reference,
          fieldKey: _referenceKey,
          textDirection: _currentAccount.langRefDirection,
          autofocus: widget.entity == null,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          validator: Validators.compose([
            Validators.error<String?>(errors['reference'], _reference),
            Validators.required<String?>(ctx)
          ]),
          onSaved: (value) => _reference = value,
        ),
        SizedBox(height: spacing),
        Row(
          children: [
            SizedBox(
                width: 128,
                child: StringInput(
                    fieldKey: _sourceKey,
                    l10nKey: 'Verse.source',
                    initialValue: widget.shared?.source ??
                        widget.entity?.source ??
                        (widget.shared?.passage == null
                            ? _currentAccount.defaultSource
                            : null),
                    optionBuilder: _sourceOptionBuilder,
                    onSaved: (value) => _source = value?.trim())),
            SizedBox(width: spacing),
            Expanded(
              child: StringInput(
                  l10nKey: 'Verse.topic',
                  initialValue: widget.entity?.topic,
                  onSaved: (value) => _topic = value?.trim()),
            ),
          ],
        ),
        SizedBox(height: spacing),
        StringInput(
          fieldKey: _imageKey,
          l10nKey: 'Verse.image',
          initialValue: widget.entity?.image,
          inputFormatters: [
            FilteringTextInputFormatter.deny(RegExp(' ')),
          ],
          validator: Validators.url(ctx),
          onSaved: (value) => _image = value?.trim(),
          actionButton: IconButton(
              onPressed: () => _retrieveImage(ctx), icon: const Icon(Icons.image)),
        ),
        SizedBox(height: spacing),
        StringInput(
            fieldKey: _passageKey,
            maxLines: 20,
            l10nKey: 'Verse.passage',
            textDirection: _currentAccount.languageDirection,
            initialValue: widget.shared?.passage ?? widget.entity?.passage,
            validator: Validators.required<String?>(ctx),
            onSaved: (value) => _passage = value),
        if (widget.entity?.commit != null) SizedBox(height: spacing),
        if (widget.entity?.commit != null)
          IntegerInput(
              l10nKey: 'Verse.level',
              initialValue: widget.entity?.level,
              validator: Validators.minimum(ctx, 0),
              onSaved: (value) => _level = value),
      ]);
  }

  void _retrieveImage(BuildContext ctx) async {
    final result = await showDialog<String>(
        useRootNavigator: false,
        context: ctx,
        builder: (BuildContext context) {
          return BlocProvider<PhotosBloc>(
              create: (BuildContext ctx) {
                return PhotosBloc(PhotosService())..add(PhotosInitiated());
              },
              child: const ImageDialog());
        });
    if (result != null) _imageKey.currentState!.didChange(result);
  }

  void _createQuery() {
    if (_referenceKey.currentState!.value != null &&
        _sourceKey.currentState!.value != null) {
      setState(() {
        _query = BibleService().query(_sourceKey.currentState!.value,
            _referenceKey.currentState!.value, _currentAccount.books);
      });
    }
  }

  FutureOr<Iterable<String>> _bookOptionBuilder(TextEditingValue value) {
    return _currentAccount.books.entries
        .where((entry) =>
            RegExp(r'^' + entry.key + r'$').hasMatch(value.text.toLowerCase()))
        .map((entry) => '${entry.value.name!} ');
  }
}

OptionBuilder _sourceOptionBuilder = (BuildContext context) {
  if (BibleService().versions != null) {
    final sortedVersions = BibleService().versions!.entries.toList()
      ..sort((a, b) => a.value.name!.compareTo(b.value.name!));
    return sortedVersions
        .map((entry) => PopupMenuItem<String>(
              value: entry.key,
              child: Text(entry.value.name!),
            ))
        .toList();
  } else {
    return [];
  }
};
