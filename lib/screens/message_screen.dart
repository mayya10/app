import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/routers/routers.dart';

import '../utils/build_context.dart';

class MessageScreen extends StatelessWidget {
  final String? title;
  final Map<String, List<String>> messages;

  const MessageScreen( {Key? key, this.messages = const {}, this.title}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
        appBar: appBar(ctx),
        body: Padding(
            padding: EdgeInsets.all(16.0),
            child: messageDisplay(ctx,
                messages: this.messages, title: this.title)));
  }

  AppBar appBar(BuildContext ctx) {
    return AppBar(
      leading: IconButton(
        tooltip: L10n.of(ctx).t8('App.title'),
        onPressed: () => BlocProvider.of<NavigationCubit>(ctx).go(HomePath()),
          icon: Icon(Icons.home_rounded)
    ),
      title: Text(L10n.of(ctx).t8('App.title')),
      actions: [
        IconButton(
            tooltip: L10n.of(ctx).t8('Documentation.title'),
            onPressed: launchDocumentation(ctx),
            icon: Icon(Icons.help_outline_rounded))
      ],
    );
  }

  Widget messageDisplay(BuildContext ctx,
      {required Map<String, List<String>> messages, String? title}) {
    return Center(
        child: Column(mainAxisSize: MainAxisSize.min, children: [
      if (title != null)
        Text(
          title,
          style: Theme.of(ctx).textTheme.headlineMedium,
        ),
      if (title != null) const SizedBox(height: 16),
      Text(
        messages['messages']!.join('\n'),
        style: Theme.of(ctx).textTheme.titleMedium,
      ),
    ]));
  }
}
