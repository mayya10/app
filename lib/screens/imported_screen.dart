import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/widgets/collection/collection.dart';

class ImportedScreen extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<CollectionsBloc, CollectionsState>(
        builder: (BuildContext ctx, CollectionsState state) {
      return Scaffold(
        appBar: AppBar(title: Text(L10n.of(ctx).t8('Imported.title'))),
        body: CollectionsList(),
      );
    });
  }

}
