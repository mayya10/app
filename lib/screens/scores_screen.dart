import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/widgets/widgets.dart';

import '../utils/build_context.dart';

class ScoresScreen extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    final size = screenSize(ctx);
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(L10n.of(ctx).t8('Scores.title')),
              Text(L10n.of(ctx).t8('Scores.summary'),
                  style: Theme.of(ctx).textTheme.titleMedium!
                  //    .titleLarge!
                  .copyWith(fontWeight: FontWeight.w300, color: Theme.of(ctx).colorScheme.onPrimary),
                  ),
            ],
          ),
        ),
        body: Column(
          children: [
            TabBar(
              labelColor: Theme.of(ctx).colorScheme.primary,
              indicatorColor: Theme.of(ctx).colorScheme.primary,
              indicatorWeight: 3,
              tabs: [
                Tab(text: L10n.of(ctx).t8('Scores.newest.title')),
                Tab(text: L10n.of(ctx).t8('Scores.highest.title')),
                Tab(text: L10n.of(ctx).t8('Scores.total.title')),
              ],
            ),
            Divider(
              height:0,
              thickness: 1,
              color: Theme.of(ctx).colorScheme.primary,
            ),
            Expanded(
              child: TabBarView(
                children: [
                  NewScores(),
                  HighScores(),
                  TotalScores(),
                ],
              ),
            )
          ],
        ),
        drawer: size == ScreenSize.small
            ? NavDrawer(nav: BlocProvider.of<NavigationCubit>(ctx))
            : null,
      ),
    );
  }
}
