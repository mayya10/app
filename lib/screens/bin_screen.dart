import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/routers/app_route_path.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';

import '../utils/build_context.dart';

/// Navigation endpoint for tag management
/// Creates a TagScreen
/// Screen for viewing and managing a list of tags
class BinScreen<T extends Entity> extends StatelessWidget {
  final IconData typeIcon;

  const BinScreen({Key? key, required this.typeIcon}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    final nav = BlocProvider.of<NavigationCubit>(ctx);
    return BlocBuilder<BinBloc<T>, BinState<T>>(
        builder: (BuildContext ctx, BinState<T> state) {
      final hasSelection =
          state is BinLoadSuccess<T> && state.selection.isNotEmpty;
      final l10n = L10n.of(ctx);
      final bloc = BlocProvider.of<BinBloc<T>>(ctx);
      return Scaffold(
        appBar: AppBar(
          title: Text(l10n.t8('Bin.title')),
          actions: [
            if (hasSelection)
              TextButton(
                style: TextButton.styleFrom(
                    primary: Colors.white, padding: EdgeInsets.all(16.0)),
                child: Text(l10n.t8('Bin.restore').toUpperCase()),
                onPressed: () => bloc.add(Restored()),
              ),
            if (hasSelection)
              TextButton(
                style: TextButton.styleFrom(
                    primary: Colors.white, padding: EdgeInsets.all(16.0)),
                child: Text(l10n.t8('Bin.clear').toUpperCase()),
                // todo: clear bin
                onPressed: () => bloc.add(Shredded()),
              ),
            // if (hasSelection) IconButton(onPressed: () {}, icon: Icon(Icons.restore_from_trash_rounded)),
            // if (!hasSelection) IconButton(onPressed: () {}, icon: Icon(Icons.delete_forever_rounded)),
          ],
        ),
        body: content(ctx, state),
        drawer:
            screenSize(ctx) == ScreenSize.small && !(nav.state is TagsPath)
                ? NavDrawer(nav: nav)
                : null,
      );
    });
  }

  Widget content(BuildContext ctx, BinState<T> state) {
    if (state is BinLoadSuccess<T>) {
      return Stack(children: [
        _listWidget(ctx, state),
        if (state.isUpdating) LoadingIndicator(),
      ]);
    } else {
      return LoadingIndicator();
    }
  }

  Widget _listWidget(BuildContext ctx, BinLoadSuccess<T> state) {
    return state.entities.length > 0
        ? ListView.builder(
            itemCount: state.entities.length,
            itemBuilder: (ctx, itemPosition) {
              Entity tag = state.entities[itemPosition];
              return _entityItem(ctx, tag as T, state);
            },
          )
        : EmptyList(icon: Icons.delete_rounded);
  }

  Widget _entityItem(BuildContext ctx, T entity, BinLoadSuccess<T> state) {
    final l10n = L10n.of(ctx);
    final date = DateFormat.yMMMd(l10n.locale.toString())
        .add_Hms()
        .format(DateTime.fromMillisecondsSinceEpoch(entity.modified));
    final isSelected = state.selection.keys.contains(entity.id);
    return ListTile(
      leading: Icon(typeIcon),
      title: Text(entity.label, overflow: TextOverflow.ellipsis),
      subtitle: Text(l10n.t8('Bin.deletedOn', [date])),
      selected: isSelected,
      trailing: Checkbox(
        value: isSelected,
        onChanged: (bool? value) =>
            BlocProvider.of<BinBloc<T>>(ctx).add(WasteSelected<T>(entity)),
      ),
    );
  }
}
