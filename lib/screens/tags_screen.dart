import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/verses/verses_event.dart';
import 'package:remem_me/dialogs/dialogs.dart';
import 'package:remem_me/routers/app_route_path.dart';
import 'package:repository/repository.dart';

import '../utils/build_context.dart';
import '../widgets/widgets.dart';

/// Navigation endpoint for tag management
/// Creates a TagScreen
/// Screen for viewing and managing a list of tags
class TagsScreen extends StatelessWidget {

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
        appBar: AppBar(title: Text(L10n.of(ctx).t8('Tags.title')), actions: [
          if (kIsWeb)
            IconButton(
                tooltip: L10n.of(ctx).t8('Tags.restore'),
                icon: const Icon(Icons.restore_from_trash_rounded),
                onPressed: () => BlocProvider.of<NavigationCubit>(ctx)
                    .go(TagsPath(activity: TagActivity.bin))),
          const HelpButton('labels/')
        ]),
        body: Container(margin: const EdgeInsets.all(16.0), child: content(ctx)),
        drawer: screenSize(ctx) == ScreenSize.small
            ? NavDrawer(nav: BlocProvider.of<NavigationCubit>(ctx))
            : null,
        floatingActionButton: _btnAddTag(ctx),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat);
  }

  Widget content(BuildContext ctx) {
    return BlocBuilder<TagsBloc, EntitiesState>(
      builder: (BuildContext ctx, EntitiesState state) {
        if (state is EntitiesLoadSuccess) {
          return Stack(children: [
            listWidget(ctx, state.entities as List<TagEntity>),
            if (state.isUpdating) LoadingIndicator(),
          ]);
        }
        return LoadingIndicator();
      },
    );
  }

  Widget listWidget(BuildContext ctx, List<TagEntity> tags) {
    return tags.isNotEmpty
        ? ListView.builder(
            itemCount: tags.length,
            itemBuilder: (ctx, itemPosition) {
              TagEntity tag = tags[itemPosition];
              return tagItem(ctx, tag);
            },
          )
        : empty(ctx);
  }

  Widget empty(BuildContext ctx) {
    return EmptyList(icon: Icons.label_rounded);
  }

  Widget tagItem(BuildContext ctx, TagEntity tag) {
    return ListTile(
      leading: _btnEditTag(ctx, tag),
      title: Text(tag.text),
      trailing: _btnDeleteTag(ctx, tag),
    );
  }

  IconButton _btnDeleteTag(BuildContext ctx, TagEntity tag) {
    return IconButton(
      tooltip: L10n.of(ctx).t8('Button.delete'),
      icon: const Icon(Icons.delete_rounded),
      onPressed: () {
        BlocProvider.of<TagsBloc>(ctx)
            .add(EntitiesUpdated<TagEntity>([tag.copyWith(deleted: true)]));
        BlocProvider.of<VersesBloc>(ctx).add(TagDeleted(tag.id));
      },
    );
  }

  Widget _btnEditTag(BuildContext ctx, TagEntity tag) {
    return IconButton(
      tooltip: L10n.of(ctx).t8('Button.edit'),
      icon: const Icon(Icons.edit_rounded),
      onPressed: () async {
        await showDialog<TagEntity>(
            context: ctx,
            builder: (BuildContext context) => TagEditDialog(
                  tag: tag,
                  onSave: (context, tag) async {
                    await RepositoryProvider.of<TagsRepository>(ctx)
                        .update(tag);
                    BlocProvider.of<TagsBloc>(ctx)
                        .add(EntitiesUpdated<TagEntity>([tag], persist: false));
                    Navigator.of(context)
                        .pop(); // get root navigator from dialog's context
                  },
                ));
      },
    );
  }

  Widget _btnAddTag(BuildContext ctx) {
    return FloatingActionButton(
      onPressed: () async {
        await showDialog<TagEntity>(
            context: ctx,
            builder: (BuildContext context) => TagEditDialog(
                  onSave: (context, tag) async {
                    await RepositoryProvider.of<TagsRepository>(ctx)
                        .create(tag);
                    BlocProvider.of<TagsBloc>(ctx)
                        .add(EntitiesAdded<TagEntity>([tag], persist: false));
                    Navigator.of(context).pop();
                  },
                ));
      },
      backgroundColor: Theme.of(ctx).colorScheme.secondary,
      child: const Icon(
        Icons.add_rounded,
      ),
    );
  }
}
