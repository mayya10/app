import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/routers/app_route_path.dart';
import 'package:remem_me/widgets/collection/collection.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';
import 'package:share_plus/share_plus.dart';

import '../models/verse.dart';
import '../theme.dart';

typedef OnImportCallback = Function(List<VerseEntity> verses);

class CollectionDetailScreen extends StatelessWidget {
  const CollectionDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<CollectionDetailBloc, CollectionDetailState>(
        builder: (BuildContext ctx, CollectionDetailState state) {
      final auth = BlocProvider.of<AuthBloc>(ctx).state;
      if (state is CollectionDetailLoadSuccess) {
        return Scaffold(
            appBar: AppBar(
              title: Text(state.collection.name),
              actions: [
                IconButton(
                    disabledColor: AppTheme.appBar.disabledColor,
                    tooltip: L10n.of(ctx).t8('Verses.import') +
                        (auth is LoginSuccess
                            ? ''
                            : ' (' +
                                L10n.of(ctx).t8('User.login.required') +
                                ')'),
                    icon: Icon(Icons.download_rounded),
                    onPressed: auth is LoginSuccess
                        ? () => _import(ctx, state)
                        : null),
                IconButton(
                    tooltip: L10n.of(ctx).t8('Collection.share'),
                    icon: Icon(Icons.share_rounded),
                    onPressed: () => _share(ctx, state)),
              ],
            ),
            body: Stack(children: [
              SingleChildScrollView(
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Flexible(
                      child: Container(
                    color: Theme.of(ctx).cardColor,
                    child: Material(
                      type: MaterialType.transparency,
                      child: CollectionContent(
                        state.collection,
                        hasFixedHeight: false,
                      ),
                    ),
                  )),
                  Divider(
                    thickness: 1.0,
                    height: 0,
                  ),
                  Flexible(
                    child: VersesList(
                        verses: state.collection.verses,
                        createTile: (verse) => SimpleVerseTile(
                              verse,
                              onTap: () => _goToFlashcard(
                                  ctx, state.collection.id, verse),
                            )),
                  )
                ],
              )),
              if (state.isUpdating) LoadingIndicator(),
            ]));
      } else {
        return Scaffold(
          appBar: AppBar(),
          body: LoadingIndicator(),
        );
      }
    });
  }

  _goToFlashcard(BuildContext ctx, int collectionId, Verse verse) {
    BlocProvider.of<NavigationCubit>(ctx).go(CollectionsPath(
        id: collectionId, activity: CollectionActivity.flashcard, verseId: verse.id));
  }

  void _import(
      BuildContext ctx, CollectionDetailLoadSuccess collectionState) async {
    BlocProvider.of<CollectionDetailBloc>(ctx)
        .add(CollectionImported(collectionState.collection.id));
  }

  void _share(BuildContext ctx, CollectionDetailLoadSuccess collectionState) {
    final subject = '${L10n.of(ctx).t8('Collection.title')}: '
        '${collectionState.collection.name}';
    Share.share(
        '$subject\n$WEB_URL/collections/${collectionState.collection.id}',
        subject: subject);
  }
}
