import 'package:app_core/app_core.dart';
import 'package:audio_service/audio_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:remem_me/widgets/home_bar/undo_button.dart';
import 'package:remem_me/widgets/search/stacked_bar.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';

import '../blocs/text_to_speech/tts_handler.dart';
import '../dialogs/dialogs.dart';

// This is the type used by the popup menu.
enum FlashcardMenuAction { edit, study, share, tags }

/// Navigation endpoint for flashcards
/// Listens for the current verse (which keeps changing during a reviewing process)
/// and creates a new flashcard screen for it
class FlashcardScreen extends StatelessWidget {
  final void Function(Verse)? onEdit;
  final void Function(Verse) onStudy;
  final void Function(Verse) onShare;

  const FlashcardScreen(
      {Key? key, this.onEdit, required this.onStudy, required this.onShare})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FlashcardBloc, FlashcardState>(
        builder: (BuildContext ctx, FlashcardState state) => Scaffold(
              appBar: StackedBar(
                  base: AppBar(
                    title: Text(_titleText(ctx, state), textScaleFactor: 0.9),
                    actions: state is FlashcardRun ? _menu(ctx, state) : null,
                  ),
                  overlay: state is MultiFlashcardRun
                      ? _progressIndicator(state, ctx)
                      : const SizedBox.shrink()),
              body: FlashcardController(
                  bloc: BlocProvider.of<FlashcardBloc>(ctx)),
            ));
  }

  Positioned _progressIndicator(MultiFlashcardRun state, BuildContext ctx) {
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Row(children: [
        Expanded(
            flex: state.countForgotten,
            child: Container(
                color: Theme.of(ctx).colorScheme.secondaryContainer,
                height: 4)),
        Expanded(
            flex: state.verses.length,
            child: Container(color: Colors.transparent, height: 4)),
        Expanded(
            flex: state.countRemembered,
            child: Container(
                color: Theme.of(ctx).colorScheme.primaryContainer, height: 4)),
      ]),
    );
  }

  _titleText(BuildContext ctx, FlashcardState state) {
    if (state is FlashcardRun) {
      if (state.reverse &&
              (!di.get<CurrentAccount>().referenceIncluded ||
                  [VerseService().lines(state.verse).length, 0]
                      .contains(state.revealLines)) ||
          (state.box == null || [Box.KNOWN, Box.ALL].contains(state.box)) &&
              state is SingleFlashcardRun) {
        return state.verse.topic != null && state.verse.topic!.isNotEmpty
            ? VerseService().title(state.verse)
            : state.verse.reference;
      } else {
        return L10n.of(ctx).t8(
            state.inverse ? 'Flashcard.sayReference' : 'Flashcard.sayPassage');
      }
    } else {
      return '';
    }
  }

  /// Creates a menu for actions related to the verse
  List<Widget> _menu(BuildContext ctx, FlashcardRun run) {
    return <Widget>[
      if (run is MultiFlashcardRun && run.before != null)
        UndoButton(
          onUndo: () => BlocProvider.of<FlashcardBloc>(ctx)
              .add(FlashcardAction(VerseAction.UNDONE)),
        ),
      _actionButtons(ctx, run),
      PopupMenuButton<FlashcardMenuAction>(
        icon: const Icon(Icons.more_vert_rounded),
        onSelected: (FlashcardMenuAction action) => _do(ctx, run, action),
        itemBuilder: (BuildContext context) =>
            <PopupMenuEntry<FlashcardMenuAction>>[
          if ((run.reverse || run is SingleFlashcardRun) && run.box == Box.NEW)
            PopupMenuItem<FlashcardMenuAction>(
              value: FlashcardMenuAction.study,
              child: Text(L10n.of(ctx).t8('Study.title')),
            ),
          if (onEdit != null)
            PopupMenuItem<FlashcardMenuAction>(
              value: FlashcardMenuAction.edit,
              child: Text(L10n.of(ctx).t8('Button.edit')),
            ),
          if (onEdit != null)
            PopupMenuItem<FlashcardMenuAction>(
              value: FlashcardMenuAction.tags,
              child: Text(L10n.of(ctx).t8('Tags.title')),
            ),
          PopupMenuItem<FlashcardMenuAction>(
            value: FlashcardMenuAction.share,
            child: Text(L10n.of(ctx).t8('Verse.share.title')),
          ),
        ],
      )
    ];
  }

  bool _hasStudyButton(FlashcardRun run) =>
      (run.reverse || run is SingleFlashcardRun) && run.box != Box.NEW;

  bool _hasSpeechButton(FlashcardRun run) {
    return run.reverse ||
        run is SingleFlashcardRun &&
            (run.box == null || [Box.KNOWN, Box.ALL].contains(run.box));
  }

  /// Perfoms actions initiated from the menu
  void _do(
      BuildContext ctx, FlashcardRun run, FlashcardMenuAction action) async {
    switch (action) {
      case FlashcardMenuAction.edit:
        return onEdit!(run.verse);
      case FlashcardMenuAction.study:
        return onStudy(run.verse);
      case FlashcardMenuAction.share:
        return onShare(run.verse);
      case FlashcardMenuAction.tags:
        return _tags(ctx, run.verse);
    }
  }

  Widget _actionButtons(BuildContext ctx, FlashcardRun run) {
    var verses = run.verses;
    var index = run.index;
    if (BlocProvider.of<FlashcardBloc>(ctx) is CollectionFlashcardBloc) {
      verses = [verses[index]];
      index = 0;
    }
    return StreamBuilder<PlaybackState>(
        stream: TtsHandler().playbackState,
        builder: (ctx, snapshot) {
          final hasSpeechButton = _hasSpeechButton(run);
          final hasStudyButton = _hasStudyButton(run);
          return Row(
            children: [
              if (hasSpeechButton && (snapshot.data?.playing ?? false)) ...[
                IconButton(
                    icon: const Icon(Icons.stop_rounded),
                    onPressed: () {
                      TtsHandler().stop();
                    }),
                const SpeechSetting(),
              ],
              if (hasSpeechButton && !(snapshot.data?.playing ?? false))
                IconButton(
                    icon: const Icon(Icons.play_arrow_rounded),
                    onPressed: () {
                      TtsHandler()
                        ..setUp(verses, index: index, box: run.box)
                        ..play(willPause: true);
                    }),
              if (hasStudyButton && !(snapshot.data?.playing ?? false))
                IconButton(
                  tooltip: L10n.of(ctx).t8('Study.title'),
                  icon: const Icon(Icons.school_rounded),
                  onPressed: () => _do(ctx, run, FlashcardMenuAction.study),
                ),
              if (!hasStudyButton && !hasSpeechButton)
                const HelpButton('/study'),
            ],
          );
        });
  }

  Future _tags(BuildContext ctx, Verse verse) async {
    final flashcardBloc = BlocProvider.of<FlashcardBloc>(ctx);
    final updatedVerses = await showDialog<List<Verse>>(
        context: ctx,
        builder: (BuildContext context) {
          return TagSelectionDialog(
            tags: List.from((BlocProvider.of<TagsBloc>(ctx).state
                    as EntitiesLoadSuccess<TagEntity>)
                .entities),
            selection: {verse.id: verse},
          );
        });
    if (updatedVerses != null) {
      flashcardBloc.add(FlashcardVerseUpdated(updatedVerses.first));
      di.get<VersesBloc>().add(EntitiesUpdated(updatedVerses, persist: false));
      di.get<UndoBloc>().add(UndoAdded([
            Undo(EntityAction.update, [verse])
          ]));
    }
  }
}
