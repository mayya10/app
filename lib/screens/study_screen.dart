import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/widgets/study/study.dart';

import '../theme.dart';
import '../widgets/widgets.dart';

class StudyScreen extends StatelessWidget {
  const StudyScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StudyBloc, StudyState>(
        builder: (BuildContext ctx, StudyState state) {
      return Scaffold(
        appBar: AppBar(
            leading: ([Obfuscation, Puzzle, LineUp, Typing]
                    .contains(state.runtimeType))
                ? IconButton(
                    tooltip: L10n.of(ctx).t8('Button.back'),
                    icon: const Icon(Icons.arrow_back_rounded),
                    onPressed: () => BlocProvider.of<StudyBloc>(ctx)
                        .add(const StudyStarted()),
                  )
                : null,
            title: Text(_title(ctx, state)),
            elevation: 4.0,
            actions: const [StudySetting(), HelpButton('study/')]),
        body: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Expanded(child: TextPane(state)),
          Theme(
              data: AppTheme.dark,
              child: BottomAppBar(
                elevation: 4.0,
                child: StudyAnimator(),
              ))
        ]),
        resizeToAvoidBottomInset: true,
      );
    });
  }

  String _title(BuildContext ctx, StudyState state) {
    final CurrentAccount currentAccount = di.get<CurrentAccount>();
    if(state is Study) {
      if(currentAccount.topicPreferred && state.verse.topic != null &&
          state.verse.topic!.isNotEmpty) {
        return state.verse.topic!;
      }
      if(!currentAccount.referenceIncluded) {
        return state.verse.reference;
      }
    }
    return L10n.of(ctx).t8('Study.title');
  }
}
