import 'package:app_core/app_core.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/routers/app_route_path.dart';
import 'package:remem_me/services/home_widget_service.dart';
import 'package:remem_me/services/replication_service.dart';
import 'package:remem_me/services/replicator.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class MockSettings extends Mock implements Settings {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockOrderedVersesDueBloc extends Mock implements OrderedVersesDueBloc {}
class MockOrderedVersesAllBloc extends Mock implements OrderedVersesAllBloc {}

class MockOrderedVersesKnownBloc extends Mock
    implements OrderedVersesKnownBloc {}

class MockTabBloc extends Mock implements TabBloc {}

class MockNavigationCubit extends Mock implements NavigationCubit {}

void main() {
  group('HomeWidgetService', () {
    late HomeWidgetService service;
    late MockTabBloc tabBloc;
    late MockNavigationCubit navigationCubit;
    late List<Verse> testVerses;

    setUpAll(() {
      registerFallbackValue(HomePath());
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      di.registerSingleton<Settings>(settings);
      final currentAccount = MockCurrentAccount();
      di.registerSingleton<CurrentAccount>(currentAccount);
      testVerses = [
        Verse(
          'ref3',
          'pas3',
          id: 3,
          modified: 0,
          accountId: 1,
        ),
        Verse(
          'ref1',
          'pas1',
          id: 1,
          modified: 0,
          accountId: 1,
        ),
        Verse(
          'ref2',
          'pas2',
          id: 2,
          modified: 0,
          accountId: 1,
        ),
      ];
      final dueBloc = MockOrderedVersesDueBloc();
      when(() => dueBloc.stream).thenAnswer((_) => NeverStream());
      when(() => dueBloc.state).thenReturn(
          OrderSuccess(VerseOrder.ALPHABET, testVerses.sublist(0, 2)));
      di.registerSingleton<OrderedVersesDueBloc>(dueBloc);
      final allBloc = MockOrderedVersesAllBloc();
      when(() => allBloc.stream).thenAnswer((_) => NeverStream());
      when(() => allBloc.state).thenReturn(
          OrderSuccess(VerseOrder.ALPHABET, testVerses));
      di.registerSingleton<OrderedVersesAllBloc>(allBloc);
      final knownBloc = MockOrderedVersesKnownBloc();
      when(() => knownBloc.stream).thenAnswer((_) => NeverStream());
      when(() => knownBloc.state).thenReturn(
          OrderSuccess(VerseOrder.ALPHABET, testVerses.sublist(2, 3)));
      di.registerSingleton<OrderedVersesKnownBloc>(knownBloc);
      tabBloc = MockTabBloc();
      di.registerSingleton<TabBloc>(tabBloc);
      navigationCubit = MockNavigationCubit();
      di.registerSingleton<NavigationCubit>(navigationCubit);
    });

    setUp(() {
      reset(tabBloc);
      reset(navigationCubit);
      service = HomeWidgetService();
    });

    test('Existing verse should be opened', () async {
      await service.launchedFromWidget(Uri(host: 'due', port: 3));
      verifyNever(() => tabBloc.add(TabUpdated(Box.DUE)));
      verify(() => navigationCubit
          .go(HomePath(id: 3, reviewMode: ReviewMode.review))).called(1);
    });

/*    test('Missing verse should not cause navigation', () async {
      await service.launchedFromWidget(Uri(host: 'due', port: 2));
      verifyNever(() => navigationCubit.go(any()));
    });*/
  });
}
