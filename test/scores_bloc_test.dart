import 'package:app_core/app_core.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:test/test.dart';
import 'package:time_machine/time_machine.dart';
import 'package:time_machine/time_machine_text_patterns.dart';

class MockSettings extends Mock implements Settings {}

class MockAuthBloc extends Mock implements AuthBloc {}

class MockReplicationBloc extends Mock implements ReplicationBloc {}

class MockConnectivityCubit extends Mock implements ConnectivityCubit {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockScoresRepository extends Mock implements ScoresRepository {}

void main() {
  group('ScoresBloc', () {
    late ScoresBloc scoresBloc;
    late MockScoresRepository repository;
    late List<ScoreEntity> testScores;
    late LocalDate testDate;
    late String testKey;

    setUpAll(() {
      final authBloc = MockAuthBloc();
      when(() => authBloc.stream).thenAnswer((_) => NeverStream());
      di.registerSingleton<AuthBloc>(authBloc);
      di.registerSingleton<CurrentAccount>(CurrentAccount());
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      di.registerSingleton<Settings>(settings);
      testKey = '2014-12-02';
      testDate = LocalDatePattern.iso.parse(testKey).value;
      final baseScore = ScoreEntity(
          vkey: 'v',
          date: testDate,
          accountId: 1,
          change: 1,
          modified: 0,
          modifiedBy: 0);
      testScores = [
        baseScore.copyWith(vkey: 'v3', date: testDate.addDays(1), id: 3),
        baseScore.copyWith(vkey: 'v1', id: 1, change: 2),
        baseScore.copyWith(vkey: 'v2', id: 2),
        baseScore.copyWith(vkey: 'v2', id: 2),
      ];
      repository = MockScoresRepository();
      when(() => repository.readList()).thenAnswer((_) => Future.value(testScores));
      di.registerSingleton<EntitiesRepository<ScoreEntity>>(repository);
    });

    setUp(() {
      scoresBloc = ScoresBloc();
    });

    blocTest<ScoresBloc, EntitiesState<ScoreEntity>>(
      'emits [EntitiesLoadSuccess()] when score is added',
      build: () => scoresBloc,
      act: (bloc) => bloc.add(EntitiesLoaded<ScoreEntity>()),
      expect: () => [
        EntitiesLoadInProgress<ScoreEntity>(),
        EntitiesLoadSuccess<ScoreEntity>(
          [testScores[1], testScores[2], testScores[0]],
        ),
      ],
    );
    
  });
}
