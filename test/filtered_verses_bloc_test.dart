import 'package:app_core/app_core.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/home_widget_service.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:test/test.dart';
import 'package:bloc_test/bloc_test.dart';

class MockSettings extends Mock implements Settings {}

class MockHomeWidgetService extends Mock implements HomeWidgetService {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockTaggedVersesBloc extends Mock implements TaggedVersesBloc {}

class MockTagsBloc extends Mock implements TagsBloc {}

void main() {
  group('FilteredVersesBloc', () {
    late FilteredVersesBloc filteredVersesBloc;
    late List<Verse> testVerses;
    late List<Tag> testTags;

    setUpAll(() {
      // final currentAccount = MockCurrentAccount();
      final homeWidgetService = MockHomeWidgetService();
      when(() => homeWidgetService.updateStackWidget(any()))
          .thenAnswer((_) => Future.value(false));
      di.registerSingleton<HomeWidgetService>(homeWidgetService);
      di.registerSingleton<CurrentAccount>(CurrentAccount());
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      di.registerSingleton<Settings>(settings);

      testVerses = [
        Verse(
          'ref3',
          'pas3',
          id: 3,
          modified: 0,
          accountId: 1,
        ),
        Verse(
          'ref1',
          'pas1',
          id: 1,
          modified: 0,
          accountId: 1,
          tags: {1: 'tag1'},
        ),
        Verse(
          'ref2',
          'pas2',
          id: 2,
          modified: 0,
          accountId: 1,
          tags: {2: 'tag2'},
        ),
      ];

      testTags = [
        Tag(
          'tag1',
          id: 1,
          modified: 0,
          accountId: 1,
          included: false,
          published: false,
          size: 1,
        ),
        Tag(
          'tag2',
          id: 2,
          modified: 0,
          accountId: 1,
          included: true,
          published: false,
          size: 1,
        ),
      ];

      final versesBloc = MockTaggedVersesBloc();
      when(() => versesBloc.stream).thenAnswer((_) => NeverStream());
      when(() => versesBloc.state).thenReturn(TaggedVersesSuccess(
        verses: testVerses,
        tags: testTags,
      ));
      di.registerSingleton<TaggedVersesBloc>(versesBloc);
    });

    setUp(() {
      filteredVersesBloc = FilteredVersesBloc();
    });

/*    test('initial state is InitialTaggedVersesState', () {
      expectAsync0(filteredVersesBloc.state, InitialTaggedVersesState());
    });*/

    blocTest<FilteredVersesBloc, FilteredVersesState>(
      'emits [TaggedVersesInProgress()] when TaggedVersesReloaded is added',
      build: () => filteredVersesBloc,
      act: (bloc) => bloc.add(FilteredVersesReloaded()),
      expect: () => [
        FilteredVersesSuccess(
          [
            Verse(
              'ref2',
              'pas2',
              id: 2,
              modified: 0,
              accountId: 1,
              tags: {2: 'tag2'},
            ),
          ],
          date: DateService().today,
          activeFilter: VisibilityFilter(),
        ),
        FilterInProgress()
      ],
    );

    blocTest<FilteredVersesBloc, FilteredVersesState>(
      'does not filter by tag if search query is active',
      build: () => filteredVersesBloc,
      act: (bloc) => bloc.add(SearchFilterUpdated('pas3')),
      expect: () => [
        FilteredVersesSuccess(
          [
            Verse(
              'ref2',
              'pas2',
              id: 2,
              modified: 0,
              accountId: 1,
              tags: {2: 'tag2'},
            ),
          ],
          date: DateService().today,
          activeFilter: VisibilityFilter(),
        ),
        FilteredVersesSuccess(
          [
            Verse(
              'ref3',
              'pas3',
              id: 3,
              modified: 0,
              accountId: 1,
            ),
          ],
          date: DateService().today,
          activeFilter: VisibilityFilter(query: 'pas3'),
        ),
      ],
    );
  });
}
