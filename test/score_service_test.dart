import 'package:app_core/app_core.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/score_service.dart';
import 'package:repository/repository.dart';
import 'package:time_machine/time_machine.dart';
import 'package:time_machine/time_machine_text_patterns.dart';

class MockSettings extends Mock implements Settings {}

void main() {
  group('ScoreService', () {
    late List<ScoreEntity> testScores;
    late LocalDate testDate;
    late String testKey;

    setUpAll(() {
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      di.registerSingleton<Settings>(settings);
      testKey = '2014-12-02';
      testDate = LocalDatePattern.iso.parse(testKey).value;
      testScores = [
        ScoreEntity(
            vkey: 'v1',
            date: testDate,
            accountId: 1,
            change: 1,
            id: 1,
            modified: 0,
            modifiedBy: 0),
        ScoreEntity(
            vkey: 'v2',
            date: testDate,
            accountId: 1,
            change: 2,
            id: 2,
            modified: 0,
            modifiedBy: 0),
      ];
      //{  vkey: Psalm 16,8-11LUT,  date: Tuesday, 02 December 2014, change: 69, account: 998, id: 78503425843733, modified: 1639904742723, modifiedBy: 380628903367170, deleted: false }]
    });

    test('scoreDays() should add scores', () async {
      Map<String, DayScore> result = ScoreService().scoreDays(testScores);
      expect(result[testKey], DayScore(testDate, 3));
    });

  });
}
