import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/tts_service.dart';
import 'package:repository/repository.dart';
import 'package:test/test.dart';

class MockSettings extends Mock implements Settings {}

class MockTtsService extends Mock implements TtsService {}

void main() {
  group('AccountsBloc', () {
    late StudyBloc studyBloc;
    late Verse testVerse;
    late List<List<String>> testLines;

    setUpAll(() {
      registerFallbackValue(Locale('en'));
      di.registerSingleton<CurrentAccount>(CurrentAccount());
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      di.registerSingleton<Settings>(settings);
      final ttsService = MockTtsService();
      when(() => ttsService.speakText(any(), any(), any())).thenAnswer((_) async {});
      when(() => ttsService.stop()).thenAnswer((_) async {});
      di.registerSingleton<TtsService>(ttsService);
      testVerse =
          Verse('aref', 'aword bword cword', id: 1, modified: 0, accountId: 1);
      testLines = [
        ['aword ', 'bword ', 'cword'],
      ];
    });

    setUp(() {
      studyBloc = StudyBloc();
    });

    blocTest<StudyBloc, StudyState>(
      'emits Typing state after Typed event',
      build: () => studyBloc,
      act: (bloc) {
        bloc.add(StudyStarted(testVerse));
        bloc.add(Typed());
        bloc.add(Typed(letters: 'a'));
        bloc.add(Typed(letters: 'x'));
        bloc.add(Typed(letters: 'b'));
        // bloc.add(Typed(letters: 'b'));
      },
      expect: () => [
        Study(testVerse, testLines),
        Typing(
          testVerse,
          testLines,
          pos: Pos(0, 2),
          placeholders: Pos(0, 2),
          nextWord: 'aword ',
        ),
        Typing(
          testVerse,
          testLines,
          pos: Pos(0, 2),
          placeholders: Pos(0, 2),
          revealed: Pos(0, 0),
          letters: 'a',
          isCorrect: true,
          nextWord: 'bword ',
        ),
        Typing(
          testVerse,
          testLines,
          pos: Pos(0, 2),
          placeholders: Pos(0, 2),
          revealed: Pos(0, 0),
          letters: 'x',
          isCorrect: false,
          nextWord: 'bword ',
        ),
        Typing(
          testVerse,
          testLines,
          pos: Pos(0, 2),
          placeholders: Pos(0, 2),
          revealed: Pos(0, 1),
          letters: 'b',
          isCorrect: true,
          nextWord: 'cword',
        ),
      ],
    );
  });
}
