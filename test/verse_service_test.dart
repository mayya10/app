import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/current_account/current_account.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';
import 'package:time_machine/time_machine.dart';

class MockSettings extends Mock implements Settings {}

void main() {
  group('VerseService', () {
    // todo: wrap plugin flutter_native_timezone
    // https://docs.flutter.dev/development/packages-and-plugins/plugins-in-tests

    setUpAll(() {
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      di.registerSingleton<Settings>(settings);
      di.registerSingleton<CurrentAccount>(CurrentAccount());
    });

    test('Verse should be committed', () async {
      Verse data = Verse(
        'reference',
        'passage',
        commit: null,
        review: null,
        level: -1,
        accountId: 0,
      );
      di.get<CurrentAccount>().setAccount(Account('Test'));
      Verse result = VerseService().committed(data);
      expect(result.commit, DateService().today);
      expect(result.review, null);
      expect(result.due, DateService().today);
      expect(result.level, 0);
    });

    test('Verse should be forgotten', () {
      Verse data = Verse(
        'reference',
        'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 3,
        accountId: 0,
      );
      di.get<CurrentAccount>().setAccount(Account('Test'));
      Verse result = VerseService().forgotten(data);
      expect(result.commit, LocalDate(2021, 1, 1));
      expect(result.review, DateService().today);
      expect(result.due, DateService().today);
      expect(result.level, 0);
    });

    test('Verse should be remembered', () {
      Verse data = Verse(
        'reference',
        'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 3,
        accountId: 0,
      );
      di.get<CurrentAccount>().setAccount(Account('Test'));
      Verse result = VerseService().remembered(data);
      expect(result.commit, LocalDate(2021, 1, 1));
      expect(result.review, DateService().today);
      expect(result.due, DateService().today.addDays(8));
      expect(result.level, 4);
    });

    test('Verse should be moved to due', () {
      Verse data = Verse(
        'reference',
        'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 3,
        accountId: 0,
      );
      di.get<CurrentAccount>().setAccount(Account('Test'));
      Verse result = VerseService().movedToDue(data);
      expect(result.commit, LocalDate(2021, 1, 1));
      expect(result.review, DateService().today.subtractDays(2));
      expect(result.due, DateService().today);
      expect(result.level, 2);
    });

    test('Verse should be moved to new', () {
      Verse data = Verse(
        'reference',
        'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 3,
        accountId: 0,
      );
      di.get<CurrentAccount>().setAccount(Account('Test'));
      Verse result = VerseService().movedToNew(data);
      expect(result.commit, null);
      expect(result.review, null);
      expect(result.due, null);
      expect(result.level, -1);
    });

    test('Max due should correspond to limit', () {
      di.get<CurrentAccount>().setAccount(Account('Test', reviewLimit: 10));
      final due = VerseService().due(level: 7, review: LocalDate(2021, 1, 1));
      expect(due, LocalDate(2021, 1, 11));
    });

    test('Spoken reference', () {
      di.get<CurrentAccount>().setAccount(Account('Test'));
      expect(VerseService().spokenPassage('1\ntest,\ntest: \ntest \nend', const Locale('en')),
          '1;\ntest,\ntest: \ntest;\nend');
    });
  });
}
