import 'package:app_core/app_core.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:test/test.dart';
import 'package:bloc_test/bloc_test.dart';

class MockSettings extends Mock implements Settings {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockVersesBloc extends Mock implements VersesBloc {}

class MockTagsBloc extends Mock implements TagsBloc {}

void main() {
  group('TaggedVersesBloc', () {
    late List<VerseEntity> testVerses;
    late List<TagEntity> testTags;
    late TagsBloc tagsBloc;
    late VersesBloc versesBloc;

    setUpAll(() {
      // final currentAccount = MockCurrentAccount();
      di.registerSingleton<CurrentAccount>(CurrentAccount());
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      di.registerSingleton<Settings>(settings);

      testVerses = [
        VerseEntity('ref3', 'pas3', id: 3, modified: 0, accountId: 1),
        VerseEntity('ref1', 'pas1',
            id: 1, modified: 0, accountId: 1, tags: {1: null}),
        VerseEntity('ref2', 'pas2',
            id: 2, modified: 0, accountId: 1, tags: {2: null}),
      ];

      testTags = [
        TagEntity('tag1', id: 1, modified: 0, accountId: 1, included: false),
        TagEntity('tag2', id: 2, modified: 0, accountId: 1, included: true),
      ];
    });

    setUp(() {
      if(di.isRegistered<VersesBloc>()) di.unregister<VersesBloc>();
      versesBloc = MockVersesBloc();
      when(() => versesBloc.stream).thenAnswer((_) => NeverStream());
      when(() => versesBloc.state).thenReturn(EntitiesLoadSuccess(testVerses));
      di.registerSingleton<VersesBloc>(versesBloc);
      if(di.isRegistered<TagsBloc>()) di.unregister<TagsBloc>();
      tagsBloc = MockTagsBloc();
      when(() => tagsBloc.stream).thenAnswer((_) => NeverStream());
      when(() => tagsBloc.state).thenReturn(EntitiesLoadSuccess(testTags));
      di.registerSingleton<TagsBloc>(tagsBloc);
    });

    blocTest<TaggedVersesBloc, TaggedVersesState>(
      'emits [TaggedVersesSuccess()]',
      setUp: () => when(() => tagsBloc.state).thenReturn(EntitiesLoadSuccess(testTags)),
      build: () => TaggedVersesBloc(),
      act: (bloc) {},
      expect: () => [
        TaggedVersesSuccess(
          verses: [
            Verse('ref3', 'pas3',
                id: 3, modified: 0, accountId: 1),
            Verse('ref1', 'pas1',
                id: 1, modified: 0, accountId: 1, tags: {1: 'tag1'}),
            Verse('ref2', 'pas2',
                id: 2, modified: 0, accountId: 1, tags: {2: 'tag2'}),
          ],
          tags: testTags.map((t) => Tag.fromEntity(t)).toList(),
        ),
      ],
    );

    blocTest<TaggedVersesBloc, TaggedVersesState>(
      'emits [TaggedVersesFailure()] if versesBloc fails',
      setUp: () => when(() => versesBloc.state).thenReturn(EntitiesLoadFailure("error")),
      build: () => TaggedVersesBloc(),
      act: (bloc) {},
      expect: () => [
        TaggedVersesFailure(),
      ],
    );

    blocTest<TaggedVersesBloc, TaggedVersesState>(
      'emits [TaggedVersesSuccess()] if tagsBloc fails',
      setUp: () => when(() => tagsBloc.state).thenReturn(EntitiesLoadFailure("error")),
      build: () => TaggedVersesBloc(),
      act: (bloc) {},
      expect: () => [
        TaggedVersesSuccess(
          verses: [
            Verse('ref3', 'pas3',
                id: 3, modified: 0, accountId: 1),
            Verse('ref1', 'pas1',
                id: 1, modified: 0, accountId: 1, tags: {1: null}),
            Verse('ref2', 'pas2',
                id: 2, modified: 0, accountId: 1, tags: {2: null}),
          ],
          tags: [],
        ),
      ],
    );
  });
}
