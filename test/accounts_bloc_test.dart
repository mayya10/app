import 'package:app_core/app_core.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:test/test.dart';
import 'package:bloc_test/bloc_test.dart';

class MockSettings extends Mock implements Settings {}

class MockAuthBloc extends Mock implements AuthBloc {}

class MockReplicationBloc extends Mock implements ReplicationBloc {}

class MockConnectivityCubit extends Mock implements ConnectivityCubit {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockAccountsRepository extends Mock implements AccountsRepository {}

void main() {
  group('AccountsBloc', () {
    late AccountsBloc accountsBloc;
    late List<AccountEntity> testAccounts;

    setUpAll(() {
      final authBloc = MockAuthBloc();
      when(() => authBloc.stream).thenAnswer((_) => NeverStream());
      di.registerSingleton<AuthBloc>(authBloc);

      final replicationBloc = MockReplicationBloc();
      when(() => authBloc.stream).thenAnswer((_) => NeverStream());
      di.registerSingleton<ReplicationBloc>(replicationBloc);

      final connectivityCubit = MockConnectivityCubit();
      when(() => connectivityCubit.stream).thenAnswer((_) => NeverStream());
      di.registerSingleton<ConnectivityCubit>(connectivityCubit);

      // final currentAccount = MockCurrentAccount();
      di.registerSingleton<CurrentAccount>(CurrentAccount());
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      di.registerSingleton<Settings>(settings);
      di.registerSingleton<EntitiesRepository<AccountEntity>>(
          MockAccountsRepository());

      testAccounts = [
        AccountEntity('acc1', id: 1, modified: 0),
      ];
    });

    setUp(() {
      accountsBloc = AccountsBloc(EntitiesLoadSuccess(testAccounts));
    });

    blocTest<AccountsBloc, EntitiesState<AccountEntity>>(
      'emits [EntitiesLoadSuccess()] when account is added',
      build: () => accountsBloc,
      act: (bloc) => bloc.add(EntitiesAdded(
          [AccountEntity('acc2', id: 2, modified: 0)],
          persist: false)),
      expect: () => [
        EntitiesLoadSuccess(
          [
            AccountEntity('acc1', id: 1, modified: 0),
            AccountEntity('acc2', id: 2, modified: 0)
          ],
        ),
      ],
    );

    blocTest<AccountsBloc, EntitiesState<AccountEntity>>(
      'ignores when identical account is added',
      build: () => accountsBloc,
      act: (bloc) => bloc.add(EntitiesAdded(
          [AccountEntity('acc1', id: 1, modified: 0)],
          persist: false)),
      expect: () => [
        EntitiesLoadSuccess(
          [
            AccountEntity('acc1', id: 1, modified: 0),
          ],
        ),
      ],
    );
  });
}
