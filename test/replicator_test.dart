import 'package:app_core/app_core.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/services/replicator.dart';
import 'package:repository/repository.dart';

class MockSettings extends Mock implements Settings {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockBloc extends Mock implements EntitiesBloc<VerseEntity> {
  @override
  Stream<EntitiesState<VerseEntity>> get stream => Stream.empty();
}

class MockHive extends Mock implements EntitiesRepositoryHive<VerseEntity> {}

class MockHttp extends Mock implements EntitiesRepositoryHttp<VerseEntity> {}

class FakeVerseEntity extends Fake implements VerseEntity {}

void main() {
  group('receiveWhenReady', () {
    final settings = MockSettings();
    when(() => settings.deviceId).thenReturn(1);
    di.registerSingleton<Settings>(settings);
    final currentAccount = MockCurrentAccount();
    di.registerSingleton<CurrentAccount>(currentAccount);
    final bloc = MockBloc();
    final repoHttp = MockHttp();
    final repoHive = MockHive();

    setUpAll(() {
      registerFallbackValue(EntitiesUpdated<VerseEntity>([]));
      registerFallbackValue(EntitiesAdded<VerseEntity>([]));
      registerFallbackValue(FakeVerseEntity());
    });

    setUp(() {
      reset(bloc);
      reset(repoHttp);
      reset(repoHive);
      List<VerseEntity> dataHttp = [
        VerseEntity(
          'ref',
          'http',
          modified: 3,
          id: 1,
          accountId: 1,
        )
      ];
      VerseEntity dataHive = VerseEntity(
        'ref',
        'hive',
        modified: 2,
        id: 1,
        accountId: 1,
      );
      when(() => currentAccount.id).thenReturn(1);
      when(() => repoHttp.readList(
          deleted: null,
          since: 1,
          client: any(named: 'client'))).thenAnswer((_) async => dataHttp);
      when(() => repoHive.read(1, accountId: 1))
          .thenAnswer((_) async => dataHive);
      when(() => repoHive.update(any()))
          .thenAnswer((_) async => dataHive);
    });

    test('All entities should be added if bloc state is EntitiesLoadFailure',
        () async {
      when(() => bloc.state)
          .thenReturn(EntitiesLoadFailure<VerseEntity>('test'));
      final replicator = Replicator<VerseEntity>(VerseEntity, bloc,
          hiveRepo: repoHive, httpRepo: repoHttp);
      await replicator.receiveWhenReady(1, 1);
      final captured = verify(() => bloc.add(captureAny())).captured;
      expect(captured.length, equals(1));
      expect(captured.last, isA<EntitiesAdded<VerseEntity>>());
    });

    test('All entities should be updated if bloc state is EntitiesLoadSuccess',
        () async {
      when(() => bloc.state).thenReturn(EntitiesLoadSuccess<VerseEntity>([]));
      final replicator = Replicator<VerseEntity>(VerseEntity, bloc,
          hiveRepo: repoHive, httpRepo: repoHttp);
      await replicator.receiveWhenReady(1, 1);
      final captured = verify(() => bloc.add(captureAny())).captured;
      expect(captured.length, equals(1));
      expect(captured.last, isA<EntitiesUpdated<VerseEntity>>());
    });
  });
}
