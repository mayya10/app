import 'package:time_machine/time_machine.dart';

class DateService {
  static final _instance = DateService._internal();

  factory DateService() {
    return _instance;
  }

  DateService._internal();

  static LocalDate _todayWithOffset() => offset2h(LocalDateTime.now());

  static LocalDate offset2h(LocalDateTime dateTime) {
    return dateTime.subtractHours(2).calendarDate;
  }

  static LocalDate atStartOfDay(LocalDateTime dateTime) {
    return dateTime.calendarDate;
  }

  /// Today's date with an offset of 2h
  LocalDate get today => _todayWithOffset();

  int now() {
    return DateTime.now().millisecondsSinceEpoch;
  }

  int daysFromToday(LocalDate date) {
    return date.epochDay - today.epochDay;
  }

}
