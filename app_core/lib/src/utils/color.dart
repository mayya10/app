import 'package:flutter/material.dart';

class Flat {
  static const List<Color> wheel = [
    indigo,
    purple,
    pink,
    red,
    scarlet,
    orange,
    amber,
    chartreuse,
    green,
    spring,
    cyan,
    azure,
    blue
  ];

  static const Color indigo = Color(0xFF6778c8);
  static const Color purple = Color(0xFF9B59B6);
  static const Color pink = Color(0xFFc15279);
  static const Color red = Color(0xFFE74C3C);
  static const Color scarlet = Color(0xFFe6652f);
  static const Color orange = Color(0xFFE67E22);
  static const Color amber = Color(0xFFeba118);
  static const Color chartreuse = Color(0xFF8fc840);
  static const Color green = Color(0xFF2ECC71);
  static const Color spring = Color(0xFF24c486);
  static const Color cyan = Color(0xFF1ABC9C);
  static const Color azure = Color(0xFF27aabb);
  static const Color blue = Color(0xFF3498DB);
  static const Color silver = Color(0xFFECF0F1);
  static const Color grey = Color(0xFF95A5A6);
  static const Color asphalt = Color(0xFF34495E);
}

class FlatDark {
  static const List<Color> wheel = [
    indigo,
    purple,
    pink,
    red,
    scarlet,
    orange,
    amber,
    chartreuse,
    green,
    spring,
    cyan,
    azure,
    blue
  ];

  static const Color indigo = Color(0xFF5b62b3);
  static const Color purple = Color(0xFF8E44AD);
  static const Color pink = Color(0xFFa73e6c);
  static const Color red = Color(0xFFC0392B);
  static const Color scarlet = Color(0xFFc94615);
  static const Color orange = Color(0xFFD35400);
  static const Color amber = Color(0xFFe37809);
  static const Color chartreuse = Color(0xFF8da539);
  static const Color green = Color(0xFF27AE60);
  static const Color spring = Color(0xFF1ea772);
  static const Color cyan = Color(0xFF16A085);
  static const Color azure = Color(0xFF1f909f);
  static const Color blue = Color(0xFF2980B9);
  static const Color silver = Color(0xFFBDC3C7);
  static const Color grey = Color(0xFF7F8C8D);
  static const Color asphalt = Color(0xFF2C3E50);
  static const Color tar = Color(0xFF151515);
}

class AppColor {
  static const Color dark = Color(0xFFA30131);
  static const Color light = Color(0xffff6666);
}

class AppColorScheme {

  static final light = ColorScheme(
    primary: AppColor.dark,  // focused form text
    primaryContainer: FlatDark.amber,  // primary icon
    secondary: AppColor.dark,  // highlight fab
    secondaryContainer: FlatDark.indigo,  // secondary icon
    surface: Colors.white,
    background: Colors.grey[100]!,
    error: Flat.pink,
    onPrimary: Colors.white,
    onSecondary: Colors.white,
    onSurface: Colors.black,
    onBackground: Colors.black,
    onError: Colors.white,
    brightness: Brightness.light,
  );

  static final dark = ColorScheme(
    primary: AppColor.light,   // focused form text
    primaryContainer: Flat.amber,  // primary icon
    secondary: AppColor.dark,  // highlight fab
    secondaryContainer: Flat.indigo, // secondary icon
    surface: Colors.grey[900]!,
    background: Colors.black,
    error: Flat.pink,
    onPrimary: Colors.white,
    onSecondary: Colors.white,
    onSurface: Colors.white,
    onBackground: Colors.white,
    onError: Colors.black,
    brightness: Brightness.dark,
  );

  static final appBar = ColorScheme(
    primary: AppColor.dark,
    primaryContainer: AppColor.light,
    secondary: Flat.amber,
    secondaryContainer: FlatDark.amber,
    surface: Colors.grey[900]!,
    background: Colors.grey[800]!,
    error: Flat.pink,
    onPrimary: Colors.white,
    onSecondary: Colors.white,
    onSurface: Colors.white,
    onBackground: Colors.white,
    onError: Colors.black,
    brightness: Brightness.dark,
  );
}