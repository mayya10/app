const MAX_INT = 9007199254740991; // pow(2, 53) - 1;
const MS_PER_DAY = 24 * 60 * 60 * 1000;
const WEB_URL = 'https://web.remem.me';
const APP_LOCALES = [
  'cs',
  'de',
  'en',
  'es',
  'fr',
  'hu',
  'it',
  'ko',
  'nb',  // Norwegian Bokmål
  'nl',
  'no',  // Norwegian
  'pl',
  'pt',
  'ro',
  'ru',
  'sv',
  'zh_Hans',
  'zh_Hant',
];
const REFERENCE_LOCALES = [
  'ar',
  'bm',
  'cs',
  'da',
  'de',
  'en',
  'es',
  'fr',
  'hu',
  'id',
  'it',
  'ko',
  'nb',
  'nl',
  'pl',
  'pt',
  'ro',
  'ru',
  'sv',
  'uk',
  'vi',
  'zh_Hans',
  'zh_Hant',
];
const BIBLE_LOCALES = [
  'af',
  'am', // Amharic
  'ar',
  'bm',
  'cs',
  'da',
  'de',
  'el',
  'en',
  'es',
  'fi',
  'fr',
  'ha', // Hausa
  'he',
  'hr',
  'hu',
  'id',
  'ig', // Igbo
  'it',
  'ko',
  'la', // Latin
  'mk',
  'ml', // Malayalam
  'nb',
  'nl',
  'pl',
  'pt',
  'ro',
  'ru',
  'sr',
  'sv',
  'sw', // Swahili
  'ta', // Tamil
  'te', // Telugu
  'th', // Thai
  'ti', // Tigrinya
  'tl', // Tagalog
  'uk',
  'vi',
  'yo', // Yoruba
  'zh',
];
const RTL_LANGUAGES = [
  'ar',  // Arabic
  'dv',  // Divehi
  'fa',  // Persian
  'ha',  // Hausa
  'he',  // Hebrew
  'ks',  // Kashmiri
  'ku',  // Kurdish
  'ps',  // Pushto
  'sd',  // Sindhi
  'ur',  // Urdu
  'yi',  // Yiddish
];
