/// Flutter icons RememMeIcons
/// Copyright (C) 2019 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// To use this font, place it in your fonts/ directory and include the
/// following in your pubspec.yaml
///
/// flutter:
///   fonts:
///    - family:  RememMeIcons
///      fonts:
///       - asset: fonts/RememMeIcons.ttf
///
/// 
///
import 'package:flutter/widgets.dart';

class RememMeIcons {
  RememMeIcons._();

  static const _kFontFam = 'RememMeIcons';

  static const IconData cards_filled = const IconData(0xe800, fontFamily: _kFontFam);
  static const IconData cards_outline = const IconData(0xe801, fontFamily: _kFontFam);
  static const IconData press_app = const IconData(0xe802, fontFamily: _kFontFam);
}
