import 'dart:convert';

class MessageException implements Exception {
  /// e.g. {
  ///          'messages': [...],  // general message strings
  ///          'reference': [...]  // messages concerning the field 'reference'
  ///      }
  final Map<String, List<String>> messages;

  MessageException(this.messages);

  @override
  String toString() {
    return 'MessageException { messages: ${jsonEncode(this.messages)} }';
  }
}