library app_core;

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

export 'src/widgets/snack_bar.dart';
export 'src/services/message_service.dart';
export 'src/services/date_service.dart';
export 'src/models/models.dart';
export 'src/l10n.dart';
export 'src/utils/utils.dart';

final di = GetIt.instance;
final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

