import 'package:flutter_test/flutter_test.dart';
import 'package:app_core/app_core.dart';
import 'package:time_machine/time_machine.dart';

void main() {
  group('DateService', () {
    test('Should be at start of day', () async {
      final time130 = LocalDateTime(2022, 1, 1, 1, 30, 0);
      final atStartOfDay = DateService.atStartOfDay(time130);
      expect(atStartOfDay, LocalDate(2022, 1, 1));
    });

    test('Should be on the day before', () async {
      final time130 = LocalDateTime(2022, 1, 1, 1, 30, 0);
      final offset2h = DateService.offset2h(time130);
      expect(offset2h, DateService.atStartOfDay(time130.subtractDays(1)));
    });

    test('Should be on the same day', () async {
      final time130 = LocalDateTime(2022, 1, 1, 22, 30, 0);
      final offset2h = DateService.offset2h(time130);
      expect(offset2h, DateService.atStartOfDay(time130));
    });
  });
}
